package com.fgs.ifgs

import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.FlutterPlugin
import io.flutter.plugins.GeneratedPluginRegistrant

open class MainActivity: FlutterFragmentActivity() {

    private val channel = "com.example.ifgs/android"

    /**
     * 配置flutter引擎
     * 创建原生方法调用入口提供flutter调用
     */
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        val flutterPlugin = FlutterPlugin()
        flutterPlugin.flutterPlugin(channel, this@MainActivity, flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, channel).setMethodCallHandler { _: MethodCall?, _: MethodChannel.Result? -> }
    }
}
