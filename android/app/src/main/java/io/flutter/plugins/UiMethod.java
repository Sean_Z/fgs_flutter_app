package io.flutter.plugins;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.Map;

import io.flutter.plugin.common.MethodChannel;

/**
 * 所有原生UI方法调用类，入参必须接收context， Map
 */
@SuppressWarnings("unused")
public class UiMethod {

    /**
     * 展示toast控件
     * @param context ApplicationContext
     * @param content flutter参数包
     */
    public static void showToast(Context context, Map<String, Object> content, MethodChannel.Result result) {
        String msg = (String) content.get("msg");
        if (!TextUtils.isEmpty(msg)) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "toast text must not null", Toast.LENGTH_SHORT).show();
        }
    }
}
