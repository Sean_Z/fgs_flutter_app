package io.flutter.plugins;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Arrays;
import java.util.Map;

import io.flutter.plugin.common.MethodChannel;

@SuppressWarnings("unused")
public class HardwareInfo {

    public static void getBatteryInfo(Context context, Map<String, Object> packageInfo, MethodChannel.Result result) {
        BatteryManager manager = (BatteryManager) context.getSystemService(Context.BATTERY_SERVICE);
        System.out.println(manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY));
        result.success(manager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY));
    }

    public static void getCpuInfo(Context context, Map<String, Object> packageInfo, MethodChannel.Result result) {
        String[] cpuTypes = Build.SUPPORTED_ABIS;
        StringBuilder cpuType = new StringBuilder();
        if (cpuTypes != null) {
            for (String item : cpuTypes) {
                if (!item.isEmpty()) {
                    cpuType.append(item).append(",");
                }

            }
            if (cpuType.length() > 1) {
                cpuType.substring(0, cpuType.length() - 2);
            }
        }
        assert cpuTypes != null;
        result.success(Arrays.asList(cpuTypes));
    }

    public static void getWIFISSID(Context context, Map<String, Object> packageInfo, MethodChannel.Result result) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        result.success(wifiInfo.getSSID());
    }
}
