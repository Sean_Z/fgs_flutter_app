package io.flutter.plugins

import android.content.Context
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class FlutterPlugin {

    /**
     * 创建反射方法
     * 反射到flutter传递过来的制定方法中
     */
    fun flutterPlugin(channel: String, context: Context, flutterEngine: FlutterEngine) {

         MethodChannel(flutterEngine.dartExecutor.binaryMessenger, channel).setMethodCallHandler { call: MethodCall, result: MethodChannel.Result? ->
             val argumentsPackage = (call.arguments) as Map<*, *>
             val className = argumentsPackage["className"] as String
             println("package parameter === $argumentsPackage")
             println("className === $className")
             println("methodName === ${call.method}")
             val methodClass = Class.forName(className)
             methodClass.getMethod(call.method, Context::class.java, Map::class.java, MethodChannel.Result::class.java).invoke(call.method, context, call.arguments, result)
        }
    }
}