void _clearScreen() {
  print(1);
}

void _deleteLastPoint() {
  print(2);
}

void _copyRoute() {
  print(3);
}

void menuAction(int value) {
  final actionMap = {
    0: () => _clearScreen(),
    1: () => _deleteLastPoint(),
    2: () => _copyRoute(),
  };
  actionMap[value]!();
}

void main() {
  menuAction(1);
}
