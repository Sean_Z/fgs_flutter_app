import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/request/ThirdPartServiceCaller.dart';
import 'package:ifgs/request/modal/FlightPlanDetailEntity.dart';
import 'package:ifgs/request/modal/FlightPlanIdEntity.dart';
import 'package:ifgs/request/modal/WorldWeatherBean.dart';
import 'package:ifgs/widget/PublicWidget/LoadingButton.dart';
import 'package:latlong2/latlong.dart' as osm_l;
import 'package:url_launcher/url_launcher.dart';

class ThirdPartApi {
  static final _timeStamp = DateTime.now().millisecondsSinceEpoch;

  static Future<List<Data>?> getWorldAirportWeather() async {
    final requestData = 'http://aviation.nmc.cn/json_data/all.json?v=${ThirdPartApi._timeStamp}';
    final _response = await ThirdPartServiceCaller.get(requestData);
    final _weatherBean = WorldWeatherBean.fromJson(_response);
    return _weatherBean.data;
  }

  static Future<GenerateFlightPlanEntity?> generateFlightPlan(String fromICAO, String toICAO) async {
    try {
      final dio = Dio();
      final postOption = Options();
      postOption.responseType = ResponseType.plain;
      postOption.headers = {'x-limit-cap': 200, 'isOwnService': false, HttpHeaders.authorizationHeader: 'Basic ${Links.flightPlanApiKey}'};
      final fplIdResponse = await dio.post(Links.flightPlanIdUrl, data: {'fromICAO': fromICAO, 'toICAO': toICAO}, options: postOption);
      final fplIdResponseJson = json.decode(fplIdResponse.toString()) as Map<String, dynamic>;
      final fplIdEntity = FlightPlanIdEntity.fromJson(fplIdResponseJson);
      final planId = fplIdEntity.id;
      final distance = fplIdEntity.distance;
      final waypointResponse = await dio.get('${Links.flightPlanDetail}$planId');
      final waypointResponseJson = json.decode(waypointResponse.toString()) as Map<String, dynamic>;
      final flightPlanDetail = FlightPlanDetailEntity.fromJson(waypointResponseJson);
      final fplDetailNodes = flightPlanDetail.route.nodes;
      final waypoints = fplDetailNodes.map((e) => e.ident).toList();
      final coordinateList = fplDetailNodes.map((e) => osm_l.LatLng(e.lat, e.lon)).toList();
      final departureName = flightPlanDetail.fromName;
      final arrivalName = flightPlanDetail.toName;
      return GenerateFlightPlanEntity(waypoints.join(' '), distance, coordinateList, departureName, arrivalName, 0, 'success');
    } catch (e) {
      loadingButtonKey.currentState?.clearLoading();
      await launchUrl(Uri.parse('http://fpltoif.com/simbrief?t=new'));
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'unknown airport'));
    }
    return null;
  }
}

class GenerateFlightPlanEntity {
  final String routeInfo;
  final double distance;
  final List<osm_l.LatLng> coordinateList;
  final String departureName;
  final String arrivalName;
  final int status;
  final String message;

  GenerateFlightPlanEntity(
      this.routeInfo, this.distance, this.coordinateList, this.departureName, this.arrivalName, this.status, this.message);
}
