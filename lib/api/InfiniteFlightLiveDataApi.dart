import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/constant/StaticData.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/request/InfiniteFlightLiveDataCaller.dart';
import 'package:ifgs/request/modal/AircraftInfoEntity.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:ifgs/request/modal/InfiniteFlightAirportStateBean.dart';
import 'package:ifgs/request/modal/InfiniteFlightAirwayLineBean.dart';
import 'package:ifgs/request/modal/InfiniteFlightAllFlightsEntity.dart';
import 'package:ifgs/request/modal/InfiniteFlightAtcListBean.dart';
import 'package:ifgs/request/modal/InfiniteFlightOnlinePlane.dart';
import 'package:ifgs/request/modal/InfiniteFlightUserGradeBean.dart';
import 'package:ifgs/request/modal/InfiniteFlightUserInfoBean.dart';
import 'package:ifgs/request/modal/OnlineFlightPlanBean.dart';
import 'package:ifgs/request/modal/UserHistoryFlightsEntity.dart';
import 'package:ifgs/request/modal/infinite_flight_livery_res_entity.dart';
import 'package:ifgs/request/modal/infinte_flight_airport_entity.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:intl/intl.dart';

import '../util/FileUtil.dart';

class InfiniteFlightLiveDataApi {
  /// 获取目标机场的ATIS信息
  static Future<String> getAtis(String airportICAO, int serverType, {bool shouldStopLoading = true, bool shouldShowError = true}) async {
    try {
      final urlFunction = interfaceUrlMap.atis(airportICAO, serverType);
      final _response = await InfiniteFlightLiveDataCaller.get(urlFunction);
      final _res = IFAtisBean.fromJson(_response);
      return _res.result ?? '';
    } catch (e) {
      return 'ATC is Offline, No ATIS information!';
    }
  }

  /// 获取ATC信息
  static Future<List<InfiniteFlightAtcBeanResult>> getATC(int serverType) async {
    final url = interfaceUrlMap.atc(serverType);
    final response = InfiniteFlightAtcListBean.fromJson(await InfiniteFlightLiveDataCaller.get(url));
    if (response.errorCode == 0) {
      return response.result;
    } else {
      throw Exception('Get atc info exception!');
    }
  }

  /// 获取IF用户信息
  static Future<InfiniteFlightUserInfoBean> getInfiniteFlightUserInfo(String userId) async {
    final _requestData = {
      'userIds': [userId]
    };
    final response = await InfiniteFlightLiveDataCaller.postLiveData(userId, _requestData);
    return InfiniteFlightUserInfoBean.fromJson(response);
  }

  /// 获取IF用户等级信息
  static Future<InfiniteFlightUserGradeBean> getInfiniteFlightUserGrade(String userId) async {
    final url = interfaceUrlMap.userGrade(userId);
    return InfiniteFlightUserGradeBean.fromJson(await InfiniteFlightLiveDataCaller.get(url));
  }

  /// 获取在线飞行计划
  static Future<OnlineFlightPlanBean> getOnlinePlan(String flightId) async {
    try {
      final url = interfaceUrlMap.flightplan(flightId, 0);
      final res = await InfiniteFlightLiveDataCaller.get(url);
      final response = InfiniteFlightOnlinePlane.fromJson(res);
      final route = response.result!.waypoints;
      final routerInfo = route.join(' ');
      final departure = route[0] == 'WPT' ? route[1] : route[0];
      final arrival = route[route.length - 1];
      final flightPlanItems = response.result!.flightPlanItems;
      final departurePosition = flightPlanItems![0].location!.toJson();
      final arrivalPosition = flightPlanItems.last.location!.toJson();
      final wholeRangePosition = [];
      for (var _item in flightPlanItems) {
        if (_item.location!.latitude != 0 && _item.location!.longitude != 0) {
          final latitude = double.parse(_item.location!.latitude.toString());
          final longitude = double.parse(_item.location!.longitude.toString());
          final point = {'latitude': latitude, 'longitude': longitude};
          wholeRangePosition.add(point);
        }
      }
      final result = OnlineFlightPlanBean.fromJson({
        'routerInfo': routerInfo,
        'departure': departure,
        'arrival': arrival,
        'departurePosition': departurePosition,
        'arrivalPosition': arrivalPosition,
        'wholeRangePosition': wholeRangePosition
      });
      return result;
    } catch (e) {
      CustomViewProvider.hideMaterialLoading();
      throw Exception('get plan exception');
    }
  }

  /// 获取在线航线
  static Future<List<AirwayLineListBean>> getAirwayLine(String flightId) async {
    final url = interfaceUrlMap.route(flightId, 0);
    final res = await InfiniteFlightLiveDataCaller.get(url);
    final response = InfiniteFlightAirwayLineBean.fromJson(res);
    final airwayList = <AirwayLineListBean>[];
    for (var _item in response.result!) {
      airwayList.add(AirwayLineListBean(
        latitude: _item.latitude!,
        longitude: _item.longitude!,
        time: _item.date!,
        altitude: _item.altitude!,
        groundSpeed: _item.groundSpeed!,
      ));
    }
    return airwayList;
  }

  static Future<List<AircraftInfoResult>> getAircraftInfo() async {
    final res = await InfiniteFlightLiveDataCaller.get(interfaceUrlMap.liveries);
    return AircraftInfoEntity.fromJson(res).result;
  }

  /// getAircraftJsonBean
  static Future<List<AircraftInfoResult>> getAirlineLiveryName() async {
    return await InfiniteFlightLiveDataApi.getAircraftInfo();
  }

  /// 获取用户飞行状态包装准备提交
  static Future<LogInfoClass?> getLogInfo(FgsOnlineFlightData _flightBean, BuildContext context) async {
    try {
      final resList = await Future.wait([
        InfiniteFlightLiveDataApi.getOnlinePlan(_flightBean.flightId),
        InfiniteFlightLiveDataApi.getAirwayLine(_flightBean.flightId),
      ]);
      final fetchOnlinePlanResponse = resList.first;
      final airwayLineResponse = resList[1];
      final airwayLineResponseRes = airwayLineResponse as List<AirwayLineListBean>;
      final boardingTime = DateTime.parse(airwayLineResponseRes[0].time);
      final departureTime = DateTime.parse(FlightJudgment.getDepartureTime(airwayLineResponseRes));
      final range = CalculateMethod.entireDistance(airwayLineResponseRes);
      final currentTime = DateTime.now();
      final flightTime = currentTime.difference(departureTime);
      final baseInfo = fetchOnlinePlanResponse as OnlineFlightPlanBean;
      final liveryName = DataHandler.getLiveryName(_flightBean.livery);
      final aircraftName = _flightBean.livery[0].aircraftName;
      final speed = _flightBean.speed.floor().abs();
      if (speed != 0) {
        CustomViewProvider.hideMaterialLoading();
        throw Exception('Please log flight until set break');
      }
      final arrivalTime = DateTime.now();
      if (baseInfo.routeInfo.isEmpty) {
        throw Exception('Flight plan is missed, please change a point in your flight plan or set plan again');
      }
      return LogInfoClass(
          departure: baseInfo.departure,
          arrival: baseInfo.arrival,
          departureTime: DateFormat('MMM d, H:mm', 'en_US').format(departureTime.toLocal()),
          arrivalTime: DateFormat('MMM d, H:mm', 'en_US').format(arrivalTime.toLocal()),
          boardingTime: DateFormat('MMM d, H:mm', 'en_US').format(boardingTime.toLocal()),
          flightNumber: _flightBean.callSign,
          route: baseInfo.routeInfo,
          flightId: _flightBean.flightId,
          livery: liveryName,
          aircraft: aircraftName,
          flightTime: flightTime.inMinutes,
          distance: range,
          rate: 0,
          searchFavorite: 0,
          isDelay: 1,
          airwayLine: airwayLineResponseRes);
    } catch (e) {
      CustomViewProvider.hideMaterialDialogLoading();
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: e.toString()));
      return null;
    }
  }

  static Future<Map<String, double>> getRangeInfo(FgsOnlineFlightData flightBean) async {
    final _currentSpeed = flightBean.speed;
    final _response = await Future.wait([
      InfiniteFlightLiveDataApi.getOnlinePlan(flightBean.flightId),
      InfiniteFlightLiveDataApi.getAirwayLine(flightBean.flightId),
    ]);
    final onlinePlaneResponse = _response[0] as OnlineFlightPlanBean;
    final airwayResponse = _response[1] as List<AirwayLineListBean>;
    final wholeRangePosition = onlinePlaneResponse.wholeRangePosition;
    final wholeRange = CalculateMethod.entireWholeDistance(wholeRangePosition);
    final alreadyFlewRange = CalculateMethod.entireDistance(airwayResponse);
    final remainTime = (wholeRange - alreadyFlewRange) / _currentSpeed;
    return {'wholeRange': wholeRange, 'flewRange': alreadyFlewRange, 'remainFlightTime': remainTime};
  }

  static Future<List<FgsOnlineFlightData>> getServerFlight(int serverType, {String specificUserName = ''}) async {
    final url = interfaceUrlMap.flights(serverType);
    final response = await InfiniteFlightLiveDataCaller.get(url);
    final serverFlightResponse = InfiniteFlightAllFlightsEntity.fromJson(response);
    final OnlineFlightDataList = <InfiniteFlightAllFlightsEntityResult>[];
    if (serverFlightResponse.errorCode == 0) {
      for (var item in serverFlightResponse.result) {
        OnlineFlightDataList.add(item);
      }
    }
    final filteredList = specificUserName.isNotEmpty
        ? OnlineFlightDataList.where((element) => element.callsign == specificUserName).toList()
        : OnlineFlightDataList;
    final listData = <FgsOnlineFlightData>[];
    for (var item in filteredList) {
      final targetLivery = StaticData.aircraftJson.where((element) => element.id == item.liveryId).toList();
      if (targetLivery.isNotEmpty) {
        final targetLiveryItem = targetLivery[0];
        final fgsItem = FgsOnlineFlightData(
            position: Position(latitude: item.latitude, longitude: item.longitude),
            angle: double.parse(item.heading.toString()),
            liveryId: item.liveryId,
            flightId: item.flightId,
            aircraftId: item.aircraftId,
            altitude: item.altitude,
            callSign: item.callsign,
            displayName: item.username,
            userId: item.userId,
            speed: item.speed,
            verticalSpeed: item.verticalSpeed,
            departure: 'Unknown',
            arrival: 'Unknown',
            livery: [
              Livery(
                  aircraftId: item.aircraftId,
                  liveryId: item.liveryId,
                  liveryName: targetLiveryItem.liveryName,
                  aircraftName: targetLiveryItem.aircraftName)
            ]);
        listData.add(fgsItem);
      }
    }
    return listData;
  }

  /// 根据用户名获取IF用户信息
  static Future<InfiniteFlightUserInfoBean> getInfiniteFlightUserInfoByUserName(String userName) async {
    final requestData = {
      'discourseNames': [userName]
    };
    final result = await InfiniteFlightLiveDataCaller.postLiveData(userName, requestData);
    return InfiniteFlightUserInfoBean.fromJson(result);
  }

  /// 获取所有FGS成员飞行信息
  static Future<List<List<FgsOnlineFlightData>>> getFgsMemberFlight() async {
    final infiniteFlightResult = await InfiniteFlightLiveDataApi.getAllServerFlight();
    final userInfo = await FileGetter.userInfo;
    final fgsUserNameList = userInfo?.list.map((e) => e.infiniteFlightUserName) ?? [];
    final result = infiniteFlightResult.where((element) => fgsUserNameList.contains(element.displayName)).toList();
    return [result, result];
  }

  /// 获取机场信息
  static Future<AirportStateResponseBean> getAirportState(String airportCode, int serverType) async {
    final airportStateUrl = interfaceUrlMap.airportStatus(airportCode, serverType);
    final responseList = await Future.wait([
      InfiniteFlightLiveDataCaller.get(airportStateUrl),
      InfiniteFlightLiveDataApi.getServerFlight(serverType),
    ]);
    final airportResponse = InfiniteFlightAirportStateBean.fromJson(responseList[0]);
    final allFlightResponse = responseList[1] as List<FgsOnlineFlightData>;
    final inboundList = <FgsOnlineFlightData>[];
    final outboundList = <FgsOnlineFlightData>[];
    final atcFacilitiesList = <AtcFacilities>[];
    if (airportResponse.errorCode == 0) {
      (airportResponse.result!.atcFacilities!).forEach((element) {
        atcFacilitiesList.add(element);
      });
      final inboundFlightsCount = airportResponse.result!.inboundFlightsCount;
      final outboundFlightsCount = airportResponse.result!.outboundFlightsCount;
      for (var _item in allFlightResponse) {
        for (var _airportItem in airportResponse.result!.inboundFlights!) {
          if (_airportItem == _item.flightId) {
            if (_item.displayName == '') {
              _item.displayName = 'Unknown';
            }
            inboundList.add(_item);
          }
        }
      }
      for (var _item in allFlightResponse) {
        for (var _airportItem in airportResponse.result!.outboundFlights!) {
          if (_airportItem == _item.flightId) {
            if (_item.displayName == '') {
              _item.displayName = 'Unknown';
            }
            outboundList.add(_item);
          }
        }
      }
      final result = AirportStateResponseBean(
        outboundFlightsCount: outboundFlightsCount ?? 0,
        inboundFlightsCount: inboundFlightsCount ?? 0,
        atcFacilitiesList: atcFacilitiesList,
        inboundList: inboundList,
        outboundList: outboundList,
      );
      return result;
    } else {
      throw Exception('get airport info exception');
    }
  }

  static List<KVClass> countAircraft(List<FgsOnlineFlightData> list) {
    final finalList = [KVClass()];
    final map = {};
    for (var i = 0; i < list.length; i++) {
      final element = list[i];
      final aircraftName = element.livery[0].aircraftName;
      if (map.keys.toList().contains(aircraftName)) {
        map[aircraftName] = map[aircraftName] + 1;
      } else {
        map[aircraftName] = 1;
      }
    }
    map.forEach((key, value) {
      finalList.add(KVClass(name: key.toString(), value: int.parse(value.toString())));
    });
    return finalList;
  }

  static Future<FgsOnlineFlightData?> getTargetInAllServerFlight(String specificUsername) async {
    final userId = await StorageUtil.getStringItem('userId') ?? '';
    final lastFlight = await getUserHistoryFlight(userId, 1);
    final _serverName = '${lastFlight.data.first.server} Server';
    StorageUtil.setStringItem('currentServerName', lastFlight.data.first.server);
    StorageUtil.setIntItem('currentServerEnum', ServerName.serverName[_serverName] ?? 0);
    final serverType = ServerName.serverName[_serverName] ?? 0;
    final flightsInTargetServer = await InfiniteFlightLiveDataApi.getServerFlight(serverType);
    final targetList = flightsInTargetServer.where((element) => element.displayName == specificUsername);
    if(targetList.isNotEmpty) return targetList.first;
    return null;
  }

  static Future<List<FgsOnlineFlightData>> getAllServerFlight() async {
    final _responseList = await Future.wait<List<FgsOnlineFlightData>>([
      InfiniteFlightLiveDataApi.getServerFlight(0),
      InfiniteFlightLiveDataApi.getServerFlight(1),
      InfiniteFlightLiveDataApi.getServerFlight(2),
    ]);
    final result = [..._responseList[0], ..._responseList[1], ..._responseList[2]];
    return result;
  }

  static Future<UserHistoryFlightsResult> getUserHistoryFlight(String userId, int page) async {
    final url = interfaceUrlMap.userFlights(userId) + '?page=$page';
    final res = await InfiniteFlightLiveDataCaller.get(url);
    return UserHistoryFlightsEntity.fromJson(res).result;
  }

  static Future<List<InfiniteFlightLiveryResResult>> getInfiniteFlightLivery() async {
    final url = interfaceUrlMap.liveries;
    final res = await InfiniteFlightLiveDataCaller.get(url);
    return InfiniteFlightLiveryResEntity.fromJson(res).result;
  }

  static Future<InfiniteFlightAirportEntity> getAirport()async {
    final url = interfaceUrlMap.airport;
    final res = await InfiniteFlightLiveDataCaller.get(url);
    return InfiniteFlightAirportEntity.fromJson(res);
  }
}

class IFAtisBean {
  int? errorCode;
  String? result;

  IFAtisBean({required this.errorCode, required this.result});

  IFAtisBean.fromJson(dynamic json) {
    errorCode = json['errorCode'] as int;
    result = json['result'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errCode'] = errorCode;
    map['result'] = result;
    return map;
  }
}

class AirportStateResponseBean {
  final int inboundFlightsCount;
  final int outboundFlightsCount;
  final List<AtcFacilities> atcFacilitiesList;
  final List<FgsOnlineFlightData> inboundList;
  final List<FgsOnlineFlightData> outboundList;

  AirportStateResponseBean(
      {required this.outboundFlightsCount,
      required this.inboundFlightsCount,
      required this.atcFacilitiesList,
      required this.inboundList,
      required this.outboundList});
}

class KVClass {
  final String? name;
  final int? value;

  KVClass({this.name, this.value});
}

class FlightCollectionClass {
  final List<KVClass> kvClass;
  final int totalAircraft;

  FlightCollectionClass({required this.kvClass, required this.totalAircraft});
}

class LogInfoClass {
  final String departure;
  final String arrival;
  final String departureTime;
  final String arrivalTime;
  final String flightNumber;
  final String route;
  final String flightId;
  final String livery;
  final String aircraft;
  final int flightTime;
  final double distance;
  final String boardingTime;
  double rate;
  int searchFavorite;
  int isDelay;
  final List<AirwayLineListBean> airwayLine;

  LogInfoClass(
      {required this.departure,
      required this.arrival,
      required this.departureTime,
      required this.arrivalTime,
      required this.flightNumber,
      required this.route,
      required this.flightId,
      required this.livery,
      required this.aircraft,
      required this.flightTime,
      required this.distance,
      required this.boardingTime,
      required this.rate,
      required this.searchFavorite,
      required this.isDelay,
      required this.airwayLine});
}
