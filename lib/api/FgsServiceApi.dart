import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/InterfaceAdapter.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/request/HttpExecutor.dart';
import 'package:ifgs/request/modal/CommonResponse.dart';
import 'package:ifgs/request/modal/FgsAircraftAdviceEntity.dart';
import 'package:ifgs/request/modal/FgsUserInfoBean.dart';
import 'package:ifgs/util/FileUtil.dart';
import 'package:ifgs/util/StorageUtil.dart';

import '../request/modal/LoginResponseEntity.dart';
import '../request/modal/flight_log_res_entity.dart';

class FgsServiceApi {
  /// login by account and password
  static Future<LoginResponseEntity> login(String username, String password) async {
    return LoginResponseEntity.fromJson(await HttpExecutor.post('user/login', {'username': username, 'password': password, 'type': 0}));
  }

  /// fingerprint login
  static Future<LoginResponseEntity> fingerprintLogin(String username) async {
    return LoginResponseEntity.fromJson(await HttpExecutor.post('user/fingerprintLogin', {'username': username}));
  }

  /// register
  static Future<CommonResponse> register(Map request) async {
    return CommonResponse.fromJson(await HttpExecutor.post('user/application', request));
  }

  /// getFlightLogs by pageNo and pageSize
  static Future<FlightLogResEntity> getFlightLog(int pageNo, int pageSize) async {
    final userId = await StorageUtil.getStringItem('userId') ?? '';
    final data = await InfiniteFlightLiveDataApi.getUserHistoryFlight(userId, pageNo);
    return await InterfaceAdapter.adapterFlightLog(data);
  }

  /// log flight into FGS Flight log database
  static Future<CommonResponse> logFlight(LogInfoClass requestData) async {
    final airway = <Map>[];
    requestData.airwayLine.forEach((element) {
      airway.add({
        'altitude': element.altitude,
        'groundSpeed': element.groundSpeed,
        'longitude': element.longitude,
        'latitude': element.latitude,
        'time': element.time
      });
    });
    final requestBody = {
      'departure': requestData.departure,
      'arrival': requestData.arrival,
      'departureTime': requestData.departureTime,
      'arrivalTime': requestData.arrivalTime,
      'flightNumber': requestData.flightNumber,
      'route': requestData.route,
      'flightId': requestData.flightId,
      'livery': requestData.livery,
      'aircraft': requestData.aircraft,
      'flightTime': requestData.flightTime,
      'distance': requestData.distance,
      'boardingTime': requestData.boardingTime,
      'airwayLine': airway,
      'rate': requestData.rate,
      'isDelay': requestData.isDelay,
      'searchFavorite': requestData.searchFavorite,
    };
    return CommonResponse.fromJson(await HttpExecutor.post('fms/logFlight', requestBody));
  }

  /// get FGS user info include base info and history flight info
  static Future<FgsUserInfoBeanEntity> getFgsUserInfo() async {
    final result = await HttpExecutor.post('user/getUserInfo', {'isUserList': 0});
    return FgsUserInfoBeanEntity.fromJson(json.decode(json.encode(result)) as Map<String, dynamic>);
  }

  /// subscribe flight send sms or email when flight time remain less then 30 minutes
  static Future<CommonResponse> subscribeFlight(String displayName, int sessionId) async {
    final _requestData = {'username': displayName, 'sessionId': sessionId};
    return CommonResponse.fromJson(await HttpExecutor.post('fms/subscribeFlight', _requestData));
  }

  static Future<bool> getSpecificAirportChart(String query, [bool shouldShowTip = true]) async {
    final _response = await FileGetter.getAirportCharts ?? [];
    if (!_response.any((element) => element.fileName == query)) {
      if (shouldShowTip) {
        UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'target not found in fgs charts system!'));
      }
      return false;
    } else {
      return true;
    }
  }

  /// upload airport charts
  static void uploadCharts(FormData requestData, String uploadTargetPath) async {
    for (final item in requestData.files) {
      if (!item.value.filename!.contains('pdf')) {
        UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'Limited to upload pdf files'));
        throw Exception('pdf file special');
      }
    }
    await FileUtil.uploadFiles(requestData, '${FgsServer.BASE_URL}fms/uploadAirportCharts', uploadTargetPath: uploadTargetPath);
    CustomViewProvider.hideMaterialLoading();
  }

  static Future<dynamic> downloadAirportChart(String fileName, int fileSize) async {
    final _response =
        await HttpExecutor.get('fms/downloadAirportCharts', 'fileName=$fileName', responseType: ResponseType.bytes, fileSize: fileSize);
    return _response.data;
  }

  /// 从腾讯云存储通获取机场航图
  static String getAirportChartsFromCos(String chartsType, String region, String chartName) {
    switch (chartsType) {
      case '1.0':
        return '${Links.fgsStorageBaseUrl}charts/AirportCharts/$region/$chartName.pdf';
      case '2.0':
        return '${Links.fgsStorageBaseUrl}charts/FctmCharts/$chartName';
      default:
        return '';
    }
  }

  /// 获取机型建议库
  static Future<List<FgsAircraftAdviceEntity>> getAircraftAdvice(BuildContext context) async {
    return await FileGetter.aircraftAdviceStaticData ?? [];
  }

  static void logError(
    String error,
  ) {
    final _errorPackage = {
      'errorMsg': error,
      'errorTime': DateTime.now().toString(),
      'errorPlatform': Platform.environment.toString(),
    };
    Log.logger.e(_errorPackage);
    HttpExecutor.post('common/log', _errorPackage);
  }
}
