import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/provider/MainProvider.dart';
import 'package:ifgs/util/TimeUtil.dart';
import 'package:ifgs/viewstate/AirportViewState.dart';
import 'package:ifgs/widget/PublicWidget/AirportWeatherWidget.dart';
import 'package:provider/provider.dart';

class AirportViewController extends GetxController {
  final BuildContext applicationContext;
  AirportViewController({required this.applicationContext});
  final state = AirportState();

  @override
  void onReady() {
    Future.delayed(Duration.zero, () {
      fetchAllInboundFlight(applicationContext, true, false, isRouterSearch: true);
    });
    super.onReady();
  }

  @override
  void onClose() {
    state.airportCodeController.dispose();
    super.onClose();
  }

  WeatherBean getDynamicWeather(String atis) {
    final airportName = atis.split(',')[0];
    final temperature = atis.split('Temperature ')[1].split(',')[0];
    final visibility = atis.split('Visibility ')[1].split(',')[0];
    final windDirection = atis.split('Wind ')[1].split(',')[0].split(' at')[0];
    final valueWindCondition = atis.split('Wind ')[1].split(',')[0].split(' at').length > 1;
    final windSpeed = valueWindCondition ? atis.split('Wind ')[1].split(',')[0].split(' at')[1].split(' ')[1] : atis.split('Wind ')[1].split(',')[0];
    final dewPoint = atis.split('Dew Point ')[1].split(',')[0];
    return WeatherBean(airportName, temperature, visibility, windSpeed, windDirection, dewPoint);
  }

  String analysisAtis(String atisInfo) {
    final weatherData = getDynamicWeather(atisInfo);
    final airportName = weatherData.airportName;
    final splitValue = atisInfo.split('time ')[1].split(' ');
    final informationVersion = atisInfo.split('you have information')[1];
    final landingRunwayCondition = atisInfo.split('Landing Runways ').length > 1;
    final takeoffRunwayCondition = atisInfo.split('Departing Runways ').length > 1;
    final landingRunwayFlag = landingRunwayCondition ? 'Landing Runways ' : 'Landing Runway ';
    final takeoffRunwayFlag = takeoffRunwayCondition ? 'Departing Runways ' : 'Departing Runway ';
    final landingRunways = atisInfo.split(landingRunwayFlag)[1].split(',')[0];
    final takeoffRunways = atisInfo.split(takeoffRunwayFlag)[1].split('.')[0];
    final peopleLanguage = '$airportName, 下面是机场通勤播报, 祖鲁时间${splitValue[0]}, 风向 ${weatherData.windDirection}, 风速 ${weatherData.windSpeed} 节, 能见度 ${weatherData.visibility} 米，气温 ${weatherData.temperature}摄氏度。'
        '修正海压 ${splitValue[14].replaceAll('.', '')}, 露点温度 ${weatherData.dewPoint}摄氏度。 备注 请使用指定的离场进场程序。 落地跑道$landingRunways, 起飞跑道$takeoffRunways。 信息编号：${informationVersion.replaceAll('.', '')}. 播报结束！';
    return peopleLanguage;
  }

  /// Fetch all of inbound and outbound flight
  Future<void> fetchAllInboundFlight(BuildContext context, bool shouldLoading, bool isRefresh, {bool isRouterSearch = false}) async {
    CustomViewProvider.showMaterialLoading('loading...', context);
    final _routerParams = RouterMethod.getRouterParameter();
    var _airportCode = '';
    var _initServerType = 0;
    if (_routerParams != null && isRouterSearch) {
      _airportCode = (_routerParams as List)[0] as String;
      _initServerType = _routerParams[1] as int;
      state.serverType = _initServerType;
    } else {
      _airportCode = state.searchedAirport == '' ? 'KLAX' : state.searchedAirport;
      _initServerType = state.serverType;
    }
    state.airportICAO = _airportCode;
    state.searchedAirport = _airportCode;
    final _responseList = await Future.wait([
      InfiniteFlightLiveDataApi.getAirportState(_airportCode, _initServerType),
      InfiniteFlightLiveDataApi.getAtis(state.searchedAirport, state.serverType, shouldStopLoading: false),
    ]);
    final atisInfo = _responseList[1] as String;
    final _response = _responseList[0] as AirportStateResponseBean;
    state.atisInfo = atisInfo;
    state.rawAtisInfo = atisInfo;
    if(!atisInfo.contains('Offline') && !atisInfo.contains('Loading')) {
      state.transformedAtisInfo = analysisAtis(atisInfo);
    }
    Provider.of<MainProvider>(applicationContext, listen: false).setAtisInfo(_responseList[1].toString());
    final _inboundFlight = _response.inboundList;
    final _outBoundFlightRes = _response.outboundList;
    state.inboundAircraftCount = _response.inboundFlightsCount;
    state.outboundAircraftCount = _response.outboundFlightsCount;
    state.inBoundFlight = _inboundFlight;
    state.outBoundFlight = _outBoundFlightRes;
    if (state.inBoundFlight.isNotEmpty || state.outBoundFlight.isNotEmpty) {
      state.isHasData = true;
    }
    // await airportWeatherWidgetKey.currentState?.getWeather(_airportCode);
    CustomViewProvider.hideMaterialLoading();
    update();
  }

  /// Count bound flight
  String BoundFlightText() {
    final flightCount = '${state.isInbound ? state.inboundAircraftCount : state.outboundAircraftCount} flights';
    return '${TimeUtil.YYYY_MM_DD_HH_MM_SS()} --/-- '
        '${state.isInbound ? (state.inBoundFlight.isEmpty ? 'No Flight' : flightCount) : state.inBoundFlight.isEmpty ? 'No Flight' : flightCount} is ${state.isInbound ? 'inbound' : 'outbound'}';
  }

  Future<bool> checkChartInCos(String icaoCode) async {
    return await FgsServiceApi.getSpecificAirportChart(icaoCode, false);
  }
}
