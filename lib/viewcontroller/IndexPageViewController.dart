import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/StartActivityEventHandler.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/pageview/FlightReviewPageView.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:ifgs/widget/FlightPlanGeneratorWidget/GenerateFlightPlanCardWidget.dart';
import 'package:ifgs/widget/IndexWidget/FgsBoundFlightWidget.dart';
import 'package:ifgs/widget/OnlineMap/FlightMapCardWidget.dart';

class IndexPageViewController extends GetxController {
  var state = '......';
  var isCheckedFavorite = false;
  var isCheckedDelay = false;
  var stateIcon = Icons.wifi_off;
  var title = '';
  var email = '';
  var isOnline = false;
  var username = '';
  var rate = 0.0;
  FgsOnlineFlightData? targetFlight;
  var flightRemainRate = 0.0;

  @override
  void onReady() {
    final context = Get.context!;
    checkUpdateApp(context);
    setUserName();
    setSignalState();
    super.onReady();
  }

  void clearChildForm() => generateFplWidgetKey.currentState?.clearForm();

  Future<void> getPlan()async => generateFplWidgetKey.currentState?.getPlan();

  Future<void> setUserName() async {
    var _userName = await InfoMethod.getUserName;
    var _mail = await StorageUtil.getStringItem('email');
    if (_userName == '') {
      RouterMethod.logout();
    } else {
      title = 'Hey $_userName';
      email = _mail!;
      username = _userName;
    }
    update();
  }

  /// 检查app更新
  Future<void> checkUpdateApp(BuildContext context) async {
    if (await StorageUtil.getBoolItem('isIgnoreUpdate') ?? false) {
      UIViewProvider.showToast('new version is found, go setting page to update app!');
    } else {
      // final startActivity = StartActivityEventHandler();
      // await startActivity.getAppInfo(context, false);
    }
    update();
  }

  /// Set flight range progress
  Future<void> setProgress(FgsOnlineFlightData flightBean) async {
    final _rangeInfo = await InfiniteFlightLiveDataApi.getRangeInfo(flightBean);
    final _flewRange = _rangeInfo['flewRange'] ?? 0;
    final _wholeRange = _rangeInfo['wholeRange'] ?? 1;
    final _percent = _flewRange / (_wholeRange == 0 ? 1 : _wholeRange);
    flightRemainRate = _percent;
    update();
  }

  Future<void> setSignalState() async {
    final _currentUsername = await InfoMethod.infiniteFlightUsername;
    final _checkResult = await InfiniteFlightLiveDataApi.getTargetInAllServerFlight(_currentUsername);
    await fgsBoundKey.currentState?.initData();
    if (_checkResult != null) {
      isOnline = true;
      targetFlight = _checkResult;
      if (flightMapCardKey.currentState != null) {
        await flightMapCardKey.currentState!.refresh(targetFlight!);
      }
      await setProgress(targetFlight!);
      state = S.current.indexPageViewSignalOnline;
      stateIcon = Icons.wifi;
    } else {
      targetFlight = null;
      isOnline = false;
      state = S.current.indexPageViewSignalOffline;
      stateIcon = Icons.wifi_off;
    }
    update();
  }

  void updateSignalText() {
    final signalState = targetFlight != null ? S.current.indexPageViewSignalOnline : S.current.indexPageViewSignalOffline;
    state = signalState;
    update();
  }

  void _clearModalBottomSheetState() {
    rate = 0;
    isCheckedDelay = false;
    isCheckedFavorite = false;
    update();
  }

  /// This method will call [InfiniteFlightLiveDataApi.getLogInfo]
  /// to package current flight info
  Future<void> submitFlight(BuildContext context) async {
    Navigator.of(context).pop();
    CustomViewProvider.showMaterialDialogLoading(context, content: 'submitting....');
    final startActivity = StartActivityEventHandler();
    final response = await Future.wait([
      InfiniteFlightLiveDataApi.getLogInfo(targetFlight!, context),
      startActivity.initCurrentUserInfo(),
    ]);
    final requestData = response[0] as LogInfoClass?;
    if (requestData is! Exception && requestData != null) {
      requestData.rate = rate;
      requestData.isDelay = isCheckedDelay == false ? 1 : 0;
      requestData.searchFavorite = isCheckedFavorite == false ? 1 : 0;
      final result = await FgsServiceApi.logFlight(requestData);
      CustomViewProvider.hideMaterialDialogLoading();
      if (result.status == 0) {
        _clearModalBottomSheetState();
        await Get.to(() => FlightReviewPageView(flightBean: targetFlight, requestData: requestData));
      } else {
        UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: result.message));
      }
    } else {
      CustomViewProvider.hideMaterialDialogLoading();
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: requestData.toString(), duration: 5));
    }
    update();
  }
}
