import 'package:flutter/cupertino.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/provider/atc/AtcMonitorProvider.dart';
import 'package:provider/provider.dart';

class AtcMonitorViewController {
  Future<void> renderAtcListView(BuildContext context) async {
    context.read<AtcMonitorProvider>().setIsFinishedQry(false);
    final serverType = context.read<AtcMonitorProvider>().serverType;
    final _response = await InfiniteFlightLiveDataApi.getATC(serverType);
    context.read<AtcMonitorProvider>().setIsFinishedQry(true);
    if (_response.isNotEmpty) {
      final ATCRows = [..._response];
      ATCRows.removeWhere((element) => element.airportName == 'unknown');
      context.read<AtcMonitorProvider>().setAtcRows(ATCRows);
      context.read<AtcMonitorProvider>().setIsHasData(true);
    } else {
      context.read<AtcMonitorProvider>().setIsHasData(false);
    }
  }

  Future<void> refresh(BuildContext context) async {
    await renderAtcListView(context);
  }
}