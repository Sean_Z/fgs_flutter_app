import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/LocalAuthService.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/request/modal/LoginResponseEntity.dart';
import 'package:ifgs/util/FileUtil.dart';
import 'package:ifgs/util/StorageUtil.dart';

class LoginViewController extends GetxController {
  final usernameController = TextEditingController();
  final pwdController = TextEditingController();

  var cachedUsername = '';
  var isChangeAccount = false;

  var username = '';

  final focusNode1 = FocusNode();
  final focusNode2 = FocusNode();

  var isEye = true;
  var isBtnEnabled = false;

  @override
  void onReady() {
    isUserSetFingerprint();
    setUserName();
    super.onReady();
  }

  @override
  void onClose() {
    CustomViewProvider.hideMaterialLoading();
    super.onClose();
  }

  void passLogin(LoginResponseEntity response) {
    StorageUtil.setStringItem('username', response.username.toString());
    StorageUtil.setStringItem('email', response.email.toString());
    StorageUtil.setStringItem('position', response.position.toString());
    StorageUtil.setStringItem('currentPosition', response.currentPosition.toString());
    StorageUtil.setStringItem('nodeToken', response.nodeToken.toString());
    StorageUtil.setStringItem('accessToken', response.accessToken.toString());
    StorageUtil.setStringItem('refreshToken', response.refreshToken.toString());
    StorageUtil.setStringItem('userId', response.infiniteFlightUserId);
    StorageUtil.setStringItem('infiniteFlightUserName', response.infiniteFlightUserName);
    UIViewProvider.showToast('Login Successfully!');
    Get.offAllNamed('/index');
  }

  void cacheAvatar() async {
    final _dio = Dio();
    final avatarDir = Directory(await FileUtil.appSupportFilePath + '/profile');
    if (!avatarDir.existsSync()) {
      await avatarDir.create();
    }
    await _dio.download(
      '${Links.fgsStorageBaseUrl}profile/$username.jpg',
      await FileUtil.appSupportFilePath + '/profile/$username.jpg',
    );
  }

  void goIndexDirect() {
    Get.offAllNamed('/index');
  }

  Future<void> fakeLogin()async {
    final response = LoginResponseEntity();
    final userInfo = await FileGetter.userInfo;
    final userName = usernameController.text;
    final currentUser = userInfo?.list.toList().where((element) => element.username == userName).first;
    final infiniteFlightUserName = currentUser?.infiniteFlightUserName;
    final infiniteFlightUserId = currentUser?.userId;
    final mail = currentUser?.mail;
    response.username = userName;
    response.email = mail ?? '';
    response.position = 'CTO';
    response.accessToken = '';
    response.refreshToken = '';
    response.nodeToken = '';
    response.currentPosition = '';
    response.infiniteFlightUserName = infiniteFlightUserName ?? '';
    response.infiniteFlightUserId = infiniteFlightUserId ?? '';
    passLogin(response);
  }

  void login(GlobalKey<FormState> formKey, BuildContext context) async {
    await fakeLogin();
    update();
  }

  Future<void> realLogin(GlobalKey<FormState> formKey, BuildContext context)async {
    if (formKey.currentState!.validate()) {
      CustomViewProvider.showMaterialLoading('login...', context);
      FocusScope.of(context).requestFocus(FocusNode());
      final _userName = usernameController.text.isEmpty ? username : usernameController.text;
      final _password = pwdController.text;
      var response = await FgsServiceApi.login(_userName, _password);
      if (response.status == 0) {
        passLogin(response);
        cacheAvatar();
      } else {
        UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: response.message.toString()));
      }
    } else {
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'check again'));
    }
    update();
  }

  Future<void> setUserName() async {
    final _username = await InfoMethod.getUserName;
    if (_username != '') {
      username = _username;
      cachedUsername = _username;
    }
    update();
  }

  Future<void> fingerprintLogin() async {
    final context = Get.context!;
    var res = await LocalAuthenticationService.authenticate();
    if (res) {
      CustomViewProvider.showMaterialLoading('loading...', context);
      final username = await InfoMethod.getUserName;
      passLogin(await FgsServiceApi.fingerprintLogin(username));
    } else {
      CustomViewProvider.hideMaterialLoading();
      UIViewProvider.showToast('Error finger！');
    }
  }

  Future<void> isUserSetFingerprint() async {
    final username = await InfoMethod.getUserName;
    final isUserSetFingerprintFlag = await StorageUtil.getStringItem(username);
    isUserSetFingerprintFlag == 'fingerSet' ? fingerprintLogin() : print('object');
  }
}
