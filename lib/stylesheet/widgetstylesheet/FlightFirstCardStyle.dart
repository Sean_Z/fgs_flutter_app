import 'package:flutter/material.dart';

class FlightFirstCardStyle {
  static final greenLineTextStyle =
      const TextStyle(color: Colors.greenAccent, fontSize: 20, fontWeight: FontWeight.bold);
  static final arrivalTimeTextStyle = const TextStyle(color: Colors.redAccent, fontSize: 16);
  static final departureTimeTextStyle = const TextStyle(color: Colors.greenAccent, fontSize: 16);
  static final liveryNameTextStyle = const TextStyle(color: Colors.yellowAccent, fontSize: 15);
  static final estTextTextStyle = const TextStyle(color: Colors.redAccent, fontSize: 14, fontWeight: FontWeight.bold);
  static final onTimeTextTextStyle =
      const TextStyle(color: Colors.greenAccent, fontSize: 14, fontWeight: FontWeight.bold);
  static final ICAOTextStyle = const TextStyle(
    color: Colors.white,
    fontSize: 16,
    shadows: [
      Shadow(
        color: Colors.black87,
        offset: Offset(2, 2),
        blurRadius: 4,
      ),
    ],
  );
}
