import 'package:flutter/material.dart';
import 'package:ifgs/core/Color.dart';

class LightTheme {
  static final themeColor = HexColor.fromHex('#4F738B');
  static final normalTextStyle = const TextStyle(fontSize: 12, color: Colors.black12);
  static final borderRadius = 10.0;
  static final screenMargin = 10.0;
  static final themeConf = ThemeData(
    primaryColorLight: themeColor,
    tabBarTheme: TabBarTheme(
      labelColor: themeColor,
      indicatorColor: themeColor,
      unselectedLabelColor: Colors.black54
    ),
    indicatorColor: themeColor,
    pageTransitionsTheme: const PageTransitionsTheme(
      builders: <TargetPlatform, PageTransitionsBuilder>{
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
      },
    ),
    cardColor: themeColor,
    switchTheme: SwitchThemeData(
      thumbColor: MaterialStateProperty.all(Colors.amberAccent),
      trackColor: MaterialStateProperty.all(Colors.amberAccent[200]),
      trackOutlineColor: MaterialStateColor.resolveWith((states) => Colors.transparent),
    ),
    primaryColor: LightTheme.themeColor,
    shadowColor: Colors.grey,
    textTheme: const TextTheme(
      bodyLarge: TextStyle(color: Colors.blueGrey),
      bodySmall: TextStyle(color: Colors.blueGrey),
    ),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(foregroundColor: themeColor),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: TextButton.styleFrom(
          elevation: 0.8,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
    )),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      selectedItemColor: LightTheme.themeColor,
      unselectedItemColor: Colors.blueGrey,
      type: BottomNavigationBarType.fixed,
      backgroundColor: Colors.white,
    ),
    popupMenuTheme: const PopupMenuThemeData(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
    ),
    cardTheme: const CardTheme(
      color: Colors.white,
      elevation: 4,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
    ),
    iconTheme: IconThemeData(color: LightTheme.themeColor),
    dataTableTheme: DataTableThemeData(headingRowColor: MaterialStateProperty.all<Color>(LightTheme.themeColor)),
    inputDecorationTheme: const InputDecorationTheme(
      labelStyle: TextStyle(color: Colors.blueGrey),
      hintStyle: TextStyle(color: Colors.blueGrey),
    ),
    bottomSheetTheme: const BottomSheetThemeData(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        backgroundColor: Colors.white),
    appBarTheme: AppBarTheme(
        iconTheme: const IconThemeData(color: Colors.white),
        backgroundColor: LightTheme.themeColor,
        titleTextStyle: const TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w600)),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      shape: const CircleBorder(),
      backgroundColor: LightTheme.themeColor,
    ),
    colorScheme: ColorScheme.fromSeed(surfaceTint: Colors.transparent, seedColor: LightTheme.themeColor),
  );
}

class DarkTheme {
  static final darkGrey = const Color(0xffafafaf);
  static final deepDarkGrey = const Color(0xff282727);
  static final themeColor = Colors.black;
  static final normalTextStyle = const TextStyle(fontSize: 12, color: Colors.white);
  static final borderRadius = 10.0;
  static final screenMargin = 10.0;
  static final textFromTextColor = Colors.white;
  static final themeConf = ThemeData(
    pageTransitionsTheme: const PageTransitionsTheme(
      builders: <TargetPlatform, PageTransitionsBuilder>{
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
      },
    ),
    popupMenuTheme: PopupMenuThemeData(
      color: DarkTheme.deepDarkGrey,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
    ),
    switchTheme: SwitchThemeData(
      thumbColor: MaterialStateProperty.all(Colors.amberAccent),
      trackColor: MaterialStateProperty.all(Colors.amberAccent[200]),
      trackOutlineColor: MaterialStateColor.resolveWith((states) => Colors.transparent),
    ),
    listTileTheme: ListTileThemeData(
      iconColor: Colors.white,
      tileColor: deepDarkGrey
    ),
    scaffoldBackgroundColor: themeColor,
    primaryColor: themeColor,
    shadowColor: Colors.lightBlueAccent,
    cardTheme: CardTheme(
        color: deepDarkGrey,
        shadowColor: Colors.white,
        clipBehavior: Clip.antiAlias,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        )),
    appBarTheme: const AppBarTheme(
      shadowColor: Colors.blue,
      backgroundColor: Colors.black,
      iconTheme: IconThemeData(color: Colors.white),
      foregroundColor: Colors.white
    ),
    textTheme: const TextTheme(
      bodyMedium: TextStyle(color: Colors.white),
      bodySmall: TextStyle(color: Colors.white),
      bodyLarge: TextStyle(color: Colors.white),
      displayLarge: TextStyle(color: Colors.white, fontSize: 16),
      displayMedium: TextStyle(color: Colors.white),
      displaySmall: TextStyle(color: Colors.white),
    ),
    primaryTextTheme: const TextTheme(
      bodyMedium: TextStyle(color: Colors.white),
      displayLarge: TextStyle(color: Colors.white, fontSize: 16),
      displayMedium: TextStyle(color: Colors.white),
      displaySmall: TextStyle(color: Colors.white),
    ),
    cardColor: Colors.black,
    primaryIconTheme: const IconThemeData(color: Colors.white),
    iconTheme: const IconThemeData(color: Colors.white),
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.deepOrangeAccent,
      type: BottomNavigationBarType.fixed,
      backgroundColor: Colors.white,
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: TextButton.styleFrom(
      backgroundColor: deepDarkGrey,
      elevation: 0.8,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
    )),
    dataTableTheme: DataTableThemeData(headingRowColor: MaterialStateProperty.all<Color>(themeColor)),
    inputDecorationTheme: const InputDecorationTheme(
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      labelStyle: TextStyle(color: Colors.white),
      hintStyle: TextStyle(color: Colors.white),
    ),
    tabBarTheme: const TabBarTheme(
      labelColor: Colors.white,
      indicatorColor: Colors.lightBlue,
      unselectedLabelColor: Colors.white54,
    ),
    bottomSheetTheme: const BottomSheetThemeData(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        backgroundColor: Colors.black87),
    colorScheme: ColorScheme.fromSeed(surfaceTint: Colors.transparent, seedColor: DarkTheme.themeColor),
  );
}
