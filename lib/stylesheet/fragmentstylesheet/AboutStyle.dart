import 'package:flutter/material.dart';

class AboutStyle {
  static final usernameTextStyle = const TextStyle(
      fontSize: 20, fontWeight: FontWeight.bold, shadows: [Shadow(color: Colors.grey, offset: Offset(0, 1))]);
  static final emailTextStyle = const TextStyle(fontSize: 15, color: Colors.grey);
  static final logOutTextStyle = const TextStyle(fontSize: 15);
}
