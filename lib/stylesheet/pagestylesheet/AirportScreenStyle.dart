import 'package:flutter/material.dart';

class AirportScreenStyle {
  static final dataColumnText = const TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white);
  static final dataRowText = const TextStyle(fontSize: 12, fontWeight: FontWeight.bold);
}
