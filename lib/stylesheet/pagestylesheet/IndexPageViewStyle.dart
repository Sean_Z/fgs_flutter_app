import 'package:flutter/material.dart';

class IndexPageViewStyle {
  static final appBarTitle = const TextStyle(color: Colors.white, fontSize: 16);
  static final iconListFont = const TextStyle(fontSize: 12);
}
