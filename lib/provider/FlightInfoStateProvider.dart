
import 'package:flutter/cupertino.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:ifgs/request/modal/OnlineFlightPlanBean.dart';

class FlightInfoStateProvider extends ChangeNotifier {
  var _atisInfo = '';
  var _serverType = 0;
  late OnlineFlightPlanBean _onlineFlightPlanBean;
  late List<AirwayLineListBean> _airwayLineEntity;
  late FgsOnlineFlightData _flightInfoEntity;

  String get atisInfo => _atisInfo;
  OnlineFlightPlanBean get onlinePlan => _onlineFlightPlanBean;
  List<AirwayLineListBean> get airwayLineEntity => _airwayLineEntity;
  int get serverType => _serverType;
  FgsOnlineFlightData get flightInfoEntity => _flightInfoEntity;

  void setAtisInfo(String value) {
    _atisInfo = value;
    notifyListeners();
  }

  void setOnlineFlightEntity(OnlineFlightPlanBean onlineFlightPlanBean) {
    _onlineFlightPlanBean = onlineFlightPlanBean;
    notifyListeners();
  }

  void setAirwayLineEntity(List<AirwayLineListBean> airwayLineEntity) {
    _airwayLineEntity = airwayLineEntity;
    notifyListeners();
  }

  void setServerType(int serverType) {
    _serverType = serverType;
    notifyListeners();
  }

  void setFlightInfoEntity(FgsOnlineFlightData flightInfoEntity) {
    _flightInfoEntity = flightInfoEntity;
    notifyListeners();
  }
}
