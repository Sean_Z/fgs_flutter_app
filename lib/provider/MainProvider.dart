import 'package:flutter/cupertino.dart';

class MainProvider extends ChangeNotifier {
  var _atisInfo = '';
  String get atisInfo => _atisInfo;
  void setAtisInfo(String value) {
    _atisInfo = value;
    notifyListeners();
  }
}