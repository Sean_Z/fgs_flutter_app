import 'package:flutter/cupertino.dart';
import 'package:ifgs/request/modal/InfiniteFlightAtcListBean.dart';

class AtcMonitorProvider extends ChangeNotifier {
  var _isHasData = false;
  var _isFinishedQry = false;
  var _serverType = 0;
  var _ATCRows = <InfiniteFlightAtcBeanResult>[];

  int get serverType => _serverType;
  bool get isFinishedQry => _isFinishedQry;
  bool get isHasData => _isHasData;
  List<InfiniteFlightAtcBeanResult> get ATCRows => _ATCRows;

  void setServerType(int value) {
    _serverType = value;
    notifyListeners();
  }

  void setIsFinishedQry(bool value) {
    _isFinishedQry = value;
    notifyListeners();
  }

  void setIsHasData(bool value) {
    _isHasData = value;
    notifyListeners();
  }

  void setAtcRows(List<InfiniteFlightAtcBeanResult> value) {
   _ATCRows = value;
   notifyListeners();
  }
}