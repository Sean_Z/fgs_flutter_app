import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/constant/StaticData.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/pageview/PdfViewer.dart';
import 'package:ifgs/viewcontroller/AirportViewController.dart';
import 'package:ifgs/viewstate/AirportViewState.dart';
import 'package:ifgs/widget/PublicWidget/AirportWeatherWidget.dart';

import 'BoundAircraftCardWidget.dart';

class AirportScreenPageView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final airportViewController = Get.put(AirportViewController(applicationContext: context));
    final _viewState = Get.find<AirportViewController>().state;
    return GetBuilder(builder: (AirportViewController controller) {
      return Scaffold(
          appBar: _BuildAppBar(context, airportViewController, _viewState),
          body: ListView(
            physics: const BouncingScrollPhysics(),
            children: [
              Wrap(
                runSpacing: 4,
                children: [
                  _AirportSearchBar(context, airportViewController, _viewState),
                  _AircraftInfoCard(context, airportViewController),
                  _AtisCard(context, _viewState, airportViewController),
                  _WeatherPanel(context, airportViewController),
                ],
              ),
              const SizedBox(height: 8),
              Container(height: 400, child: BoundAircraftCardWidget(
                isWidget: true,
                title: 'Bound Flight',
                serverType: _viewState.serverType,
                isInbound: _viewState.isInbound,
                outBoundFlight: _viewState.outBoundFlight,
                inBoundFlight: _viewState.inBoundFlight,
                context: context,
              )),
              SafeArea(child: _AirportChart(airportViewController, _viewState)),
            ],
          ));
    });
  }

  Widget _AirportSearchBar(BuildContext context, AirportViewController airportViewController, AirportState airportState) {
    return Flex(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      direction: Axis.horizontal,
      children: [
        Flexible(
          flex: 3,
          child: Card(
            child: TextField(
                textCapitalization: TextCapitalization.characters,
                style: Theme.of(context).textTheme.bodyLarge,
                maxLengthEnforcement: MaxLengthEnforcement.enforced,
                maxLength: 4,
                focusNode: airportState.airportCodeNode,
                onSubmitted: (String data) {
                  airportState.searchedAirport = data;
                  airportViewController.fetchAllInboundFlight(context, true, false);
                },
                controller: airportState.airportCodeController,
                decoration: const InputDecoration(
                    counterText: '',
                    border: InputBorder.none,
                    prefixIcon: Icon(Icons.flight),
                    suffixIcon: Icon(Icons.search))),
          ),
        ),
        Flexible(
          flex: 1,
          child: Card(
            child: DropdownButton(
              style: Theme.of(context).textTheme.bodyLarge,
                underline: Container(),
                borderRadius: const BorderRadius.all(Radius.circular(8)),
                onTap: () => airportState.airportCodeNode.unfocus(),
                value: airportState.serverType,
                items: [
                  const DropdownMenuItem(
                      value: 0,
                      child: Row(
                        children: [
                          Icon(Icons.military_tech),
                          SizedBox(width: 10),
                          Text('ES'),
                        ],
                      )),
                  const DropdownMenuItem(
                      value: 1,
                      child: Row(
                        children: [
                          Icon(Icons.trending_up),
                          SizedBox(width: 10),
                          Text('TS'),
                        ],
                      )),
                  const DropdownMenuItem(
                      value: 2,
                      child: Row(
                        children: [
                          Icon(Icons.school),
                          SizedBox(width: 10),
                          Text('CS'),
                        ],
                      ))
                ],
                onChanged: (value) {
                  airportState.serverType = value as int;
                  airportState.airportCodeNode.unfocus();
                  airportViewController.fetchAllInboundFlight(context, true, false);
                }),
          ),
        )
      ],
    );
  }

  /// Build bound aircraft count info CardView
  Widget _AircraftInfoCard(BuildContext context, AirportViewController airportViewController) {
    return Card(
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(15),
        width: Get.width,
        child: Text(airportViewController.BoundFlightText()),
      ),
    );
  }

  Widget _AtisCard(BuildContext context, AirportState airportState, AirportViewController airportViewController) {
    final _titleStyle = const TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w600,
      shadows: [Shadow(color: Colors.black54, offset: Offset(2, 2), blurRadius: 8.0)],
    );
    final shouldRenderTransformSwitch = airportState.atisInfo.contains('Offline') || airportState.atisInfo.contains('Loading');
    return Card(
      child: Container(
        margin: const EdgeInsets.all(16),
        width: Get.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('ATIS Info', style: _titleStyle),
                shouldRenderTransformSwitch
                    ? Container()
                    : Wrap(
                        spacing: 8,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          const Text('Translate',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              )),
                          Switch(
                            value: airportState.transformAtis,
                            inactiveThumbColor: Colors.blueGrey,
                            activeColor: Colors.blue,
                            onChanged: (bool onChanged) {
                              airportViewController.state.transformAtis = onChanged;
                              final fixAtis = onChanged == true
                                  ? airportViewController.state.transformedAtisInfo
                                  : airportViewController.state.rawAtisInfo;
                              airportViewController.state.atisInfo = fixAtis;
                              airportViewController.update();
                            },
                          ),
                        ],
                      )
              ],
            ),
            const SizedBox(height: 8),
            Text(airportState.atisInfo),
          ],
        ),
      ),
    );
  }

  Widget _WeatherPanel(BuildContext context, AirportViewController airportViewController) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Container(
        width: Get.width,
        child: AirportWeatherWidget(
          key: airportWeatherWidgetKey,
          icaoCode: ((RouterMethod.getRouterParameter() ?? [airportViewController.state.airportICAO]) as List)[0] as String,
        ),
      ),
    );
  }

  AppBar _BuildAppBar(BuildContext context, AirportViewController airportViewController, AirportState airportState) {
    const flagTextStyle = TextStyle(color: Colors.yellow, fontWeight: FontWeight.bold, fontSize: 16);
    final airportList = StaticData.airportStaticData;
    final target = airportList.where((element) => element.icao == airportState.airportICAO);
    final has3DBuilding = target.isNotEmpty ? target.first.has3dBuildings == true ? '(3D)' : '' : '';
    return AppBar(
      title: Transform(
        transform: Matrix4.translationValues(-30, 0.0, 0.0),
        child: Text('${airportState.airportICAO.toUpperCase()} ${S.current.Screen} $has3DBuilding'),
      ),
      centerTitle: false,
      actions: [
        Wrap(
          direction: Axis.horizontal,
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: 8,
          children: [
            Text(airportState.isInbound ? S.current.Inbound : S.current.Outbound, style: flagTextStyle),
            Switch(
                value: airportState.isInbound,
                inactiveThumbColor: Theme.of(context).primaryColor,
                activeColor: Colors.blue,
                activeTrackColor: Colors.yellow,
                onChanged: (bool onChange) {
                  airportState.isInbound = onChange;
                  airportViewController.update();
                })
          ],
        )
      ],
    );
  }

  FutureBuilder<bool> _AirportChart(AirportViewController airportViewController, AirportState airportState) {
    return FutureBuilder(
      future: airportViewController.checkChartInCos(airportState.airportICAO),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data ?? false) {
            return SizedBox(
              height: 400,
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Wrap(
                        direction: Axis.horizontal,
                        spacing: 8,
                        children: [
                          Text('Chart',
                              style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w600,
                                color: UIAdapter.setFontPrimaryColor(context),
                              )),
                          const Icon(Icons.auto_stories)
                        ],
                      ),
                    ),
                    Expanded(
                      child: PdfViewer(
                        title: airportState.airportICAO,
                        pdfLink: FgsServiceApi.getAirportChartsFromCos('1.0', 'China', airportState.airportICAO),
                        isWidget: true,
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Container();
          }
        } else {
          return Container();
        }
      },
    );
  }
}
