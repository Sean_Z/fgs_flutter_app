import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:ifgs/widget/PublicWidget/DynamicWidget.dart';
import 'package:provider/provider.dart';

import 'FlightInfoPageView.dart';

class BoundAircraftCardWidget extends StatelessWidget {
  const BoundAircraftCardWidget({
    Key? key,
    required bool isInbound,
    required int serverType,
    required List<FgsOnlineFlightData>? inBoundFlight,
    required List<FgsOnlineFlightData>? outBoundFlight,
    required this.context,
    required String title,
    required bool isWidget,
  })  : _isInbound = isInbound,
        _serverType = serverType,
        _inBoundFlight = inBoundFlight,
        _outBoundFlight = outBoundFlight,
        _title = title,
        _isWidget = isWidget,
        super(key: key);

  final bool _isInbound;
  final int _serverType;
  final bool _isWidget;
  final String _title;
  final List<FgsOnlineFlightData>? _inBoundFlight;
  final List<FgsOnlineFlightData>? _outBoundFlight;
  final BuildContext context;

  void _pushSelf() {
    Navigator.of(context).push<void>(MaterialPageRoute(builder: (_) {
      return BoundAircraftCardWidget(
        isWidget: false,
        title: 'Bound Flight',
        isInbound: _isInbound,
        outBoundFlight: _outBoundFlight,
        inBoundFlight: _inBoundFlight,
        serverType: _serverType,
        context: context,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    final _cardMargin = _isWidget ? 8.0 : 0.0;
    final _cardRadius = _isWidget ? 8.0 : 0.0;
    final _listViewPhysics = _isWidget ? const NeverScrollableScrollPhysics() : const BouncingScrollPhysics();
    final loadedData = _inBoundFlight != null || _outBoundFlight != null;
    return Scaffold(
      body: Card(
        color: Colors.black87,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(_cardRadius))),
        margin: EdgeInsets.only(left: _cardMargin, right: _cardMargin, bottom: _cardMargin),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Stack(
            children: [
              DynamicWidget(
                condition: loadedData,
                positiveWidget: ListView(
                  physics: _listViewPhysics,
                  children: _BuildListCell(context),
                ),
                negativeWidget: const SpinKitRing(
                  color: Colors.white,
                  size: 50,
                  lineWidth: 4,
                  duration: Duration(milliseconds: 800),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _BuildListCell(BuildContext context) {
    final _titleStyle = const TextStyle(
      fontSize: 20,
      color: Colors.tealAccent,
      fontWeight: FontWeight.w600,
      shadows: [Shadow(color: Colors.black54, offset: Offset(2, 2), blurRadius: 8.0)],
    );
    final _baseTextStyle = const TextStyle(color: Colors.tealAccent, fontSize: 12);
    final _titleTextStyle = const TextStyle(
      color: Colors.tealAccent,
      fontSize: 14,
      fontWeight: FontWeight.w800,
    );
    final _flexCell = <Widget>[];
    _flexCell.add(Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Text(_title, style: _titleStyle, textAlign: TextAlign.start),
    ));
    _flexCell.add(Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Flex(
        direction: Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Flexible(flex: 1, child: Text('Pilot', style: _titleTextStyle)),
          Flexible(flex: 1, child: Text('Aircraft', style: _titleTextStyle)),
          Flexible(flex: 1, child: Text('Airline', style: _titleTextStyle)),
          Flexible(flex: 1, child: Text('State', style: _titleTextStyle))
        ],
      ),
    ));
    final _listData = _isInbound ? _inBoundFlight : _outBoundFlight;
    _listData?.forEach((element) {
      final _state = StatusJudgment.getStatus(element.verticalSpeed, element.altitude, element.speed);
      _flexCell.add(const Divider(height: 8, color: Colors.white24));
      _flexCell.add(InkWell(
        onLongPress: () {
          Provider.of<FlightInfoStateProvider>(context, listen: false).setFlightInfoEntity(element);
          Get.to(() => FlightInfoPageView());
        },
        onTap: () {
          Provider.of<FlightInfoStateProvider>(context, listen: false).setFlightInfoEntity(element);
          _isWidget ? _pushSelf() : Get.to(() => FlightInfoPageView());
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: Get.width / 5,
              height: 20,
              child: Text(
                element.displayName,
                maxLines: 1,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: _baseTextStyle,
              ),
            ),
            const SizedBox(width: 6),
            SizedBox(
              width: Get.width / 5,
              height: 20,
              child: Text(
                element.livery[0].aircraftName,
                maxLines: 1,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: _baseTextStyle,
              ),
            ),
            const SizedBox(width: 6),
            SizedBox(
              width: Get.width / 5,
              height: 20,
              child: Text(
                element.livery[0].liveryName ?? '',
                maxLines: 1,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: _baseTextStyle,
              ),
            ),
            const SizedBox(width: 6),
            Container(
              margin: const EdgeInsets.only(top: 6, bottom: 6),
              padding: const EdgeInsets.all(2),
              width: Get.width / 5,
              height: 24,
              decoration: BoxDecoration(
                color: _state.color,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Text(
                _state.status,
                textAlign: TextAlign.center,
                style: _baseTextStyle.merge(const TextStyle(color: Colors.white)),
              ),
            )
          ],
        ),
      ));
    });
    return _flexCell;
  }
}
