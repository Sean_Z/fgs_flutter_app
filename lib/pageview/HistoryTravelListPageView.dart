import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/plugin/ticket/TicketIndex.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';

class HistoryTravelListPageView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => Get.toNamed(RouterSheet.markupFlight),
        child: const Icon(Icons.backup),
      ),
      appBar: AppBar(
        title: Text(S.current.HistoryTravelListTitle),
      ),
      body: PageView(
        physics: const AlwaysScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        children: [
          Container(
            margin: const EdgeInsets.only(top: 20),
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: TicketIndex(),
          ),
        ],
      ),
    );
  }
}
