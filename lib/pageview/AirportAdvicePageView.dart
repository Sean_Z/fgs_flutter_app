import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/pageview/AirportAdviceDetailPageView.dart';
import 'package:ifgs/widget/AirportAdviceSearchBar/AirportAdviceSearchBar.dart';

import '../request/modal/infinte_flight_airport_entity.dart';

class AirportAdvicePageView extends StatelessWidget {
  AirportAdvicePageView({super.key});

  final icaoController = TextEditingController();

  void onItemClicked(InfiniteFlightAirportResult icao) {
    Get.to(() => AirportAdviceDetailPageView(airportResult: icao));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Transform(
          transform: Matrix4.translationValues(-30, 0.0, 0.0),
          child: const Text('Airport Advice'),
        ),
        actions: [
          const TextButton(
              onPressed: null,
              child: Text(
                'Favorite',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w800, color: Colors.white),
              ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            AirportAdviceSearchBar(
              onItemClicked: onItemClicked,
            )
          ],
        ),
      ),
    );
  }
}
