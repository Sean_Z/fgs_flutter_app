import 'package:animated_flip_counter/animated_flip_counter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/plugin/ticket/TicketMain.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:ifgs/widget/OnlineMap/FlightDataChart.dart';
import 'package:ifgs/widget/OnlineMap/FlightMapCardWidget.dart';
import 'package:provider/provider.dart';

import '../request/modal/flight_log_res_entity.dart';

import '../request/modal/flight_log_res_entity.dart';

class FlightReviewPageView extends StatefulWidget {
  const FlightReviewPageView({Key? key, this.flightBean, this.requestData}) : super(key: key);
  final FgsOnlineFlightData? flightBean;
  final LogInfoClass? requestData;

  @override
  _FlightReviewPageViewState createState() => _FlightReviewPageViewState();
}

class _FlightReviewPageViewState extends State<FlightReviewPageView> with SingleTickerProviderStateMixin {
  var _bonus = 0;
  Future<FlightLogResEntity> _getLogList() async {
    return await FgsServiceApi.getFlightLog(1, 1);
  }

  Future<List<AirwayLineListBean>> _getAirwayLine(String flightId) async {
    return await InfiniteFlightLiveDataApi.getAirwayLine(flightId);
  }

  int getBonus(int flightTime, double range) {
    return flightTime + range ~/ 10;
  }

  void _setBonus() {
    final _requestData = widget.requestData!;
    setState(() {
      _bonus = getBonus(_requestData.flightTime, _requestData.distance);
    });
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 800), () {
      _setBonus();
      Provider.of<FlightInfoStateProvider>(context).setFlightInfoEntity(widget.flightBean!);
    });
  }

  @override
  Widget build(BuildContext context) {
    final _textStyle = const TextStyle(fontSize: 24, color: Colors.white, fontWeight: FontWeight.w800);
    final _subTextStyle = const TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.w600);
    final _subIconTextStyle = TextStyle(color: UIAdapter.setFontBlueGreyColor(context), fontSize: 16, fontWeight: FontWeight.w600);
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Builder(builder: (context) {
        return CustomScrollView(
          slivers: [
            SliverAppBar(
              title: const Text('Flights Review'),
              pinned: true,
              actions: <Widget>[
                Container(
                  margin: const EdgeInsets.all(8),
                  child: UIViewProvider.getUserAvatar(radius: 30),
                ),
              ],
              titleSpacing: 0,
              expandedHeight: 140.0,
              flexibleSpace: FlexibleSpaceBar(
                background: Container(
                  margin: const EdgeInsets.only(top: 64),
                  child: Flex(
                    direction: Axis.horizontal,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Flexible(
                        flex: 1,
                        child: Text(widget.requestData!.departure, style: _textStyle),
                      ),
                      Flexible(
                          flex: 1,
                          child: Wrap(
                            direction: Axis.vertical,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: [
                              const Icon(Icons.airplane_ticket, color: Colors.white, size: 32),
                              Text(FlightJudgment.formatFlightTime(widget.requestData!.flightTime), style: _subTextStyle)
                            ],
                          )),
                      Flexible(
                        flex: 1,
                        child: Text(widget.requestData!.arrival, style: _textStyle),
                      ),
                    ],
                  ),
                ),
                collapseMode: CollapseMode.pin,
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int i) {
                  return Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(16)),
                    ),
                    margin: const EdgeInsets.all(8),
                    padding: const EdgeInsets.all(8),
                    width: Get.width,
                    child: Column(
                      children: [
                        Flex(
                          direction: Axis.horizontal,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                                flex: 1,
                                child: Wrap(
                                  direction: Axis.vertical,
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  spacing: 16,
                                  children: [
                                    Container(
                                      decoration: const BoxDecoration(shape: BoxShape.circle),
                                      width: 50,
                                      height: 50,
                                      child: UIViewProvider.getAirlineLogo(widget.flightBean!.livery[0].liveryName ?? 'unknown'),
                                    ),
                                    const Icon(Icons.flight_takeoff, color: Colors.amber, size: 32),
                                    Text(
                                      'Flight Bounds',
                                      style: _subIconTextStyle,
                                    ),
                                    AnimatedFlipCounter(
                                      duration: const Duration(milliseconds: 600),
                                      prefix: '\$',
                                      value: _bonus,
                                      textStyle: _subIconTextStyle.copyWith(fontSize: 24),
                                    ),
                                  ],
                                )),
                            Flexible(
                              flex: 2,
                              child: Container(
                                clipBehavior: Clip.antiAlias,
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(8)),
                                ),
                                margin: const EdgeInsets.only(left: 16),
                                height: 200,
                                child: const FlightMapCardWidget(),
                              ),
                            )
                          ],
                        ),
                        const SizedBox(height: 8),
                        const Divider(thickness: 1, height: 8),
                        Container(
                          clipBehavior: Clip.antiAlias,
                          decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8))),
                          height: 300,
                          child: FutureBuilder(
                              future: _getAirwayLine(widget.flightBean!.flightId),
                              builder: (BuildContext context, AsyncSnapshot snapshot) {
                                if (snapshot.connectionState == ConnectionState.done) {
                                  final flightData = snapshot.data as List<AirwayLineListBean>;
                                  Provider.of<FlightInfoStateProvider>(context, listen: false).setAirwayLineEntity(flightData);
                                  return const FlightDataChart();
                                } else {
                                  return Container(height: 200, child: UIViewProvider.getSkeletonWidget(2, true));
                                }
                              }),
                        ),
                        SizedBox(
                          height: 450,
                          child: Container(
                            margin: const EdgeInsets.only(top: 8),
                            child: FutureBuilder(
                                future: _getLogList(),
                                builder: (BuildContext context, AsyncSnapshot snapShot) {
                                  if (snapShot.connectionState == ConnectionState.done) {
                                    final _data = snapShot.data as FlightLogResEntity;
                                    return SizedBox(
                                        height: 700,
                                        child: TicketMain(
                                          boardingPass: _data.list.first,
                                          onClick: () {},
                                          isReview: true,
                                        ));
                                  } else {
                                    return SizedBox(
                                      height: 200,
                                      child: UIViewProvider.getSkeletonWidget(2, true),
                                    );
                                  }
                                }),
                          ),
                        )
                      ],
                    ),
                  );
                },
                childCount: 1,
              ),
            )
          ],
        );
      }),
    );
  }
}
