import 'dart:math';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/pageview/AircraftAdviceDetailPageView.dart';
import 'package:ifgs/request/modal/FgsAircraftAdviceEntity.dart';

import '../request/modal/AircraftInfoEntity.dart';

class AircraftAdvicePageView extends StatefulWidget {
  const AircraftAdvicePageView({Key? key}) : super(key: key);

  @override
  _AircraftAdvicePageViewState createState() => _AircraftAdvicePageViewState();
}

class _AircraftAdvicePageViewState extends State<AircraftAdvicePageView> {
  var _adviceCardList = <FgsAircraftAdviceEntity>[];
  var _liveryData = <AircraftInfoResult>[];

  Future<void> _getAircraftAdvice() async {
    CustomViewProvider.showMaterialDialogLoading(context);
    final _liveryList = await InfiniteFlightLiveDataApi.getAirlineLiveryName();
    final _list = await FgsServiceApi.getAircraftAdvice(context);
    setState(() {
      _adviceCardList = _list;
      _liveryData = _liveryList;
    });
    CustomViewProvider.hideMaterialDialogLoading();
  }

  @override
  void initState() {
    _getAircraftAdvice();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(S.current.MenuListAircraftAdvice),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () => _liveryData.isNotEmpty ? showSearch(context: context, delegate: SearchBarDelegate(_adviceCardList, _liveryData)) : null,
          )
        ],
      ),
      body: _AircraftStaggeredGridView(),
    );
  }

  RefreshIndicator _AircraftStaggeredGridView() {
    return RefreshIndicator(
        onRefresh: _getAircraftAdvice,
        child: GridView.custom(
          gridDelegate: SliverQuiltedGridDelegate(
            crossAxisCount: 4,
            mainAxisSpacing: 4,
            crossAxisSpacing: 4,
            repeatPattern: QuiltedGridRepeatPattern.inverted,
            pattern: [
              const QuiltedGridTile(2, 2),
              const QuiltedGridTile(1, 1),
              const QuiltedGridTile(1, 1),
            ],
          ),
          physics: const BouncingScrollPhysics(),
          childrenDelegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return _BuildContainer(index);
            },
            childCount: _adviceCardList.length,
          ),
        ));
  }

  Widget _BuildContainer(int index) {
    final _filterResult = _liveryData.where((element) => element.aircraftName == _adviceCardList[index].model).toList();
    return _filterResult.isNotEmpty ? OpenContainer(
      transitionType: ContainerTransitionType.fadeThrough,
      transitionDuration: const Duration(milliseconds: 300),
      openBuilder: (BuildContext context, void Function({Object? returnValue}) action) {
        return AircraftAdviceDetailPageView(title: _filterResult.first.aircraftName, aircraftInfo: _adviceCardList[index], livery: _filterResult[index]);
      },
      closedBuilder: (BuildContext context, void Function() action) {
        return UIViewProvider.getAssetsAircraftImage(context, _filterResult[index].id, _filterResult[index].aircraftID);
      },
    ) : Container();
  }
}

class SearchBarDelegate extends SearchDelegate<FgsAircraftAdviceEntity> {
  SearchBarDelegate(this.adviceList, this.livery);

  final List<FgsAircraftAdviceEntity> adviceList;
  final List<AircraftInfoResult> livery;

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () => query = '',
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, adviceList[0]);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Text('Search result：$query');
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty ? adviceList : adviceList.where((input) => input.model!.contains(query)).toList();
    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: RichText(
              text: TextSpan(
            text: (suggestionList.elementAt(index).model ?? 'unknown') + '    /    ',
            style: TextStyle(color: UIAdapter.setFontColor(context), fontWeight: FontWeight.bold),
            children: [
              TextSpan(text: 'speed ' + suggestionList[index].cruisingSpeed.toString(), style: const TextStyle(color: Colors.grey)),
            ],
          )),
          onTap: () {
            query = suggestionList[index].model ?? 'unknown';
            Get.to(() => AircraftAdviceDetailPageView(
              title: suggestionList[index].model ?? 'unknown',
              aircraftInfo: suggestionList[index],
              livery: livery.where((element) => element.aircraftName == suggestionList[index].model).toList()[Random().nextInt(10)],
            ));
          },
        );
      },
    );
  }
}
