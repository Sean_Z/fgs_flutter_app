import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/request/modal/fgs_file_entity.dart';
import 'package:ifgs/util/FileUtil.dart';
import 'package:material_floating_search_bar_2/material_floating_search_bar_2.dart';

class ChartsManagerPageView extends StatefulWidget {
  ChartsManagerPageView({Key? key}) : super(key: key);

  @override
  _ChartsManagerPageViewState createState() => _ChartsManagerPageViewState();
}

class _ChartsManagerPageViewState extends State<ChartsManagerPageView> {
  final _pageViewController = PageController();
  List<FgsFileEntity> chartList = [];
  List<FgsFileEntity> renderChartList = [];
  List<FgsFileEntity> _specificFileList = [];
  final _scrollController = ScrollController();
  var _totalPage = 1;
  var _pageNo = 1;
  var _isLoadingMore = false;
  var _isSearching = false;

  List<Widget> _renderChartsList(List<FgsFileEntity> fileList) {
    final _chartsList = <Widget>[];
    fileList.forEach((element) {
      final _showFileName =
          element.bizType == 1 ? element.fileName.substring(0, 4) : element.fileName.substring(0, element.fileName.length - 4);
      _chartsList.add(
        Card(
          color: UIAdapter.setContainerColor(context),
          child: ListTile(
              onTap: () async {
                final _cosPath = _pageViewController.page == 0 ? 'China' : '';
                await Get.toNamed(RouterSheet.pdfViewer, arguments: [
                  FgsServiceApi.getAirportChartsFromCos(element.bizType.toString(), _cosPath, element.fileName),
                  _showFileName
                ]);
              },
              leading: const Icon(Icons.picture_as_pdf, color: Colors.red),
              subtitle: Text(element.modifyTime, style: TextStyle(color: UIAdapter.setFontColor(context))),
              title: Text((_showFileName + '_Charts').toUpperCase(), style: TextStyle(color: UIAdapter.setFontColor(context))),
              trailing: Column(
                children: [
                  Flexible(
                      child: CircleAvatar(
                    key: const ValueKey(1),
                    backgroundColor: Colors.transparent,
                    radius: 40,
                    foregroundImage: NetworkImage(
                      '${Links.fgsStorageBaseUrl}profile/${element.uploader}.jpg',
                    ),
                    onForegroundImageError: (Object exception, StackTrace? stackTrace) =>
                        const NetworkImage('${Links.fgsStorageBaseUrl}profile/ufo.png'),
                  )),
                  Text(
                    'Provider',
                    style: TextStyle(color: UIAdapter.setFontColor(context)),
                  ),
                ],
              )),
        ),
      );
    });
    return _chartsList;
  }

  Future<void> _getFileList(bool shouldLoading) async {
    _pageNo = 1;
    final _response = await FileGetter.getAirportCharts ?? [];
    if (_response.isNotEmpty) {
      _totalPage = 1;
      chartList = _response;
      _pageViewController.page == 0
          ? renderChartList = _response.where((element) => element.bizType == 1).toList()
          : renderChartList = _response.where((element) => element.bizType == 2).toList();
      setState(() {});
    }
  }

  void _addScrollListener() {
    _scrollController.addListener(() async {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        _pageNo++;
        if (_pageNo > _totalPage) {
          /// stop count pageNo
          _pageNo = _totalPage;
          UIViewProvider.showScaffoldMessenger(context, 'no more data!');
          return;
        }
        setState(() {
          _isLoadingMore = true;
        });
        final _nextPageData = await FileGetter.getAirportCharts ?? [];
        renderChartList.addAll(_nextPageData.where((element) => element.bizType == FileBizType.airport.index).toList());
        setState(() {
          _isLoadingMore = false;
        });
      }
    });
  }

  @override
  void initState() {
    _getFileList(true);
    _addScrollListener();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    CustomViewProvider.hideMaterialLoading();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Airport Charts')),
      body: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 60),
            child: SafeArea(
              child: Column(
                children: [
                  _BuildTabBar(context),
                  Expanded(
                    child: PageView(
                      controller: _pageViewController,
                      children: [
                        RefreshIndicator(
                          onRefresh: () => _getFileList(false),
                          child: SafeArea(
                            child: ListView(
                              controller: _scrollController,
                              children: _renderChartsList(renderChartList),
                            ),
                          ),
                        ),
                        SafeArea(
                          child: ListView(
                            controller: _scrollController,
                            children: _renderChartsList(renderChartList),
                          ),
                        ),
                      ],
                    ),
                  ),
                  _isLoadingMore
                      ? Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: const SizedBox(
                            height: 30,
                            width: 30,
                            child: CircularProgressIndicator(strokeWidth: 3),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          _BuildFloatingSearchBar(context)
        ],
      ),
    );
  }

  Card _BuildTabBar(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: Material(
              color: UIAdapter.setContainerColor(context),
              child: TabBar(
                onTap: (int index) {
                  _pageViewController.animateToPage(index, duration: const Duration(milliseconds: 300), curve: Curves.easeIn);
                  renderChartList = index == 0.0
                      ? chartList.where((element) => element.bizType == 1).toList()
                      : chartList.where((element) => element.bizType == 2).toList();
                  setState(() {});
                },
                automaticIndicatorColorAdjustment: true,
                indicatorSize: TabBarIndicatorSize.label,
                tabs: [
                  const Tab(child: Text('Airport Charts')),
                  const Tab(child: Text('Aircraft FCTM')),
                ],
              ),
            ),
          ),
          const Divider(height: 0, thickness: 2)
        ],
      ),
    );
  }

  FloatingSearchBar _BuildFloatingSearchBar(BuildContext context) {
    return FloatingSearchBar(
      backgroundColor: UIAdapter.setContainerColor(context),
      shadowColor: Colors.white,
      iconColor: UIAdapter.setContainerColorReverse(context),
      queryStyle: TextStyle(color: UIAdapter.setFontPrimaryColor(context)),
      hintStyle: TextStyle(color: UIAdapter.setFontPrimaryColor(context)),
      progress: _isSearching,
      hint: 'Search Airport Chart by ICAO code',
      onQueryChanged: (String query) async {
        if (query.length == 4) {
          setState(() {
            _isSearching = true;
          });
          final _response = await FileGetter.getAirportCharts ?? [];
          final _targetFileList = _response.where((element) => element.fileName.contains(query)).toList();
          if (_targetFileList.isEmpty) {
            UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'Target not found in FGS Charts manager system!'));
          }
          setState(() {
            _specificFileList = _targetFileList;
            _isSearching = false;
          });
        }
        if (query.isEmpty) {
          setState(() {
            renderChartList = chartList;
            _isSearching = false;
          });
        }
      },
      automaticallyImplyBackButton: false,
      transitionDuration: const Duration(milliseconds: 200),
      builder: (BuildContext context, Animation<double> transition) {
        return Material(
          color: Colors.transparent,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: _renderChartsList(_specificFileList),
          ),
        );
      },
    );
  }
}
