import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/util/GeographyUtil.dart';

import '../core/CustomUIViewProvider.dart';

class RegisterPageView extends StatefulWidget {
  RegisterPageView({Key? key}) : super(key: key);

  @override
  _RegisterPageViewState createState() => _RegisterPageViewState();
}

class _RegisterPageViewState extends State<RegisterPageView> {
  final _ifcUsernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _groupController = TextEditingController();
  final _ifcUsernameFocusNode = FocusNode();
  final _emailFocusNode = FocusNode();
  final _groupFocusNode = FocusNode();
  final _formKey = GlobalKey<FormState>();

  /// 获取IF 用户信息
  Future<Map<String, dynamic>> _getUserStatus() async {
    CustomViewProvider.showMaterialLoading('fetching if user info', context);
    final _response = await InfiniteFlightLiveDataApi.getInfiniteFlightUserInfoByUserName(_ifcUsernameController.text);
    final _position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.best,
      forceAndroidLocationManager: true,
    );
    final _location = await GeographyUtil.decodeLocation('${_position.longitude.toString()},${_position.latitude.toString()}');
    final _country = _location.regeocode!.addressComponent!.city;
    if (_response.errorCode == 0 && _response.result.isNotEmpty) {
      final _submitUserInfo = {
        'username': _ifcUsernameController.text,
        'email': _emailController.text,
        'joinedGroup': _groupController.text,
        'violations': _response.result[0].violations,
        'landings': _response.result[0].landingCount,
        'ifFlightTime': _response.result[0].flightTime ~/ 60,
        'country': _country,
      };
      return _submitUserInfo;
    } else {
      CustomViewProvider.hideMaterialLoading();
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'Get InfiniteFlight Info failed'));
      throw Exception('Get InfiniteFlight Info failed!');
    }
  }

  void _register() async {
    final form = _formKey.currentState;
    if (form!.validate()) {
      final _request = await _getUserStatus();
      final _applyResponse = await FgsServiceApi.register(_request);
      if (_applyResponse.status == 0) {
        UIViewProvider.showScaffoldSuccess(ScaffoldMessageConfig(message: _applyResponse.message));
      } else {
        UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: _applyResponse.message));
      }
      CustomViewProvider.hideMaterialLoading();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        minimum: const EdgeInsets.only(top: 80),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                Container(
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, boxShadow: [BoxShadow(color: Colors.grey, offset: Offset(2, 2), blurRadius: 8)]),
                  child: const CircleAvatar(
                    radius: 80,
                    backgroundImage: AssetImage('images/fgsLogo.png'),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(32),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          TextFormField(
                            style: TextStyle(color: Theme.of(context).textTheme.bodyLarge!.color),
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            validator: (String? val) => (val == null || val.isEmpty) ? 'ifc username is required!' : null,
                            controller: _ifcUsernameController,
                            focusNode: _ifcUsernameFocusNode,
                            decoration: InputDecoration(
                              labelText: 'IFC Account',
                              prefixIcon: Icon(Icons.account_circle, color: Theme.of(context).iconTheme.color),
                            ),
                          ),
                          TextFormField(
                            style: TextStyle(color: Theme.of(context).textTheme.bodyLarge!.color),
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            validator: (String? val) => !GetUtils.isEmail(val ?? '') ? 'email is not illegal!' : null,
                            controller: _emailController,
                            focusNode: _emailFocusNode,
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.mail, color: Theme.of(context).iconTheme.color),
                              labelText: 'Email',
                            ),
                          ),
                          TextFormField(
                            style: TextStyle(color: Theme.of(context).textTheme.bodyLarge!.color),
                            controller: _groupController,
                            focusNode: _groupFocusNode,
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.group, color: Theme.of(context).iconTheme.color),
                              labelText: 'Joined Group',
                            ),
                          ),
                          const SizedBox(height: 60),
                          ElevatedButton(
                              onPressed: () => _register(),
                              child: Text(
                                'Register'.toUpperCase(),
                              ))
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
