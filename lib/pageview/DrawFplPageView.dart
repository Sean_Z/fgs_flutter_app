import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/fragment/OsmMapFragment.dart';
import 'package:ifgs/widget/PublicWidget/FgsPopupMenu.dart';
import 'package:latlong2/latlong.dart';

class DrawFplPageView extends StatefulWidget {
  DrawFplPageView({Key? key}) : super(key: key);

  @override
  _DrawFplPageViewState createState() => _DrawFplPageViewState();
}

class _DrawFplPageViewState extends State<DrawFplPageView> {
  final _mapController = MapController();
  final _popupLayerController = PopupController();

  /// 地图类型菜单
  final List<PopupMenuClass> _mapStyleList = [
    PopupMenuClass(const Icon(Icons.nightlight_round), 'dark'),
    PopupMenuClass(const Icon(Icons.traffic), 'street'),
    PopupMenuClass(const Icon(Icons.wb_sunny), 'light'),
    PopupMenuClass(const Icon(Icons.satellite), 'satellite'),
  ];

  final List<Marker> _markerList = [];
  final List<Polyline> _polyline = [];
  final List<LatLng> _latLngList = [];

  OsmMapStyle _mapType = OsmMapStyle.light;

  @override
  void initState() {
    _moveToCurrentUserPosition();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Customized route'),
      ),
      floatingActionButton: FgsPopupMenuButton(
        callback: (value) {
          menuAction(int.parse(value.toString()));
        },
        icon: Icons.construction,
        iconSize: 40,
        buttonInfoList: _buttonInfoList,
      ),
      body: Stack(
        children: [
          FlutterMap(
            mapController: _mapController,
            options: MapOptions(
              initialZoom: 5,
              onTap: (TapPosition tapPosition, LatLng latLng) {
                _markerList.add(Marker(
                    point: latLng,
                    child: const Icon(
                      Icons.room,
                      color: Colors.blueAccent,
                    )));
                _latLngList.add(latLng);
                _polyline.add(Polyline(
                  color: _mapType == OsmMapStyle.dark ? Colors.greenAccent : Colors.indigo,
                  strokeWidth: 2,
                  points: _latLngList,
                ));
                setState(() {});
              },
              initialCenter: const LatLng(39.9042, 116.4074),
            ),
            children: [
              TileLayer(urlTemplate: Links.osmMapUrl, additionalOptions: {
                'id': Links.getOsmTheme(_mapType),
              }),
              PolylineLayer(polylines: _polyline),
              _markerList.isNotEmpty
                  ? PopupMarkerLayer(
                      options: PopupMarkerLayerOptions(
                        popupController: _popupLayerController,
                        markers: _markerList,
                        popupDisplayOptions: PopupDisplayOptions(
                          builder: (BuildContext context, Marker marker) {
                            return Container();
                          },
                          animation: const PopupAnimation.fade(duration: Duration(microseconds: 400), curve: Curves.bounceInOut),
                        ),
                        // markerRotateAlignment: PopupMarkerLayerOptions.rotationAlignmentFor(AnchorAlign.center),
                      ),
                    )
                  : Container()
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 10, right: 10),
            alignment: Alignment.topRight,
            child: FgsPopupMenuButton(
              callback: (value) => {_setMapType(int.parse(value.toString()))},
              icon: Icons.map,
              buttonInfoList: _mapStyleList,
            ),
          )
        ],
      ),
    );
  }

  final List<PopupMenuClass> _buttonInfoList = [
    PopupMenuClass(const Icon(Icons.clear_all), 'clear'),
    PopupMenuClass(const Icon(Icons.backspace), 'delete'),
    PopupMenuClass(const Icon(Icons.copy), 'copy'),
  ];

  Future<void> _moveToCurrentUserPosition() async {
    final _username = await InfoMethod.infiniteFlightUsername;
    final _response = await InfiniteFlightLiveDataApi.getTargetInAllServerFlight(_username);
    if (_response != null) {
      final _target = _response;
      final _targetPosition = LatLng(_target.position.latitude, _target.position.longitude);
      _mapController.move(_targetPosition, _mapController.camera.zoom);
      _latLngList.add(_targetPosition);
      _markerList.add(Marker(
          point: _targetPosition,
          child: RotationTransition(
            turns: AlwaysStoppedAnimation(_target.angle / 360),
            child: const Icon(
              Icons.airplanemode_active,
              color: Colors.blue,
              size: 25.0,
            ),
          )));
      setState(() {});
    }
  }

  void _clearScreen() {
    _markerList.clear();
    _latLngList.clear();
    _polyline.clear();
    setState(() {});
  }

  void _deleteLastPoint() {
    _latLngList.removeLast();
    _markerList.removeLast();
    setState(() {});
  }

  void _copyRoute() {
    final routeList = <String>[];
    _latLngList.forEach((element) {
      routeList.add('${element.latitude.toStringAsFixed(2)}N/${element.longitude.toStringAsFixed(2)}E');
    });
    final _routerInfo = routeList.join(' ').replaceAll('.', '');
    Clipboard.setData(ClipboardData(text: _routerInfo));
    UIViewProvider.showScaffoldSuccess(ScaffoldMessageConfig(message: 'route copied'));
  }

  /// 设置地图类型
  void _setMapType(int mapType) {
    final mapTypeMap = {
      0: OsmMapStyle.dark,
      1: OsmMapStyle.streets,
      2: OsmMapStyle.light,
      3: OsmMapStyle.satellite,
    };
    _mapType = mapTypeMap[mapType] ?? OsmMapStyle.dark;
    setState(() {});
  }

  void menuAction(int value) {
    final actionMap = {
      0: () => _clearScreen(),
      1: () => _deleteLastPoint(),
      2: () => _copyRoute(),
    };
    actionMap[value]!();
  }
}
