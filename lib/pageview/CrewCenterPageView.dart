import 'package:flutter/material.dart';
import 'package:ifgs/fragment/AboutMeFragment.dart';
import 'package:ifgs/fragment/FlightListFragment.dart';
import 'package:ifgs/fragment/OsmMapFragment.dart';

class CrewCenterPageView extends StatefulWidget {
  CrewCenterPageView({Key? key}) : super(key: key);

  @override
  _CrewCenter createState() => _CrewCenter();
}

class _CrewCenter extends State<CrewCenterPageView> {
  var _currentIndex = 0;
  final _pageController = PageController(initialPage: 0, keepPage: true);
  final _pages = <Widget>[const OsmMapFragment(), FlightListFragment(), AboutMeFragment()];

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  /// 创建BottomNavigationBar
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView(physics: const NeverScrollableScrollPhysics(), controller: _pageController, children: _pages),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
              _pageController.animateToPage(index,
                  duration: const Duration(milliseconds: 300), curve: Curves.fastOutSlowIn);
            });
          },
          items: [
            const BottomNavigationBarItem(icon: Icon(Icons.track_changes), label: ('Status')),
            const BottomNavigationBarItem(icon: Icon(Icons.category), label: ('Flight List')),
            const BottomNavigationBarItem(icon: Icon(Icons.book), label: ('Make Plan')),
            const BottomNavigationBarItem(icon: Icon(Icons.people), label: ('Me')),
          ],
        ));
  }
}
