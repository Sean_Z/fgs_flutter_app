import 'package:flutter/material.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/util/FileUtil.dart';
import 'package:ifgs/widget/PublicWidget/BesselWidget.dart';

class CertificatePackagePageView extends StatefulWidget {
  CertificatePackagePageView({Key? key}) : super(key: key);

  @override
  _CertificatePackagePageViewState createState() => _CertificatePackagePageViewState();
}

class _CertificatePackagePageViewState extends State<CertificatePackagePageView> {
  var _username = 'loading....';
  var _email = 'loading....';

  Future<void> _setUserInfo() async {
    _username = await InfoMethod.getUserName;
    final userInfo = await FileGetter.userInfo;
    var targetUser = userInfo?.list.firstWhere((element) => element.username == _username);
    if (targetUser != null && targetUser.mail.isNotEmpty) {
      _email = targetUser.mail.replaceFirst(RegExp(r'\d{4}'), '****', 3);
    } else {
      _email = 'unset';
    }
    setState(() {});
  }

  @override
  void initState() {
    _setUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _leftFontStyle = const TextStyle(color: Colors.indigo, fontWeight: FontWeight.w800, fontSize: 16);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Certificate Package'),
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(),
        children: [_IdCard(_leftFontStyle)],
      ),
    );
  }

  /// IFC account and FGS account verified
  Card _IdCard(TextStyle _leftFontStyle) {
    return Card(
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          const BesselWidget(),
          Container(
            height: 150,
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      children: [
                        const Icon(Icons.folder_shared, color: Colors.indigo),
                        const SizedBox(width: 8),
                        Text('FGS Identity Card', style: _leftFontStyle),
                      ],
                    ),
                    Row(
                      children: [
                        const Icon(Icons.people, color: Colors.indigo),
                        const SizedBox(width: 8),
                        Text(_username, style: _leftFontStyle),
                      ],
                    ),
                    Row(
                      children: [
                        const Icon(Icons.phone, color: Colors.indigo),
                        const SizedBox(width: 8),
                        SizedBox(width: 180, child: Text(_email, style: _leftFontStyle, overflow: TextOverflow.ellipsis)),
                      ],
                    ),
                  ],
                ),
                const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.verified_user,
                      size: 40,
                      color: Colors.greenAccent,
                    ),
                    SizedBox(
                      width: 100,
                      child: Text(
                        'IFC Account Verified!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.greenAccent,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
