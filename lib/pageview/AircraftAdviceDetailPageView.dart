import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/request/modal/AircraftInfoEntity.dart';
import 'package:ifgs/request/modal/FgsAircraftAdviceEntity.dart';

class AircraftInfoListBean {
  final String title;
  final String subTitle;
  final Icon leading;
  final String tile;
  AircraftInfoListBean(this.title, this.subTitle, this.leading, this.tile);
}

class AircraftAdviceDetailPageView extends StatefulWidget {
  const AircraftAdviceDetailPageView({
    Key? key,
    required this.title,
    required this.aircraftInfo,
    required this.livery,
  }) : super(key: key);

  final String title;
  final FgsAircraftAdviceEntity aircraftInfo;
  final AircraftInfoResult livery;

  @override
  _AircraftAdviceDetailPageViewState createState() => _AircraftAdviceDetailPageViewState();
}

class _AircraftAdviceDetailPageViewState extends State<AircraftAdviceDetailPageView> {
  var _isExpand = false;

  List<Widget> _BuildInfoList() {
    final _info = <AircraftInfoListBean>[
      AircraftInfoListBean(
          'Curise Speed',
          'typical curise speed',
          Icon(Icons.speed, color: UIAdapter.setIconColor(context)),
          widget.aircraftInfo.cruisingSpeed.toString() + ' M/KT'),
      AircraftInfoListBean('Range Time', 'data from infinite flight not actual value ',
          Icon(Icons.schedule, color: UIAdapter.setIconColor(context)), widget.aircraftInfo.rangeTime ?? 'unknown'),
      AircraftInfoListBean(
          'Seats',
          'maximum passenger',
          Icon(Icons.airline_seat_recline_extra, color: UIAdapter.setIconColor(context)),
          widget.aircraftInfo.maxPassenger.toString()),
      AircraftInfoListBean('Cargo', 'maximum cargo load', Icon(Icons.work, color: UIAdapter.setIconColor(context)),
          '${widget.aircraftInfo.maxCargo ?? '0'} ton'),
      AircraftInfoListBean('MTOW', 'maximum takeoff weight', Icon(Icons.speed, color: UIAdapter.setIconColor(context)),
          '${widget.aircraftInfo.mtow ?? '0 tonnes'} ton'),
      AircraftInfoListBean('MLW', 'maximum landing weight', Icon(Icons.speed, color: UIAdapter.setIconColor(context)),
          '${widget.aircraftInfo.mlw ?? '0 tonnes'} ton'),
      AircraftInfoListBean(
          'LTM',
          'landing trim',
          Icon(Icons.settings_input_component_outlined, color: UIAdapter.setIconColor(context)),
          '${widget.aircraftInfo.landingTrim ?? '30'}%'),
      AircraftInfoListBean(
          'TOT',
          'take off trim',
          Icon(Icons.settings_input_component_outlined, color: UIAdapter.setIconColor(context)),
          '${widget.aircraftInfo.takeoffTrim ?? '30'}%'),
      AircraftInfoListBean(
          'TOF',
          'takeoff flaps',
          Icon(Icons.settings_input_component_outlined, color: UIAdapter.setIconColor(context)),
          widget.aircraftInfo.takeoffFlaps ?? '1'),
      AircraftInfoListBean(
          'LDF',
          'landing flaps',
          Icon(Icons.settings_input_component_outlined, color: UIAdapter.setIconColor(context)),
          widget.aircraftInfo.landingFlaps ?? '1'),
      AircraftInfoListBean(
          'MTD',
          'maximum takeoff distance',
          Icon(Icons.flight_takeoff, color: UIAdapter.setIconColor(context)),
          '${widget.aircraftInfo.takeoffDistance ?? ''} ft'),
      AircraftInfoListBean(
          'MLD',
          'maximum landing distance',
          Icon(Icons.flight_land, color: UIAdapter.setIconColor(context)),
          '${widget.aircraftInfo.landingDistance ?? ''} ft'),
    ];
    final _listTile = <Widget>[];
    _info.forEach((element) {
      _listTile.add(ListTile(
        tileColor: UIAdapter.setContainerColor(context),
        leading: element.leading,
        title: Text(element.title, style: TextStyle(color: UIAdapter.setFontColor(context))),
        subtitle: Text(
          element.subTitle,
          style: TextStyle(color: UIAdapter.setFontColor(context)),
        ),
        trailing: Container(
          constraints: const BoxConstraints(maxWidth: 200),
          child: Text(element.tile, style: TextStyle(color: UIAdapter.setFontColor(context), fontSize: 16)),
        ),
      ));
      _listTile.add(const Divider(height: 1));
    });
    _listTile.add(ExpansionPanelList(
      dividerColor: Colors.transparent,
      expandedHeaderPadding: const EdgeInsets.all(0),
      animationDuration: const Duration(milliseconds: 500),
      expansionCallback: (int index, bool isOpen) {
        setState(() {
          _isExpand = !isOpen;
        });
      },
      children: [
        ExpansionPanel(
          backgroundColor: UIAdapter.setContainerColor(context),
          canTapOnHeader: true,
          isExpanded: _isExpand,
          headerBuilder: (BuildContext context, bool isOpen) => Ink(
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(width: 16),
                Icon(Icons.info),
                SizedBox(width: 10),
                Text('Remark'),
              ],
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.only(bottom: 16.0, left: 16, right: 16),
            child: Text(widget.aircraftInfo.remark ?? ''),
          ),
        )
      ],
    ));
    return _listTile;
  }

  @override
  Widget build(BuildContext context) {
    final liveryId = widget.livery.id;
    final aircraftId = widget.livery.aircraftID;
    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      body: ListView(
        children: [
          Container(
              height: 200, width: Get.width, child: UIViewProvider.getAssetsAircraftImage(context, liveryId, aircraftId)),
          Column(
            children: _BuildInfoList(),
          )
        ],
      ),
    );
  }
}
