import 'package:flutter/material.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:webview_flutter/webview_flutter.dart';

class UserPrivatePrivacyPageView extends StatefulWidget {
  const UserPrivatePrivacyPageView({Key? key}) : super(key: key);

  @override
  _UserPrivatePrivacyPageViewState createState() => _UserPrivatePrivacyPageViewState();
}

class _UserPrivatePrivacyPageViewState extends State<UserPrivatePrivacyPageView> {
  final WebViewController _webViewController = WebViewController()
  ..setJavaScriptMode(JavaScriptMode.unrestricted)
  ..setNavigationDelegate(NavigationDelegate());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.current.UserPrivatePrivacy),
      ),
      body: Container(
        constraints: const BoxConstraints.expand(),
        child: WebViewWidget(
          controller: _webViewController,
        ),
      ),
    );
  }
}
