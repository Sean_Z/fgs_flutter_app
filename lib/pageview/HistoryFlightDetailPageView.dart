import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/request/modal/flight_log_res_entity.dart';
import 'package:ifgs/widget/FlightPlanGeneratorWidget/MainInfoCard.dart';
import 'package:intl/intl.dart';

class HistoryFlightDetailPageView extends StatefulWidget {
  HistoryFlightDetailPageView({Key? key, required this.log}) : super(key: key);

  final FlightLogResList log;

  @override
  _HistoryFlightDetailPageViewState createState() => _HistoryFlightDetailPageViewState();
}

class _HistoryFlightDetailPageViewState extends State<HistoryFlightDetailPageView> {

  String formatBoardingTime() {
    final rawBoardingTime = DateTime.parse(widget.log.departureTime).subtract(Duration(minutes: double.parse(widget.log.flightTime).toInt() + 30));
    final boardingTime = DateFormat('yyyy MM dd - HH:mm', 'en_US').format(rawBoardingTime);
    return boardingTime;
  }

  bool get shouldUseDark {
    return widget.log.nightTime > widget.log.dayTime;
  }

  @override
  Widget build(BuildContext context) {
    final backgroundImg = 'images/basic/dark_sky.jpeg';
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Image.asset(
              backgroundImg,
              height: 300,
              width: Get.width,
              fit: BoxFit.cover,
            ),
            _TopBlock(),
            MainInfoCard(log: widget.log),
          ],
        ));
  }

  SafeArea _TopBlock() {
    final airlineNameText = const TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w600);
    final dateText = const TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.w600);
    final pilotNameText = const TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.w600);
    final flightClass = FlightJudgment.getFlightHaulType(widget.log.distance, widget.log.departure, widget.log.arrival);
    final subText = const TextStyle(color: Colors.white, fontSize: 18);
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16, top: 8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Wrap(
                  direction: Axis.horizontal,
                  alignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  spacing: 8,
                  children: [
                    ConstrainedBox(
                      constraints: const BoxConstraints(maxWidth: 120),
                      child: Text(
                        widget.log.livery,
                        style: airlineNameText,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      height: 30,
                      child: UIViewProvider.getAirlineLogo(widget.log.livery),
                    ),
                  ],
                ),
                Text(
                  '\n${shouldUseDark ? 'Night Flight' : 'Day Flight'}',
                  style: dateText,
                ),
              ],
            ),
            const SizedBox(height: 16),
            Text(widget.log.pilot, style: pilotNameText),
            const SizedBox(height: 32),
            Wrap(
              direction: Axis.horizontal,
              children: [
                Container(
                  width: 32,
                  height: 32,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.amber,
                  ),
                  child: Text(flightClass.substring(0, 1), style: subText),
                ),
                const SizedBox(width: 8),
                Wrap(
                  direction: Axis.vertical,
                  children: [
                    Text(
                      flightClass,
                      style: const TextStyle(color: Colors.yellowAccent, fontWeight: FontWeight.w600),
                    ),
                    Text(
                      'BOARDING ${formatBoardingTime()}',
                      style: const TextStyle(color: Colors.yellowAccent, fontWeight: FontWeight.w600),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
