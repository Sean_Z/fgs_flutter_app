import 'dart:math';

import 'package:flutter/material.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/widget/AchievementWidget/FlightDataWithSparkLineChartWidget.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../widget/AchievementWidget/FlightAchievementInfoCard.dart';
import '../widget/PublicWidget/LoadingMask.dart';

class InfiniteFlightAchievementPageView extends StatefulWidget {
  InfiniteFlightAchievementPageView({Key? key}) : super(key: key);

  @override
  _InfiniteFlightAchievementPageViewState createState() => _InfiniteFlightAchievementPageViewState();
}

class _InfiniteFlightAchievementPageViewState extends State<InfiniteFlightAchievementPageView> {
  List<ChartData> _chartData = [
    ChartData('Low', 3500, const Color.fromRGBO(235, 97, 143, 1)),
    ChartData('Average', 7200, const Color.fromRGBO(145, 132, 202, 1)),
    ChartData('High', 10500, const Color.fromRGBO(69, 187, 161, 1)),
  ];
  var _flightTimeList = <double>[];
  var _userName = '';
  var _totalFlightTime = 0;
  var _totalLandings = 0;
  var _atcOperationCount = 0;
  var _totalViolations = 0;
  var _landingsIn90 = 0;
  var _flightTimeIn90 = '0';
  var _grade = 'Pilot Info';
  var _isQuerying = true;

  Future<void> _setAvatar() async {
    final param = RouterMethod.getRouterParameter();
    final _username = param ?? await InfoMethod.infiniteFlightUsername;
    setState(() {
      _userName = _username.toString();
    });
  }

  Future<void> _getInfiniteFlightUserInfo() async {
    final param = RouterMethod.getRouterParameter();
    final isOwnInfo = param == null;
    final username = isOwnInfo ? await InfoMethod.infiniteFlightUsername : param;
    final infiniteFlightUserInfo = await InfiniteFlightLiveDataApi.getInfiniteFlightUserInfoByUserName(username.toString());
    final tempFlightTimeList = List.generate(30, (index) => Random().nextDouble());
    if (infiniteFlightUserInfo.errorCode == 0 && infiniteFlightUserInfo.result.isNotEmpty) {
      final baseInfo = infiniteFlightUserInfo.result[0];
      final gradeInfo = await InfiniteFlightLiveDataApi.getInfiniteFlightUserGrade(baseInfo.userId);
      final flightTime = baseInfo.flightTime;
      final landings = baseInfo.landingCount;
      final xp = baseInfo.xp;
      final atcOperations = baseInfo.atcOperations;
      _atcOperationCount = baseInfo.atcOperations;
      _landingsIn90 = (gradeInfo.result!.gradeDetails!.grades![0].rules![10].userValue!).toInt();
      _flightTimeIn90 = (gradeInfo.result!.gradeDetails!.grades![0].rules![9].userValueString!);
      _totalFlightTime = flightTime;
      _totalLandings = landings;
      _totalViolations = baseInfo.violations;
      _flightTimeList = tempFlightTimeList;
      _grade = 'G${baseInfo.grade.toString()} Pilot';
      _chartData = [
        ChartData('Flight Time', flightTime.toDouble(), const Color.fromRGBO(235, 97, 143, 1)),
        ChartData('XP', xp.toDouble(), const Color.fromRGBO(69, 187, 161, 1)),
        ChartData('Landing', landings.toDouble(), const Color.fromRGBO(145, 132, 202, 1)),
        ChartData('ATV Ops', atcOperations.toDouble(), const Color.fromRGBO(231, 65, 12, 1.0)),
      ];
    }else {
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'Fetch IF user information failed!'));
      Navigator.of(context).pop();
    }
    _isQuerying = false;
    setState(() {});
  }

  String _getCardDesc(String type) {
    final dataMap = {
      'flightTime': 'Flight time can intuitively illustrate the experience of a captain',
      'landing': 'Landing count can be a good indicator of the experience and level of a pilot',
      'atc': 'A professional captain also need to master the knowledge of ATC proficiently',
      'vio': 'To be great pilot in IF and community, we should keep our professional',
    };
    return dataMap[type] ?? '';
  }

  @override
  void initState() {
    _setAvatar();
    _getInfiniteFlightUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _isQuerying ? const LoadingMask() : _MainContainer(context);
  }

  Widget _MainContainer(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: _TitleContent(),
            centerTitle: false,
            backgroundColor: UIAdapter.setContainerColor(context),
            toolbarHeight: 100,
            surfaceTintColor: Colors.white,
            shadowColor: Colors.black45,
            automaticallyImplyLeading: false,
            floating: true,
            pinned: true,
            snap: false,
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              _BriefCard(context),
              FlightDataWithSparkLineChartWidget(
                  baseColor: Colors.tealAccent,
                  title: 'Total Flight Time',
                  value: (_totalFlightTime / 60).toStringAsFixed(1) + ' Hrs',
                  backgroundImageName: 'ifAchievementFlightTimeChartBg.jpeg',
                  data: _flightTimeList),
              FlightAchievementInfoCard(
                totalLandings: _totalLandings,
                atcOperationCount: _atcOperationCount,
                totalViolations: _totalViolations,
              ),
              _BottomScrollView()
            ]),
          )
        ],
      ),
    );
  }

  Wrap _TitleContent() {
    return Wrap(
      direction: Axis.vertical,
      crossAxisAlignment: WrapCrossAlignment.start,
      spacing: 16,
      children: [
        Text(
          S.current.InfiniteFlightAchievementTitle,
          style: const TextStyle(fontSize: 24, fontWeight: FontWeight.w800),
        ),
        Text(
          S.current.InfiniteFlightAchievementAnalyze,
          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
        ),
      ],
    );
  }

  SingleChildScrollView _BottomScrollView() {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          _BuildSubInfoCard('Flight Time', Colors.indigo, _getCardDesc('flightTime'), Icons.loyalty, Colors.greenAccent),
          _BuildSubInfoCard('Landing', Colors.teal, _getCardDesc('landing'), Icons.landscape, Colors.cyanAccent),
          _BuildSubInfoCard('ATC Ops', Colors.deepOrange, _getCardDesc('atc'), Icons.record_voice_over, Colors.lightBlueAccent),
          _BuildSubInfoCard('Violations', Colors.redAccent, _getCardDesc('vio'), Icons.gavel, Colors.redAccent),
        ],
      ),
    );
  }

  Container _BriefCard(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Wrap(
            spacing: 8,
            children: [
              const Icon(Icons.account_circle, size: 30),
              Text(
                _userName + ' $_grade',
                style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
              ),
            ],
          ),
          Card(
            color: Colors.tealAccent,
            shadowColor: Colors.tealAccent,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(80),
                topLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
                bottomLeft: Radius.circular(10),
              ),
            ),
            child: Flex(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              direction: Axis.horizontal,
              children: [
                Flexible(
                  flex: 1,
                  child: Wrap(
                    spacing: 20,
                    direction: Axis.vertical,
                    children: [
                      _BuildInfoItem(context, 'Flight Time in 90 days', _flightTimeIn90, Icons.access_time, Colors.greenAccent),
                      _BuildInfoItem(
                          context, 'Landings in 90 days', _landingsIn90.toString() + ' landings', Icons.flight_land, Colors.green)
                    ],
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Container(
                    alignment: Alignment.center,
                    constraints: const BoxConstraints(maxHeight: 180, maxWidth: 140),
                    child: SfCircularChart(
                      annotations: [
                        CircularChartAnnotation(widget: _BuildUserAvatar()),
                      ],
                      series: [
                        RadialBarSeries<ChartData, String>(
                            maximumValue: 6000,
                            enableTooltip: true,
                            radius: '100%',
                            dataSource: _chartData,
                            animationDuration: 1500,
                            dataLabelMapper: (ChartData data, _) => data.x,
                            cornerStyle: CornerStyle.bothCurve,
                            xValueMapper: (ChartData data, _) => data.x,
                            yValueMapper: (ChartData data, _) => data.y,
                            pointColorMapper: (ChartData data, _) => data.color)
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _BuildUserAvatar() {
    final param = RouterMethod.getRouterParameter();
    if (param != null) {
      return const CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 30,
        foregroundImage: AssetImage('images/fgsLogo.png'),
      );
    } else {
      return UIViewProvider.getUserAvatar(radius: 30);
    }
  }
}

Row _BuildInfoItem(
  BuildContext context,
  String title,
  String data,
  IconData iconData,
  Color dividerColor,
) {
  return Row(
    children: [
      Container(
        height: 50,
        child: VerticalDivider(
          color: dividerColor,
          width: 20,
          thickness: 3,
        ),
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: const TextStyle(
              fontWeight: FontWeight.w900,
              color: Colors.blueGrey,
            ),
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              Icon(iconData, color: Colors.indigoAccent),
              const SizedBox(width: 8),
              Text(
                data,
                style: const TextStyle(
                  color: Colors.indigoAccent,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ],
      )
    ],
  );
}

Widget _BuildSubInfoCard(String title, Color color, String data, IconData iconData, Color tipColor) {
  return Stack(
    children: [
      Container(
        constraints: const BoxConstraints(minHeight: 300),
        width: 160,
        padding: const EdgeInsets.all(8.0),
        child: Card(
          shadowColor: color,
          color: color,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(80),
              topLeft: Radius.circular(20),
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                title,
                style: const TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w800),
              ),
              Container(
                padding: const EdgeInsets.all(16),
                child: Text(
                  data,
                  style: const TextStyle(color: Colors.white, fontSize: 15.5),
                ),
              ),
            ],
          ),
        ),
      ),
      Icon(
        iconData,
        size: 50,
        color: tipColor,
      ),
    ],
  );
}

class ChartData {
  ChartData(this.x, this.y, [this.color]);

  final String x;
  final double y;
  final Color? color;
}
