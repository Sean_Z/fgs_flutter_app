import 'package:flutter/material.dart';

import '../request/modal/infinte_flight_airport_entity.dart';

class AirportAdviceDetailPageView extends StatefulWidget {
  const AirportAdviceDetailPageView({super.key, required this.airportResult});

  final InfiniteFlightAirportResult airportResult;

  @override
  State<AirportAdviceDetailPageView> createState() => _AirportAdviceDetailPageViewState();
}

class _AirportAdviceDetailPageViewState extends State<AirportAdviceDetailPageView> {
  Widget TitleContent() {
    final airportName = widget.airportResult.name ?? 'Unknown';
    final airportIcao = widget.airportResult.icao ?? 'Unknown';
    final titleTextStyle = const TextStyle(
      fontSize: 22,
      color: Colors.white,
      fontWeight: FontWeight.w500
    );
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(60)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(top: 70),
            child: Text(
              '$airportName ($airportIcao)',
              style: titleTextStyle,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            automaticallyImplyLeading: false,
            expandedHeight: 180,
            centerTitle: true,
            backgroundColor: Colors.transparent,
            flexibleSpace: FlexibleSpaceBar(
              background: TitleContent(),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              const SizedBox(height: 1000)
            ]),
          )
        ],
      ),
    );
  }
}
