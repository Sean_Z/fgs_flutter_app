import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/stylesheet/Theme.dart';
import 'package:ifgs/stylesheet/pagestylesheet/IndexPageViewStyle.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:ifgs/viewcontroller/IndexPageViewController.dart';
import 'package:ifgs/viewmodal/IndexPageViewModal.dart';
import 'package:ifgs/widget/FlightPlanGeneratorWidget/GenerateFlightPlanCardWidget.dart';
import 'package:ifgs/widget/IndexWidget/DrawWidget.dart';
import 'package:ifgs/widget/OnlineMap/FlightMapCardWidget.dart';
import 'package:ifgs/widget/PublicWidget/LoadingButton.dart';

import '../main.dart';
import '../widget/IndexWidget/FgsBoundFlightWidget.dart';
import '../widget/IndexWidget/IndexPageViewMenuListCard.dart';

class IndexPageView extends StatefulWidget {
  @override
  State<IndexPageView> createState() => _IndexPageViewState();
}

class _IndexPageViewState extends State<IndexPageView> with RouteAware {
  final _viewController = Get.put(IndexPageViewController());

  @override
  void didChangeDependencies() async {
    routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);
    super.didChangeDependencies();
  }

  @override
  void didPopNext() async {
    final lang = await StorageUtil.getStringItem('lang') ?? 'en';
    lang == 'en' ? UIAdapter.changeLocalLangToEN() : UIAdapter.changeLocalLangToCN();
    await Get.forceAppUpdate();
    _viewController.updateSignalText();
    super.didPopNext();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder(builder: (IndexPageViewController controller) {
      return Scaffold(
        key: _scaffoldKey,
        drawer: Drawer(
          elevation: 12,
          child: IndexDrawer(email: _viewController.email),
        ),
        appBar: AppBar(
          centerTitle: false,
          titleSpacing: 0.0,
          leading: GestureDetector(
            onTap: () => _scaffoldKey.currentState?.openDrawer(),
            child: const Icon(Icons.menu),
          ),
          title: _TitleActionButton(),
          actions: [_SignalButton(context)],
        ),
        body: RefreshIndicator(
          onRefresh: _viewController.setSignalState,
          child: ListView(
            physics: const AlwaysScrollableScrollPhysics(),
            children: [
              _PlanCard(context),
              _SelfFlightMap(),
              const IndexPageViewMenuListCard(),
              _viewController.isOnline ? _CurrentFlightCard(context) : Container(),
              FgsBoundFlightWidget(key: fgsBoundKey)
            ],
          ),
        ),
      );
    });
  }

  SingleChildScrollView _TitleActionButton() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: GestureDetector(
        child: Wrap(
          alignment: WrapAlignment.start,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: <Widget>[
            Text(
              '${_viewController.title}',
              style: IndexPageViewStyle.appBarTitle,
              softWrap: true,
            ),
          ],
        ),
      ),
    );
  }

  Container _SignalButton(BuildContext context) {
    final _shape = RoundedRectangleBorder(borderRadius: BorderRadius.circular(20));
    return Container(
        margin: const EdgeInsets.all(10),
        child: ElevatedButton(
          style: TextButton.styleFrom(shape: _shape),
          onPressed: () {
            if (_viewController.isOnline) {
              Get.toNamed(RouterSheet.flightInfo, arguments: _viewController.targetFlight);
            }
          },
          child: Row(children: <Widget>[
            AnimatedSwitcher(
              switchInCurve: Curves.easeInCirc,
              duration: const Duration(milliseconds: 300),
              transitionBuilder: (child, animation) {
                return ScaleTransition(scale: animation, child: child);
              },
              child: Icon(
                _viewController.stateIcon,
                key: ValueKey(_viewController.state),
                color: UIAdapter.setIconColor(context),
              ),
            ),
            const SizedBox(width: 10),
            AnimatedSwitcher(
              switchInCurve: Curves.bounceIn,
              duration: const Duration(milliseconds: 300),
              transitionBuilder: (child, animation) {
                return ScaleTransition(scale: animation, child: child);
              },
              child: Text(
                _viewController.state,
                key: ValueKey(_viewController.state),
                style: TextStyle(color: UIAdapter.setFontPrimaryColorWhite(context)),
              ),
            )
          ]),
        ));
  }

  Widget _SelfFlightMap() {
    return _viewController.targetFlight != null
        ? SizedBox(
            height: 200,
            child: Card(
              margin: const EdgeInsets.fromLTRB(8, 4, 8, 4),
              child: GestureDetector(
                child: AbsorbPointer(
                  absorbing: true,
                  child: FlightMapCardWidget(key: flightMapCardKey),
                ),
              ),
            ),
          )
        : Container();
  }

  Card _PlanCard(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(8),
      child: SizedBox(
        height: 200,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(child: Image.asset('images/basic/bg_white.png', fit: BoxFit.cover)),
            GenerateFlightPlanCardWidget(key: generateFplWidgetKey),
            Positioned(left: 40, right: 40, bottom: 20, child: _PlanCardActionButton(context))
          ],
        ),
      ),
    );
  }

  Wrap _PlanCardActionButton(BuildContext context) {
    return Wrap(
      direction: Axis.horizontal,
      runAlignment: WrapAlignment.center,
      spacing: 32,
      alignment: WrapAlignment.center,
      children: <Widget>[
        LoadingButton(
            key: loadingButtonKey,
            buttonStyle: TextButton.styleFrom(
              foregroundColor: Colors.white,
              shadowColor: Colors.white,
              backgroundColor: UIAdapter.isDarkTheme(context) ? DarkTheme.deepDarkGrey : Colors.blueGrey,
            ),
            indicatorColor: Colors.white,
            action: () => _viewController.getPlan(),
            label: Text('${S.current.FlightPlanWidgetCopyRoute}')),
        ElevatedButton(
          style: TextButton.styleFrom(
            backgroundColor: Colors.redAccent,
            foregroundColor: Colors.white,
          ),
          onPressed: () => _viewController.clearChildForm(),
          child: Text('${S.current.FlightPlanWidgetClear} !'),
        )
      ],
    );
  }

  /// menuList
  List<MenuListDataClass> get _StatusList {
    return [
      MenuListDataClass(
        title: S.current.indexPageViewSpeed,
        value: _viewController.targetFlight!.speed.toInt().toString() + ' kts',
        icon: Icons.speed,
      ),
      MenuListDataClass(
        title: S.current.IndexPageViewAltitude,
        value: _viewController.targetFlight!.altitude.toInt().toString() + ' ft',
        icon: Icons.landscape,
      ),
      MenuListDataClass(
        title: S.current.IndexPageViewVs,
        value: _viewController.targetFlight!.verticalSpeed.toInt().toString() + ' ft/min',
        icon: Icons.landscape,
      ),
      MenuListDataClass(
        title: S.current.IndexPageViewHeading,
        value: _viewController.targetFlight!.angle.toInt().toString(),
        icon: Icons.explore,
      ),
    ];
  }

  Card _CurrentFlightCard(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(left: 8, right: 8, bottom: 8, top: 4),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GridView.builder(
            shrinkWrap: true,
            itemCount: _StatusList.length,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 3,
              crossAxisSpacing: 10,
              childAspectRatio: 5,
            ),
            itemBuilder: (context, index) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Icon(_StatusList[index].icon),
                      const SizedBox(width: 8),
                      Text(
                        _StatusList[index].title,
                        style: TextStyle(color: UIAdapter.setFontPrimaryColor(context)),
                      ),
                    ],
                  ),
                  Text(
                    _StatusList[index].value,
                    style: TextStyle(color: UIAdapter.setFontPrimaryColor(context)),
                  ),
                ],
              );
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Wrap(
                  spacing: 8,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    const Icon(Icons.business),
                    const Text(
                      'livery',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Container(
                      constraints: const BoxConstraints(maxWidth: 120),
                      child: Text(
                        DataHandler.getLiveryName(_viewController.targetFlight!.livery),
                        style: TextStyle(color: Theme.of(context).colorScheme.secondary),
                      ),
                    ),
                  ],
                ),
                Builder(builder: (context) {
                  return TextButton(
                    onPressed: () {
                      showModalBottomSheet(
                        builder: (BuildContext context) {
                          return _BottomSheet(context);
                        },
                        context: context,
                      );
                    },
                    child: Row(
                      children: [
                        const Icon((Icons.save), color: Colors.red),
                        const SizedBox(width: 8),
                        Text(
                          '${S.current.IndexPageViewSubmit}!',
                          style: const TextStyle(color: Colors.red),
                        ),
                      ],
                    ),
                  );
                }),
              ],
            ),
          ),
          LinearProgressIndicator(
            backgroundColor: Colors.blue[200],
            valueColor: const AlwaysStoppedAnimation(Colors.blue),
            value: _viewController.flightRemainRate,
          )
        ],
      ),
    );
  }

  /// Build BottomSheet to alert user make sure submit flight
  /// call method [_viewController.submitFlight(context)] to submit
  Widget _BottomSheet(BuildContext context) {
    final bottomTitleStyle = TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w600,
      color: UIAdapter.setFontColor(context),
    );
    final listTileFontStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: UIAdapter.setFontColor(context));
    final subTileFontStyle = TextStyle(color: UIAdapter.setFontColor(context));
    return Container(
      decoration: BoxDecoration(
        color: UIAdapter.setContainerColor(context),
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Wrap(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16.0, left: 16),
                child: Wrap(
                  spacing: 8,
                  alignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  direction: Axis.horizontal,
                  children: [
                    Text('Submit Flight', style: bottomTitleStyle),
                    const Icon(Icons.history_edu, size: 30, color: Colors.blueGrey),
                  ],
                ),
              ),
              ListTile(
                title: Text('Love it?', style: listTileFontStyle),
                leading: const Icon(Icons.favorite, color: Colors.redAccent, size: 32),
                subtitle: Text('mark as favorite', style: subTileFontStyle),
                trailing: Switch(
                  activeColor: Colors.white,
                  inactiveTrackColor: Colors.deepPurpleAccent,
                  activeTrackColor: Colors.pinkAccent,
                  value: _viewController.isCheckedFavorite,
                  onChanged: (bool checked) {
                    _viewController.isCheckedFavorite = checked;
                    Get.appUpdate();
                  },
                ),
              ),
              ListTile(
                title: Text('Delayed?', style: listTileFontStyle),
                leading: const Icon(Icons.pending_actions, color: Colors.deepOrangeAccent, size: 32),
                subtitle: Text('mark as favorite', style: subTileFontStyle),
                trailing: Switch(
                  activeColor: Colors.white,
                  inactiveTrackColor: Colors.green,
                  activeTrackColor: Colors.red,
                  value: _viewController.isCheckedDelay,
                  onChanged: (bool checked) {
                    _viewController.isCheckedDelay = checked;
                    Get.appUpdate();
                  },
                ),
              ),
              ListTile(
                title: Text('What about Scenery?', style: listTileFontStyle),
                leading: const Icon(Icons.stars, color: Colors.amber, size: 32),
                subtitle: RatingBar.builder(
                  unratedColor: Colors.black54,
                  itemSize: 24,
                  initialRating: _viewController.rate,
                  itemBuilder: (context, _) => const Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    _viewController.rate = rating;
                  },
                ),
              ),
            ],
          ),
          SafeArea(
            child: Wrap(
              direction: Axis.vertical,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: ElevatedButton(
                    style: TextButton.styleFrom(
                      fixedSize: const Size(180, 40),
                    ),
                    onPressed: () async {
                      await _viewController.submitFlight(context);
                    },
                    child: Wrap(
                      spacing: 16,
                      alignment: WrapAlignment.center,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      direction: Axis.horizontal,
                      children: [
                        const Icon(Icons.save),
                        Text(
                          S.current.IndexPageViewSubmit,
                          style: const TextStyle(fontSize: 18),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
