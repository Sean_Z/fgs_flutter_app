import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/provider/atc/AtcMonitorProvider.dart';
import 'package:ifgs/viewcontroller/AtcMonitorViewController.dart';
import 'package:ifgs/widget/ATCAirportTableWidget.dart';
import 'package:ifgs/widget/PublicWidget/InsteadNoData.dart';
import 'package:ifgs/widget/PublicWidget/SkeletonWidget.dart';
import 'package:provider/provider.dart';

class ATCMonitorPageView extends StatefulWidget {
  @override
  _ATCMonitorPageViewState createState() => _ATCMonitorPageViewState();
}

class _ATCMonitorPageViewState extends State<ATCMonitorPageView> {
  final viewController = AtcMonitorViewController();

  /// Set indicatorKey used for toggle RefreshIndicator manually
  final GlobalKey<RefreshIndicatorState> _indicatorKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    Future.delayed(Duration.zero, () => viewController.renderAtcListView(context));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final provider = context.watch<AtcMonitorProvider>();
    return Scaffold(
      body: RefreshIndicator(
        key: _indicatorKey,
        onRefresh: () => viewController.refresh(context),
        child: DefaultTabController(
          length: 2,
          child: CustomScrollView(slivers: [
            SliverAppBar(
              backgroundColor: UIAdapter.setContainerColor(context),
              bottom: TabBar(
                  labelStyle: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  onTap: (int index) {
                    provider.setServerType(index);
                    _indicatorKey.currentState!.show();
                  },
                  tabs: [
                    const Tab(text: 'ES ATC'),
                    const Tab(text: 'TS ATC'),
                  ]),
              flexibleSpace: FlexibleSpaceBar(
                background: Image.asset('images/background/ATC.png'),
                collapseMode: CollapseMode.pin,
              ),
              expandedHeight: 200.0,
              floating: true,
              pinned: true,
              snap: false,
            ),
            provider.isFinishedQry
                ? SliverFixedExtentList(
                    delegate: SliverChildBuilderDelegate(
                      (context, i) => Container(
                        margin: const EdgeInsets.all(10),
                        child: provider.isHasData
                            ? AtcSingleCardWidget(
                                time: provider.ATCRows[i].startTime,
                                type: provider.ATCRows[i].type,
                                airportName: provider.ATCRows[i].airportName,
                                atcName: provider.ATCRows[i].username,
                                serverType: provider.serverType,
                                organization: (provider.ATCRows[i].virtualOrganization ?? 'unknown') as String)
                            : const InsteadNoData(
                                content: '      No ATC \n click to refresh',
                              ),
                      ),
                      childCount: provider.isHasData ? provider.ATCRows.length : 1,
                    ),
                    itemExtent: provider.isHasData ? 150 : 300,
                  )
                : SliverFixedExtentList(
                    delegate: SliverChildBuilderDelegate(
                      (context, i) => Container(
                        margin: const EdgeInsets.all(10),
                        width: Get.width,
                        child: const SkeletonWidget(count: 20),
                      ),
                      childCount: 1,
                    ),
                    itemExtent: 800,
                  )
          ]),
        ),
      ),
    );
  }
}
