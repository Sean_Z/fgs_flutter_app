import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/core/Color.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';
import 'package:ifgs/request/modal/OnlineFlightPlanBean.dart';
import 'package:ifgs/widget/OnlineMap/FlightCardFirstWidget.dart';
import 'package:ifgs/widget/OnlineMap/FlightCardSecondWidget.dart';
import 'package:ifgs/widget/OnlineMap/FlightDataChart.dart';
import 'package:ifgs/widget/OnlineMap/FlightMapCardWidget.dart';
import 'package:ifgs/widget/PublicWidget/DynamicWidget.dart';
import 'package:provider/provider.dart';

class FlightInfoPageView extends StatefulWidget {
  FlightInfoPageView({Key? key, this.isShowAppBar, this.serverType}) : super(key: key);

  final bool? isShowAppBar;
  final int? serverType;

  @override
  _FlightInfoPageViewState createState() => _FlightInfoPageViewState();
}

class _FlightInfoPageViewState extends State<FlightInfoPageView> {
  var refreshComponent = false;
  var _queryFinished = false;
  var _isMyself = false;

  void _getUsername() async {
    final username = await InfoMethod.infiniteFlightUsername;
    final flightBean = Provider.of<FlightInfoStateProvider>(context, listen: false).flightInfoEntity;
    setState(() {
      _isMyself = username == flightBean.displayName;
    });
  }

  Future<void> getMainData() async {
    final flightBean = Provider.of<FlightInfoStateProvider>(context, listen: false).flightInfoEntity;
    CustomViewProvider.showMaterialLoading('loading....', context);
    final response = await Future.wait([
      InfiniteFlightLiveDataApi.getOnlinePlan(flightBean.flightId),
      InfiniteFlightLiveDataApi.getAirwayLine(flightBean.flightId),
    ]);
    CustomViewProvider.hideMaterialLoading();
    _queryFinished = true;
    final onlineFlightPlanBean = response[0] as OnlineFlightPlanBean;
    final airwayLineEntity = response[1] as List<AirwayLineListBean>;
    Provider.of<FlightInfoStateProvider>(context, listen: false).setOnlineFlightEntity(onlineFlightPlanBean);
    Provider.of<FlightInfoStateProvider>(context, listen: false).setAirwayLineEntity(airwayLineEntity);
    Provider.of<FlightInfoStateProvider>(context, listen: false).setServerType(widget.serverType ?? 0);
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      _getUsername();
      getMainData();
    });
  }

  @override
  void dispose() {
    super.dispose();
    CustomViewProvider.hideMaterialLoading();
  }

  @override
  Widget build(BuildContext context) {
    final isShowAppBar = widget.isShowAppBar ?? true;
    final flightBean = Provider.of<FlightInfoStateProvider>(context).flightInfoEntity;
    final liveryList = flightBean.livery;
    final livery = liveryList[0];

    return Scaffold(
      appBar: isShowAppBar ? AppBar(title: Text(S.current.FlightInfoPageViewTitle, style: const TextStyle(color: Colors.white))) : null,
      body: DynamicWidget(
          positiveWidget: ListView(
            physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(left: 4, right: 4, top: 8),
                  width: MediaQuery.of(context).size.width,
                  height: 165,
                  child: DynamicWidget(positiveWidget: FlightCardFirst(), negativeWidget: Container(), condition: _queryFinished)),
              Container(
                margin: const EdgeInsets.all(4),
                child:
                    DynamicWidget(positiveWidget: const FlightCardSecondWidget(), negativeWidget: Container(), condition: _queryFinished),
              ),
              Card(
                margin: const EdgeInsets.fromLTRB(8, 4, 8, 4),
                child: Container(
                    height: 300,
                    child: DynamicWidget(positiveWidget: const FlightDataChart(), negativeWidget: Container(), condition: _queryFinished)),
              ),
              const Card(
                  margin: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: ClipRRect(borderRadius: BorderRadius.all(Radius.circular(10)), child: FlightMapCardWidget())),
              Card(
                margin: const EdgeInsets.fromLTRB(8, 4, 8, 4),
                child: Column(
                  children: [
                    Container(
                        color: HexColor.fromHex('093B6D'),
                        alignment: Alignment.center,
                        height: 35,
                        child: Text('Airline: ${livery.liveryName}', style: const TextStyle(color: Colors.greenAccent))),
                    UIViewProvider.getAirlineAvatar(context, flightBean.liveryId, flightBean.aircraftId, BoxFit.cover),
                    SafeArea(
                      child: Container(
                          color: HexColor.fromHex('093B6D'),
                          alignment: Alignment.center,
                          height: 35,
                          child: Text('Aircraft: ${livery.aircraftName}', style: const TextStyle(color: Colors.greenAccent))),
                    )
                  ],
                ),
              ),
              _isMyself
                  ? SafeArea(
                      minimum: const EdgeInsets.only(bottom: 10, top: 4),
                      child: Container(
                          height: 45,
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(6.0)),
                          margin: const EdgeInsets.only(left: 8.0, right: 8),
                          child: ElevatedButton(
                            style: TextButton.styleFrom(backgroundColor: HexColor.fromHex('093B6D')),
                            onPressed: () async {
                              final res = await FgsServiceApi.subscribeFlight(flightBean.displayName, widget.serverType ?? 0);
                              UIViewProvider.showToast(res.message);
                            },
                            child: Text('${S.current.FlightInfoPageViewFollowThisFlight}!',
                                style: const TextStyle(color: Colors.greenAccent, fontWeight: FontWeight.bold)),
                          )),
                    )
                  : Container()
            ],
          ),
          negativeWidget: Container(),
          condition: _queryFinished),
    );
  }
}
