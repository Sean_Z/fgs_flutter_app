import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/util/RegExp.dart';
import 'package:ifgs/viewcontroller/LoginViewController.dart';

class LoginPageView extends StatelessWidget {
  final _viewController = Get.put(LoginViewController());
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder(builder: (LoginViewController controller) {
      return Scaffold(
        key: const ValueKey('loginKey'),
        body: ListView(
          shrinkWrap: true,
          children: [
            Wrap(
              alignment: WrapAlignment.center,
              children: [_Avatar(), _LoginForm(context), _ActionButtonGroup(context)],
            ),
            _UserPrivatePrivacy(context),
          ],
        ),
      );
    });
  }

  SafeArea _Avatar() {
    return SafeArea(
      child: Container(
        margin: const EdgeInsets.fromLTRB(16, 32, 16, 16),
        clipBehavior: Clip.antiAlias,
        height: 150,
        constraints: const BoxConstraints(maxHeight: 150),
        decoration: const BoxDecoration(shape: BoxShape.circle, boxShadow: [
          BoxShadow(color: Colors.black87, offset: Offset(2, 2), blurRadius: 4),
        ]),
        child: _ProfileImage(),
      ),
    );
  }

  Widget _ProfileImage() {
    return CircleAvatar(
      radius: 60,
      child: Image.asset('images/fgsLogo.png'),
    );
  }

  Wrap _ActionButtonGroup(BuildContext context) {
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      direction: Axis.vertical,
      children: [
        Container(
          height: 40,
          width: 250,
          margin: const EdgeInsets.only(top: 32),
          child: ElevatedButton(
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
              backgroundColor: _viewController.isBtnEnabled ? Theme.of(context).primaryColor : Colors.grey,
            ),
            onPressed: () => _viewController.login(formKey, context),
            child: const Text('Login', style: TextStyle(fontSize: 18.0, color: Colors.white)),
          ),
        ),
        TextButton(
          onPressed: () => Get.toNamed(RouterSheet.register),
          child: const Text('Register?', style: TextStyle(color: Colors.blueGrey, fontSize: 15.0)),
        ),
        TextButton(
          onPressed: () {
            _viewController.isChangeAccount = true;
            _viewController.update();
          },
          child: const Text('Change Account', style: TextStyle(color: Colors.blueGrey, fontSize: 15.0)),
        ),
      ],
    );
  }

  /// 用户隐私协议
  Container _UserPrivatePrivacy(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 32),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text('登录即代表我已阅读并同意'),
          GestureDetector(
            onTap: () => Get.toNamed(RouterSheet.userPrivatePrivacy),
            child: Text('用户隐私协议', style: TextStyle(color: UIAdapter.setFontPrimaryColor(context))),
          )
        ],
      ),
    );
  }

  Card _LoginForm(BuildContext context) {
    final shouldDisplayUsernameInput = _viewController.cachedUsername.isEmpty || _viewController.isChangeAccount;
    final cleanIcon = IconButton(
        icon: const Icon(Icons.clear, size: 21),
        onPressed: () {
          _viewController.usernameController.text = '';
          _viewController.username = '';
        });
    final clearIcon = _viewController.usernameController.text.isNotEmpty ? cleanIcon : null;
    return Card(
        margin: const EdgeInsets.only(left: 16, right: 16),
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
        child: Container(
            padding: const EdgeInsets.all(16),
            child: Form(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              key: formKey,
              child: Column(
                children: [
                  shouldDisplayUsernameInput
                      ? TextFormField(
                          ///用户名
                          style: const TextStyle(
                            color: Colors.blueGrey,
                          ),
                          controller: _viewController.usernameController,
                          focusNode: _viewController.focusNode1,

                          ///关联focusNode1
                          keyboardType: TextInputType.text,

                          ///键盘类型
                          maxLength: 20,
                          textInputAction: TextInputAction.next,

                          cursorColor: Colors.blueGrey,
                          validator: (String? value) {
                            if (!RegExpUtil.isUsername(value)) {
                              _viewController.isBtnEnabled = false;
                              return 'invalid username';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              hintText: 'Account',
                              labelText: 'Account',
                              contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: const BorderSide(color: Colors.blue),
                              ),
                              prefixIcon: Icon(Icons.lock, color: Theme.of(context).iconTheme.color),
                              suffixIcon: clearIcon),
                        )
                      : Container(),
                  const SizedBox(height: 16.0),
                  TextFormField(
                    style: const TextStyle(
                      color: Colors.blueGrey,
                    ),
                    controller: _viewController.pwdController,
                    focusNode: _viewController.focusNode2,
                    obscureText: _viewController.isEye,
                    maxLength: 12,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        hintText: 'password',
                        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        prefixIcon: Icon(Icons.lock, color: Theme.of(context).iconTheme.color),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: const BorderSide(color: Colors.blue),
                        ),
                        suffixIcon: IconButton(
                          icon: const Icon(Icons.remove_red_eye, size: 21),
                          onPressed: () {
                            _viewController.isEye = !_viewController.isEye;
                            _viewController.update();
                          },
                        )),
                    onChanged: (v) => _viewController.update(),
                    validator: (v) {
                      if (!RegExpUtil.isPassword(v)) {
                        _viewController.isBtnEnabled = false;
                        return 'Password must consists of 6-12 digits and lowercase letters';
                      } else {
                        _viewController.isBtnEnabled = true;
                      }
                      return null;
                    },
                  ),
                ],
              ),
            )));
  }
}
