import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';
import 'package:ifgs/util/RegExp.dart';
import 'package:ifgs/widget/MarkupFlightWidget/LiverySearchWidget.dart';
import 'package:intl/intl.dart';

import '../api/FgsServiceApi.dart';
import '../widget/MarkupFlightWidget/AircraftModalSearchWidget.dart';

class MarkupFlightPageView extends StatefulWidget {
  const MarkupFlightPageView({Key? key, this.liveryName = '', this.aircraftName = ''}) : super(key: key);
  final String? liveryName;
  final String? aircraftName;

  @override
  _MarkupFlightPageViewState createState() => _MarkupFlightPageViewState();
}

class _MarkupFlightPageViewState extends State<MarkupFlightPageView> {
  final _deptController = TextEditingController();
  final _apprController = TextEditingController();
  final _deptTimeController = TextEditingController();
  final _apprTimeController = TextEditingController();
  final _liveryNameController = TextEditingController();
  final _aircraftModalController = TextEditingController();
  final _flightNumberController = TextEditingController();
  final _distanceController = TextEditingController();
  var rawDeptTime = DateTime.now();
  var rawApprTime = DateTime.now();
  final _formKey = GlobalKey<FormState>();
  Widget _icon = const Icon(Icons.backup);
  var isUploaded = 0;

  void noticeSuccess() async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Markup successfully'),
          content: const Text(
            'Flight is already logged! \nCaution markup flight will not reward any points!',
          ),
          actions: [
            ElevatedButton(
              style: TextButton.styleFrom(
                backgroundColor: Theme.of(context).primaryColor,
              ),
              onPressed: () => Get.offAndToNamed(RouterSheet.historyList),
              child: const Text('Ok', style: TextStyle(color: Colors.white)),
            )
          ],
        );
      },
    );
  }

  void _submitMarkupFlight() async {
    if (isUploaded == 3) {
      return;
    }
    if (_formKey.currentState!.validate()) {
      final flightTime = rawApprTime.compareTo(rawDeptTime);
      final markupFlightData = LogInfoClass(
        departure: _deptController.text,
        arrival: _apprController.text,
        departureTime: _deptTimeController.text,
        arrivalTime: _apprTimeController.text,
        flightNumber: _flightNumberController.text,
        route: '${_deptController.text} ${_apprController.text}',
        flightId: 'markupflight${DateTime.now().toLocal().toString()}',
        livery: _liveryNameController.text,
        aircraft: _aircraftModalController.text,
        flightTime: flightTime,
        distance: double.parse(_distanceController.text),
        boardingTime: DateFormat('MMM d, H:mm', 'en_US').format(rawDeptTime.toLocal()),
        rate: 0,
        searchFavorite: 0,
        isDelay: 0,
        airwayLine: [AirwayLineListBean(latitude: 0.0, longitude: 0.0, time: 'time', altitude: 0.0, groundSpeed: 0.0)],
      );
      setState(() {
        isUploaded = 3;
        _icon = const CircularProgressIndicator(color: Colors.white);
      });
      final result = await FgsServiceApi.logFlight(markupFlightData);
      if (result.status == 0) {
        setState(() {
          isUploaded = 1;
          _icon = const Icon(Icons.backup);
        });
        UIViewProvider.showScaffoldSuccess(ScaffoldMessageConfig(message: 'passed'));
        noticeSuccess();
      }
    } else {
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'check form again'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: AnimatedSwitcher(
        switchInCurve: Curves.easeInCirc,
        duration: const Duration(milliseconds: 300),
        transitionBuilder: (child, animation) {
          return ScaleTransition(scale: animation, child: child);
        },
        child: FloatingActionButton(
          key: ValueKey(isUploaded),
          onPressed: _submitMarkupFlight,
          child: _icon,
        ),
      ),
      appBar: AppBar(
        title: const Text('Markup Flight'),
      ),
      body: ListView(
        children: [
          Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Flex(
                  direction: Axis.horizontal,
                  children: [
                    Flexible(
                      flex: 1,
                      child: TextFormField(
                        controller: _deptController,
                        maxLength: 4,
                        textCapitalization: TextCapitalization.characters,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: (String? val) => !RegExpUtil.isICAO(val) ? 'check icao code' : null,
                        decoration: const InputDecoration(
                          counterText: '',
                          hintText: 'Departure ICAO',
                          prefixIcon: Icon(Icons.flight_takeoff),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: TextFormField(
                        controller: _apprController,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        textCapitalization: TextCapitalization.characters,
                        validator: (val) => !RegExpUtil.isICAO(val) ? 'check icao code' : null,
                        maxLength: 4,
                        decoration: const InputDecoration(
                          hintText: 'Arrival ICAO',
                          counterText: '',
                          prefixIcon: Icon(Icons.flight_land),
                        ),
                      ),
                    ),
                  ],
                ),
                Flex(
                  direction: Axis.horizontal,
                  children: [
                    Flexible(
                      flex: 1,
                      child: TextFormField(
                        controller: _flightNumberController,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: (String? val) => val!.isEmpty ? 'required' : null,
                        decoration: const InputDecoration(
                          hintText: 'Flight Number',
                          prefixIcon: Icon(Icons.pin),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        maxLength: 5,
                        controller: _distanceController,
                        validator: (String? val) => val!.isEmpty ? 'required' : null,
                        decoration: const InputDecoration(
                          counterText: '',
                          hintText: 'distance',
                          prefixIcon: Icon(Icons.add_road),
                        ),
                      ),
                    ),
                  ],
                ),
                Flex(
                  direction: Axis.horizontal,
                  children: [
                    Flexible(
                      flex: 1,
                      child: TextFormField(
                        readOnly: true,
                        controller: _deptTimeController,
                        validator: (String? val) => val!.isEmpty ? 'required' : null,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        decoration: const InputDecoration(
                          hintText: 'Departure Time',
                          prefixIcon: Icon(Icons.schedule),
                        ),
                        onTap: () async {
                          var departureDateTime = await _showDatePicker() as DateTime;
                          final departureTime = await _showTimePicker() as TimeOfDay;
                          departureDateTime = departureDateTime.add(Duration(hours: departureTime.hour));
                          departureDateTime = departureDateTime.add(Duration(minutes: departureTime.minute));
                          rawDeptTime = departureDateTime;
                          final formatDeptTime = DateFormat('MMM d, H:mm', 'en_US').format(departureDateTime);
                          _deptTimeController.value = TextEditingValue(text: formatDeptTime.toString());
                        },
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: TextFormField(
                        readOnly: true,
                        controller: _apprTimeController,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: (String? val) => val!.isEmpty ? 'required' : null,
                        decoration: const InputDecoration(
                          hintText: 'Arrival Time',
                          prefixIcon: Icon(Icons.schedule),
                        ),
                        onTap: () async {
                          var arrivalDateTime = await _showDatePicker() as DateTime;
                          final arrivalTime = await _showTimePicker() as TimeOfDay;
                          arrivalDateTime = arrivalDateTime.add(Duration(hours: arrivalTime.hour));
                          arrivalDateTime = arrivalDateTime.add(Duration(minutes: arrivalTime.minute));
                          rawApprTime = arrivalDateTime;
                          final formatDeptTime = DateFormat('MMM d, H:mm', 'en_US').format(arrivalDateTime);
                          _apprTimeController.value = TextEditingValue(text: formatDeptTime.toString());
                        },
                      ),
                    ),
                  ],
                ),
                TextFormField(
                  readOnly: true,
                  controller: _liveryNameController,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (String? val) => val!.isEmpty ? 'required' : null,
                  decoration: const InputDecoration(
                    hintText: 'Livery Name',
                    prefixIcon: Icon(Icons.airline_seat_recline_extra),
                  ),
                  onTap: () async {
                    final data = await showSearch(context: context, delegate: LiverySearchWidget());
                    _liveryNameController.value = TextEditingValue(text: data.toString());
                  },
                ),
                TextFormField(
                  readOnly: true,
                  controller: _aircraftModalController,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (String? val) => val!.isEmpty ? 'required' : null,
                  decoration: const InputDecoration(
                    hintText: 'Aircraft Modal',
                    prefixIcon: Icon(Icons.flight),
                  ),
                  onTap: () async {
                    final data = await showSearch(context: context, delegate: AircraftModalSearchModal());
                    _aircraftModalController.value = TextEditingValue(text: data.toString());
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Future _showDatePicker() {
    return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year - 1),
      lastDate: DateTime.now(),
    );
  }

  Future _showTimePicker() {
    return showTimePicker(
      context: context,
      initialTime: TimeOfDay(
        hour: DateTime.now().hour,
        minute: DateTime.now().minute,
      ),
    );
  }
}
