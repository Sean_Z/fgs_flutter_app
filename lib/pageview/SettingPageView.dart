import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/GetController.dart';
import 'package:ifgs/core/LocalAuthService.dart';
import 'package:ifgs/core/StartActivityEventHandler.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/stylesheet/Theme.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:ifgs/widget/PublicWidget/LoadingButton.dart';

class SettingPageView extends StatefulWidget {
  SettingPageView({Key? key}) : super(key: key);

  @override
  _SettingPageViewState createState() => _SettingPageViewState();
}

class _SettingPageViewState extends State<SettingPageView> {
  final startActivity = StartActivityEventHandler();
  final _themeChanger = GetThemeController();
  var _lang = 'cn';
  var _isSetFingerprint = false;
  var _fplMode = 1;
  var _isDarkMode = false;
  String _username = '';

  Future<void> initLang() async {
    var envLang = await StorageUtil.getStringItem('lang');
    final isDarkMode = await StorageUtil.getBoolItem('isDarkMode');
    setState(() {
      _lang = envLang ?? 'en';
      _isDarkMode = isDarkMode ?? false;
    });
  }

  void _initFingerprint() async {
    var currentUsername = await InfoMethod.getUserName;
    var isFingerPrintSet = await StorageUtil.getStringItem(currentUsername);
    isFingerPrintSet == 'fingerSet' ? _isSetFingerprint = true : _isSetFingerprint = false;
    setState(() {});
  }

  void _setFingerprint() async {
    _username = await InfoMethod.getUserName;
    StorageUtil.setStringItem(_username, 'fingerSet');
  }

  void _fingerSwitchAction() async {
    final flag = await LocalAuthenticationService.authenticate();
    if (_isSetFingerprint && flag) {
      setState(() {
        _isSetFingerprint = true;
      });
      _setFingerprint();
    } else {
      var username = await InfoMethod.getUserName;
      StorageUtil.remove(username);
    }
  }

  @override
  void initState() {
    initLang();
    _initFingerprint();
    super.initState();
  }

  Future<void> updateStaticData() async {
    await Future.wait([
      startActivity.initAirlineAndAircraftJson(shouldFileOverwrite: true),
      startActivity.initOwnAircraftPhotoList(shouldFileOverwrite: true),
      startActivity.initAirportStaticData(shouldFileOverwrite: true),
      startActivity.initCurrentUserInfo(),
      startActivity.initAirportCharts(),
      startActivity.initAvatar()
    ]).catchError((onError) {
      CustomViewProvider.hideMaterialDialogLoading();
      return [];
    });
    UIViewProvider.showScaffoldSuccess(ScaffoldMessageConfig(message: 'static data updated!'));
    CustomViewProvider.hideMaterialDialogLoading();
  }

  /// Render setting item list
  List<ListTile> RenderSettingList() {
    final textStyle = Theme.of(context).textTheme.bodyLarge;
    final buttonStyle = ButtonStyle(
      shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      elevation: MaterialStateProperty.all(0.8),
    );
    final dataList = [
      SettingListBean(
          title: S.current.SettingPageViewIniLang,
          leadingIcon: Icons.g_translate,
          trailingWidget: Switch(
              value: _lang == 'cn' ? false : true,
              activeColor: Colors.lightBlue,
              inactiveThumbColor: LightTheme.themeColor,
              onChanged: (bool onChange) {
                _lang = onChange == true ? 'en' : 'cn';
                _lang == 'en' ? UIAdapter.changeLocalLangToEN() : UIAdapter.changeLocalLangToCN();
                setState(() {});
              })),
      SettingListBean(
          title: S.current.SettingPageViewFingerprint,
          leadingIcon: Icons.fingerprint,
          trailingWidget: Switch(
              value: _isSetFingerprint,
              activeColor: Colors.lightBlue,
              inactiveThumbColor: LightTheme.themeColor,
              thumbIcon: MaterialStateProperty.all(const Icon(Icons.fingerprint, color: Colors.white)),
              onChanged: (bool onChange) {
                setState(() {
                  _isSetFingerprint = onChange;
                });
                _fingerSwitchAction();
              })),
      SettingListBean(
          title: S.current.SettingPageViewThemeMode,
          leadingIcon: Icons.nights_stay,
          trailingWidget: Switch(
              value: _isDarkMode,
              activeColor: Colors.lightBlue,
              inactiveThumbColor: LightTheme.themeColor,
              thumbIcon: MaterialStateProperty.resolveWith<Icon>((states) {
                final flag = states.contains('selected');
                return flag ? const Icon(Icons.sunny) : const Icon(Icons.nights_stay);
              }),
              onChanged: (bool onChange) {
                StorageUtil.setBoolItem('isDarkMode', onChange);
                setState(() {
                  _isDarkMode = onChange;
                });
                _themeChanger.setTheme(onChange);
              })),
      SettingListBean(
          title: S.current.FplMode,
          leadingIcon: Icons.book,
          trailingWidget: DropdownButton(
            dropdownColor: Theme.of(context).cardTheme.color,
            style: const TextStyle(fontWeight: FontWeight.w800).merge(Theme.of(context).textTheme.bodyLarge),
            value: _fplMode,
            items: [
              const DropdownMenuItem(value: 0, child: Text('Fpl to IF')),
              const DropdownMenuItem(value: 1, child: Text('FGS Database')),
            ],
            onChanged: (int? value) {
              setState(() {
                _fplMode = value ?? 1;
                StorageUtil.setIntItem('fplSource', value ?? 1);
              });
            },
          )),
      SettingListBean(
          title: S.current.SettingPageViewCheckUpdate,
          leadingIcon: Icons.update,
          trailingWidget: ElevatedButton(
            style: buttonStyle,
            onPressed: () => {startActivity.getAppInfo(context, true)},
            child: Text(S.current.Update, style: textStyle),
          )),
      SettingListBean(
          title: S.current.SettingPageViewUpdateData,
          leadingIcon: Icons.cloud_download,
          trailingWidget: LoadingButton(
            buttonStyle: buttonStyle,
            action: updateStaticData,
            label: Text(S.current.Update, style: textStyle),
          )),
    ];
    if (GetPlatform.isAndroid) {
      dataList.add(SettingListBean(
          title: S.current.SettingPageViewFixApp,
          leadingIcon: Icons.handyman,
          trailingWidget: ElevatedButton(
            style: buttonStyle,
            onPressed: () {
              if (Platform.isAndroid) {
                startActivity.downloadApk();
              } else {
                UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'quick fix only support for Android Platform'));
              }
            },
            child: Text(S.current.Fix, style: textStyle),
          )));
    }
    final settingListWidget = dataList
        .map((element) => ListTile(
              title: Text(
                element.title,
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              leading: Icon(element.leadingIcon),
              trailing: element.trailingWidget,
            ))
        .toList();
    return settingListWidget;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).SettingPageViewTitle),
      ),
      body: ListView(
        children: RenderSettingList(),
      ),
    );
  }
}

class SettingListBean {
  final String title;
  final Widget trailingWidget;
  final IconData leadingIcon;

  SettingListBean({required this.title, required this.trailingWidget, required this.leadingIcon});
}
