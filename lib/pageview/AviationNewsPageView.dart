import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/Color.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AviationNewsPageView extends StatefulWidget {
  const AviationNewsPageView({Key? key}) : super(key: key);

  @override
  _AviationNewsPageViewState createState() => _AviationNewsPageViewState();
}

class _AviationNewsPageViewState extends State<AviationNewsPageView> {
  final WebViewController _webViewController = WebViewController()
    ..setJavaScriptMode(JavaScriptMode.unrestricted)
    ..loadRequest(Uri.parse('https://infiniteflight.com/timeline'))
    ..setNavigationDelegate(NavigationDelegate(
      onWebResourceError: (WebResourceError error) {
        UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: error.description));
        CustomViewProvider.hideMaterialDialogLoading();
      },
      onPageFinished: (String url) {
        CustomViewProvider.hideMaterialDialogLoading();
      },
    ));
  var _canPopUp = false;

  void _willPopTip() {
    showDialog(
        context: context,
        builder: (builder) {
          return AlertDialog(
            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
            content: const Text('Wanna quit?'),
            actions: [
              TextButton(
                  onPressed: () {
                    setState(() {
                      _canPopUp = true;
                    });
                    Navigator.of(context).pop();
                    Get.back();
                    return;
                  },
                  child: Text('yes'.toUpperCase())),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    setState(() {
                      _canPopUp = false;
                    });
                    return;
                  },
                  child: Text('no'.toUpperCase())),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: _canPopUp,
      onPopInvoked: (bool didPop) async {
        if (didPop) {
          return;
        }
        if (!await _webViewController.canGoBack()) {
          _willPopTip();
        } else {
          await _webViewController.goBack();
        }
        return;
      },
      child: Scaffold(
        backgroundColor: HexColor.fromHex('#1C1C1D'),
        body: SafeArea(
          bottom: false,
          child: Container(
            height: Get.height,
            child: WebViewWidget(
              controller: _webViewController,
            ),
          ),
        ),
      ),
    );
  }
}
