import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PdfViewer extends StatefulWidget {
  PdfViewer({
    Key? key,
    this.title,
    this.pdfLink,
    this.isWidget,
  }) : super(key: key);

  final String? title;
  final String? pdfLink;
  final bool? isWidget;

  @override
  _PdfViewerState createState() => _PdfViewerState();
}

class _PdfViewerState extends State<PdfViewer> {
  final _pdfController = PdfViewerController();
  var _isShowFab = false;

  void _goSelf(String icaoCode) async {
    final _isFgsHasThisChart = await FgsServiceApi.getSpecificAirportChart(icaoCode);
    if (_isFgsHasThisChart) {
      await Get.toNamed(
        RouterSheet.pdfViewer,
        arguments: [FgsServiceApi.getAirportChartsFromCos('1.0', 'China', icaoCode), icaoCode],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final _pdfLink = widget.pdfLink ?? RouterMethod.getRouterParameter<List<String>>()[0];
    final _pdfName = widget.title ?? RouterMethod.getRouterParameter<List<String>>()[1];
    final _appBar = (widget.isWidget ?? false) ? null : AppBar(title: Text('$_pdfName Charts View'));
    final _pageFab = _isShowFab
        ? FloatingActionButton(
            onPressed: () => _pdfController.jumpToPage(1),
            child: const Icon(Icons.arrow_upward),
          )
        : FloatingActionButton(
            onPressed: () => _pdfController.jumpToPage(_pdfController.pageCount),
            child: const Icon(Icons.arrow_downward),
          );
    final _fab = (widget.isWidget ?? false)
        ? FloatingActionButton(
            onPressed: () => _goSelf(_pdfName),
            child: const Icon(Icons.open_in_full),
          )
        : _pageFab;
    return Scaffold(
      appBar: _appBar,
      floatingActionButton: _fab,
      body: Container(
        height: Get.height,
        child: SfPdfViewer.network(
          _pdfLink,
          onDocumentLoadFailed: (PdfDocumentLoadFailedDetails details) {
            UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: details.description));
          },
          onPageChanged: (PdfPageChangedDetails pdfPageChangedDetails) {
            if (pdfPageChangedDetails.newPageNumber >= 3) {
              setState(() {
                _isShowFab = true;
              });
            } else if (pdfPageChangedDetails.newPageNumber < 3) {
              setState(() {
                _isShowFab = false;
              });
            }
          },
          controller: _pdfController,
          pageSpacing: 12,
        ),
      ),
    );
  }
}
