import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/AircraftInfoEntity.dart';

AircraftInfoEntity $AircraftInfoEntityFromJson(Map<String, dynamic> json) {
  final aircraftInfoEntity = AircraftInfoEntity();
  final errorCode = jsonConvert.convert<int>(json['errorCode']);
  if (errorCode != null) {
    aircraftInfoEntity.errorCode = errorCode;
  }
  final result = (json['result'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<AircraftInfoResult>(e) as AircraftInfoResult).toList();
  if (result != null) {
    aircraftInfoEntity.result = result;
  }
  return aircraftInfoEntity;
}

Map<String, dynamic> $AircraftInfoEntityToJson(AircraftInfoEntity entity) {
  final data = <String, dynamic>{};
  data['errorCode'] = entity.errorCode;
  data['result'] = entity.result.map((v) => v.toJson()).toList();
  return data;
}

extension AircraftInfoEntityExtension on AircraftInfoEntity {
  AircraftInfoEntity copyWith({
    int? errorCode,
    List<AircraftInfoResult>? result,
  }) {
    return AircraftInfoEntity()
      ..errorCode = errorCode ?? this.errorCode
      ..result = result ?? this.result;
  }
}

AircraftInfoResult $AircraftInfoResultFromJson(Map<String, dynamic> json) {
  final aircraftInfoResult = AircraftInfoResult();
  final id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    aircraftInfoResult.id = id;
  }
  final aircraftID = jsonConvert.convert<String>(json['aircraftID']);
  if (aircraftID != null) {
    aircraftInfoResult.aircraftID = aircraftID;
  }
  final aircraftName = jsonConvert.convert<String>(json['aircraftName']);
  if (aircraftName != null) {
    aircraftInfoResult.aircraftName = aircraftName;
  }
  final liveryName = jsonConvert.convert<String>(json['liveryName']);
  if (liveryName != null) {
    aircraftInfoResult.liveryName = liveryName;
  }
  return aircraftInfoResult;
}

Map<String, dynamic> $AircraftInfoResultToJson(AircraftInfoResult entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['aircraftID'] = entity.aircraftID;
  data['aircraftName'] = entity.aircraftName;
  data['liveryName'] = entity.liveryName;
  return data;
}

extension AircraftInfoResultExtension on AircraftInfoResult {
  AircraftInfoResult copyWith({
    String? id,
    String? aircraftID,
    String? aircraftName,
    String? liveryName,
  }) {
    return AircraftInfoResult()
      ..id = id ?? this.id
      ..aircraftID = aircraftID ?? this.aircraftID
      ..aircraftName = aircraftName ?? this.aircraftName
      ..liveryName = liveryName ?? this.liveryName;
  }
}