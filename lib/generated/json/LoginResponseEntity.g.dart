import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/LoginResponseEntity.dart';

LoginResponseEntity $LoginResponseEntityFromJson(Map<String, dynamic> json) {
  final loginResponseEntity = LoginResponseEntity();
  final accessToken = jsonConvert.convert<String>(json['accessToken']);
  if (accessToken != null) {
    loginResponseEntity.accessToken = accessToken;
  }
  final currentPosition = jsonConvert.convert<String>(json['currentPosition']);
  if (currentPosition != null) {
    loginResponseEntity.currentPosition = currentPosition;
  }
  final email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    loginResponseEntity.email = email;
  }
  final infiniteFlightUserId = jsonConvert.convert<String>(json['infiniteFlightUserId']);
  if (infiniteFlightUserId != null) {
    loginResponseEntity.infiniteFlightUserId = infiniteFlightUserId;
  }
  final infiniteFlightUserName = jsonConvert.convert<String>(json['infiniteFlightUserName']);
  if (infiniteFlightUserName != null) {
    loginResponseEntity.infiniteFlightUserName = infiniteFlightUserName;
  }
  final message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    loginResponseEntity.message = message;
  }
  final position = jsonConvert.convert<String>(json['position']);
  if (position != null) {
    loginResponseEntity.position = position;
  }
  final refreshToken = jsonConvert.convert<String>(json['refreshToken']);
  if (refreshToken != null) {
    loginResponseEntity.refreshToken = refreshToken;
  }
  final status = jsonConvert.convert<int>(json['status']);
  if (status != null) {
    loginResponseEntity.status = status;
  }
  final username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    loginResponseEntity.username = username;
  }
  final authToken = jsonConvert.convert<String>(json['authToken']);
  if (authToken != null) {
    loginResponseEntity.authToken = authToken;
  }
  final nodeToken = jsonConvert.convert<String>(json['nodeToken']);
  if (nodeToken != null) {
    loginResponseEntity.nodeToken = nodeToken;
  }
  return loginResponseEntity;
}

Map<String, dynamic> $LoginResponseEntityToJson(LoginResponseEntity entity) {
  final data = <String, dynamic>{};
  data['accessToken'] = entity.accessToken;
  data['currentPosition'] = entity.currentPosition;
  data['email'] = entity.email;
  data['infiniteFlightUserId'] = entity.infiniteFlightUserId;
  data['infiniteFlightUserName'] = entity.infiniteFlightUserName;
  data['message'] = entity.message;
  data['position'] = entity.position;
  data['refreshToken'] = entity.refreshToken;
  data['status'] = entity.status;
  data['username'] = entity.username;
  data['authToken'] = entity.authToken;
  data['nodeToken'] = entity.nodeToken;
  return data;
}

extension LoginResponseEntityExtension on LoginResponseEntity {
  LoginResponseEntity copyWith({
    String? accessToken,
    String? currentPosition,
    String? email,
    String? infiniteFlightUserId,
    String? infiniteFlightUserName,
    String? message,
    String? position,
    String? refreshToken,
    int? status,
    String? username,
    String? authToken,
    String? nodeToken,
  }) {
    return LoginResponseEntity()
      ..accessToken = accessToken ?? this.accessToken
      ..currentPosition = currentPosition ?? this.currentPosition
      ..email = email ?? this.email
      ..infiniteFlightUserId = infiniteFlightUserId ?? this.infiniteFlightUserId
      ..infiniteFlightUserName = infiniteFlightUserName ?? this.infiniteFlightUserName
      ..message = message ?? this.message
      ..position = position ?? this.position
      ..refreshToken = refreshToken ?? this.refreshToken
      ..status = status ?? this.status
      ..username = username ?? this.username
      ..authToken = authToken ?? this.authToken
      ..nodeToken = nodeToken ?? this.nodeToken;
  }
}