import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/fgs_file_entity.dart';

FgsFileEntity $FgsFileEntityFromJson(dynamic json) {
  final fgsFileEntity = FgsFileEntity();
  final bizType = jsonConvert.convert<double>(json['bizType']);
  if (bizType != null) {
    fgsFileEntity.bizType = bizType;
  }
  final createTime = jsonConvert.convert<String>(json['createTime']);
  if (createTime != null) {
    fgsFileEntity.createTime = createTime;
  }
  final fileName = jsonConvert.convert<String>(json['fileName']);
  if (fileName != null) {
    fgsFileEntity.fileName = fileName;
  }
  final fileSize = jsonConvert.convert<double>(json['fileSize']);
  if (fileSize != null) {
    fgsFileEntity.fileSize = fileSize;
  }
  final fileType = jsonConvert.convert<String>(json['fileType']);
  if (fileType != null) {
    fgsFileEntity.fileType = fileType;
  }
  final id = jsonConvert.convert<double>(json['id']);
  if (id != null) {
    fgsFileEntity.id = id;
  }
  final modifyTime = jsonConvert.convert<String>(json['modifyTime']);
  if (modifyTime != null) {
    fgsFileEntity.modifyTime = modifyTime;
  }
  final uploader = jsonConvert.convert<String>(json['uploader']);
  if (uploader != null) {
    fgsFileEntity.uploader = uploader;
  }
  return fgsFileEntity;
}

Map<String, dynamic> $FgsFileEntityToJson(FgsFileEntity entity) {
  final data = <String, dynamic>{};
  data['bizType'] = entity.bizType;
  data['createTime'] = entity.createTime;
  data['fileName'] = entity.fileName;
  data['fileSize'] = entity.fileSize;
  data['fileType'] = entity.fileType;
  data['id'] = entity.id;
  data['modifyTime'] = entity.modifyTime;
  data['uploader'] = entity.uploader;
  return data;
}

extension FgsFileEntityExtension on FgsFileEntity {
  FgsFileEntity copyWith({
    double? bizType,
    String? createTime,
    String? fileName,
    double? fileSize,
    String? fileType,
    double? id,
    String? modifyTime,
    String? uploader,
  }) {
    return FgsFileEntity()
      ..bizType = bizType ?? this.bizType
      ..createTime = createTime ?? this.createTime
      ..fileName = fileName ?? this.fileName
      ..fileSize = fileSize ?? this.fileSize
      ..fileType = fileType ?? this.fileType
      ..id = id ?? this.id
      ..modifyTime = modifyTime ?? this.modifyTime
      ..uploader = uploader ?? this.uploader;
  }
}