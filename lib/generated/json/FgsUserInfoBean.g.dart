import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/FgsUserInfoBean.dart';

FgsUserInfoBeanEntity $FgsUserInfoBeanEntityFromJson(Map<String, dynamic> json) {
  final fgsUserInfoBeanEntity = FgsUserInfoBeanEntity();
  final endRow = jsonConvert.convert<int>(json['endRow']);
  if (endRow != null) {
    fgsUserInfoBeanEntity.endRow = endRow;
  }
  final hasNextPage = jsonConvert.convert<bool>(json['hasNextPage']);
  if (hasNextPage != null) {
    fgsUserInfoBeanEntity.hasNextPage = hasNextPage;
  }
  final hasPreviousPage = jsonConvert.convert<bool>(json['hasPreviousPage']);
  if (hasPreviousPage != null) {
    fgsUserInfoBeanEntity.hasPreviousPage = hasPreviousPage;
  }
  final isFirstPage = jsonConvert.convert<bool>(json['isFirstPage']);
  if (isFirstPage != null) {
    fgsUserInfoBeanEntity.isFirstPage = isFirstPage;
  }
  final isLastPage = jsonConvert.convert<bool>(json['isLastPage']);
  if (isLastPage != null) {
    fgsUserInfoBeanEntity.isLastPage = isLastPage;
  }
  final list = (json['list'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<FgsUserInfoBeanList>(e) as FgsUserInfoBeanList).toList();
  if (list != null) {
    fgsUserInfoBeanEntity.list = list;
  }
  final navigateFirstPage = jsonConvert.convert<int>(json['navigateFirstPage']);
  if (navigateFirstPage != null) {
    fgsUserInfoBeanEntity.navigateFirstPage = navigateFirstPage;
  }
  final navigateLastPage = jsonConvert.convert<int>(json['navigateLastPage']);
  if (navigateLastPage != null) {
    fgsUserInfoBeanEntity.navigateLastPage = navigateLastPage;
  }
  final navigatePages = jsonConvert.convert<int>(json['navigatePages']);
  if (navigatePages != null) {
    fgsUserInfoBeanEntity.navigatePages = navigatePages;
  }
  final navigatepageNums = (json['navigatepageNums'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<int>(e) as int).toList();
  if (navigatepageNums != null) {
    fgsUserInfoBeanEntity.navigatepageNums = navigatepageNums;
  }
  final nextPage = jsonConvert.convert<int>(json['nextPage']);
  if (nextPage != null) {
    fgsUserInfoBeanEntity.nextPage = nextPage;
  }
  final pageNum = jsonConvert.convert<int>(json['pageNum']);
  if (pageNum != null) {
    fgsUserInfoBeanEntity.pageNum = pageNum;
  }
  final pageSize = jsonConvert.convert<int>(json['pageSize']);
  if (pageSize != null) {
    fgsUserInfoBeanEntity.pageSize = pageSize;
  }
  final pages = jsonConvert.convert<int>(json['pages']);
  if (pages != null) {
    fgsUserInfoBeanEntity.pages = pages;
  }
  final prePage = jsonConvert.convert<int>(json['prePage']);
  if (prePage != null) {
    fgsUserInfoBeanEntity.prePage = prePage;
  }
  final size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    fgsUserInfoBeanEntity.size = size;
  }
  final startRow = jsonConvert.convert<int>(json['startRow']);
  if (startRow != null) {
    fgsUserInfoBeanEntity.startRow = startRow;
  }
  final total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    fgsUserInfoBeanEntity.total = total;
  }
  return fgsUserInfoBeanEntity;
}

Map<String, dynamic> $FgsUserInfoBeanEntityToJson(FgsUserInfoBeanEntity entity) {
  final data = <String, dynamic>{};
  data['endRow'] = entity.endRow;
  data['hasNextPage'] = entity.hasNextPage;
  data['hasPreviousPage'] = entity.hasPreviousPage;
  data['isFirstPage'] = entity.isFirstPage;
  data['isLastPage'] = entity.isLastPage;
  data['list'] = entity.list.map((v) => v.toJson()).toList();
  data['navigateFirstPage'] = entity.navigateFirstPage;
  data['navigateLastPage'] = entity.navigateLastPage;
  data['navigatePages'] = entity.navigatePages;
  data['navigatepageNums'] = entity.navigatepageNums;
  data['nextPage'] = entity.nextPage;
  data['pageNum'] = entity.pageNum;
  data['pageSize'] = entity.pageSize;
  data['pages'] = entity.pages;
  data['prePage'] = entity.prePage;
  data['size'] = entity.size;
  data['startRow'] = entity.startRow;
  data['total'] = entity.total;
  return data;
}

extension FgsUserInfoBeanEntityExtension on FgsUserInfoBeanEntity {
  FgsUserInfoBeanEntity copyWith({
    int? endRow,
    bool? hasNextPage,
    bool? hasPreviousPage,
    bool? isFirstPage,
    bool? isLastPage,
    List<FgsUserInfoBeanList>? list,
    int? navigateFirstPage,
    int? navigateLastPage,
    int? navigatePages,
    List<int>? navigatepageNums,
    int? nextPage,
    int? pageNum,
    int? pageSize,
    int? pages,
    int? prePage,
    int? size,
    int? startRow,
    int? total,
  }) {
    return FgsUserInfoBeanEntity()
      ..endRow = endRow ?? this.endRow
      ..hasNextPage = hasNextPage ?? this.hasNextPage
      ..hasPreviousPage = hasPreviousPage ?? this.hasPreviousPage
      ..isFirstPage = isFirstPage ?? this.isFirstPage
      ..isLastPage = isLastPage ?? this.isLastPage
      ..list = list ?? this.list
      ..navigateFirstPage = navigateFirstPage ?? this.navigateFirstPage
      ..navigateLastPage = navigateLastPage ?? this.navigateLastPage
      ..navigatePages = navigatePages ?? this.navigatePages
      ..navigatepageNums = navigatepageNums ?? this.navigatepageNums
      ..nextPage = nextPage ?? this.nextPage
      ..pageNum = pageNum ?? this.pageNum
      ..pageSize = pageSize ?? this.pageSize
      ..pages = pages ?? this.pages
      ..prePage = prePage ?? this.prePage
      ..size = size ?? this.size
      ..startRow = startRow ?? this.startRow
      ..total = total ?? this.total;
  }
}

FgsUserInfoBeanList $FgsUserInfoBeanListFromJson(Map<String, dynamic> json) {
  final fgsUserInfoBeanList = FgsUserInfoBeanList();
  final country = jsonConvert.convert<String>(json['country']);
  if (country != null) {
    fgsUserInfoBeanList.country = country;
  }
  final createTime = jsonConvert.convert<int>(json['createTime']);
  if (createTime != null) {
    fgsUserInfoBeanList.createTime = createTime;
  }
  final infiniteFlightUserName = jsonConvert.convert<String>(json['infiniteFlightUserName']);
  if (infiniteFlightUserName != null) {
    fgsUserInfoBeanList.infiniteFlightUserName = infiniteFlightUserName;
  }
  final mail = jsonConvert.convert<String>(json['mail']);
  if (mail != null) {
    fgsUserInfoBeanList.mail = mail;
  }
  final position = jsonConvert.convert<String>(json['position']);
  if (position != null) {
    fgsUserInfoBeanList.position = position;
  }
  final updateTime = jsonConvert.convert<int>(json['updateTime']);
  if (updateTime != null) {
    fgsUserInfoBeanList.updateTime = updateTime;
  }
  final userId = jsonConvert.convert<String>(json['userId']);
  if (userId != null) {
    fgsUserInfoBeanList.userId = userId;
  }
  final username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    fgsUserInfoBeanList.username = username;
  }
  return fgsUserInfoBeanList;
}

Map<String, dynamic> $FgsUserInfoBeanListToJson(FgsUserInfoBeanList entity) {
  final data = <String, dynamic>{};
  data['country'] = entity.country;
  data['createTime'] = entity.createTime;
  data['infiniteFlightUserName'] = entity.infiniteFlightUserName;
  data['mail'] = entity.mail;
  data['position'] = entity.position;
  data['updateTime'] = entity.updateTime;
  data['userId'] = entity.userId;
  data['username'] = entity.username;
  return data;
}

extension FgsUserInfoBeanListExtension on FgsUserInfoBeanList {
  FgsUserInfoBeanList copyWith({
    String? country,
    int? createTime,
    String? infiniteFlightUserName,
    String? mail,
    String? position,
    int? updateTime,
    String? userId,
    String? username,
  }) {
    return FgsUserInfoBeanList()
      ..country = country ?? this.country
      ..createTime = createTime ?? this.createTime
      ..infiniteFlightUserName = infiniteFlightUserName ?? this.infiniteFlightUserName
      ..mail = mail ?? this.mail
      ..position = position ?? this.position
      ..updateTime = updateTime ?? this.updateTime
      ..userId = userId ?? this.userId
      ..username = username ?? this.username;
  }
}