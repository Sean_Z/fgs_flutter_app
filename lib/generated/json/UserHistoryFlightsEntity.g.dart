import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/UserHistoryFlightsEntity.dart';

UserHistoryFlightsEntity $UserHistoryFlightsEntityFromJson(Map<String, dynamic> json) {
  final userHistoryFlightsEntity = UserHistoryFlightsEntity();
  final errorCode = jsonConvert.convert<int>(json['errorCode']);
  if (errorCode != null) {
    userHistoryFlightsEntity.errorCode = errorCode;
  }
  final result = jsonConvert.convert<UserHistoryFlightsResult>(json['result']);
  if (result != null) {
    userHistoryFlightsEntity.result = result;
  }
  return userHistoryFlightsEntity;
}

Map<String, dynamic> $UserHistoryFlightsEntityToJson(UserHistoryFlightsEntity entity) {
  final data = <String, dynamic>{};
  data['errorCode'] = entity.errorCode;
  data['result'] = entity.result.toJson();
  return data;
}

extension UserHistoryFlightsEntityExtension on UserHistoryFlightsEntity {
  UserHistoryFlightsEntity copyWith({
    int? errorCode,
    UserHistoryFlightsResult? result,
  }) {
    return UserHistoryFlightsEntity()
      ..errorCode = errorCode ?? this.errorCode
      ..result = result ?? this.result;
  }
}

UserHistoryFlightsResult $UserHistoryFlightsResultFromJson(Map<String, dynamic> json) {
  final userHistoryFlightsResult = UserHistoryFlightsResult();
  final pageIndex = jsonConvert.convert<int>(json['pageIndex']);
  if (pageIndex != null) {
    userHistoryFlightsResult.pageIndex = pageIndex;
  }
  final totalPages = jsonConvert.convert<int>(json['totalPages']);
  if (totalPages != null) {
    userHistoryFlightsResult.totalPages = totalPages;
  }
  final totalCount = jsonConvert.convert<int>(json['totalCount']);
  if (totalCount != null) {
    userHistoryFlightsResult.totalCount = totalCount;
  }
  final hasPreviousPage = jsonConvert.convert<bool>(json['hasPreviousPage']);
  if (hasPreviousPage != null) {
    userHistoryFlightsResult.hasPreviousPage = hasPreviousPage;
  }
  final hasNextPage = jsonConvert.convert<bool>(json['hasNextPage']);
  if (hasNextPage != null) {
    userHistoryFlightsResult.hasNextPage = hasNextPage;
  }
  final data = (json['data'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<UserHistoryFlightsResultData>(e) as UserHistoryFlightsResultData).toList();
  if (data != null) {
    userHistoryFlightsResult.data = data;
  }
  return userHistoryFlightsResult;
}

Map<String, dynamic> $UserHistoryFlightsResultToJson(UserHistoryFlightsResult entity) {
  final data = <String, dynamic>{};
  data['pageIndex'] = entity.pageIndex;
  data['totalPages'] = entity.totalPages;
  data['totalCount'] = entity.totalCount;
  data['hasPreviousPage'] = entity.hasPreviousPage;
  data['hasNextPage'] = entity.hasNextPage;
  data['data'] = entity.data.map((v) => v.toJson()).toList();
  return data;
}

extension UserHistoryFlightsResultExtension on UserHistoryFlightsResult {
  UserHistoryFlightsResult copyWith({
    int? pageIndex,
    int? totalPages,
    int? totalCount,
    bool? hasPreviousPage,
    bool? hasNextPage,
    List<UserHistoryFlightsResultData>? data,
  }) {
    return UserHistoryFlightsResult()
      ..pageIndex = pageIndex ?? this.pageIndex
      ..totalPages = totalPages ?? this.totalPages
      ..totalCount = totalCount ?? this.totalCount
      ..hasPreviousPage = hasPreviousPage ?? this.hasPreviousPage
      ..hasNextPage = hasNextPage ?? this.hasNextPage
      ..data = data ?? this.data;
  }
}

UserHistoryFlightsResultData $UserHistoryFlightsResultDataFromJson(Map<String, dynamic> json) {
  final userHistoryFlightsResultData = UserHistoryFlightsResultData();
  final id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    userHistoryFlightsResultData.id = id;
  }
  final created = jsonConvert.convert<String>(json['created']);
  if (created != null) {
    userHistoryFlightsResultData.created = created;
  }
  final userId = jsonConvert.convert<String>(json['userId']);
  if (userId != null) {
    userHistoryFlightsResultData.userId = userId;
  }
  final aircraftId = jsonConvert.convert<String>(json['aircraftId']);
  if (aircraftId != null) {
    userHistoryFlightsResultData.aircraftId = aircraftId;
  }
  final liveryId = jsonConvert.convert<String>(json['liveryId']);
  if (liveryId != null) {
    userHistoryFlightsResultData.liveryId = liveryId;
  }
  final callsign = jsonConvert.convert<String>(json['callsign']);
  if (callsign != null) {
    userHistoryFlightsResultData.callsign = callsign;
  }
  final server = jsonConvert.convert<String>(json['server']);
  if (server != null) {
    userHistoryFlightsResultData.server = server;
  }
  final dayTime = jsonConvert.convert<double>(json['dayTime']);
  if (dayTime != null) {
    userHistoryFlightsResultData.dayTime = dayTime;
  }
  final nightTime = jsonConvert.convert<double>(json['nightTime']);
  if (nightTime != null) {
    userHistoryFlightsResultData.nightTime = nightTime;
  }
  final totalTime = jsonConvert.convert<double>(json['totalTime']);
  if (totalTime != null) {
    userHistoryFlightsResultData.totalTime = totalTime;
  }
  final landingCount = jsonConvert.convert<int>(json['landingCount']);
  if (landingCount != null) {
    userHistoryFlightsResultData.landingCount = landingCount;
  }
  final originAirport = jsonConvert.convert<String>(json['originAirport']);
  if (originAirport != null) {
    userHistoryFlightsResultData.originAirport = originAirport;
  }
  final destinationAirport = jsonConvert.convert<String>(json['destinationAirport']);
  if (destinationAirport != null) {
    userHistoryFlightsResultData.destinationAirport = destinationAirport;
  }
  final xp = jsonConvert.convert<int>(json['xp']);
  if (xp != null) {
    userHistoryFlightsResultData.xp = xp;
  }
  final worldType = jsonConvert.convert<int>(json['worldType']);
  if (worldType != null) {
    userHistoryFlightsResultData.worldType = worldType;
  }
  final violations = (json['violations'] as List<dynamic>?)?.map(
          (e) => e).toList();
  if (violations != null) {
    userHistoryFlightsResultData.violations = violations;
  }
  return userHistoryFlightsResultData;
}

Map<String, dynamic> $UserHistoryFlightsResultDataToJson(UserHistoryFlightsResultData entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['created'] = entity.created;
  data['userId'] = entity.userId;
  data['aircraftId'] = entity.aircraftId;
  data['liveryId'] = entity.liveryId;
  data['callsign'] = entity.callsign;
  data['server'] = entity.server;
  data['dayTime'] = entity.dayTime;
  data['nightTime'] = entity.nightTime;
  data['totalTime'] = entity.totalTime;
  data['landingCount'] = entity.landingCount;
  data['originAirport'] = entity.originAirport;
  data['destinationAirport'] = entity.destinationAirport;
  data['xp'] = entity.xp;
  data['worldType'] = entity.worldType;
  data['violations'] = entity.violations;
  return data;
}

extension UserHistoryFlightsResultDataExtension on UserHistoryFlightsResultData {
  UserHistoryFlightsResultData copyWith({
    String? id,
    String? created,
    String? userId,
    String? aircraftId,
    String? liveryId,
    String? callsign,
    String? server,
    double? dayTime,
    double? nightTime,
    double? totalTime,
    int? landingCount,
    String? originAirport,
    String? destinationAirport,
    int? xp,
    int? worldType,
    List<dynamic>? violations,
  }) {
    return UserHistoryFlightsResultData()
      ..id = id ?? this.id
      ..created = created ?? this.created
      ..userId = userId ?? this.userId
      ..aircraftId = aircraftId ?? this.aircraftId
      ..liveryId = liveryId ?? this.liveryId
      ..callsign = callsign ?? this.callsign
      ..server = server ?? this.server
      ..dayTime = dayTime ?? this.dayTime
      ..nightTime = nightTime ?? this.nightTime
      ..totalTime = totalTime ?? this.totalTime
      ..landingCount = landingCount ?? this.landingCount
      ..originAirport = originAirport ?? this.originAirport
      ..destinationAirport = destinationAirport ?? this.destinationAirport
      ..xp = xp ?? this.xp
      ..worldType = worldType ?? this.worldType
      ..violations = violations ?? this.violations;
  }
}