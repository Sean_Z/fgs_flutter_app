import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/FlightPlanDetailEntity.dart';

FlightPlanDetailEntity $FlightPlanDetailEntityFromJson(Map<String, dynamic> json) {
  final flightPlanDetailEntity = FlightPlanDetailEntity();
  final id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    flightPlanDetailEntity.id = id;
  }
  final fromICAO = jsonConvert.convert<String>(json['fromICAO']);
  if (fromICAO != null) {
    flightPlanDetailEntity.fromICAO = fromICAO;
  }
  final toICAO = jsonConvert.convert<String>(json['toICAO']);
  if (toICAO != null) {
    flightPlanDetailEntity.toICAO = toICAO;
  }
  final fromName = jsonConvert.convert<String>(json['fromName']);
  if (fromName != null) {
    flightPlanDetailEntity.fromName = fromName;
  }
  final toName = jsonConvert.convert<String>(json['toName']);
  if (toName != null) {
    flightPlanDetailEntity.toName = toName;
  }
  final dynamic flightNumber = json['flightNumber'];
  if (flightNumber != null) {
    flightPlanDetailEntity.flightNumber = flightNumber;
  }
  final distance = jsonConvert.convert<double>(json['distance']);
  if (distance != null) {
    flightPlanDetailEntity.distance = distance;
  }
  final maxAltitude = jsonConvert.convert<int>(json['maxAltitude']);
  if (maxAltitude != null) {
    flightPlanDetailEntity.maxAltitude = maxAltitude;
  }
  final waypoints = jsonConvert.convert<int>(json['waypoints']);
  if (waypoints != null) {
    flightPlanDetailEntity.waypoints = waypoints;
  }
  final likes = jsonConvert.convert<int>(json['likes']);
  if (likes != null) {
    flightPlanDetailEntity.likes = likes;
  }
  final downloads = jsonConvert.convert<int>(json['downloads']);
  if (downloads != null) {
    flightPlanDetailEntity.downloads = downloads;
  }
  final popularity = jsonConvert.convert<int>(json['popularity']);
  if (popularity != null) {
    flightPlanDetailEntity.popularity = popularity;
  }
  final notes = jsonConvert.convert<String>(json['notes']);
  if (notes != null) {
    flightPlanDetailEntity.notes = notes;
  }
  final encodedPolyline = jsonConvert.convert<String>(json['encodedPolyline']);
  if (encodedPolyline != null) {
    flightPlanDetailEntity.encodedPolyline = encodedPolyline;
  }
  final createdAt = jsonConvert.convert<String>(json['createdAt']);
  if (createdAt != null) {
    flightPlanDetailEntity.createdAt = createdAt;
  }
  final updatedAt = jsonConvert.convert<String>(json['updatedAt']);
  if (updatedAt != null) {
    flightPlanDetailEntity.updatedAt = updatedAt;
  }
  final tags = (json['tags'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<String>(e) as String).toList();
  if (tags != null) {
    flightPlanDetailEntity.tags = tags;
  }
  final user = jsonConvert.convert<FlightPlanDetailUser>(json['user']);
  if (user != null) {
    flightPlanDetailEntity.user = user;
  }
  final route = jsonConvert.convert<FlightPlanDetailRoute>(json['route']);
  if (route != null) {
    flightPlanDetailEntity.route = route;
  }
  return flightPlanDetailEntity;
}

Map<String, dynamic> $FlightPlanDetailEntityToJson(FlightPlanDetailEntity entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['fromICAO'] = entity.fromICAO;
  data['toICAO'] = entity.toICAO;
  data['fromName'] = entity.fromName;
  data['toName'] = entity.toName;
  data['flightNumber'] = entity.flightNumber;
  data['distance'] = entity.distance;
  data['maxAltitude'] = entity.maxAltitude;
  data['waypoints'] = entity.waypoints;
  data['likes'] = entity.likes;
  data['downloads'] = entity.downloads;
  data['popularity'] = entity.popularity;
  data['notes'] = entity.notes;
  data['encodedPolyline'] = entity.encodedPolyline;
  data['createdAt'] = entity.createdAt;
  data['updatedAt'] = entity.updatedAt;
  data['tags'] = entity.tags;
  data['user'] = entity.user.toJson();
  data['route'] = entity.route.toJson();
  return data;
}

extension FlightPlanDetailEntityExtension on FlightPlanDetailEntity {
  FlightPlanDetailEntity copyWith({
    int? id,
    String? fromICAO,
    String? toICAO,
    String? fromName,
    String? toName,
    dynamic flightNumber,
    double? distance,
    int? maxAltitude,
    int? waypoints,
    int? likes,
    int? downloads,
    int? popularity,
    String? notes,
    String? encodedPolyline,
    String? createdAt,
    String? updatedAt,
    List<String>? tags,
    FlightPlanDetailUser? user,
    FlightPlanDetailRoute? route,
  }) {
    return FlightPlanDetailEntity()
      ..id = id ?? this.id
      ..fromICAO = fromICAO ?? this.fromICAO
      ..toICAO = toICAO ?? this.toICAO
      ..fromName = fromName ?? this.fromName
      ..toName = toName ?? this.toName
      ..flightNumber = flightNumber ?? this.flightNumber
      ..distance = distance ?? this.distance
      ..maxAltitude = maxAltitude ?? this.maxAltitude
      ..waypoints = waypoints ?? this.waypoints
      ..likes = likes ?? this.likes
      ..downloads = downloads ?? this.downloads
      ..popularity = popularity ?? this.popularity
      ..notes = notes ?? this.notes
      ..encodedPolyline = encodedPolyline ?? this.encodedPolyline
      ..createdAt = createdAt ?? this.createdAt
      ..updatedAt = updatedAt ?? this.updatedAt
      ..tags = tags ?? this.tags
      ..user = user ?? this.user
      ..route = route ?? this.route;
  }
}

FlightPlanDetailUser $FlightPlanDetailUserFromJson(Map<String, dynamic> json) {
  final flightPlanDetailUser = FlightPlanDetailUser();
  final id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    flightPlanDetailUser.id = id;
  }
  final username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    flightPlanDetailUser.username = username;
  }
  final gravatarHash = jsonConvert.convert<String>(json['gravatarHash']);
  if (gravatarHash != null) {
    flightPlanDetailUser.gravatarHash = gravatarHash;
  }
  final dynamic location = json['location'];
  if (location != null) {
    flightPlanDetailUser.location = location;
  }
  return flightPlanDetailUser;
}

Map<String, dynamic> $FlightPlanDetailUserToJson(FlightPlanDetailUser entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['gravatarHash'] = entity.gravatarHash;
  data['location'] = entity.location;
  return data;
}

extension FlightPlanDetailUserExtension on FlightPlanDetailUser {
  FlightPlanDetailUser copyWith({
    int? id,
    String? username,
    String? gravatarHash,
    dynamic location,
  }) {
    return FlightPlanDetailUser()
      ..id = id ?? this.id
      ..username = username ?? this.username
      ..gravatarHash = gravatarHash ?? this.gravatarHash
      ..location = location ?? this.location;
  }
}

FlightPlanDetailRoute $FlightPlanDetailRouteFromJson(Map<String, dynamic> json) {
  final flightPlanDetailRoute = FlightPlanDetailRoute();
  final nodes = (json['nodes'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<FlightPlanDetailRouteNodes>(e) as FlightPlanDetailRouteNodes).toList();
  if (nodes != null) {
    flightPlanDetailRoute.nodes = nodes;
  }
  return flightPlanDetailRoute;
}

Map<String, dynamic> $FlightPlanDetailRouteToJson(FlightPlanDetailRoute entity) {
  final data = <String, dynamic>{};
  data['nodes'] = entity.nodes.map((v) => v.toJson()).toList();
  return data;
}

extension FlightPlanDetailRouteExtension on FlightPlanDetailRoute {
  FlightPlanDetailRoute copyWith({
    List<FlightPlanDetailRouteNodes>? nodes,
  }) {
    return FlightPlanDetailRoute()
      ..nodes = nodes ?? this.nodes;
  }
}

FlightPlanDetailRouteNodes $FlightPlanDetailRouteNodesFromJson(Map<String, dynamic> json) {
  final flightPlanDetailRouteNodes = FlightPlanDetailRouteNodes();
  final type = jsonConvert.convert<String>(json['type']);
  if (type != null) {
    flightPlanDetailRouteNodes.type = type;
  }
  final ident = jsonConvert.convert<String>(json['ident']);
  if (ident != null) {
    flightPlanDetailRouteNodes.ident = ident;
  }
  final name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    flightPlanDetailRouteNodes.name = name;
  }
  final lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    flightPlanDetailRouteNodes.lat = lat;
  }
  final lon = jsonConvert.convert<double>(json['lon']);
  if (lon != null) {
    flightPlanDetailRouteNodes.lon = lon;
  }
  final alt = jsonConvert.convert<int>(json['alt']);
  if (alt != null) {
    flightPlanDetailRouteNodes.alt = alt;
  }
  final dynamic via = json['via'];
  if (via != null) {
    flightPlanDetailRouteNodes.via = via;
  }
  return flightPlanDetailRouteNodes;
}

Map<String, dynamic> $FlightPlanDetailRouteNodesToJson(FlightPlanDetailRouteNodes entity) {
  final data = <String, dynamic>{};
  data['type'] = entity.type;
  data['ident'] = entity.ident;
  data['name'] = entity.name;
  data['lat'] = entity.lat;
  data['lon'] = entity.lon;
  data['alt'] = entity.alt;
  data['via'] = entity.via;
  return data;
}

extension FlightPlanDetailRouteNodesExtension on FlightPlanDetailRouteNodes {
  FlightPlanDetailRouteNodes copyWith({
    String? type,
    String? ident,
    String? name,
    double? lat,
    double? lon,
    int? alt,
    dynamic via,
  }) {
    return FlightPlanDetailRouteNodes()
      ..type = type ?? this.type
      ..ident = ident ?? this.ident
      ..name = name ?? this.name
      ..lat = lat ?? this.lat
      ..lon = lon ?? this.lon
      ..alt = alt ?? this.alt
      ..via = via ?? this.via;
  }
}