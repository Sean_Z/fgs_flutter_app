import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/FlightPlanIdEntity.dart';

FlightPlanIdEntity $FlightPlanIdEntityFromJson(Map<String, dynamic> json) {
  final flightPlanIdEntity = FlightPlanIdEntity();
  final id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    flightPlanIdEntity.id = id;
  }
  final fromICAO = jsonConvert.convert<String>(json['fromICAO']);
  if (fromICAO != null) {
    flightPlanIdEntity.fromICAO = fromICAO;
  }
  final toICAO = jsonConvert.convert<String>(json['toICAO']);
  if (toICAO != null) {
    flightPlanIdEntity.toICAO = toICAO;
  }
  final fromName = jsonConvert.convert<String>(json['fromName']);
  if (fromName != null) {
    flightPlanIdEntity.fromName = fromName;
  }
  final toName = jsonConvert.convert<String>(json['toName']);
  if (toName != null) {
    flightPlanIdEntity.toName = toName;
  }
  final dynamic flightNumber = json['flightNumber'];
  if (flightNumber != null) {
    flightPlanIdEntity.flightNumber = flightNumber;
  }
  final distance = jsonConvert.convert<double>(json['distance']);
  if (distance != null) {
    flightPlanIdEntity.distance = distance;
  }
  final maxAltitude = jsonConvert.convert<int>(json['maxAltitude']);
  if (maxAltitude != null) {
    flightPlanIdEntity.maxAltitude = maxAltitude;
  }
  final waypoints = jsonConvert.convert<int>(json['waypoints']);
  if (waypoints != null) {
    flightPlanIdEntity.waypoints = waypoints;
  }
  final popularity = jsonConvert.convert<int>(json['popularity']);
  if (popularity != null) {
    flightPlanIdEntity.popularity = popularity;
  }
  final notes = jsonConvert.convert<String>(json['notes']);
  if (notes != null) {
    flightPlanIdEntity.notes = notes;
  }
  final encodedPolyline = jsonConvert.convert<String>(json['encodedPolyline']);
  if (encodedPolyline != null) {
    flightPlanIdEntity.encodedPolyline = encodedPolyline;
  }
  final createdAt = jsonConvert.convert<String>(json['createdAt']);
  if (createdAt != null) {
    flightPlanIdEntity.createdAt = createdAt;
  }
  final updatedAt = jsonConvert.convert<String>(json['updatedAt']);
  if (updatedAt != null) {
    flightPlanIdEntity.updatedAt = updatedAt;
  }
  final tags = (json['tags'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<String>(e) as String).toList();
  if (tags != null) {
    flightPlanIdEntity.tags = tags;
  }
  final user = jsonConvert.convert<FlightPlanIdUser>(json['user']);
  if (user != null) {
    flightPlanIdEntity.user = user;
  }
  return flightPlanIdEntity;
}

Map<String, dynamic> $FlightPlanIdEntityToJson(FlightPlanIdEntity entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['fromICAO'] = entity.fromICAO;
  data['toICAO'] = entity.toICAO;
  data['fromName'] = entity.fromName;
  data['toName'] = entity.toName;
  data['flightNumber'] = entity.flightNumber;
  data['distance'] = entity.distance;
  data['maxAltitude'] = entity.maxAltitude;
  data['waypoints'] = entity.waypoints;
  data['popularity'] = entity.popularity;
  data['notes'] = entity.notes;
  data['encodedPolyline'] = entity.encodedPolyline;
  data['createdAt'] = entity.createdAt;
  data['updatedAt'] = entity.updatedAt;
  data['tags'] = entity.tags;
  data['user'] = entity.user.toJson();
  return data;
}

extension FlightPlanIdEntityExtension on FlightPlanIdEntity {
  FlightPlanIdEntity copyWith({
    int? id,
    String? fromICAO,
    String? toICAO,
    String? fromName,
    String? toName,
    dynamic flightNumber,
    double? distance,
    int? maxAltitude,
    int? waypoints,
    int? popularity,
    String? notes,
    String? encodedPolyline,
    String? createdAt,
    String? updatedAt,
    List<String>? tags,
    FlightPlanIdUser? user,
  }) {
    return FlightPlanIdEntity()
      ..id = id ?? this.id
      ..fromICAO = fromICAO ?? this.fromICAO
      ..toICAO = toICAO ?? this.toICAO
      ..fromName = fromName ?? this.fromName
      ..toName = toName ?? this.toName
      ..flightNumber = flightNumber ?? this.flightNumber
      ..distance = distance ?? this.distance
      ..maxAltitude = maxAltitude ?? this.maxAltitude
      ..waypoints = waypoints ?? this.waypoints
      ..popularity = popularity ?? this.popularity
      ..notes = notes ?? this.notes
      ..encodedPolyline = encodedPolyline ?? this.encodedPolyline
      ..createdAt = createdAt ?? this.createdAt
      ..updatedAt = updatedAt ?? this.updatedAt
      ..tags = tags ?? this.tags
      ..user = user ?? this.user;
  }
}

FlightPlanIdUser $FlightPlanIdUserFromJson(Map<String, dynamic> json) {
  final flightPlanIdUser = FlightPlanIdUser();
  final id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    flightPlanIdUser.id = id;
  }
  final username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    flightPlanIdUser.username = username;
  }
  final gravatarHash = jsonConvert.convert<String>(json['gravatarHash']);
  if (gravatarHash != null) {
    flightPlanIdUser.gravatarHash = gravatarHash;
  }
  final dynamic location = json['location'];
  if (location != null) {
    flightPlanIdUser.location = location;
  }
  return flightPlanIdUser;
}

Map<String, dynamic> $FlightPlanIdUserToJson(FlightPlanIdUser entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['gravatarHash'] = entity.gravatarHash;
  data['location'] = entity.location;
  return data;
}

extension FlightPlanIdUserExtension on FlightPlanIdUser {
  FlightPlanIdUser copyWith({
    int? id,
    String? username,
    String? gravatarHash,
    dynamic location,
  }) {
    return FlightPlanIdUser()
      ..id = id ?? this.id
      ..username = username ?? this.username
      ..gravatarHash = gravatarHash ?? this.gravatarHash
      ..location = location ?? this.location;
  }
}