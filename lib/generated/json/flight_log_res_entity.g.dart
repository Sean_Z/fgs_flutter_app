import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/flight_log_res_entity.dart';

FlightLogResEntity $FlightLogResEntityFromJson(Map<String, dynamic> json) {
  final flightLogResEntity = FlightLogResEntity();
  final endRow = jsonConvert.convert<int>(json['endRow']);
  if (endRow != null) {
    flightLogResEntity.endRow = endRow;
  }
  final hasNextPage = jsonConvert.convert<bool>(json['hasNextPage']);
  if (hasNextPage != null) {
    flightLogResEntity.hasNextPage = hasNextPage;
  }
  final hasPreviousPage = jsonConvert.convert<bool>(json['hasPreviousPage']);
  if (hasPreviousPage != null) {
    flightLogResEntity.hasPreviousPage = hasPreviousPage;
  }
  final isFirstPage = jsonConvert.convert<bool>(json['isFirstPage']);
  if (isFirstPage != null) {
    flightLogResEntity.isFirstPage = isFirstPage;
  }
  final isLastPage = jsonConvert.convert<bool>(json['isLastPage']);
  if (isLastPage != null) {
    flightLogResEntity.isLastPage = isLastPage;
  }
  final list = (json['list'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<FlightLogResList>(e) as FlightLogResList).toList();
  if (list != null) {
    flightLogResEntity.list = list;
  }
  final navigateFirstPage = jsonConvert.convert<int>(json['navigateFirstPage']);
  if (navigateFirstPage != null) {
    flightLogResEntity.navigateFirstPage = navigateFirstPage;
  }
  final navigateLastPage = jsonConvert.convert<int>(json['navigateLastPage']);
  if (navigateLastPage != null) {
    flightLogResEntity.navigateLastPage = navigateLastPage;
  }
  final navigatePages = jsonConvert.convert<int>(json['navigatePages']);
  if (navigatePages != null) {
    flightLogResEntity.navigatePages = navigatePages;
  }
  final navigatepageNums = (json['navigatepageNums'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<int>(e) as int).toList();
  if (navigatepageNums != null) {
    flightLogResEntity.navigatepageNums = navigatepageNums;
  }
  final nextPage = jsonConvert.convert<int>(json['nextPage']);
  if (nextPage != null) {
    flightLogResEntity.nextPage = nextPage;
  }
  final pageNum = jsonConvert.convert<int>(json['pageNum']);
  if (pageNum != null) {
    flightLogResEntity.pageNum = pageNum;
  }
  final pageSize = jsonConvert.convert<int>(json['pageSize']);
  if (pageSize != null) {
    flightLogResEntity.pageSize = pageSize;
  }
  final pages = jsonConvert.convert<int>(json['pages']);
  if (pages != null) {
    flightLogResEntity.pages = pages;
  }
  final prePage = jsonConvert.convert<int>(json['prePage']);
  if (prePage != null) {
    flightLogResEntity.prePage = prePage;
  }
  final size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    flightLogResEntity.size = size;
  }
  final startRow = jsonConvert.convert<int>(json['startRow']);
  if (startRow != null) {
    flightLogResEntity.startRow = startRow;
  }
  final total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    flightLogResEntity.total = total;
  }
  return flightLogResEntity;
}

Map<String, dynamic> $FlightLogResEntityToJson(FlightLogResEntity entity) {
  final data = <String, dynamic>{};
  data['endRow'] = entity.endRow;
  data['hasNextPage'] = entity.hasNextPage;
  data['hasPreviousPage'] = entity.hasPreviousPage;
  data['isFirstPage'] = entity.isFirstPage;
  data['isLastPage'] = entity.isLastPage;
  data['list'] = entity.list.map((v) => v.toJson()).toList();
  data['navigateFirstPage'] = entity.navigateFirstPage;
  data['navigateLastPage'] = entity.navigateLastPage;
  data['navigatePages'] = entity.navigatePages;
  data['navigatepageNums'] = entity.navigatepageNums;
  data['nextPage'] = entity.nextPage;
  data['pageNum'] = entity.pageNum;
  data['pageSize'] = entity.pageSize;
  data['pages'] = entity.pages;
  data['prePage'] = entity.prePage;
  data['size'] = entity.size;
  data['startRow'] = entity.startRow;
  data['total'] = entity.total;
  return data;
}

extension FlightLogResEntityExtension on FlightLogResEntity {
  FlightLogResEntity copyWith({
    int? endRow,
    bool? hasNextPage,
    bool? hasPreviousPage,
    bool? isFirstPage,
    bool? isLastPage,
    List<FlightLogResList>? list,
    int? navigateFirstPage,
    int? navigateLastPage,
    int? navigatePages,
    List<int>? navigatepageNums,
    int? nextPage,
    int? pageNum,
    int? pageSize,
    int? pages,
    int? prePage,
    int? size,
    int? startRow,
    int? total,
  }) {
    return FlightLogResEntity()
      ..endRow = endRow ?? this.endRow
      ..hasNextPage = hasNextPage ?? this.hasNextPage
      ..hasPreviousPage = hasPreviousPage ?? this.hasPreviousPage
      ..isFirstPage = isFirstPage ?? this.isFirstPage
      ..isLastPage = isLastPage ?? this.isLastPage
      ..list = list ?? this.list
      ..navigateFirstPage = navigateFirstPage ?? this.navigateFirstPage
      ..navigateLastPage = navigateLastPage ?? this.navigateLastPage
      ..navigatePages = navigatePages ?? this.navigatePages
      ..navigatepageNums = navigatepageNums ?? this.navigatepageNums
      ..nextPage = nextPage ?? this.nextPage
      ..pageNum = pageNum ?? this.pageNum
      ..pageSize = pageSize ?? this.pageSize
      ..pages = pages ?? this.pages
      ..prePage = prePage ?? this.prePage
      ..size = size ?? this.size
      ..startRow = startRow ?? this.startRow
      ..total = total ?? this.total;
  }
}

FlightLogResList $FlightLogResListFromJson(Map<String, dynamic> json) {
  final flightLogResList = FlightLogResList();
  final aircraft = jsonConvert.convert<String>(json['aircraft']);
  if (aircraft != null) {
    flightLogResList.aircraft = aircraft;
  }
  final airwayPoint = jsonConvert.convert<String>(json['airwayPoint']);
  if (airwayPoint != null) {
    flightLogResList.airwayPoint = airwayPoint;
  }
  final arrival = jsonConvert.convert<String>(json['arrival']);
  if (arrival != null) {
    flightLogResList.arrival = arrival;
  }
  final arrivalAirportName = jsonConvert.convert<String>(json['arrivalAirportName']);
  if (arrivalAirportName != null) {
    flightLogResList.arrivalAirportName = arrivalAirportName;
  }
  final arrivalTime = jsonConvert.convert<String>(json['arrivalTime']);
  if (arrivalTime != null) {
    flightLogResList.arrivalTime = arrivalTime;
  }
  final boardingTime = jsonConvert.convert<String>(json['boardingTime']);
  if (boardingTime != null) {
    flightLogResList.boardingTime = boardingTime;
  }
  final createDate = jsonConvert.convert<int>(json['createDate']);
  if (createDate != null) {
    flightLogResList.createDate = createDate;
  }
  final delay = jsonConvert.convert<int>(json['delay']);
  if (delay != null) {
    flightLogResList.delay = delay;
  }
  final departure = jsonConvert.convert<String>(json['departure']);
  if (departure != null) {
    flightLogResList.departure = departure;
  }
  final departureAirportName = jsonConvert.convert<String>(json['departureAirportName']);
  if (departureAirportName != null) {
    flightLogResList.departureAirportName = departureAirportName;
  }
  final departureTime = jsonConvert.convert<String>(json['departureTime']);
  if (departureTime != null) {
    flightLogResList.departureTime = departureTime;
  }
  final distance = jsonConvert.convert<int>(json['distance']);
  if (distance != null) {
    flightLogResList.distance = distance;
  }
  final favorite = jsonConvert.convert<int>(json['favorite']);
  if (favorite != null) {
    flightLogResList.favorite = favorite;
  }
  final flightId = jsonConvert.convert<String>(json['flightId']);
  if (flightId != null) {
    flightLogResList.flightId = flightId;
  }
  final flightNumber = jsonConvert.convert<String>(json['flightNumber']);
  if (flightNumber != null) {
    flightLogResList.flightNumber = flightNumber;
  }
  final flightTime = jsonConvert.convert<String>(json['flightTime']);
  if (flightTime != null) {
    flightLogResList.flightTime = flightTime;
  }
  final livery = jsonConvert.convert<String>(json['livery']);
  if (livery != null) {
    flightLogResList.livery = livery;
  }
  final logId = jsonConvert.convert<int>(json['logId']);
  if (logId != null) {
    flightLogResList.logId = logId;
  }
  final modifyDate = jsonConvert.convert<int>(json['modifyDate']);
  if (modifyDate != null) {
    flightLogResList.modifyDate = modifyDate;
  }
  final pilot = jsonConvert.convert<String>(json['pilot']);
  if (pilot != null) {
    flightLogResList.pilot = pilot;
  }
  final rate = jsonConvert.convert<int>(json['rate']);
  if (rate != null) {
    flightLogResList.rate = rate;
  }
  final route = jsonConvert.convert<String>(json['route']);
  if (route != null) {
    flightLogResList.route = route;
  }
  final searchFavorite = jsonConvert.convert<int>(json['searchFavorite']);
  if (searchFavorite != null) {
    flightLogResList.searchFavorite = searchFavorite;
  }
  final dayTime = jsonConvert.convert<double>(json['dayTime']);
  if (dayTime != null) {
    flightLogResList.dayTime = dayTime;
  }
  final nightTime = jsonConvert.convert<double>(json['nightTime']);
  if (nightTime != null) {
    flightLogResList.nightTime = nightTime;
  }
  return flightLogResList;
}

Map<String, dynamic> $FlightLogResListToJson(FlightLogResList entity) {
  final data = <String, dynamic>{};
  data['aircraft'] = entity.aircraft;
  data['airwayPoint'] = entity.airwayPoint;
  data['arrival'] = entity.arrival;
  data['arrivalAirportName'] = entity.arrivalAirportName;
  data['arrivalTime'] = entity.arrivalTime;
  data['boardingTime'] = entity.boardingTime;
  data['createDate'] = entity.createDate;
  data['delay'] = entity.delay;
  data['departure'] = entity.departure;
  data['departureAirportName'] = entity.departureAirportName;
  data['departureTime'] = entity.departureTime;
  data['distance'] = entity.distance;
  data['favorite'] = entity.favorite;
  data['flightId'] = entity.flightId;
  data['flightNumber'] = entity.flightNumber;
  data['flightTime'] = entity.flightTime;
  data['livery'] = entity.livery;
  data['logId'] = entity.logId;
  data['modifyDate'] = entity.modifyDate;
  data['pilot'] = entity.pilot;
  data['rate'] = entity.rate;
  data['route'] = entity.route;
  data['searchFavorite'] = entity.searchFavorite;
  data['dayTime'] = entity.dayTime;
  data['nightTime'] = entity.nightTime;
  return data;
}

extension FlightLogResListExtension on FlightLogResList {
  FlightLogResList copyWith({
    String? aircraft,
    String? airwayPoint,
    String? arrival,
    String? arrivalAirportName,
    String? arrivalTime,
    String? boardingTime,
    int? createDate,
    int? delay,
    String? departure,
    String? departureAirportName,
    String? departureTime,
    int? distance,
    int? favorite,
    String? flightId,
    String? flightNumber,
    String? flightTime,
    String? livery,
    int? logId,
    int? modifyDate,
    String? pilot,
    int? rate,
    String? route,
    int? searchFavorite,
    double? dayTime,
    double? nightTime,
  }) {
    return FlightLogResList()
      ..aircraft = aircraft ?? this.aircraft
      ..airwayPoint = airwayPoint ?? this.airwayPoint
      ..arrival = arrival ?? this.arrival
      ..arrivalAirportName = arrivalAirportName ?? this.arrivalAirportName
      ..arrivalTime = arrivalTime ?? this.arrivalTime
      ..boardingTime = boardingTime ?? this.boardingTime
      ..createDate = createDate ?? this.createDate
      ..delay = delay ?? this.delay
      ..departure = departure ?? this.departure
      ..departureAirportName = departureAirportName ?? this.departureAirportName
      ..departureTime = departureTime ?? this.departureTime
      ..distance = distance ?? this.distance
      ..favorite = favorite ?? this.favorite
      ..flightId = flightId ?? this.flightId
      ..flightNumber = flightNumber ?? this.flightNumber
      ..flightTime = flightTime ?? this.flightTime
      ..livery = livery ?? this.livery
      ..logId = logId ?? this.logId
      ..modifyDate = modifyDate ?? this.modifyDate
      ..pilot = pilot ?? this.pilot
      ..rate = rate ?? this.rate
      ..route = route ?? this.route
      ..searchFavorite = searchFavorite ?? this.searchFavorite
      ..dayTime = dayTime ?? this.dayTime
      ..nightTime = nightTime ?? this.nightTime;
  }
}