import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/infinte_flight_airport_entity.dart';

InfiniteFlightAirportEntity $InfinteFlightAirportEntityFromJson(Map<String, dynamic> json) {
  final infinteFlightAirportEntity = InfiniteFlightAirportEntity.InfiniteFlightAirportEntity();
  final errorCode = jsonConvert.convert<int>(json['errorCode']);
  if (errorCode != null) {
    infinteFlightAirportEntity.errorCode = errorCode;
  }
  final result = (json['result'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<InfiniteFlightAirportResult>(e) as InfiniteFlightAirportResult).toList();
  if (result != null) {
    infinteFlightAirportEntity.result = result;
  }
  return infinteFlightAirportEntity;
}

Map<String, dynamic> $InfinteFlightAirportEntityToJson(InfiniteFlightAirportEntity entity) {
  final data = <String, dynamic>{};
  data['errorCode'] = entity.errorCode;
  data['result'] = entity.result?.map((v) => v.toJson()).toList();
  return data;
}

extension InfinteFlightAirportEntityExtension on InfiniteFlightAirportEntity {
  InfiniteFlightAirportEntity copyWith({
    int? errorCode,
    List<InfiniteFlightAirportResult>? result,
  }) {
    return InfiniteFlightAirportEntity.InfiniteFlightAirportEntity()
      ..errorCode = errorCode ?? this.errorCode
      ..result = result ?? this.result;
  }
}

InfiniteFlightAirportResult $InfinteFlightAirportResultFromJson(Map<String, dynamic> json) {
  final infinteFlightAirportResult = InfiniteFlightAirportResult.InfiniteFlightAirportResult();
  final icao = jsonConvert.convert<String>(json['icao']);
  if (icao != null) {
    infinteFlightAirportResult.icao = icao;
  }
  final iata = jsonConvert.convert<String>(json['iata']);
  if (iata != null) {
    infinteFlightAirportResult.iata = iata;
  }
  final name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    infinteFlightAirportResult.name = name;
  }
  final city = jsonConvert.convert<String>(json['city']);
  if (city != null) {
    infinteFlightAirportResult.city = city;
  }
  final state = jsonConvert.convert<String>(json['state']);
  if (state != null) {
    infinteFlightAirportResult.state = state;
  }
  final country = jsonConvert.convert<InfiniteFlightAirportResultCountry>(json['country']);
  if (country != null) {
    infinteFlightAirportResult.country = country;
  }
  final clazz = jsonConvert.convert<int>(json['clazz']);
  if (clazz != null) {
    infinteFlightAirportResult.clazz = clazz;
  }
  final frequenciesCount = jsonConvert.convert<int>(json['frequenciesCount']);
  if (frequenciesCount != null) {
    infinteFlightAirportResult.frequenciesCount = frequenciesCount;
  }
  final elevation = jsonConvert.convert<int>(json['elevation']);
  if (elevation != null) {
    infinteFlightAirportResult.elevation = elevation;
  }
  final latitude = jsonConvert.convert<double>(json['latitude']);
  if (latitude != null) {
    infinteFlightAirportResult.latitude = latitude;
  }
  final longitude = jsonConvert.convert<double>(json['longitude']);
  if (longitude != null) {
    infinteFlightAirportResult.longitude = longitude;
  }
  final timezone = jsonConvert.convert<String>(json['timezone']);
  if (timezone != null) {
    infinteFlightAirportResult.timezone = timezone;
  }
  final has3dBuildings = jsonConvert.convert<bool>(json['has3dBuildings']);
  if (has3dBuildings != null) {
    infinteFlightAirportResult.has3dBuildings = has3dBuildings;
  }
  final hasJetbridges = jsonConvert.convert<bool>(json['hasJetbridges']);
  if (hasJetbridges != null) {
    infinteFlightAirportResult.hasJetbridges = hasJetbridges;
  }
  final hasSafedockUnits = jsonConvert.convert<bool>(json['hasSafedockUnits']);
  if (hasSafedockUnits != null) {
    infinteFlightAirportResult.hasSafedockUnits = hasSafedockUnits;
  }
  final hasTaxiwayRouting = jsonConvert.convert<bool>(json['hasTaxiwayRouting']);
  if (hasTaxiwayRouting != null) {
    infinteFlightAirportResult.hasTaxiwayRouting = hasTaxiwayRouting;
  }
  return infinteFlightAirportResult;
}

Map<String, dynamic> $InfiniteFlightAirportResultToJson(InfiniteFlightAirportResult entity) {
  final data = <String, dynamic>{};
  data['icao'] = entity.icao;
  data['iata'] = entity.iata;
  data['name'] = entity.name;
  data['city'] = entity.city;
  data['state'] = entity.state;
  data['country'] = entity.country?.toJson();
  data['clazz'] = entity.clazz;
  data['frequenciesCount'] = entity.frequenciesCount;
  data['elevation'] = entity.elevation;
  data['latitude'] = entity.latitude;
  data['longitude'] = entity.longitude;
  data['timezone'] = entity.timezone;
  data['has3dBuildings'] = entity.has3dBuildings;
  data['hasJetbridges'] = entity.hasJetbridges;
  data['hasSafedockUnits'] = entity.hasSafedockUnits;
  data['hasTaxiwayRouting'] = entity.hasTaxiwayRouting;
  return data;
}

extension InfinteFlightAirportResultExtension on InfiniteFlightAirportResult {
  InfiniteFlightAirportResult copyWith({
    String? icao,
    String? iata,
    String? name,
    String? city,
    String? state,
    InfiniteFlightAirportResultCountry? country,
    int? clazz,
    int? frequenciesCount,
    int? elevation,
    double? latitude,
    double? longitude,
    String? timezone,
    bool? has3dBuildings,
    bool? hasJetbridges,
    bool? hasSafedockUnits,
    bool? hasTaxiwayRouting,
  }) {
    return InfiniteFlightAirportResult.InfiniteFlightAirportResult()
      ..icao = icao ?? this.icao
      ..iata = iata ?? this.iata
      ..name = name ?? this.name
      ..city = city ?? this.city
      ..state = state ?? this.state
      ..country = country ?? this.country
      ..clazz = clazz ?? this.clazz
      ..frequenciesCount = frequenciesCount ?? this.frequenciesCount
      ..elevation = elevation ?? this.elevation
      ..latitude = latitude ?? this.latitude
      ..longitude = longitude ?? this.longitude
      ..timezone = timezone ?? this.timezone
      ..has3dBuildings = has3dBuildings ?? this.has3dBuildings
      ..hasJetbridges = hasJetbridges ?? this.hasJetbridges
      ..hasSafedockUnits = hasSafedockUnits ?? this.hasSafedockUnits
      ..hasTaxiwayRouting = hasTaxiwayRouting ?? this.hasTaxiwayRouting;
  }
}

InfiniteFlightAirportResultCountry $InfinteFlightAirportResultCountryFromJson(Map<String, dynamic> json) {
  final infinteFlightAirportResultCountry = InfiniteFlightAirportResultCountry.InfiniteFlightAirportResultCountry();
  final id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    infinteFlightAirportResultCountry.id = id;
  }
  final name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    infinteFlightAirportResultCountry.name = name;
  }
  final isoCode = jsonConvert.convert<String>(json['isoCode']);
  if (isoCode != null) {
    infinteFlightAirportResultCountry.isoCode = isoCode;
  }
  return infinteFlightAirportResultCountry;
}

Map<String, dynamic> $InfinteFlightAirportResultCountryToJson(InfiniteFlightAirportResultCountry entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['isoCode'] = entity.isoCode;
  return data;
}

extension InfinteFlightAirportResultCountryExtension on InfiniteFlightAirportResultCountry {
  InfiniteFlightAirportResultCountry copyWith({
    int? id,
    String? name,
    String? isoCode,
  }) {
    return InfiniteFlightAirportResultCountry.InfiniteFlightAirportResultCountry()
      ..id = id ?? this.id
      ..name = name ?? this.name
      ..isoCode = isoCode ?? this.isoCode;
  }
}