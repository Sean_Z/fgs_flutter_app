import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/infinite_flight_livery_res_entity.dart';

InfiniteFlightLiveryResEntity $InfiniteFlightLiveryResEntityFromJson(Map<String, dynamic> json) {
  final infiniteFlightLiveryResEntity = InfiniteFlightLiveryResEntity();
  final errorCode = jsonConvert.convert<int>(json['errorCode']);
  if (errorCode != null) {
    infiniteFlightLiveryResEntity.errorCode = errorCode;
  }
  final result = (json['result'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<InfiniteFlightLiveryResResult>(e) as InfiniteFlightLiveryResResult).toList();
  if (result != null) {
    infiniteFlightLiveryResEntity.result = result;
  }
  return infiniteFlightLiveryResEntity;
}

Map<String, dynamic> $InfiniteFlightLiveryResEntityToJson(InfiniteFlightLiveryResEntity entity) {
  final data = <String, dynamic>{};
  data['errorCode'] = entity.errorCode;
  data['result'] = entity.result.map((v) => v.toJson()).toList();
  return data;
}

extension InfiniteFlightLiveryResEntityExtension on InfiniteFlightLiveryResEntity {
  InfiniteFlightLiveryResEntity copyWith({
    int? errorCode,
    List<InfiniteFlightLiveryResResult>? result,
  }) {
    return InfiniteFlightLiveryResEntity()
      ..errorCode = errorCode ?? this.errorCode
      ..result = result ?? this.result;
  }
}

InfiniteFlightLiveryResResult $InfiniteFlightLiveryResResultFromJson(Map<String, dynamic> json) {
  final infiniteFlightLiveryResResult = InfiniteFlightLiveryResResult();
  final id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    infiniteFlightLiveryResResult.id = id;
  }
  final aircraftID = jsonConvert.convert<String>(json['aircraftID']);
  if (aircraftID != null) {
    infiniteFlightLiveryResResult.aircraftID = aircraftID;
  }
  final aircraftName = jsonConvert.convert<String>(json['aircraftName']);
  if (aircraftName != null) {
    infiniteFlightLiveryResResult.aircraftName = aircraftName;
  }
  final liveryName = jsonConvert.convert<String>(json['liveryName']);
  if (liveryName != null) {
    infiniteFlightLiveryResResult.liveryName = liveryName;
  }
  return infiniteFlightLiveryResResult;
}

Map<String, dynamic> $InfiniteFlightLiveryResResultToJson(InfiniteFlightLiveryResResult entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['aircraftID'] = entity.aircraftID;
  data['aircraftName'] = entity.aircraftName;
  data['liveryName'] = entity.liveryName;
  return data;
}

extension InfiniteFlightLiveryResResultExtension on InfiniteFlightLiveryResResult {
  InfiniteFlightLiveryResResult copyWith({
    String? id,
    String? aircraftID,
    String? aircraftName,
    String? liveryName,
  }) {
    return InfiniteFlightLiveryResResult()
      ..id = id ?? this.id
      ..aircraftID = aircraftID ?? this.aircraftID
      ..aircraftName = aircraftName ?? this.aircraftName
      ..liveryName = liveryName ?? this.liveryName;
  }
}