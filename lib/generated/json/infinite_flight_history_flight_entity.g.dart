import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/infinite_flight_history_flight_entity.dart';

InfiniteFlightHistoryFlightEntity $InfiniteFlightHistoryFlightEntityFromJson(Map<String, dynamic> json) {
  final infiniteFlightHistoryFlightEntity = InfiniteFlightHistoryFlightEntity();
  final errorCode = jsonConvert.convert<int>(json['errorCode']);
  if (errorCode != null) {
    infiniteFlightHistoryFlightEntity.errorCode = errorCode;
  }
  final result = jsonConvert.convert<InfiniteFlightHistoryFlightResult>(json['result']);
  if (result != null) {
    infiniteFlightHistoryFlightEntity.result = result;
  }
  return infiniteFlightHistoryFlightEntity;
}

Map<String, dynamic> $InfiniteFlightHistoryFlightEntityToJson(InfiniteFlightHistoryFlightEntity entity) {
  final data = <String, dynamic>{};
  data['errorCode'] = entity.errorCode;
  data['result'] = entity.result.toJson();
  return data;
}

extension InfiniteFlightHistoryFlightEntityExtension on InfiniteFlightHistoryFlightEntity {
  InfiniteFlightHistoryFlightEntity copyWith({
    int? errorCode,
    InfiniteFlightHistoryFlightResult? result,
  }) {
    return InfiniteFlightHistoryFlightEntity()
      ..errorCode = errorCode ?? this.errorCode
      ..result = result ?? this.result;
  }
}

InfiniteFlightHistoryFlightResult $InfiniteFlightHistoryFlightResultFromJson(Map<String, dynamic> json) {
  final infiniteFlightHistoryFlightResult = InfiniteFlightHistoryFlightResult();
  final pageIndex = jsonConvert.convert<int>(json['pageIndex']);
  if (pageIndex != null) {
    infiniteFlightHistoryFlightResult.pageIndex = pageIndex;
  }
  final totalPages = jsonConvert.convert<int>(json['totalPages']);
  if (totalPages != null) {
    infiniteFlightHistoryFlightResult.totalPages = totalPages;
  }
  final totalCount = jsonConvert.convert<int>(json['totalCount']);
  if (totalCount != null) {
    infiniteFlightHistoryFlightResult.totalCount = totalCount;
  }
  final hasPreviousPage = jsonConvert.convert<bool>(json['hasPreviousPage']);
  if (hasPreviousPage != null) {
    infiniteFlightHistoryFlightResult.hasPreviousPage = hasPreviousPage;
  }
  final hasNextPage = jsonConvert.convert<bool>(json['hasNextPage']);
  if (hasNextPage != null) {
    infiniteFlightHistoryFlightResult.hasNextPage = hasNextPage;
  }
  final data = (json['data'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<InfiniteFlightHistoryFlightResultData>(e) as InfiniteFlightHistoryFlightResultData).toList();
  if (data != null) {
    infiniteFlightHistoryFlightResult.data = data;
  }
  return infiniteFlightHistoryFlightResult;
}

Map<String, dynamic> $InfiniteFlightHistoryFlightResultToJson(InfiniteFlightHistoryFlightResult entity) {
  final data = <String, dynamic>{};
  data['pageIndex'] = entity.pageIndex;
  data['totalPages'] = entity.totalPages;
  data['totalCount'] = entity.totalCount;
  data['hasPreviousPage'] = entity.hasPreviousPage;
  data['hasNextPage'] = entity.hasNextPage;
  data['data'] = entity.data.map((v) => v.toJson()).toList();
  return data;
}

extension InfiniteFlightHistoryFlightResultExtension on InfiniteFlightHistoryFlightResult {
  InfiniteFlightHistoryFlightResult copyWith({
    int? pageIndex,
    int? totalPages,
    int? totalCount,
    bool? hasPreviousPage,
    bool? hasNextPage,
    List<InfiniteFlightHistoryFlightResultData>? data,
  }) {
    return InfiniteFlightHistoryFlightResult()
      ..pageIndex = pageIndex ?? this.pageIndex
      ..totalPages = totalPages ?? this.totalPages
      ..totalCount = totalCount ?? this.totalCount
      ..hasPreviousPage = hasPreviousPage ?? this.hasPreviousPage
      ..hasNextPage = hasNextPage ?? this.hasNextPage
      ..data = data ?? this.data;
  }
}

InfiniteFlightHistoryFlightResultData $InfiniteFlightHistoryFlightResultDataFromJson(Map<String, dynamic> json) {
  final infiniteFlightHistoryFlightResultData = InfiniteFlightHistoryFlightResultData();
  final id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    infiniteFlightHistoryFlightResultData.id = id;
  }
  final created = jsonConvert.convert<String>(json['created']);
  if (created != null) {
    infiniteFlightHistoryFlightResultData.created = created;
  }
  final userId = jsonConvert.convert<String>(json['userId']);
  if (userId != null) {
    infiniteFlightHistoryFlightResultData.userId = userId;
  }
  final aircraftId = jsonConvert.convert<String>(json['aircraftId']);
  if (aircraftId != null) {
    infiniteFlightHistoryFlightResultData.aircraftId = aircraftId;
  }
  final liveryId = jsonConvert.convert<String>(json['liveryId']);
  if (liveryId != null) {
    infiniteFlightHistoryFlightResultData.liveryId = liveryId;
  }
  final callsign = jsonConvert.convert<String>(json['callsign']);
  if (callsign != null) {
    infiniteFlightHistoryFlightResultData.callsign = callsign;
  }
  final server = jsonConvert.convert<String>(json['server']);
  if (server != null) {
    infiniteFlightHistoryFlightResultData.server = server;
  }
  final dayTime = jsonConvert.convert<double>(json['dayTime']);
  if (dayTime != null) {
    infiniteFlightHistoryFlightResultData.dayTime = dayTime;
  }
  final nightTime = jsonConvert.convert<int>(json['nightTime']);
  if (nightTime != null) {
    infiniteFlightHistoryFlightResultData.nightTime = nightTime;
  }
  final totalTime = jsonConvert.convert<double>(json['totalTime']);
  if (totalTime != null) {
    infiniteFlightHistoryFlightResultData.totalTime = totalTime;
  }
  final landingCount = jsonConvert.convert<int>(json['landingCount']);
  if (landingCount != null) {
    infiniteFlightHistoryFlightResultData.landingCount = landingCount;
  }
  final originAirport = jsonConvert.convert<String>(json['originAirport']);
  if (originAirport != null) {
    infiniteFlightHistoryFlightResultData.originAirport = originAirport;
  }
  final destinationAirport = jsonConvert.convert<String>(json['destinationAirport']);
  if (destinationAirport != null) {
    infiniteFlightHistoryFlightResultData.destinationAirport = destinationAirport;
  }
  final xp = jsonConvert.convert<int>(json['xp']);
  if (xp != null) {
    infiniteFlightHistoryFlightResultData.xp = xp;
  }
  return infiniteFlightHistoryFlightResultData;
}

Map<String, dynamic> $InfiniteFlightHistoryFlightResultDataToJson(InfiniteFlightHistoryFlightResultData entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['created'] = entity.created;
  data['userId'] = entity.userId;
  data['aircraftId'] = entity.aircraftId;
  data['liveryId'] = entity.liveryId;
  data['callsign'] = entity.callsign;
  data['server'] = entity.server;
  data['dayTime'] = entity.dayTime;
  data['nightTime'] = entity.nightTime;
  data['totalTime'] = entity.totalTime;
  data['landingCount'] = entity.landingCount;
  data['originAirport'] = entity.originAirport;
  data['destinationAirport'] = entity.destinationAirport;
  data['xp'] = entity.xp;
  return data;
}

extension InfiniteFlightHistoryFlightResultDataExtension on InfiniteFlightHistoryFlightResultData {
  InfiniteFlightHistoryFlightResultData copyWith({
    String? id,
    String? created,
    String? userId,
    String? aircraftId,
    String? liveryId,
    String? callsign,
    String? server,
    double? dayTime,
    int? nightTime,
    double? totalTime,
    int? landingCount,
    String? originAirport,
    String? destinationAirport,
    int? xp,
  }) {
    return InfiniteFlightHistoryFlightResultData()
      ..id = id ?? this.id
      ..created = created ?? this.created
      ..userId = userId ?? this.userId
      ..aircraftId = aircraftId ?? this.aircraftId
      ..liveryId = liveryId ?? this.liveryId
      ..callsign = callsign ?? this.callsign
      ..server = server ?? this.server
      ..dayTime = dayTime ?? this.dayTime
      ..nightTime = nightTime ?? this.nightTime
      ..totalTime = totalTime ?? this.totalTime
      ..landingCount = landingCount ?? this.landingCount
      ..originAirport = originAirport ?? this.originAirport
      ..destinationAirport = destinationAirport ?? this.destinationAirport
      ..xp = xp ?? this.xp;
  }
}