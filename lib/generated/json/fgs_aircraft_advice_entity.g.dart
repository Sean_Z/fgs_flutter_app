import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/FgsAircraftAdviceEntity.dart';

FgsAircraftAdviceEntity $FgsAircraftAdviceEntityFromJson(dynamic json) {
  final fgsAircraftAdviceEntity = FgsAircraftAdviceEntity();
  final id = jsonConvert.convert<double>(json['id']);
  if (id != null) {
    fgsAircraftAdviceEntity.id = id;
  }
  final model = jsonConvert.convert<String>(json['model']);
  if (model != null) {
    fgsAircraftAdviceEntity.model = model;
  }
  final takeoffTrim = jsonConvert.convert<String>(json['takeoff_trim']);
  if (takeoffTrim != null) {
    fgsAircraftAdviceEntity.takeoffTrim = takeoffTrim;
  }
  final landingTrim = jsonConvert.convert<String>(json['landing_trim']);
  if (landingTrim != null) {
    fgsAircraftAdviceEntity.landingTrim = landingTrim;
  }
  final cruisingSpeed = jsonConvert.convert<double>(json['cruising_speed']);
  if (cruisingSpeed != null) {
    fgsAircraftAdviceEntity.cruisingSpeed = cruisingSpeed;
  }
  final landingFlaps = jsonConvert.convert<String>(json['landing_flaps']);
  if (landingFlaps != null) {
    fgsAircraftAdviceEntity.landingFlaps = landingFlaps;
  }
  final takeoffFlaps = jsonConvert.convert<String>(json['takeoff_flaps']);
  if (takeoffFlaps != null) {
    fgsAircraftAdviceEntity.takeoffFlaps = takeoffFlaps;
  }
  final maxPassenger = jsonConvert.convert<String>(json['max_passenger']);
  if (maxPassenger != null) {
    fgsAircraftAdviceEntity.maxPassenger = maxPassenger;
  }
  final maxCargo = jsonConvert.convert<String>(json['max_cargo']);
  if (maxCargo != null) {
    fgsAircraftAdviceEntity.maxCargo = maxCargo;
  }
  final mlw = jsonConvert.convert<String>(json['mlw']);
  if (mlw != null) {
    fgsAircraftAdviceEntity.mlw = mlw;
  }
  final mtow = jsonConvert.convert<String>(json['mtow']);
  if (mtow != null) {
    fgsAircraftAdviceEntity.mtow = mtow;
  }
  final rangeTime = jsonConvert.convert<String>(json['range_time']);
  if (rangeTime != null) {
    fgsAircraftAdviceEntity.rangeTime = rangeTime;
  }
  final takeoffDistance = jsonConvert.convert<String>(json['takeoff_distance']);
  if(takeoffDistance != null) {
    fgsAircraftAdviceEntity.takeoffDistance = takeoffDistance;
  }
  final landingDistance = jsonConvert.convert<String>(json['landing_distance']);
  if(landingDistance != null) {
    fgsAircraftAdviceEntity.landingDistance = landingDistance;
  }
  final company = jsonConvert.convert<String>(json['landingDistance']);
  if(company != null) {
    fgsAircraftAdviceEntity.company = company;
  }
  final remark = jsonConvert.convert<String>(json['remark']);
  if (remark != null) {
    fgsAircraftAdviceEntity.remark = remark;
  }
  final createTime = jsonConvert.convert<String>(json['create_time']);
  if (createTime != null) {
    fgsAircraftAdviceEntity.createTime = createTime;
  }
  final modifyTime = jsonConvert.convert<String>(json['modify_time']);
  if (modifyTime != null) {
    fgsAircraftAdviceEntity.modifyTime = modifyTime;
  }
  return fgsAircraftAdviceEntity;
}

Map<String, dynamic> $FgsAircraftAdviceEntityToJson(FgsAircraftAdviceEntity entity) {
  final data = <String, dynamic>{};
  data['id'] = entity.id;
  data['model'] = entity.model;
  data['takeoff_trim'] = entity.takeoffTrim;
  data['landing_trim'] = entity.landingTrim;
  data['cruising_speed'] = entity.cruisingSpeed;
  data['landing_flaps'] = entity.landingFlaps;
  data['takeoff_flaps'] = entity.takeoffFlaps;
  data['max_passenger'] = entity.maxPassenger;
  data['max_cargo'] = entity.maxCargo;
  data['mlw'] = entity.mlw;
  data['mtow'] = entity.mtow;
  data['range_time'] = entity.rangeTime;
  data['takeoff_distance'] = entity.takeoffDistance;
  data['landing_distance'] = entity.landingDistance;
  data['company'] = entity.company;
  data['remark'] = entity.remark;
  data['create_time'] = entity.createTime;
  data['modify_time'] = entity.modifyTime;
  return data;
}

extension FgsAircraftAdviceEntityExtension on FgsAircraftAdviceEntity {
  FgsAircraftAdviceEntity copyWith({
    double? id,
    String? model,
    String? takeoffTrim,
    String? landingTrim,
    double? cruisingSpeed,
    String? landingFlaps,
    String? takeoffFlaps,
    String? maxPassenger,
    String? maxCargo,
    String? mlw,
    String? mtow,
    String? rangeTime,
    String? takeoffDistance,
    String? landingDistance,
    String? company,
    String? remark,
    String? createTime,
    String? modifyTime,
  }) {
    return FgsAircraftAdviceEntity()
      ..id = id ?? this.id
      ..model = model ?? this.model
      ..takeoffTrim = takeoffTrim ?? this.takeoffTrim
      ..landingTrim = landingTrim ?? this.landingTrim
      ..cruisingSpeed = cruisingSpeed ?? this.cruisingSpeed
      ..landingFlaps = landingFlaps ?? this.landingFlaps
      ..takeoffFlaps = takeoffFlaps ?? this.takeoffFlaps
      ..maxPassenger = maxPassenger ?? this.maxPassenger
      ..maxCargo = maxCargo ?? this.maxCargo
      ..mlw = mlw ?? this.mlw
      ..mtow = mtow ?? this.mtow
      ..rangeTime = rangeTime ?? this.rangeTime
      ..takeoffDistance = takeoffDistance ?? this.takeoffDistance
      ..landingDistance = landingDistance ?? this.landingDistance
      ..company = company ?? this.company
      ..remark = remark ?? this.remark
      ..createTime = createTime ?? this.createTime
      ..modifyTime = modifyTime ?? this.modifyTime;
  }
}