import 'package:ifgs/generated/json/base/json_convert_content.dart';
import 'package:ifgs/request/modal/InfiniteFlightAllFlightsEntity.dart';

InfiniteFlightAllFlightsEntity $InfiniteFlightAllFlightsEntityFromJson(Map<String, dynamic> json) {
  final infiniteFlightAllFlightsEntity = InfiniteFlightAllFlightsEntity();
  final errorCode = jsonConvert.convert<int>(json['errorCode']);
  if (errorCode != null) {
    infiniteFlightAllFlightsEntity.errorCode = errorCode;
  }
  final result = (json['result'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<InfiniteFlightAllFlightsEntityResult>(e) as InfiniteFlightAllFlightsEntityResult).toList();
  if (result != null) {
    infiniteFlightAllFlightsEntity.result = result;
  }
  return infiniteFlightAllFlightsEntity;
}

Map<String, dynamic> $InfiniteFlightAllFlightsEntityToJson(InfiniteFlightAllFlightsEntity entity) {
  final data = <String, dynamic>{};
  data['errorCode'] = entity.errorCode;
  data['result'] = entity.result.map((v) => v.toJson()).toList();
  return data;
}

extension InfiniteFlightAllFlightsEntityExtension on InfiniteFlightAllFlightsEntity {
  InfiniteFlightAllFlightsEntity copyWith({
    int? errorCode,
    List<InfiniteFlightAllFlightsEntityResult>? result,
  }) {
    return InfiniteFlightAllFlightsEntity()
      ..errorCode = errorCode ?? this.errorCode
      ..result = result ?? this.result;
  }
}

InfiniteFlightAllFlightsEntityResult $InfiniteFlightAllFlightsEntityResultFromJson(Map<String, dynamic> json) {
  final infiniteFlightAllFlightsEntityResult = InfiniteFlightAllFlightsEntityResult();
  final username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    infiniteFlightAllFlightsEntityResult.username = username;
  }
  final callsign = jsonConvert.convert<String>(json['callsign']);
  if (callsign != null) {
    infiniteFlightAllFlightsEntityResult.callsign = callsign;
  }
  final latitude = jsonConvert.convert<double>(json['latitude']);
  if (latitude != null) {
    infiniteFlightAllFlightsEntityResult.latitude = latitude;
  }
  final longitude = jsonConvert.convert<double>(json['longitude']);
  if (longitude != null) {
    infiniteFlightAllFlightsEntityResult.longitude = longitude;
  }
  final altitude = jsonConvert.convert<double>(json['altitude']);
  if (altitude != null) {
    infiniteFlightAllFlightsEntityResult.altitude = altitude;
  }
  final speed = jsonConvert.convert<double>(json['speed']);
  if (speed != null) {
    infiniteFlightAllFlightsEntityResult.speed = speed;
  }
  final verticalSpeed = jsonConvert.convert<double>(json['verticalSpeed']);
  if (verticalSpeed != null) {
    infiniteFlightAllFlightsEntityResult.verticalSpeed = verticalSpeed;
  }
  final track = jsonConvert.convert<double>(json['track']);
  if (track != null) {
    infiniteFlightAllFlightsEntityResult.track = track;
  }
  final lastReport = jsonConvert.convert<String>(json['lastReport']);
  if (lastReport != null) {
    infiniteFlightAllFlightsEntityResult.lastReport = lastReport;
  }
  final flightId = jsonConvert.convert<String>(json['flightId']);
  if (flightId != null) {
    infiniteFlightAllFlightsEntityResult.flightId = flightId;
  }
  final userId = jsonConvert.convert<String>(json['userId']);
  if (userId != null) {
    infiniteFlightAllFlightsEntityResult.userId = userId;
  }
  final aircraftId = jsonConvert.convert<String>(json['aircraftId']);
  if (aircraftId != null) {
    infiniteFlightAllFlightsEntityResult.aircraftId = aircraftId;
  }
  final liveryId = jsonConvert.convert<String>(json['liveryId']);
  if (liveryId != null) {
    infiniteFlightAllFlightsEntityResult.liveryId = liveryId;
  }
  final heading = jsonConvert.convert<double>(json['heading']);
  if (heading != null) {
    infiniteFlightAllFlightsEntityResult.heading = heading;
  }
  return infiniteFlightAllFlightsEntityResult;
}

Map<String, dynamic> $InfiniteFlightAllFlightsEntityResultToJson(InfiniteFlightAllFlightsEntityResult entity) {
  final data = <String, dynamic>{};
  data['username'] = entity.username;
  data['callsign'] = entity.callsign;
  data['latitude'] = entity.latitude;
  data['longitude'] = entity.longitude;
  data['altitude'] = entity.altitude;
  data['speed'] = entity.speed;
  data['verticalSpeed'] = entity.verticalSpeed;
  data['track'] = entity.track;
  data['lastReport'] = entity.lastReport;
  data['flightId'] = entity.flightId;
  data['userId'] = entity.userId;
  data['aircraftId'] = entity.aircraftId;
  data['liveryId'] = entity.liveryId;
  data['heading'] = entity.heading;
  return data;
}

extension InfiniteFlightAllFlightsEntityResultExtension on InfiniteFlightAllFlightsEntityResult {
  InfiniteFlightAllFlightsEntityResult copyWith({
    String? username,
    String? callsign,
    double? latitude,
    double? longitude,
    double? altitude,
    double? speed,
    double? verticalSpeed,
    double? track,
    String? lastReport,
    String? flightId,
    String? userId,
    String? aircraftId,
    String? liveryId,
    double? heading,
  }) {
    return InfiniteFlightAllFlightsEntityResult()
      ..username = username ?? this.username
      ..callsign = callsign ?? this.callsign
      ..latitude = latitude ?? this.latitude
      ..longitude = longitude ?? this.longitude
      ..altitude = altitude ?? this.altitude
      ..speed = speed ?? this.speed
      ..verticalSpeed = verticalSpeed ?? this.verticalSpeed
      ..track = track ?? this.track
      ..lastReport = lastReport ?? this.lastReport
      ..flightId = flightId ?? this.flightId
      ..userId = userId ?? this.userId
      ..aircraftId = aircraftId ?? this.aircraftId
      ..liveryId = liveryId ?? this.liveryId
      ..heading = heading ?? this.heading;
  }
}