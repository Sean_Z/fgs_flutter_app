// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a zh locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'zh';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "AboutMeFragmentAeroMaster":
            MessageLookupByLibrary.simpleMessage("飞行大师"),
        "AboutMeFragmentCardPack": MessageLookupByLibrary.simpleMessage("用户证件"),
        "AboutMeFragmentFFP": MessageLookupByLibrary.simpleMessage("航司证件卡包"),
        "AboutMeFragmentFavoriteRoute":
            MessageLookupByLibrary.simpleMessage("收藏航路"),
        "AboutMeFragmentHistory": MessageLookupByLibrary.simpleMessage("历史航班"),
        "AchievementNoCurrentFlight":
            MessageLookupByLibrary.simpleMessage("未查询到当前航班"),
        "AchievementRangeAchievement":
            MessageLookupByLibrary.simpleMessage("里程成就"),
        "AchievementTabCurrentFlight":
            MessageLookupByLibrary.simpleMessage("当前航班"),
        "AchievementTabHistoryFlight":
            MessageLookupByLibrary.simpleMessage("历史航班"),
        "AchievementTitleMyAchievement":
            MessageLookupByLibrary.simpleMessage("我的成就"),
        "Airline": MessageLookupByLibrary.simpleMessage("航司"),
        "Airport": MessageLookupByLibrary.simpleMessage("机场"),
        "AirportAdvice": MessageLookupByLibrary.simpleMessage("机场指南"),
        "Already": MessageLookupByLibrary.simpleMessage("已经"),
        "AlreadyFlownRange": MessageLookupByLibrary.simpleMessage("已经飞行了"),
        "AlreadyFlownTime": MessageLookupByLibrary.simpleMessage("已经飞行了"),
        "Altitude": MessageLookupByLibrary.simpleMessage("高度"),
        "DrawerMenuListSetting": MessageLookupByLibrary.simpleMessage("设置"),
        "Fix": MessageLookupByLibrary.simpleMessage("修复"),
        "FlightInfoPageViewArrival": MessageLookupByLibrary.simpleMessage("进场"),
        "FlightInfoPageViewArrived":
            MessageLookupByLibrary.simpleMessage("已抵达"),
        "FlightInfoPageViewDeparture":
            MessageLookupByLibrary.simpleMessage("离场"),
        "FlightInfoPageViewFollowThisFlight":
            MessageLookupByLibrary.simpleMessage("订阅此航班"),
        "FlightInfoPageViewOnTime": MessageLookupByLibrary.simpleMessage("准点"),
        "FlightInfoPageViewReached":
            MessageLookupByLibrary.simpleMessage("即将抵达"),
        "FlightInfoPageViewTitle": MessageLookupByLibrary.simpleMessage("航班详情"),
        "FlightJudgmentATIS": MessageLookupByLibrary.simpleMessage("广播"),
        "FlightJudgmentAircraft": MessageLookupByLibrary.simpleMessage("飞行"),
        "FlightJudgmentApproach": MessageLookupByLibrary.simpleMessage("进近"),
        "FlightJudgmentClearance": MessageLookupByLibrary.simpleMessage("净场"),
        "FlightJudgmentClimbing": MessageLookupByLibrary.simpleMessage("爬升"),
        "FlightJudgmentCruising": MessageLookupByLibrary.simpleMessage("巡航"),
        "FlightJudgmentDeparture": MessageLookupByLibrary.simpleMessage("离场"),
        "FlightJudgmentDescending": MessageLookupByLibrary.simpleMessage("下高"),
        "FlightJudgmentOnGround": MessageLookupByLibrary.simpleMessage("地面"),
        "FlightJudgmentTaxing": MessageLookupByLibrary.simpleMessage("滑行"),
        "FlightJudgmentTower": MessageLookupByLibrary.simpleMessage("塔台"),
        "FlightPlanWidgetClear": MessageLookupByLibrary.simpleMessage("清除"),
        "FlightPlanWidgetCopyRoute":
            MessageLookupByLibrary.simpleMessage("创建航路"),
        "FlightPlanWidgetEstimate": MessageLookupByLibrary.simpleMessage("预计"),
        "FplMode": MessageLookupByLibrary.simpleMessage("航路来源"),
        "HistoryTravelListTitle": MessageLookupByLibrary.simpleMessage("历史行程"),
        "Inbound": MessageLookupByLibrary.simpleMessage("进港航班"),
        "IndexPageViewAltitude": MessageLookupByLibrary.simpleMessage("海拔"),
        "IndexPageViewHeading": MessageLookupByLibrary.simpleMessage("磁航向"),
        "IndexPageViewSubmit": MessageLookupByLibrary.simpleMessage("提交"),
        "IndexPageViewVs": MessageLookupByLibrary.simpleMessage("爬升率"),
        "InfiniteFlightAchievementAnalyze":
            MessageLookupByLibrary.simpleMessage("IF 数据分析"),
        "InfiniteFlightAchievementTitle":
            MessageLookupByLibrary.simpleMessage("IF 成就"),
        "LogOut": MessageLookupByLibrary.simpleMessage("退出登录"),
        "MenuListAbout": MessageLookupByLibrary.simpleMessage("我的"),
        "MenuListAircraftAdvice": MessageLookupByLibrary.simpleMessage("机型建议"),
        "MenuListAirport": MessageLookupByLibrary.simpleMessage("机场大屏"),
        "MenuListAviationNews": MessageLookupByLibrary.simpleMessage("IF 进展"),
        "MenuListChart": MessageLookupByLibrary.simpleMessage("航图&手册"),
        "MenuListDrawFpl": MessageLookupByLibrary.simpleMessage("绘制航路"),
        "MenuListFgsBoard": MessageLookupByLibrary.simpleMessage("FGS看板"),
        "MenuListFlightATC": MessageLookupByLibrary.simpleMessage("空管"),
        "MenuListFlightFFP": MessageLookupByLibrary.simpleMessage("航司证件包"),
        "MenuListFlightIfAchievement":
            MessageLookupByLibrary.simpleMessage("成就"),
        "MenuListFlightList": MessageLookupByLibrary.simpleMessage("航班列表"),
        "MenuListMap": MessageLookupByLibrary.simpleMessage("地图"),
        "MenuListPlan": MessageLookupByLibrary.simpleMessage("航路计划"),
        "Outbound": MessageLookupByLibrary.simpleMessage("离港航班"),
        "Pilot": MessageLookupByLibrary.simpleMessage("机长"),
        "Screen": MessageLookupByLibrary.simpleMessage("机场大屏"),
        "SettingPageViewCheckUpdate":
            MessageLookupByLibrary.simpleMessage("检查更新"),
        "SettingPageViewFingerprint":
            MessageLookupByLibrary.simpleMessage("指纹设置"),
        "SettingPageViewFixApp": MessageLookupByLibrary.simpleMessage("修复app"),
        "SettingPageViewIniLang": MessageLookupByLibrary.simpleMessage("中文"),
        "SettingPageViewThemeMode":
            MessageLookupByLibrary.simpleMessage("切换主题"),
        "SettingPageViewTitle": MessageLookupByLibrary.simpleMessage("设置"),
        "SettingPageViewUpdateData":
            MessageLookupByLibrary.simpleMessage("更新数据"),
        "Unknown": MessageLookupByLibrary.simpleMessage("未知"),
        "Update": MessageLookupByLibrary.simpleMessage("更新"),
        "UserPrivatePrivacy": MessageLookupByLibrary.simpleMessage("用户协议"),
        "Visibility": MessageLookupByLibrary.simpleMessage("能见度"),
        "WindDirection": MessageLookupByLibrary.simpleMessage("风向"),
        "WindSpeed": MessageLookupByLibrary.simpleMessage("风速"),
        "flew": MessageLookupByLibrary.simpleMessage("飞行了"),
        "indexPageViewSignalOffline":
            MessageLookupByLibrary.simpleMessage("离线"),
        "indexPageViewSignalOnline": MessageLookupByLibrary.simpleMessage("在线"),
        "indexPageViewSpeed": MessageLookupByLibrary.simpleMessage("地速")
      };
}
