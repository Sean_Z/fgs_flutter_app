// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "AboutMeFragmentAeroMaster":
            MessageLookupByLibrary.simpleMessage("Aero Master"),
        "AboutMeFragmentCardPack":
            MessageLookupByLibrary.simpleMessage("Card Pack"),
        "AboutMeFragmentFFP": MessageLookupByLibrary.simpleMessage("FFP Card"),
        "AboutMeFragmentFavoriteRoute":
            MessageLookupByLibrary.simpleMessage("Favorite Route"),
        "AboutMeFragmentHistory":
            MessageLookupByLibrary.simpleMessage("History"),
        "AchievementNoCurrentFlight":
            MessageLookupByLibrary.simpleMessage("No Current Flight"),
        "AchievementRangeAchievement":
            MessageLookupByLibrary.simpleMessage("Range Achievement"),
        "AchievementTabCurrentFlight":
            MessageLookupByLibrary.simpleMessage("Current Flight"),
        "AchievementTabHistoryFlight":
            MessageLookupByLibrary.simpleMessage("History Flight"),
        "AchievementTitleMyAchievement":
            MessageLookupByLibrary.simpleMessage("My Achievement"),
        "Airline": MessageLookupByLibrary.simpleMessage("Airline"),
        "Airport": MessageLookupByLibrary.simpleMessage("Airport"),
        "AirportAdvice": MessageLookupByLibrary.simpleMessage("Airport Advice"),
        "Already": MessageLookupByLibrary.simpleMessage("Already"),
        "AlreadyFlownRange": MessageLookupByLibrary.simpleMessage("Have flown"),
        "AlreadyFlownTime":
            MessageLookupByLibrary.simpleMessage("Have been flying for"),
        "Altitude": MessageLookupByLibrary.simpleMessage("Altitude"),
        "DrawerMenuListSetting":
            MessageLookupByLibrary.simpleMessage("Setting"),
        "Fix": MessageLookupByLibrary.simpleMessage("Fix"),
        "FlightInfoPageViewArrival":
            MessageLookupByLibrary.simpleMessage("Arrival"),
        "FlightInfoPageViewArrived":
            MessageLookupByLibrary.simpleMessage("Arrived"),
        "FlightInfoPageViewDeparture":
            MessageLookupByLibrary.simpleMessage("Departure"),
        "FlightInfoPageViewFollowThisFlight":
            MessageLookupByLibrary.simpleMessage("Follow this flight"),
        "FlightInfoPageViewOnTime":
            MessageLookupByLibrary.simpleMessage("On Time"),
        "FlightInfoPageViewReached":
            MessageLookupByLibrary.simpleMessage("Reached"),
        "FlightInfoPageViewTitle":
            MessageLookupByLibrary.simpleMessage("Flight Detail"),
        "FlightJudgmentATIS": MessageLookupByLibrary.simpleMessage("ATIS"),
        "FlightJudgmentAircraft":
            MessageLookupByLibrary.simpleMessage("Aircraft"),
        "FlightJudgmentApproach":
            MessageLookupByLibrary.simpleMessage("Approach"),
        "FlightJudgmentClearance":
            MessageLookupByLibrary.simpleMessage("Clearance"),
        "FlightJudgmentClimbing":
            MessageLookupByLibrary.simpleMessage("Climbing"),
        "FlightJudgmentCruising":
            MessageLookupByLibrary.simpleMessage("Cruising"),
        "FlightJudgmentDeparture":
            MessageLookupByLibrary.simpleMessage("Departure"),
        "FlightJudgmentDescending":
            MessageLookupByLibrary.simpleMessage("Descending"),
        "FlightJudgmentOnGround":
            MessageLookupByLibrary.simpleMessage("On Ground"),
        "FlightJudgmentTaxing": MessageLookupByLibrary.simpleMessage("Taxing"),
        "FlightJudgmentTower": MessageLookupByLibrary.simpleMessage("Tower"),
        "FlightPlanWidgetClear": MessageLookupByLibrary.simpleMessage("Clear"),
        "FlightPlanWidgetCopyRoute":
            MessageLookupByLibrary.simpleMessage("Generate!"),
        "FlightPlanWidgetEstimate":
            MessageLookupByLibrary.simpleMessage("Estimate"),
        "FplMode": MessageLookupByLibrary.simpleMessage("Fpl Source"),
        "HistoryTravelListTitle":
            MessageLookupByLibrary.simpleMessage("History Travel List"),
        "Inbound": MessageLookupByLibrary.simpleMessage("Inbound"),
        "IndexPageViewAltitude":
            MessageLookupByLibrary.simpleMessage("Altitude"),
        "IndexPageViewHeading": MessageLookupByLibrary.simpleMessage("Heading"),
        "IndexPageViewSubmit": MessageLookupByLibrary.simpleMessage("Submit"),
        "IndexPageViewVs": MessageLookupByLibrary.simpleMessage("VS"),
        "InfiniteFlightAchievementAnalyze":
            MessageLookupByLibrary.simpleMessage(
                "Infinite Flight User Data Analyze"),
        "InfiniteFlightAchievementTitle":
            MessageLookupByLibrary.simpleMessage("IF Achievement"),
        "LogOut": MessageLookupByLibrary.simpleMessage("Log out"),
        "MenuListAbout": MessageLookupByLibrary.simpleMessage("About"),
        "MenuListAircraftAdvice":
            MessageLookupByLibrary.simpleMessage("Aircraft Advice"),
        "MenuListAirport": MessageLookupByLibrary.simpleMessage("Airport"),
        "MenuListAviationNews": MessageLookupByLibrary.simpleMessage("IF News"),
        "MenuListChart": MessageLookupByLibrary.simpleMessage("Charts & FCTM"),
        "MenuListDrawFpl": MessageLookupByLibrary.simpleMessage("Draw Fpl"),
        "MenuListFgsBoard": MessageLookupByLibrary.simpleMessage("FGS Board"),
        "MenuListFlightATC": MessageLookupByLibrary.simpleMessage("ATC"),
        "MenuListFlightFFP":
            MessageLookupByLibrary.simpleMessage("Airline FFP"),
        "MenuListFlightIfAchievement":
            MessageLookupByLibrary.simpleMessage("Achievement"),
        "MenuListFlightList":
            MessageLookupByLibrary.simpleMessage("Flight List"),
        "MenuListMap": MessageLookupByLibrary.simpleMessage("Map"),
        "MenuListPlan": MessageLookupByLibrary.simpleMessage("Plan"),
        "Outbound": MessageLookupByLibrary.simpleMessage("Outbound"),
        "Pilot": MessageLookupByLibrary.simpleMessage("Pilot"),
        "Screen": MessageLookupByLibrary.simpleMessage("Screen"),
        "SettingPageViewCheckUpdate":
            MessageLookupByLibrary.simpleMessage("Check Update"),
        "SettingPageViewFingerprint":
            MessageLookupByLibrary.simpleMessage("Fingerprint"),
        "SettingPageViewFixApp":
            MessageLookupByLibrary.simpleMessage("Fix App"),
        "SettingPageViewIniLang":
            MessageLookupByLibrary.simpleMessage("English"),
        "SettingPageViewThemeMode":
            MessageLookupByLibrary.simpleMessage("Dark Mode"),
        "SettingPageViewTitle": MessageLookupByLibrary.simpleMessage("Setting"),
        "SettingPageViewUpdateData":
            MessageLookupByLibrary.simpleMessage("Update Data"),
        "Unknown": MessageLookupByLibrary.simpleMessage("Unknown"),
        "Update": MessageLookupByLibrary.simpleMessage("Update"),
        "UserPrivatePrivacy":
            MessageLookupByLibrary.simpleMessage("User Private Privacy"),
        "Visibility": MessageLookupByLibrary.simpleMessage("Visibility"),
        "WindDirection": MessageLookupByLibrary.simpleMessage("Wind Direction"),
        "WindSpeed": MessageLookupByLibrary.simpleMessage("Wind Speed"),
        "flew": MessageLookupByLibrary.simpleMessage("flew"),
        "indexPageViewSignalOffline":
            MessageLookupByLibrary.simpleMessage("Offline"),
        "indexPageViewSignalOnline":
            MessageLookupByLibrary.simpleMessage("Online"),
        "indexPageViewSpeed": MessageLookupByLibrary.simpleMessage("Speed")
      };
}
