// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Online`
  String get indexPageViewSignalOnline {
    return Intl.message(
      'Online',
      name: 'indexPageViewSignalOnline',
      desc: '',
      args: [],
    );
  }

  /// `Offline`
  String get indexPageViewSignalOffline {
    return Intl.message(
      'Offline',
      name: 'indexPageViewSignalOffline',
      desc: '',
      args: [],
    );
  }

  /// `Speed`
  String get indexPageViewSpeed {
    return Intl.message(
      'Speed',
      name: 'indexPageViewSpeed',
      desc: '',
      args: [],
    );
  }

  /// `Altitude`
  String get IndexPageViewAltitude {
    return Intl.message(
      'Altitude',
      name: 'IndexPageViewAltitude',
      desc: '',
      args: [],
    );
  }

  /// `VS`
  String get IndexPageViewVs {
    return Intl.message(
      'VS',
      name: 'IndexPageViewVs',
      desc: '',
      args: [],
    );
  }

  /// `Heading`
  String get IndexPageViewHeading {
    return Intl.message(
      'Heading',
      name: 'IndexPageViewHeading',
      desc: '',
      args: [],
    );
  }

  /// `Submit`
  String get IndexPageViewSubmit {
    return Intl.message(
      'Submit',
      name: 'IndexPageViewSubmit',
      desc: '',
      args: [],
    );
  }

  /// `Log out`
  String get LogOut {
    return Intl.message(
      'Log out',
      name: 'LogOut',
      desc: '',
      args: [],
    );
  }

  /// `Airline`
  String get Airline {
    return Intl.message(
      'Airline',
      name: 'Airline',
      desc: '',
      args: [],
    );
  }

  /// `Pilot`
  String get Pilot {
    return Intl.message(
      'Pilot',
      name: 'Pilot',
      desc: '',
      args: [],
    );
  }

  /// `Already`
  String get Already {
    return Intl.message(
      'Already',
      name: 'Already',
      desc: '',
      args: [],
    );
  }

  /// `flew`
  String get flew {
    return Intl.message(
      'flew',
      name: 'flew',
      desc: '',
      args: [],
    );
  }

  /// `Altitude`
  String get Altitude {
    return Intl.message(
      'Altitude',
      name: 'Altitude',
      desc: '',
      args: [],
    );
  }

  /// `Have flown`
  String get AlreadyFlownRange {
    return Intl.message(
      'Have flown',
      name: 'AlreadyFlownRange',
      desc: '',
      args: [],
    );
  }

  /// `Have been flying for`
  String get AlreadyFlownTime {
    return Intl.message(
      'Have been flying for',
      name: 'AlreadyFlownTime',
      desc: '',
      args: [],
    );
  }

  /// `Unknown`
  String get Unknown {
    return Intl.message(
      'Unknown',
      name: 'Unknown',
      desc: '',
      args: [],
    );
  }

  /// `Clear`
  String get FlightPlanWidgetClear {
    return Intl.message(
      'Clear',
      name: 'FlightPlanWidgetClear',
      desc: '',
      args: [],
    );
  }

  /// `Flight Detail`
  String get FlightInfoPageViewTitle {
    return Intl.message(
      'Flight Detail',
      name: 'FlightInfoPageViewTitle',
      desc: '',
      args: [],
    );
  }

  /// `Reached`
  String get FlightInfoPageViewReached {
    return Intl.message(
      'Reached',
      name: 'FlightInfoPageViewReached',
      desc: '',
      args: [],
    );
  }

  /// `Arrived`
  String get FlightInfoPageViewArrived {
    return Intl.message(
      'Arrived',
      name: 'FlightInfoPageViewArrived',
      desc: '',
      args: [],
    );
  }

  /// `On Time`
  String get FlightInfoPageViewOnTime {
    return Intl.message(
      'On Time',
      name: 'FlightInfoPageViewOnTime',
      desc: '',
      args: [],
    );
  }

  /// `Departure`
  String get FlightInfoPageViewDeparture {
    return Intl.message(
      'Departure',
      name: 'FlightInfoPageViewDeparture',
      desc: '',
      args: [],
    );
  }

  /// `Arrival`
  String get FlightInfoPageViewArrival {
    return Intl.message(
      'Arrival',
      name: 'FlightInfoPageViewArrival',
      desc: '',
      args: [],
    );
  }

  /// `Estimate`
  String get FlightPlanWidgetEstimate {
    return Intl.message(
      'Estimate',
      name: 'FlightPlanWidgetEstimate',
      desc: '',
      args: [],
    );
  }

  /// `Follow this flight`
  String get FlightInfoPageViewFollowThisFlight {
    return Intl.message(
      'Follow this flight',
      name: 'FlightInfoPageViewFollowThisFlight',
      desc: '',
      args: [],
    );
  }

  /// `Generate!`
  String get FlightPlanWidgetCopyRoute {
    return Intl.message(
      'Generate!',
      name: 'FlightPlanWidgetCopyRoute',
      desc: '',
      args: [],
    );
  }

  /// `Map`
  String get MenuListMap {
    return Intl.message(
      'Map',
      name: 'MenuListMap',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get MenuListAbout {
    return Intl.message(
      'About',
      name: 'MenuListAbout',
      desc: '',
      args: [],
    );
  }

  /// `Plan`
  String get MenuListPlan {
    return Intl.message(
      'Plan',
      name: 'MenuListPlan',
      desc: '',
      args: [],
    );
  }

  /// `Airport`
  String get MenuListAirport {
    return Intl.message(
      'Airport',
      name: 'MenuListAirport',
      desc: '',
      args: [],
    );
  }

  /// `Charts & FCTM`
  String get MenuListChart {
    return Intl.message(
      'Charts & FCTM',
      name: 'MenuListChart',
      desc: '',
      args: [],
    );
  }

  /// `Flight List`
  String get MenuListFlightList {
    return Intl.message(
      'Flight List',
      name: 'MenuListFlightList',
      desc: '',
      args: [],
    );
  }

  /// `ATC`
  String get MenuListFlightATC {
    return Intl.message(
      'ATC',
      name: 'MenuListFlightATC',
      desc: '',
      args: [],
    );
  }

  /// `Achievement`
  String get MenuListFlightIfAchievement {
    return Intl.message(
      'Achievement',
      name: 'MenuListFlightIfAchievement',
      desc: '',
      args: [],
    );
  }

  /// `Draw Fpl`
  String get MenuListDrawFpl {
    return Intl.message(
      'Draw Fpl',
      name: 'MenuListDrawFpl',
      desc: '',
      args: [],
    );
  }

  /// `Airline FFP`
  String get MenuListFlightFFP {
    return Intl.message(
      'Airline FFP',
      name: 'MenuListFlightFFP',
      desc: '',
      args: [],
    );
  }

  /// `Aircraft Advice`
  String get MenuListAircraftAdvice {
    return Intl.message(
      'Aircraft Advice',
      name: 'MenuListAircraftAdvice',
      desc: '',
      args: [],
    );
  }

  /// `IF News`
  String get MenuListAviationNews {
    return Intl.message(
      'IF News',
      name: 'MenuListAviationNews',
      desc: '',
      args: [],
    );
  }

  /// `FGS Board`
  String get MenuListFgsBoard {
    return Intl.message(
      'FGS Board',
      name: 'MenuListFgsBoard',
      desc: '',
      args: [],
    );
  }

  /// `Aero Master`
  String get AboutMeFragmentAeroMaster {
    return Intl.message(
      'Aero Master',
      name: 'AboutMeFragmentAeroMaster',
      desc: '',
      args: [],
    );
  }

  /// `Favorite Route`
  String get AboutMeFragmentFavoriteRoute {
    return Intl.message(
      'Favorite Route',
      name: 'AboutMeFragmentFavoriteRoute',
      desc: '',
      args: [],
    );
  }

  /// `History`
  String get AboutMeFragmentHistory {
    return Intl.message(
      'History',
      name: 'AboutMeFragmentHistory',
      desc: '',
      args: [],
    );
  }

  /// `FFP Card`
  String get AboutMeFragmentFFP {
    return Intl.message(
      'FFP Card',
      name: 'AboutMeFragmentFFP',
      desc: '',
      args: [],
    );
  }

  /// `Card Pack`
  String get AboutMeFragmentCardPack {
    return Intl.message(
      'Card Pack',
      name: 'AboutMeFragmentCardPack',
      desc: '',
      args: [],
    );
  }

  /// `My Achievement`
  String get AchievementTitleMyAchievement {
    return Intl.message(
      'My Achievement',
      name: 'AchievementTitleMyAchievement',
      desc: '',
      args: [],
    );
  }

  /// `Current Flight`
  String get AchievementTabCurrentFlight {
    return Intl.message(
      'Current Flight',
      name: 'AchievementTabCurrentFlight',
      desc: '',
      args: [],
    );
  }

  /// `History Flight`
  String get AchievementTabHistoryFlight {
    return Intl.message(
      'History Flight',
      name: 'AchievementTabHistoryFlight',
      desc: '',
      args: [],
    );
  }

  /// `Range Achievement`
  String get AchievementRangeAchievement {
    return Intl.message(
      'Range Achievement',
      name: 'AchievementRangeAchievement',
      desc: '',
      args: [],
    );
  }

  /// `No Current Flight`
  String get AchievementNoCurrentFlight {
    return Intl.message(
      'No Current Flight',
      name: 'AchievementNoCurrentFlight',
      desc: '',
      args: [],
    );
  }

  /// `Setting`
  String get SettingPageViewTitle {
    return Intl.message(
      'Setting',
      name: 'SettingPageViewTitle',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get SettingPageViewIniLang {
    return Intl.message(
      'English',
      name: 'SettingPageViewIniLang',
      desc: '',
      args: [],
    );
  }

  /// `Dark Mode`
  String get SettingPageViewThemeMode {
    return Intl.message(
      'Dark Mode',
      name: 'SettingPageViewThemeMode',
      desc: '',
      args: [],
    );
  }

  /// `Fingerprint`
  String get SettingPageViewFingerprint {
    return Intl.message(
      'Fingerprint',
      name: 'SettingPageViewFingerprint',
      desc: '',
      args: [],
    );
  }

  /// `Check Update`
  String get SettingPageViewCheckUpdate {
    return Intl.message(
      'Check Update',
      name: 'SettingPageViewCheckUpdate',
      desc: '',
      args: [],
    );
  }

  /// `Fix App`
  String get SettingPageViewFixApp {
    return Intl.message(
      'Fix App',
      name: 'SettingPageViewFixApp',
      desc: '',
      args: [],
    );
  }

  /// `Update Data`
  String get SettingPageViewUpdateData {
    return Intl.message(
      'Update Data',
      name: 'SettingPageViewUpdateData',
      desc: '',
      args: [],
    );
  }

  /// `Fix`
  String get Fix {
    return Intl.message(
      'Fix',
      name: 'Fix',
      desc: '',
      args: [],
    );
  }

  /// `Update`
  String get Update {
    return Intl.message(
      'Update',
      name: 'Update',
      desc: '',
      args: [],
    );
  }

  /// `Setting`
  String get DrawerMenuListSetting {
    return Intl.message(
      'Setting',
      name: 'DrawerMenuListSetting',
      desc: '',
      args: [],
    );
  }

  /// `History Travel List`
  String get HistoryTravelListTitle {
    return Intl.message(
      'History Travel List',
      name: 'HistoryTravelListTitle',
      desc: '',
      args: [],
    );
  }

  /// `On Ground`
  String get FlightJudgmentOnGround {
    return Intl.message(
      'On Ground',
      name: 'FlightJudgmentOnGround',
      desc: '',
      args: [],
    );
  }

  /// `Climbing`
  String get FlightJudgmentClimbing {
    return Intl.message(
      'Climbing',
      name: 'FlightJudgmentClimbing',
      desc: '',
      args: [],
    );
  }

  /// `Cruising`
  String get FlightJudgmentCruising {
    return Intl.message(
      'Cruising',
      name: 'FlightJudgmentCruising',
      desc: '',
      args: [],
    );
  }

  /// `Taxing`
  String get FlightJudgmentTaxing {
    return Intl.message(
      'Taxing',
      name: 'FlightJudgmentTaxing',
      desc: '',
      args: [],
    );
  }

  /// `Descending`
  String get FlightJudgmentDescending {
    return Intl.message(
      'Descending',
      name: 'FlightJudgmentDescending',
      desc: '',
      args: [],
    );
  }

  /// `Approach`
  String get FlightJudgmentApproach {
    return Intl.message(
      'Approach',
      name: 'FlightJudgmentApproach',
      desc: '',
      args: [],
    );
  }

  /// `Departure`
  String get FlightJudgmentDeparture {
    return Intl.message(
      'Departure',
      name: 'FlightJudgmentDeparture',
      desc: '',
      args: [],
    );
  }

  /// `Clearance`
  String get FlightJudgmentClearance {
    return Intl.message(
      'Clearance',
      name: 'FlightJudgmentClearance',
      desc: '',
      args: [],
    );
  }

  /// `Tower`
  String get FlightJudgmentTower {
    return Intl.message(
      'Tower',
      name: 'FlightJudgmentTower',
      desc: '',
      args: [],
    );
  }

  /// `Aircraft`
  String get FlightJudgmentAircraft {
    return Intl.message(
      'Aircraft',
      name: 'FlightJudgmentAircraft',
      desc: '',
      args: [],
    );
  }

  /// `ATIS`
  String get FlightJudgmentATIS {
    return Intl.message(
      'ATIS',
      name: 'FlightJudgmentATIS',
      desc: '',
      args: [],
    );
  }

  /// `Infinite Flight User Data Analyze`
  String get InfiniteFlightAchievementAnalyze {
    return Intl.message(
      'Infinite Flight User Data Analyze',
      name: 'InfiniteFlightAchievementAnalyze',
      desc: '',
      args: [],
    );
  }

  /// `IF Achievement`
  String get InfiniteFlightAchievementTitle {
    return Intl.message(
      'IF Achievement',
      name: 'InfiniteFlightAchievementTitle',
      desc: '',
      args: [],
    );
  }

  /// `User Private Privacy`
  String get UserPrivatePrivacy {
    return Intl.message(
      'User Private Privacy',
      name: 'UserPrivatePrivacy',
      desc: '',
      args: [],
    );
  }

  /// `Outbound`
  String get Outbound {
    return Intl.message(
      'Outbound',
      name: 'Outbound',
      desc: '',
      args: [],
    );
  }

  /// `Inbound`
  String get Inbound {
    return Intl.message(
      'Inbound',
      name: 'Inbound',
      desc: '',
      args: [],
    );
  }

  /// `Screen`
  String get Screen {
    return Intl.message(
      'Screen',
      name: 'Screen',
      desc: '',
      args: [],
    );
  }

  /// `Visibility`
  String get Visibility {
    return Intl.message(
      'Visibility',
      name: 'Visibility',
      desc: '',
      args: [],
    );
  }

  /// `Wind Speed`
  String get WindSpeed {
    return Intl.message(
      'Wind Speed',
      name: 'WindSpeed',
      desc: '',
      args: [],
    );
  }

  /// `Wind Direction`
  String get WindDirection {
    return Intl.message(
      'Wind Direction',
      name: 'WindDirection',
      desc: '',
      args: [],
    );
  }

  /// `Airport Advice`
  String get AirportAdvice {
    return Intl.message(
      'Airport Advice',
      name: 'AirportAdvice',
      desc: '',
      args: [],
    );
  }

  /// `Airport`
  String get Airport {
    return Intl.message(
      'Airport',
      name: 'Airport',
      desc: '',
      args: [],
    );
  }

  /// `Fpl Source`
  String get FplMode {
    return Intl.message(
      'Fpl Source',
      name: 'FplMode',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'zh'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
