import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';

enum ServerType { prod, local, dev, wifi }

abstract class AppVersionInfo {
  static const appVersion = '1.2.0';
}

abstract class IFServerMap {
  static final serverMap = {
    0: 'Expert Server',
    1: 'Training Server',
    2: 'Casual Server',
  };
}

abstract class FgsServer {
  static final BASE_URL = 'http://10.73.243.135:529/FGS_MiddleWare/';
}

abstract class KeyManager {
  static const amapGeoKey = 'ff46edce02ee0bc7dfa81f11f5ca80a2';
  static const infiniteFlightApiKey = 'vscogyri4dqs9s7rmevka7fc8ypj13zu';
}

abstract class HttpConfig {
  static const encryptList = [
    '/infiniteFlight/fetchOnlinePlan',
    '/infiniteFlight/airwayLine',
    '/infiniteFlight/getAllServerFlight',
    '/user/fingerprintLogin',
  ];
  static const tokenWhiteList = [
    '/user/resetPassword',
    '/user/application',
    '/user/sendVerifyCode',
    '/user/login',
    '/user/fingerprintLogin',
    '/common/getAppInfo',
    '/fms/uploadAirportCharts',
  ];
}

abstract class OwnAircraftPhotoList {
  /// After StartActivityEventHandler start this variable will be rewrite
  static var ownAircraftPhotoList = <String>[];
}

abstract class NativeMethodName {
  static const androidPlatformChannelName = MethodChannel('com.example.ifgs/android');
  static const String showToast = 'showToast';
  static const String getBatteryInfo = 'getBatteryInfo';
  static const String getCpuInfo = 'getCpuInfo';
  static const String getWifiSSID = 'getWIFISSID';
}

abstract class Links {
  static const String airlineIconUrl = 'https://www.flightradar24.com/static/images/data/operators/';
  static const String fgsStorageBaseUrl = 'https://fgs-1300812707.cos.ap-shanghai.myqcloud.com/';
  static const String aircraftPhotoBaseLink = 'https://cdn.liveflightapp.com/aircraft-images/';
  static const String unknownAircraft = 'https://www.cloudpassage.com/wp-content/uploads/2020/01/CloudTrail.jpg';
  static const String testflightSchemeUrl = 'itms-beta://';
  static const String wechatSchemeUrl = 'weixin://';
  static const String androidReleaseApkUrl = 'https://fgs-1300812707.cos.ap-shanghai.myqcloud.com/Apk/app-release.apk';
  static const String flightPlanIdUrl = 'https://api.flightplandatabase.com/auto/generate';
  static const String flightPlanApiKey = 'ZN05FcLi82bBJOySk0FkTpuk6NsNiCFdEkqT8iQY';
  static const String flightPlanDetail = 'https://api.flightplandatabase.com/plan/';
  static const String osmMapUrl =
      'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic2VhbjE5OTciLCJhIjoiY2tudmhudDBqMG1zbDJvcm1tM2VhcGJ2bSJ9.N3MiY_b4mfA98t_XLfFvGw';

  static String getOsmTheme(OsmMapStyle osmMapType) {
    final typeMap = {
      OsmMapStyle.streets: 'mapbox/streets-v11',
      OsmMapStyle.light: 'mapbox/light-v10',
      OsmMapStyle.dark: 'mapbox/dark-v10',
      OsmMapStyle.satellite: 'mapbox/satellite-v9',
    };
    return typeMap[osmMapType] ?? 'mapbox/satellite-v9';
  }

  static String getSessionId(int serverType) {
    final serverTypeMap = {
      0: 'df2a8d19-3a54-4ce5-ae65-0b722186e44c',
      1: '45173539-5080-4c95-9b93-a24713d96ec8',
      2: 'd01006e4-3114-473c-8f69-020b89d02884'
    };
    return serverTypeMap[serverType] ?? 'df2a8d19-3a54-4ce5-ae65-0b722186e44c';
  }
}

enum OsmMapStyle {
  streets,
  light,
  dark,
  satellite,
}

enum InfiniteFlightServerType { expert, training, casual }

enum FileBizType { avatar, airport, fctm }

abstract class ServerName {
  static final serverName = {
    'Expert Server': 0,
    'Training Server': 1,
    'Casual Server': 2,
  };
}

abstract class InfiniteFlightAtcType {
  static final type = {
    0: S.current.FlightJudgmentOnGround,
    1: S.current.FlightJudgmentTower,
    2: 'Unicom',
    3: S.current.FlightJudgmentClearance,
    4: S.current.FlightJudgmentApproach,
    5: S.current.FlightJudgmentDeparture,
    6: S.current.FlightJudgmentAircraft,
    7: S.current.FlightJudgmentATIS,
    8: 'Recorded',
    9: S.current.Unknown,
    10: 'Unused'
  };
}

abstract class RouterConfig {
  static final wipRouteList = [RouterSheet.flightPlanGenerator];
  static final platformUnSupportList = [RouterSheet.aviationNews];
}

class InfiniteFlightAtcTypeColor {
  static final colorType = {
    0: Colors.amberAccent,
    1: Colors.blue,
    2: Colors.redAccent,
    3: Colors.greenAccent,
    4: Colors.deepOrangeAccent,
    5: Colors.greenAccent,
    6: Colors.cyan,
    7: Colors.blue,
    8: Colors.blue,
    9: Colors.red,
    10: Colors.red
  };
}

abstract class ProblematicalData {
  static List<Map<String, String>> problematicalUsernameListMap = [
    {'FGS-Rainbow': 'FGS-0554-Keaton'}
  ];

  static List<String> get problemNameList {
    return ['FGS-0554-Keaton'];
  }
}
