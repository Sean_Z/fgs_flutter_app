class AirlineInfo {
  static List<Map<String, String>> airlineInfo = [
    {'airline_name': 'Airbus - Old', 'icao_code': 'AIB'},
    {'airline_name': 'Airbus - New', 'icao_code': 'AIB'},
    {'airline_name': 'Airbus', 'icao_code': 'AIB'},
    {'airline_name': 'Airbus Factory Carbon', 'icao_code': 'AIB'},
    {'airline_name': 'American Airlines', 'icao_code': 'AAL'},
    {'airline_name': 'American Eagle', 'icao_code': 'AAL'},
    {'airline_name': 'American Airlines - Old', 'icao_code': 'AAL'},
    {'airline_name': 'American Airlines - 2013', 'icao_code': 'AAL'},
    {'airline_name': 'American Airlines - 1968', 'icao_code': 'AAL'},
    {'airline_name': 'American Airlines - One World', 'icao_code': 'AAL'},
    {'airline_name': 'Delta Air Lines', 'icao_code': 'DAL'},
    {'airline_name': 'United Airlines', 'icao_code': 'UAL'},
    {'airline_name': 'United Express', 'icao_code': 'UAL'},
    {'airline_name': 'Southwest - Canyon Blue', 'icao_code': 'SWA'},
    {'airline_name': 'Southwest - Desert Gold', 'icao_code': 'SWA'},
    {'airline_name': 'Southwest - Heart', 'icao_code': 'SWA'},
    {'airline_name': 'Southwest - Illinois One', 'icao_code': 'SWA'},
    {'airline_name': 'Southwest - Shamu', 'icao_code': 'SWA'},
    {'airline_name': 'China Southern', 'icao_code': 'CSN'},
    {'airline_name': 'China Eastern', 'icao_code': 'CES'},
    {'airline_name': 'China Eastern Airlines', 'icao_code': 'CES'},
    {'airline_name': 'China Cargo', 'icao_code': 'CKK'},
    {'airline_name': 'SkyWest Airlines', 'icao_code': 'SKW'},
    {'airline_name': 'China Express Airlines', 'icao_code': 'HXA'},
    {'airline_name': 'Aer Lingus', 'icao_code': 'EIN'},
    {'airline_name': 'Swiss International Air Lines', 'icao_code': 'SWR'},
    {'airline_name': 'Air Transat', 'icao_code': 'TSC'},
    {'airline_name': 'Virgin Atlantic', 'icao_code': 'VIR'},
    {'airline_name': 'Thai International Airways', 'icao_code': 'THA'},
    {'airline_name': 'Scoot', 'icao_code': 'TGW'},
    {'airline_name': 'KLM - 2015', 'icao_code': 'KLM'},
    {'airline_name': 'Nippon Cargo', 'icao_code': 'NCA'},
    {'airline_name': 'FrenchBee', 'icao_code': 'FBU'},
    {'airline_name': 'Air China', 'icao_code': 'CCA'},
    {'airline_name': 'Federal Express', 'icao_code': 'FDX'},
    {'airline_name': 'Ryanair', 'icao_code': 'RYR'},
    {'airline_name': 'Expressjet', 'icao_code': 'BTA'},
    {'airline_name': 'Turkish Airlines', 'icao_code': 'THY'},
    {'airline_name': 'Lufthansa Cargo', 'icao_code': 'GEC'},
    {'airline_name': 'Lufthansa', 'icao_code': 'GEC'},
    {'airline_name': 'Lufthansa - 2018', 'icao_code': 'GEC'},
    {'airline_name': 'S7', 'icao_code': 'SBI'},
    {'airline_name': 'British Airways', 'icao_code': 'BAW'},
    {'airline_name': 'Emirates', 'icao_code': 'UAE'},
    {'airline_name': 'Emirates Cargo', 'icao_code': 'UAE'},
    {'airline_name': 'UPS', 'icao_code': 'UPS'},
    {'airline_name': 'Easyjet', 'icao_code': 'EZY'},
    {'airline_name': 'Air France', 'icao_code': 'AFR'},
    {'airline_name': 'Air France - SkyTeam', 'icao_code': 'AFR'},
    {'airline_name': 'JetBlue', 'icao_code': 'JBU'},
    {'airline_name': 'ANA', 'icao_code': 'ANA'},
    {'airline_name': 'Qatar Airways', 'icao_code': 'QTR'},
    {'airline_name': 'Qatar', 'icao_code': 'QTR'},
    {'airline_name': 'Envoy Air Inc.', 'icao_code': 'ENY'},
    {'airline_name': 'Aeroflot', 'icao_code': 'AFL'},
    {'airline_name': 'Shenzhen Airlines', 'icao_code': 'CSZ'},
    {'airline_name': 'Air Canada', 'icao_code': 'ACA'},
    {'airline_name': 'Air Canada Express', 'icao_code': 'ACA'},
    {'airline_name': 'TAM', 'icao_code': 'TAM'},
    {'airline_name': 'Japan Airlines', 'icao_code': 'JAL'},
    {'airline_name': 'Korean Air', 'icao_code': 'KAL'},
    {'airline_name': 'Korean Airlines', 'icao_code': 'KAL'},
    {'airline_name': 'Korean Air Cargo', 'icao_code': 'KAL'},
    {'airline_name': 'Saudia', 'icao_code': 'SVA'},
    {'airline_name': 'Qatar Cargo', 'icao_code': 'QTR'},
    {'airline_name': 'Air Tahiti Nui', 'icao_code': 'THT'},
    {'airline_name': 'United Airlines - Old', 'icao_code': 'UAL'},
    {'airline_name': 'Alaska Airlines', 'icao_code': 'ASA'},
    {'airline_name': 'Avianca', 'icao_code': 'AVA'},
    {'airline_name': 'Transavia', 'icao_code': 'TRA'},
    {'airline_name': 'Hainan Airlines', 'icao_code': 'CHH'},
    {'airline_name': 'SAS', 'icao_code': 'SAS'},
    {'airline_name': 'Garuda Indonesia', 'icao_code': 'GIA'},
    {'airline_name': 'Cathay Pacific', 'icao_code': 'CPA'},
    {'airline_name': 'Republic Airlines', 'icao_code': 'RPA'},
    {'airline_name': 'Saudi Arabian Airlines', 'icao_code': 'SVA'},
    {'airline_name': 'Azul Brazilian Airlines', 'icao_code': 'AZU'},
    {'airline_name': 'Xiamen Airlines', 'icao_code': 'CXA'},
    {'airline_name': 'VRG Linhas Aereas S.A. - Grupo GOL', 'icao_code': 'GLO'},
    {'airline_name': 'Jazz Aviation LP', 'icao_code': 'JZA'},
    {'airline_name': 'Etihad', 'icao_code': 'ETD'},
    {'airline_name': 'Etihad Airways', 'icao_code': 'ETD'},
    {'airline_name': 'LAN', 'icao_code': 'LAN'},
    {'airline_name': 'LAN Cargo', 'icao_code': 'LAN'},
    {'airline_name': 'LATAM', 'icao_code': 'LAN'},
    {'airline_name': 'Endeavor Air', 'icao_code': 'FLG'},
    {'airline_name': 'Aruba Airlines', 'icao_code': 'ARU'},
    {'airline_name': 'Mesa Airlines, Inc.', 'icao_code': 'ASH'},
    {'airline_name': 'Qantas', 'icao_code': 'QFA'},
    {'airline_name': 'EVA Air', 'icao_code': 'EVA'},
    {'airline_name': 'MexicanaLink', 'icao_code': 'AMX'},
    {'airline_name': 'Dragonair', 'icao_code': 'HDA'},
    {'airline_name': 'GOL', 'icao_code': 'GLO'},
    {'airline_name': 'Spicejet', 'icao_code': 'SEJ'},
    {'airline_name': 'Bombardier', 'icao_code': 'AIB'},
    {'airline_name': 'Alaska Airlines - 2016', 'icao_code': 'ASA'},
    {'airline_name': 'GOL - 2018', 'icao_code': 'GLO'},
    {'airline_name': 'GOL+', 'icao_code': 'GLO'},
    {'airline_name': 'Pakistan International Airlines', 'icao_code': 'PIA'},
    {'airline_name': 'Copa Airlines', 'icao_code': 'CMP'},
    {'airline_name': 'South African Airways', 'icao_code': 'SAA'},
    {'airline_name': 'Air Caraibes', 'icao_code': 'FWI'},
    {'airline_name': 'Air Mauritius', 'icao_code': 'MAU'},
    {'airline_name': 'Norwegian Air Shuttle', 'icao_code': 'NAX'},
    {'airline_name': 'Air Bridge Cargo', 'icao_code': 'ORC'},
    {'airline_name': 'Thomson', 'icao_code': 'TUI'},
    {'airline_name': 'Egypt Air', 'icao_code': 'MSR'},
    {'airline_name': 'Lion Air', 'icao_code': 'LNI'},
    {'airline_name': 'WestJet', 'icao_code': 'WJA'},
    {'airline_name': 'KLM', 'icao_code': 'KLM'},
    {'airline_name': 'KLM - Orange Pride', 'icao_code': 'KLM'},
    {'airline_name': 'Nordwind Airlines', 'icao_code': 'NWS'},
    {'airline_name': 'Austrian Airlines', 'icao_code': 'AUA'},
    {'airline_name': 'Lufthansa Cityline', 'icao_code': 'DLH'},
    {'airline_name': 'Lufthansa 2018', 'icao_code': 'DLH'},
    {'airline_name': 'British Airways - Comair', 'icao_code': 'BAW'},
    {'airline_name': 'AeroLogic', 'icao_code': 'BOX'},
    {'airline_name': 'JAL Express', 'icao_code': 'JAL'},
    {'airline_name': 'Sriwijaya Air', 'icao_code': 'SJY'},
    {'airline_name': 'Singapore Airlines', 'icao_code': 'SIA'},
    {'airline_name': 'Singapore', 'icao_code': 'SIA'},
    {'airline_name': 'PSA Airlines', 'icao_code': 'JIA'},
    {'airline_name': 'Air India', 'icao_code': 'AIC'},
    {'airline_name': 'Sichuan Airlines', 'icao_code': 'CSC'},
    {'airline_name': 'Interglobe Aviation Ltd. dba Indigo', 'icao_code': 'IGO'},
    {'airline_name': 'Boeing', 'icao_code': 'BOE'},
    {'airline_name': 'Vueling', 'icao_code': 'VLG'},
    {'airline_name': 'Alitalia', 'icao_code': 'AZA'},
    {'airline_name': 'AVIANCA', 'icao_code': 'AVA'},
    {'airline_name': 'Air New Zealand', 'icao_code': 'ANZ'},
    {'airline_name': 'Air New Zealand - All Blacks', 'icao_code': 'ANZ'},
    {'airline_name': 'Iran Air', 'icao_code': 'IRA'},
    {'airline_name': 'Royal Air Maroc', 'icao_code': 'RAM'},
    {'airline_name': 'Korean', 'icao_code': 'KAL'},
    {'airline_name': 'Cargolux', 'icao_code': 'CLX'},
    {'airline_name': 'Virgin Australia', 'icao_code': 'VAU'},
    {'airline_name': 'Virgin Atlantic Airways', 'icao_code': 'VIR'},
    {'airline_name': 'Shuttle America', 'icao_code': 'TCF'},
    {'airline_name': 'Shandong Airlines', 'icao_code': 'CDG'},
    {'airline_name': 'Jet Airways', 'icao_code': 'JAI'},
    {'airline_name': 'Tianjin Airlines', 'icao_code': 'GCR'},
    {'airline_name': 'Vietnam Airlines', 'icao_code': 'HVN'},
    {'airline_name': 'COPA Airlines', 'icao_code': 'CMP'},
    {'airline_name': 'Shanghai Airlines', 'icao_code': 'CSH'},
    {'airline_name': 'Air Berlin', 'icao_code': 'BER'},
    {'airline_name': 'Austrian', 'icao_code': 'AUA'},
    {'airline_name': 'Spirit Airlines', 'icao_code': 'NKS'},
    {'airline_name': 'Asiana Airlines', 'icao_code': 'AAR'},
    {'airline_name': 'China Airlines', 'icao_code': 'CAL'},
    {'airline_name': 'Allegiant Air LLC', 'icao_code': 'AAY'},
    {'airline_name': 'Malaysia Airlines', 'icao_code': 'MAS'},
    {'airline_name': 'Air Asia X', 'icao_code': 'XAX'},
    {'airline_name': 'Air Asia', 'icao_code': 'AXM'},
    {'airline_name': 'Thai Airways', 'icao_code': 'THA'},
    {'airline_name': 'Iberia', 'icao_code': 'IBE'},
    {'airline_name': 'flybe', 'icao_code': 'BEE'},
    {'airline_name': 'Ethiopian Airlines', 'icao_code': 'ETH'},
    {'airline_name': 'Jetstar Airways Pty Limited', 'icao_code': 'JST'},
    {'airline_name': 'Wizz Air Hungary Ltd.', 'icao_code': 'WZZ'},
    {'airline_name': 'Air Wisconsin Airlines Corporation (AWAC)', 'icao_code': 'AWI'},
    {'airline_name': 'Swiss', 'icao_code': 'SWR'},
    {'airline_name': 'Trans States Airlines, LLC', 'icao_code': 'LOF'},
    {'airline_name': 'EVA', 'icao_code': 'EVA'},
    {'airline_name': 'S7 Airlines', 'icao_code': 'SBI'},
    {'airline_name': 'Aeromexico', 'icao_code': 'AMX'},
    {'airline_name': 'Interjet', 'icao_code': 'AIJ'},
    {'airline_name': 'Aerolitoral S.A. de C.V.', 'icao_code': 'SLI'},
    {'airline_name': 'Capital Airlines', 'icao_code': 'CBJ'},
    {'airline_name': 'Pegasus Airlines', 'icao_code': 'PGT'},
    {'airline_name': 'UTair', 'icao_code': 'UTA'},
    {'airline_name': 'Thomson Airways Limited', 'icao_code': 'TUI'},
    {'airline_name': 'Thomson Cook', 'icao_code': 'TUI'},
    {'airline_name': 'Compass Airlines LLC', 'icao_code': 'CPZ'},
    {'airline_name': 'TAP', 'icao_code': 'TAP'},
    {'airline_name': 'Virgin America', 'icao_code': 'VRD'},
    {'airline_name': 'Volaris', 'icao_code': 'VOI'},
    {'airline_name': 'Germanwings GmbH', 'icao_code': 'GWI'},
    {'airline_name': 'Jet2', 'icao_code': 'EXS'},
    {'airline_name': 'TUI', 'icao_code': 'TUI'},
    {'airline_name': 'Philippine Airlines', 'icao_code': 'PAL'},
    {'airline_name': 'Peach Aviation', 'icao_code': 'APJ'},
    {'airline_name': 'Finnair', 'icao_code': 'FIN'},
    {'airline_name': 'Libyan Airlines', 'icao_code': 'LAA'},
    {'airline_name': 'Aer Lingus 2019', 'icao_code': 'EIN'},
    {'airline_name': 'Eurowings', 'icao_code': 'EWG'},
    {'airline_name': 'FedEx', 'icao_code': 'FDX'},
    {'airline_name': 'Fedex', 'icao_code': 'FDX'},
    {'airline_name': 'Spring Airlines Limited Corporation', 'icao_code': 'CQH'},
    {'airline_name': 'Frontier Airlines, Inc.', 'icao_code': 'FFT'},
    {'airline_name': 'Cebu Pacific Air', 'icao_code': 'CEB'},
    {'airline_name': 'Norwegian Air Shuttle A.S.', 'icao_code': 'NAX'}
  ];
}
