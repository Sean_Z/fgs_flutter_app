import 'package:ifgs/core/StartActivityEventHandler.dart';
import 'package:ifgs/request/modal/infinte_flight_airport_entity.dart';

import '../request/modal/AirlineJsonBean.dart';
import '../request/modal/infinite_flight_livery_res_entity.dart';

class StaticData {
  /// this data will be over write after [StartActivityEventHandler] start
  static var airlineJson = <AirlineJsonBean>[];
  static var aircraftJson = <InfiniteFlightLiveryResResult>[];
  static var airportStaticData = <InfiniteFlightAirportResult>[];
  static var osmFlightId = '';
}
