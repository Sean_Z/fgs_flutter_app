import 'package:ifgs/constant/StaticData.dart';

import 'ConfigData.dart';

class AirlineListMap {
  static String getIcaoCode(String airlineName) {
    var targetList = StaticData.airlineJson.where((element) => element.airlineName == airlineName).toList();
    if (targetList.isNotEmpty) {
      return Links.airlineIconUrl + targetList[0].icaoCode! + '_logo0.png';
    } else if (airlineName.contains('Infinite') || airlineName.contains('Generic')) {
      return '${Links.fgsStorageBaseUrl}profile/infinite.png';
    } else {
      return '${Links.fgsStorageBaseUrl}profile/ufo.png';
    }
  }
}
