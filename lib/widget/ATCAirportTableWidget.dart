import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/request/modal/InfiniteFlightAtcListBean.dart';
import 'package:ifgs/widget/PublicWidget/LineGradualLineWidget.dart';
import 'package:ifgs/widget/PublicWidget/SkeletonWidget.dart';

import 'PublicWidget/InsteadNoData.dart';

class ATCAirportTableWidget extends StatefulWidget {
  ATCAirportTableWidget({Key? key, this.atcList, this.shouldRefresh = false, required this.serverType}) : super(key: key);
  final List<InfiniteFlightAtcBeanResult>? atcList;
  final bool? shouldRefresh;
  final int serverType;

  @override
  _ATCAirportTableWidgetState createState() => _ATCAirportTableWidgetState();
}

class _ATCAirportTableWidgetState extends State<ATCAirportTableWidget> {
  /// 为refreshIndicator设置key来手动触发刷新
  final GlobalKey<RefreshIndicatorState> _indicatorKey = GlobalKey<RefreshIndicatorState>();

  /// 此刻是否查到数据
  var _isHasData = false;

  /// 是否结束查询
  var _isFinishedQry = false;

  /// ListView Controller
  late ScrollController _scrollController;

  /// 初始行
  late List<InfiniteFlightAtcBeanResult> _ATCRows;

  /// 渲染ATC列表
  Future<void> renderAtcListView() async {
    final _response = widget.atcList ?? await InfiniteFlightLiveDataApi.getATC(0);
    _isFinishedQry = true;
    if (_response.isNotEmpty) {
      _ATCRows = [..._response];
      setState(() {
        _isHasData = true;
      });
    } else {
      setState(() {
        _isHasData = false;
      });
    }
  }

  /// 刷新ATC状态
  Future<void> refresh() async => {
        if (_indicatorKey.currentState != null)
          {
            _indicatorKey.currentState!.show(),
          },
        await renderAtcListView(),
      };

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController(initialScrollOffset: 0.0, keepScrollOffset: true);
    renderAtcListView();
  }

  Widget AtcListView() {
    return _isHasData
        ? Container(
            height: Get.height,
            width: Get.width - 30,
            child: RefreshIndicator(
              key: _indicatorKey,
              onRefresh: () => refresh(),
              child: ListView.builder(
                  physics: const BouncingScrollPhysics(),
                  controller: _scrollController,
                  itemCount: _ATCRows.length,
                  addAutomaticKeepAlives: true,
                  itemBuilder: (context, i) {
                    return AtcSingleCardWidget(
                      time: _ATCRows[i].startTime,
                      type: _ATCRows[i].type,
                      airportName: _ATCRows[i].airportName,
                      atcName: _ATCRows[i].username,
                      organization: (_ATCRows[i].virtualOrganization ?? 'unknown') as String,
                      serverType: widget.serverType,
                    );
                  }),
            ),
          )
        : InkWell(
            onTap: () => refresh(),
            child: const InsteadNoData(
              content: '      No ATC \n click to refresh',
            ),
          );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: _isFinishedQry
              ? AtcListView()
              : Container(
                  margin: const EdgeInsets.all(10),
                  width: Get.width,
                  child: const SkeletonWidget(count: 20),
                )),
    );
  }
}

class AtcSingleCardWidget extends StatelessWidget {
  const AtcSingleCardWidget({
    Key? key,
    required this.time,
    required this.type,
    required this.airportName,
    required this.atcName,
    required this.organization,
    required this.serverType,
  }) : super(key: key);

  final String time;
  final int type;
  final String? airportName;
  final String atcName;
  final String? organization;
  final int serverType;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(10),
      onTap: () {
        Get.toNamed(RouterSheet.airportScreen, arguments: [airportName, serverType]);
      },
      child: Card(
        elevation: 4,
        shadowColor: InfiniteFlightAtcTypeColor.colorType[type],
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: const EdgeInsets.all(10),
                  child: CircleAvatar(
                    radius: 20,
                    child: Text(atcName.substring(0, 1)),
                  ),
                ),
                Container(margin: const EdgeInsets.all(10), child: Text(time, style: const TextStyle(color: Colors.blueGrey))),
                Container(
                  padding: const EdgeInsets.all(10),
                  margin: const EdgeInsets.only(right: 10),
                  decoration: BoxDecoration(
                      color: InfiniteFlightAtcTypeColor.colorType[type], borderRadius: const BorderRadius.all(Radius.circular(20))),
                  child: Text(InfiniteFlightAtcType.type[type]!, style: const TextStyle(color: Colors.white, fontSize: 12)),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const LineGradualLineWidget(isReverse: false),
                Text(airportName ?? 'unknown'),
                const LineGradualLineWidget(isReverse: true),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    margin: const EdgeInsets.all(10),
                    child: Text(atcName, style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w400))),
                Container(
                    margin: const EdgeInsets.all(10),
                    child: Text('Org: ${organization ?? 'unknown'}',
                        style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w400))),
              ],
            )
          ],
        ),
      ),
    );
  }
}
