import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AboutMeFragmentVipCardWidget extends StatelessWidget {
  const AboutMeFragmentVipCardWidget({
    Key? key,
    required String email,
  })  : _email = email,
        super(key: key);

  final String _email;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 170,
      child: Container(
        width: Get.width,
        alignment: Alignment.center,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                color: Colors.black,
                border: Border.all(
                  width: 1,
                  color: Colors.yellowAccent,
                  style: BorderStyle.solid,
                ),
              ),
              width: Get.width - 40,
              height: 150,
              child: const Stack(
                children: [
                  Positioned(
                    right: 90,
                    top: 50,
                    child: RotatedBox(
                      quarterTurns: 45,
                      child: Opacity(
                        opacity: 0.2,
                        child: Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 100,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    child: RotatedBox(
                      quarterTurns: 95,
                      child: Opacity(
                        opacity: 0.2,
                        child: Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 140,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 40, top: 20),
              child: Row(
                children: [
                  Stack(
                    children: [
                      const Icon(
                        CupertinoIcons.hexagon_fill,
                        color: Colors.white,
                        size: 35,
                      ),
                      Positioned(
                        left: 5,
                        top: 5,
                        child: Icon(
                          CupertinoIcons.star_fill,
                          color: Colors.yellowAccent[400],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
                bottom: 30,
                left: 30,
                child: Text(
                  _email,
                  style: const TextStyle(color: Colors.white),
                ))
          ],
        ),
      ),
    );
  }
}
