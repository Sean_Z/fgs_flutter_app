import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ifgs/api/ThirdPartApi.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/stylesheet/Theme.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:ifgs/widget/PublicWidget/LoadingButton.dart';
import 'package:url_launcher/url_launcher.dart';

GlobalKey<_GenerateFlightPlanCardWidgetState> generateFplWidgetKey = GlobalKey();

class GenerateFlightPlanCardWidget extends StatefulWidget {
  GenerateFlightPlanCardWidget({required Key key}) : super(key: key);

  @override
  _GenerateFlightPlanCardWidgetState createState() => _GenerateFlightPlanCardWidgetState();
}

class _GenerateFlightPlanCardWidgetState extends State<GenerateFlightPlanCardWidget> {
  final _departureTextController = TextEditingController();
  final _arrivalTextController = TextEditingController();
  final _departureFocusNode = FocusNode();
  final _arrivalFocusNode = FocusNode();
  final _ICAOReg = RegExp(r'^[A-Za-z0-9]+$');

  void clearForm() {
    _arrivalTextController.clear();
    _departureTextController.clear();
    _arrivalFocusNode.unfocus();
    _departureFocusNode.unfocus();
    loadingButtonKey.currentState?.clearLoading();
  }

  /// 获取航路
  Future<void> getPlan() async {
    final departureICAO = _departureTextController.text;
    final arrivalICAO = _arrivalTextController.text;
    final fplSource = await StorageUtil.getIntItem('fplSource') ?? 1;
    if(fplSource == 0) {
      loadingButtonKey.currentState?.clearLoading();
      clearForm();
      await launchUrl(Uri.parse('http://fpltoif.com/simbrief?t=new'));
      return null;
    }
    if (_arrivalTextController.text != '' && arrivalICAO != '') {
      _departureFocusNode.unfocus();
      _arrivalFocusNode.unfocus();
      final flightPlanResponse = await ThirdPartApi.generateFlightPlan(departureICAO, arrivalICAO);
      if(flightPlanResponse == null) return;
      final status = flightPlanResponse.status;
      final message = flightPlanResponse.message;
      if (status != 0) {
        EventMethod.handleException(message);
        return;
      }
      final _wayPointListString = flightPlanResponse.routeInfo;
      await Clipboard.setData(ClipboardData(text: _wayPointListString));
      UIViewProvider.showScaffoldMessenger(context, 'Route Copied!', backgroundColor: Colors.green, duration: 2);
      clearForm();
    } else {
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'Empty input！'));
    }
  }

  Widget GeneratePlanCard() {
    return Wrap(
      direction: Axis.horizontal,
      crossAxisAlignment: WrapCrossAlignment.center,
      spacing: 32,
      alignment: WrapAlignment.spaceBetween,
      children: <Widget>[
        Container(
          width: 120,
          height: 70,
          child: Opacity(
            opacity: 0.8,
            child: _TextField(
              _departureFocusNode,
              _departureTextController,
              Icons.flight_takeoff,
            ),
          ),
        ),
        const Icon(Icons.flight),
        Container(
          height: 70,
          width: 120,
          child: Opacity(
            opacity: 0.8,
            child: _TextField(
              _arrivalFocusNode,
              _arrivalTextController,
              Icons.flight_land,
            ),
          ),
        ),
      ],
    );
  }

  TextFormField _TextField(FocusNode focusNode, TextEditingController controller, IconData iconData) {
    return TextFormField(
      maxLength: 4,
      maxLengthEnforcement: MaxLengthEnforcement.enforced,
      autocorrect: true,
      validator: (val) => !_ICAOReg.hasMatch(val!) ? 'check code format of code' : null,
      style: TextStyle(color: LightTheme.themeColor, fontWeight: FontWeight.bold),
      textCapitalization: TextCapitalization.characters,
      focusNode: focusNode,
      controller: controller,
      decoration: InputDecoration(
          filled: true,
          counterStyle: TextStyle(color: LightTheme.themeColor),
          labelStyle: TextStyle(color: LightTheme.themeColor),
          hintStyle: TextStyle(color: LightTheme.themeColor),
          prefixIcon: Icon(
            iconData,
            color: UIAdapter.setIconColor(context),
          )),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _arrivalTextController.dispose();
    _departureTextController.dispose();
    _departureFocusNode.dispose();
    _arrivalFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GeneratePlanCard();
  }
}
