import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/Color.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/request/modal/flight_log_res_entity.dart';
import 'package:ifgs/widget/PublicWidget/FgsCircleLiner.dart';
import 'package:intl/intl.dart';

class MainInfoCard extends StatelessWidget {
  const MainInfoCard({
    Key? key,
    required this.log,
  }) : super(key: key);
  final FlightLogResList log;
  String _formatTime(String time) {
    final rawDate = DateTime.parse(time);
    final boardingTime = DateFormat('MMM d, H:mm', 'en_US').format(rawDate);
    final firstPart = boardingTime.split(',')[1].trim();
    final secondPart = firstPart.substring(0, 2).replaceAll(':', '');
    return firstPart + (int.parse(secondPart) >= 12 ? ' PM' : ' AM');
  }

  String getDeptTime() {
    return DateTime.parse(log.arrivalTime).subtract(Duration(minutes: double.parse(log.flightTime).toInt())).toString();
  }

  bool get shouldUseDark {
    return log.nightTime > log.dayTime;
  }

  @override
  Widget build(BuildContext context) {
    final mainColor = shouldUseDark ? Colors.white : HexColor.fromHex('#3F4779');
    final subText = const TextStyle(color: Colors.white, fontSize: 14);
    final secText = TextStyle(color: mainColor, fontSize: 18, fontWeight: FontWeight.w600);
    final mainText = TextStyle(color: mainColor, fontSize: 20, fontWeight: FontWeight.w600);
    final icaoText = TextStyle(color: mainColor, fontSize: 24, fontWeight: FontWeight.w600);
    final containerColor = shouldUseDark ? HexColor.fromHex('#161423') : HexColor.fromHex('#F5F5FA');
    return Positioned(
      top: 220,
      child: Container(
        height: Get.height * 0.66,
        width: Get.width,
        padding: const EdgeInsets.only(bottom: 50),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(topRight: Radius.circular(64), bottomLeft: Radius.circular(64)),
          boxShadow: [const BoxShadow(color: Colors.grey, blurRadius: 2)],
          color: containerColor,
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(32, 0, 32, 0),
          child: ListView(
            physics: const BouncingScrollPhysics(),
            children: [
              _ExecuteFlightTime(mainText),
              const SizedBox(height: 24),
              _DeptArrivalRow(icaoText, subText, mainColor),
              const SizedBox(height: 24),
              _DeptAndArrival(subText),
              const SizedBox(height: 24),
              _FlightInfo(secText),
              const SizedBox(height: 24),
              _BottomPart(mainText),
            ],
          ),
        ),
      ),
    );
  }

  Wrap _ExecuteFlightTime(TextStyle mainText) {
    return Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.spaceBetween,
      children: [
        Text(
          _formatTime(getDeptTime()),
          style: mainText,
        ),
        Text(
          _formatTime(log.arrivalTime),
          style: mainText,
        ),
      ],
    );
  }

  Flex _DeptArrivalRow(TextStyle icaoText, TextStyle subText, Color mainColor) {
    return Flex(
      direction: Axis.horizontal,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(
          child: Wrap(
            direction: Axis.vertical,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(log.departure, style: icaoText),
              Text(
                log.departureAirportName,
                textAlign: TextAlign.center,
                style: subText.copyWith(color: mainColor),
              ),
            ],
          ),
        ),
        Flexible(
          child: Wrap(
            alignment: WrapAlignment.center,
            children: [
              const FgsCircleLiner(count: 5),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(top: 4, left: 16, right: 16),
                padding: const EdgeInsets.only(top: 4, bottom: 4),
                decoration: BoxDecoration(
                  color: Colors.indigoAccent,
                  borderRadius: BorderRadius.circular(16),
                ),
                child: Text(FlightJudgment.formatFlightTime(double.parse(log.flightTime).toInt()), style: const TextStyle(color: Colors.white)),
              ),
            ],
          ),
        ),
        Flexible(
          child: Wrap(
            direction: Axis.vertical,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(log.arrival, style: icaoText),
              Container(
                child: Text(
                  log.arrivalAirportName,
                  textAlign: TextAlign.center,
                  style: subText.copyWith(color: mainColor),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Flex _DeptAndArrival(TextStyle subText) {
    return Flex(
      direction: Axis.horizontal,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          direction: Axis.horizontal,
          spacing: 8,
          children: [
            Text('DEPARTURE', style: subText.copyWith(color: Colors.blue, fontWeight: FontWeight.w800)),
            const Icon(Icons.flight_takeoff, color: Colors.blue),
          ],
        ),
        Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          direction: Axis.horizontal,
          spacing: 8,
          children: [
            Text('ARRIVAL', style: subText.copyWith(color: Colors.amber, fontWeight: FontWeight.w800)),
            const Icon(Icons.flight_land, color: Colors.amber),
          ],
        ),
      ],
    );
  }

  Flex _FlightInfo(TextStyle secText) {
    return Flex(
      direction: Axis.horizontal,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          flex: 1,
          child: Wrap(
            direction: Axis.vertical,
            spacing: 16,
            children: [
              Text('Flight', style: secText.copyWith(fontWeight: FontWeight.w400)),
              Container(
                constraints: const BoxConstraints(maxWidth: 140),
                child: Text(log.flightNumber, style: secText.copyWith(overflow: TextOverflow.ellipsis)),
              ),
            ],
          ),
        ),
        Flexible(
          flex: 1,
          child: Wrap(
            direction: Axis.vertical,
            spacing: 16,
            children: [
              Text('Range', style: secText.copyWith(fontWeight: FontWeight.w400)),
              Text(log.distance.toString() + ' NM', style: secText),
            ],
          ),
        ),
        Flexible(
          flex: 1,
          child: Wrap(
            direction: Axis.vertical,
            crossAxisAlignment: WrapCrossAlignment.end,
            spacing: 16,
            children: [
              Text('Gate', style: secText.copyWith(fontWeight: FontWeight.w400)),
              Text('A${Random.secure().nextInt(200).toString()}', style: secText),
            ],
          ),
        ),
      ],
    );
  }

  Flex _BottomPart(TextStyle mainText) {
    return Flex(
      direction: Axis.horizontal,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
            flex: 1,
            child: Image.asset(
              'images/basic/passed.png',
              width: 120,
              color: Colors.green,
            )),
        Flexible(
          flex: 1,
          child: Wrap(
            alignment: WrapAlignment.start,
            spacing: 10,
            direction: Axis.vertical,
            children: [
              ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 200),
                child: Text(
                  log.livery,
                  style: mainText.copyWith(fontSize: 18),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 200),
                child: Text(log.aircraft, style: mainText.copyWith(fontSize: 16)),
              ),
              Container(
                alignment: Alignment.centerLeft,
                width: 80,
                height: 30,
                child: UIViewProvider.getAirlineLogo(log.livery),
              ),
            ],
          ),
        )
      ],
    );
  }
}
