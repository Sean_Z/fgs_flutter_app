import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart' as osm;
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/stylesheet/Theme.dart';
import 'package:latlong2/latlong.dart' as osm_l;

class FlightPlanMapWidget extends StatefulWidget {
  @override
  _FlightPlanMapWidgetState createState() => _FlightPlanMapWidgetState();

  FlightPlanMapWidget(
      {Key? key, required this.departureFullName, required this.markers, required this.airwayLine, required this.airwayPoints})
      : super(key: key);

  final String departureFullName;
  final List<osm.Marker> markers;
  final List<osm.Polyline> airwayLine;
  final List<osm_l.LatLng> airwayPoints;
}

class _FlightPlanMapWidgetState extends State<FlightPlanMapWidget> {
  Widget AirwayMap() {
    final airwayBounds = LatLngBounds(widget.airwayPoints[0], widget.airwayPoints[1]);
    final emptyBounds = LatLngBounds(const osm_l.LatLng(0, 0), const osm_l.LatLng(0, 0));
    final bounds = widget.airwayPoints.isNotEmpty ? airwayBounds : emptyBounds;
    return Container(
      width: Get.width,
      height: Get.height,
      child: osm.FlutterMap(
        options: osm.MapOptions(
          initialCameraFit: CameraFit.bounds(bounds: bounds),
          interactionOptions: const InteractionOptions(enableMultiFingerGestureRace: false),
          initialZoom: 5,
        ),
        children: [
          MarkerLayer(markers: widget.markers),
          TileLayer(
            urlTemplate: Links.osmMapUrl,
            additionalOptions: {
              'id': Links.getOsmTheme(OsmMapStyle.dark),
            },
          ),
          PolylineLayer(polylines: widget.airwayLine),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Flight Airway Map'), backgroundColor: LightTheme.themeColor),
      body: AirwayMap(),
    );
  }
}
