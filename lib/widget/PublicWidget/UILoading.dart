import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';

class UILoading extends StatelessWidget {
  const UILoading({
    Key? key,
    required this.content
  }) : super(key: key);
  final String content;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onLongPress: () => CustomViewProvider.hideMaterialLoading(),
        child: Container(
          constraints: const BoxConstraints.expand(),
          color: Colors.black26,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SpinKitRing(
                color: Colors.white,
                size: 50,
                lineWidth: 4,
                duration: Duration(milliseconds: 800),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                content,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}