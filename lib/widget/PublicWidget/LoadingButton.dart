import 'dart:async';

import 'package:flutter/material.dart';

GlobalKey<_LoadingButtonState> loadingButtonKey = GlobalKey();
class LoadingButton extends StatefulWidget {
  final Future<void> Function() action;
  final Text label;
  final Color? indicatorColor;
  final ButtonStyle? buttonStyle;

  const LoadingButton({super.key, required this.action, required this.label, this.buttonStyle, this.indicatorColor});

  @override
  State<LoadingButton> createState() => _LoadingButtonState();
}

class _LoadingButtonState extends State<LoadingButton> {
  var _isRoundCircleShape = false;

  void clearLoading() {
    setState(() {
      _isRoundCircleShape = false;
    });
  }

  Future<void> doAction() async {
    setState(() {
      _isRoundCircleShape = true;
    });
    unawaited(widget.action().then((value) {
      setState(() {
        _isRoundCircleShape = false;
      });
    }).catchError((onError) {
      setState(() {
        _isRoundCircleShape = false;
      });
    }));
  }

  Widget ButtonWithProgress() {
    return Stack(
      alignment: Alignment.center,
      children: [
        ElevatedButton(
          style: widget.buttonStyle ?? widget.buttonStyle!.merge(ButtonStyle(
            shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(double.infinity))),
          )),
          onPressed: doAction,
          child: Text(widget.label.data ?? '', style: const TextStyle(color: Colors.transparent)),
        ),
        SizedBox(
          width: 20,
          height: 20,
          child: CircularProgressIndicator(strokeWidth: 2, color: widget.indicatorColor),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      switchInCurve: Curves.easeInOutCirc,
      duration: const Duration(milliseconds: 200),
      transitionBuilder: (child, animation) {
        return ScaleTransition(scale: animation, child: child);
      },
      child: _isRoundCircleShape
          ? ButtonWithProgress()
          : ElevatedButton(
              style: widget.buttonStyle,
              onPressed: doAction,
              child: widget.label,
            ),
    );
  }
}
