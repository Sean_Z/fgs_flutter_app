import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';

import '../../core/Color.dart';

class LoadingMask extends StatelessWidget {
  const LoadingMask({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            alignment: Alignment.center,
            color: HexColor.fromHex('#000000'),
            child: Image.asset('images/sprite/circle_loading.gif', width: 280),
          ),
          AnimatedTextKit(
            totalRepeatCount: 1,
            repeatForever: true,
            animatedTexts: [
              ColorizeAnimatedText('Loading...',
                  speed: const Duration(milliseconds: 500),
                  colors: [
                    Theme.of(context).primaryColor,
                    Colors.white,
                  ],
                  textStyle: const TextStyle(fontSize: 32)),
            ],
          ),
        ],
      ),
    );
  }
}
