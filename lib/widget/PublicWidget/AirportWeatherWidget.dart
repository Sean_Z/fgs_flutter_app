import 'package:flutter/material.dart';
import 'package:ifgs/api/ThirdPartApi.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/provider/MainProvider.dart';
import 'package:ifgs/request/modal/WorldWeatherBean.dart';
import 'package:ifgs/util/FileUtil.dart';
import 'package:ifgs/widget/PublicWidget/FgsVerticalDivider.dart';
import 'package:provider/provider.dart';

GlobalKey<_AirportWeatherWidgetState> airportWeatherWidgetKey = GlobalKey();

class AirportWeatherWidget extends StatefulWidget {
  AirportWeatherWidget({Key? key, required this.icaoCode, this.airportName, this.elevation}) : super(key: key);
  final String icaoCode;
  final String? airportName;
  final double? elevation;

  @override
  _AirportWeatherWidgetState createState() => _AirportWeatherWidgetState();
}

class _AirportWeatherWidgetState extends State<AirportWeatherWidget> {
  late Data _weather;
  bool _isLoading = true;
  var airportName = '';

  Future<void> getWeather(String icaoCode) async {
    try {
      final _worldWeather = await ThirdPartApi.getWorldAirportWeather();
      if (_worldWeather != null) {
        final _res = _worldWeather.where((element) => element.icao == icaoCode).toList();
        if (_res.isNotEmpty) {
          setState(() {
            _weather = _res.first;
            _isLoading = false;
          });
        }
      }
    } catch (e) {
      throw Exception('weather service exception');
    }
  }

  Future<void> initAirportName() async {
    final airportJson = await FileGetter.airportStaticData;
    final filteredList = airportJson?.result?.where((element) => element.icao == widget.icaoCode) ?? [];
    if (filteredList.isNotEmpty) {
      setState(() {
        airportName = filteredList.first.name ?? (widget.airportName ?? _weather.airportName.toString());
      });
    } else {
      setState(() {
        airportName = widget.airportName ?? _weather.airportName.toString();
      });
    }
  }

  @override
  void initState() {
    getWeather(widget.icaoCode);
    initAirportName();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant AirportWeatherWidget oldWidget) {
    initAirportName();
    super.didUpdateWidget(oldWidget);
  }

  WeatherBean getDynamicWeather(String atis) {
    try {
      if (atis.contains('Offline') || atis.contains('Loading')) {
        final airportName = widget.airportName ?? _weather.airportName.toString();
        return WeatherBean(airportName, _weather.temperature.toString(), _weather.visibility.toString(),
            _weather.windSpeed!.toInt().toString(), _weather.windDirection.toString(), '');
      } else {
        final airportName = atis.split(',')[0];
        final temperature = atis.split('Temperature ')[1].split(',')[0];
        final visibility = atis.split('Visibility ')[1].split(',')[0];
        final windDirection = atis.split('Wind ')[1].split(',')[0].split(' at')[0];
        final windSpeed = atis.split('Wind ')[1].split(',')[0].split(' at')[1].split(' ')[1];
        final dewPoint = atis.split('Dew Point ')[1].split(',')[0];
        return WeatherBean(airportName, temperature, visibility, windSpeed, windDirection, dewPoint);
      }
    } catch (e) {
      print('waiting for initialize activity');
      return WeatherBean('', '', '', '', '', '');
    }
  }

  @override
  Widget build(BuildContext context) {
    final state = Provider.of<MainProvider>(context);
    final dynamicWeather = getDynamicWeather(state.atisInfo);
    return !_isLoading
        ? Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text(
                      airportName,
                      style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(height: 8),
                    const Text(
                      'FGS Airport Weather Service',
                      style: TextStyle(fontSize: 14),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.filter_drama,
                      size: 40,
                    ),
                    const SizedBox(width: 8),
                    Text(
                      dynamicWeather.temperature + ' ℃',
                      style: const TextStyle(fontSize: 20),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    _weather.weatherCode.toString(),
                    style: const TextStyle(fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(children: [
                      Text(S.current.Visibility),
                      Text(dynamicWeather.visibility + ' m', style: const TextStyle(fontSize: 16))
                    ]),
                    FgsVerticalDivider(),
                    Column(children: [
                      Text(S.current.WindSpeed),
                      Text('${dynamicWeather.windSpeed} kt', style: const TextStyle(fontSize: 16))
                    ]),
                    FgsVerticalDivider(),
                    Column(children: [
                      Text(S.current.WindDirection),
                      Text(dynamicWeather.windDirection, style: const TextStyle(fontSize: 16))
                    ]),
                  ],
                )
              ],
            ),
          )
        : Container();
  }
}

class WeatherBean {
  final String airportName;
  final String temperature;
  final String visibility;
  final String windSpeed;
  final String windDirection;
  final String dewPoint;

  WeatherBean(this.airportName, this.temperature, this.visibility, this.windSpeed, this.windDirection, this.dewPoint);
}
