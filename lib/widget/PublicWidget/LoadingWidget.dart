import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingWidget extends StatefulWidget {
  LoadingWidget({Key? key}) : super(key: key);

  @override
  _LoadingWidgetState createState() => _LoadingWidgetState();
}

class _LoadingWidgetState extends State<LoadingWidget> {
  double size = 60.0;
  @override
  void initState() {
    super.initState();
    setState(() {
      size = size;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints.expand(),
      color: Colors.black26,
      child: Opacity(
        opacity: 1,
        child: SpinKitDualRing(color: Colors.white, size: size),
      ),
    );
  }
}
