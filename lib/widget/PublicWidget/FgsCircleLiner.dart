import 'package:flutter/material.dart';

class FgsCircleLiner extends StatelessWidget {
  const FgsCircleLiner({Key? key, required this.count}) : super(key: key);

  final int count;
  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:
            List.generate(count, (index) => Container(margin: const EdgeInsets.all(4), width: 4, height: 4, decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.indigoAccent))));
  }
}
