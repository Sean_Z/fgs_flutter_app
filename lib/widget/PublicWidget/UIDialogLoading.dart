import 'package:flutter/material.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';

class UIDialogLoading extends StatelessWidget {
  const UIDialogLoading({Key? key, required this.content}) : super(key: key);
  final String content;
  
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Stack(
        children: [
          Opacity(
            opacity: 0.5,
            child: InkWell(
              onLongPress: () => CustomViewProvider.hideMaterialDialogLoading(),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                color: Colors.black,
              ),
            ),
          ),
          AlertDialog(
            backgroundColor: UIAdapter.setContainerColor(context),
            titlePadding: const EdgeInsets.only(top: 16, left: 16),
            contentPadding: const EdgeInsets.all(16),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
            titleTextStyle: const TextStyle(fontSize: 15, color: Colors.blueGrey),
            title: Text(content),
            content: const Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                LinearProgressIndicator(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
