import 'package:flutter/material.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class BesselWidget extends StatelessWidget {
  const BesselWidget({Key? key, this.colorList}) : super(key: key);
  final List<List<Color>>? colorList;

  @override
  Widget build(BuildContext context) {
    final _defaultColorList = [
      [Colors.red, const Color(0xEEF44336)],
      [Colors.red[200]!, const Color(0x77E57373)],
      [Colors.orange, const Color(0x66FF9800)],
      [Colors.yellow, const Color(0x55FFEB3B)]
    ];
    return Container(
      height: 60,
      child: WaveWidget(
        config: CustomConfig(
          gradients: colorList ?? _defaultColorList,
          durations: [35000, 19440, 10800, 6000],
          heightPercentages: [0.20, 0.23, 0.25, 0.30],
          gradientBegin: Alignment.bottomLeft,
          gradientEnd: Alignment.topRight,
        ),
        size: const Size(double.infinity, double.infinity),
      ),
    );
  }
}
