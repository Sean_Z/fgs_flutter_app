import 'package:flutter/material.dart';

class AnimatedFloatingActionButton extends StatefulWidget {
  AnimatedFloatingActionButton({
    Key? key,
    required this.onPressed,
  }) : super(key: key);
  final VoidCallback onPressed;

  @override
  _AnimatedFloatingActionButtonState createState() => _AnimatedFloatingActionButtonState();
}

class _AnimatedFloatingActionButtonState extends State<AnimatedFloatingActionButton>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  var _quarterTurns = 0.0;

  @override
  void initState() {
    _controller = AnimationController(vsync: this, duration: const Duration(seconds: 50))..repeat();
    _controller.addListener(() {
      _quarterTurns += _controller.value;
      if (_quarterTurns == 60) {
        _quarterTurns = 0;
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (BuildContext context, Widget? child) {
        return Transform.rotate(
          angle: _quarterTurns,
          child: FloatingActionButton(
            onPressed: widget.onPressed,
            child: const Icon(Icons.scatter_plot),
          ),
        );
      },
    );
  }
}
