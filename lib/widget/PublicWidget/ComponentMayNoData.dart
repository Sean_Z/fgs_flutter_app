import 'package:flutter/material.dart';

import 'InsteadNoData.dart';

class ComponentMayNoData extends StatefulWidget {
  const ComponentMayNoData({
    Key? key,
    required this.data,
    required this.widget,
    required this.noneLabel,
  }) : super(key: key);
  final List? data;
  final Widget widget;
  final String noneLabel;

  @override
  _ComponentMayNoDataState createState() => _ComponentMayNoDataState();
}

class _ComponentMayNoDataState extends State<ComponentMayNoData> {
  @override
  Widget build(BuildContext context) {
    if (widget.data == null) {
      return InsteadNoData(content: widget.noneLabel);
    }
    if (widget.data!.isEmpty) {
      return InsteadNoData(content: widget.noneLabel);
    }
    return widget.widget;
  }
}
