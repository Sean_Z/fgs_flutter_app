import 'package:flutter/material.dart';
import 'package:ifgs/fragment/OsmMapFragment.dart';
import 'package:ifgs/stylesheet/Theme.dart';

class FgsPopupMenuButton extends StatelessWidget {
  const FgsPopupMenuButton({Key? key, required this.buttonInfoList, required this.icon, required this.callback, this.iconSize = 30.0})
      : super(key: key);

  final List<PopupMenuClass> buttonInfoList;
  final IconData icon;
  final ValueChanged callback;
  final double? iconSize;

  @override
  Widget build(BuildContext context) {
    final buttonList = <PopupMenuEntry>[];
    for (var i = 0; i < buttonInfoList.length; ++i) {
      var _item = buttonInfoList[i];
      buttonList.add(
        PopupMenuItem(
            value: i,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _item.iconData,
                const SizedBox(width: 8),
                Text(
                  _item.title,
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
              ],
            )),
      );
    }
    return Material(
      color: Colors.transparent,
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: PopupMenuButton(
          onSelected: (value) => callback(value),
          icon: Icon(icon, color: LightTheme.themeColor),
          iconSize: iconSize,
          itemBuilder: (BuildContext context) {
            return buttonList;
          },
        ),
      ),
    );
  }
}
