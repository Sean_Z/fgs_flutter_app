import 'package:flutter/material.dart';
import 'package:ifgs/core/Common.dart';
import 'package:shimmer/shimmer.dart';

class SkeletonWidget extends StatelessWidget {
  const SkeletonWidget({
    Key? key,
    required this.count,
    this.isLoading = true,
  }) : super(key: key);

  final bool isLoading;
  final int count;

  Widget BuildShimmerItem(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: UIAdapter.isDarkTheme(context) ? Colors.white : Colors.grey[300]!,
      enabled: true,
      highlightColor: UIAdapter.isDarkTheme(context) ? Colors.blue : Colors.grey[100]!,
      child: Container(
        margin: const EdgeInsets.only(bottom: 5, left: 8, right: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2),
          color: UIAdapter.isDarkTheme(context) ? Colors.white70 : Colors.white,
        ),
        height: 10,
      ),
    );
  }

  Widget BuildAllItemInOneCard(BuildContext context) {
    var items = <Widget>[];
    items.add(Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      enabled: true,
      highlightColor: UIAdapter.isDarkTheme(context) ? Theme.of(context).colorScheme.secondary : Colors.grey[100]!,
      child: Container(
        padding: const EdgeInsets.all(8),
        child: Row(
          children: [
            CircleAvatar(
              radius: 20,
              backgroundColor: UIAdapter.isDarkTheme(context) ? Colors.greenAccent[200]! : Colors.white,
            ),
          ],
        ),
      ),
    ));
    for (var i = 0; i < 8; ++i) {
      items.add(BuildShimmerItem(context));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: items,
    );
  }

  List<Widget> BuildAll(BuildContext context) {
    var items = <Widget>[];
    for (var i = 0; i < count; ++i) {
      items.add(BuildAllItemInOneCard(context));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: BuildAll(context),
      ),
    );
  }
}
