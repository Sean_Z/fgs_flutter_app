import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class InsteadNoData extends StatelessWidget {
  const InsteadNoData({Key? key, required this.content}) : super(key: key);

  final String content;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          const SizedBox(height: 50),
          SvgPicture.asset('images/icon/UFO.svg', width: 150),
          Text(content, style: const TextStyle(color: Colors.blueGrey)),
        ],
      ),
    );
  }
}
