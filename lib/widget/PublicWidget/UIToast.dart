import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UIToast extends StatelessWidget {
  const UIToast({Key? key, required this.content}) : super(key: key);
  final String content;

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Material(
          color: Colors.transparent,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                bottom: Get.height * 0.1,
                child: Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.all(8),
                  padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.8),
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Text(content, style: const TextStyle(color: Colors.white)),
                ),
              )
            ],
          )
      ),
    );
  }
}
