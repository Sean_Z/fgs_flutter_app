import 'package:flutter/material.dart';

class DynamicWidget extends StatefulWidget {
  final Widget positiveWidget;
  final Widget negativeWidget;
  final bool condition;
  DynamicWidget({super.key, required this.positiveWidget, required this.negativeWidget, required this.condition});

  @override
  State<DynamicWidget> createState() => _DynamicWidgetState();
}

class _DynamicWidgetState extends State<DynamicWidget> {
  @override
  Widget build(BuildContext context) {
    return widget.condition ? widget.positiveWidget : widget.negativeWidget;
  }
}

