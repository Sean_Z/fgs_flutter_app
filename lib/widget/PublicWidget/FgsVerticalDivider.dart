import 'package:flutter/material.dart';

class FgsVerticalDivider extends StatelessWidget {
  final Color? color;
  FgsVerticalDivider({
    this.color,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        child: VerticalDivider(color: color ?? Colors.white, width: 1, thickness: 1));
  }
}
