import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/stylesheet/Theme.dart';

class LineGradualLineWidget extends StatelessWidget {
  const LineGradualLineWidget({Key? key, required this.isReverse, this.width = 50}) : super(key: key);

  final bool isReverse;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 5, right: 5),
      height: 1,
      width: width,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
            UIAdapter.setContainerColor(context),
            LightTheme.themeColor,
          ],
              begin: !isReverse ? FractionalOffset.centerLeft : FractionalOffset.centerRight,
              end: !isReverse ? FractionalOffset.centerRight : FractionalOffset.centerLeft)),
    );
  }
}
