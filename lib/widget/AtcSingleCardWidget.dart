import 'package:flutter/material.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/stylesheet/Theme.dart';
import 'package:ifgs/widget/PublicWidget/LineGradualLineWidget.dart';

class AtcSingleCardWidget extends StatelessWidget {
  const AtcSingleCardWidget({
    Key? key,
    required this.time,
    required this.type,
    required this.airportName,
    required this.atcName,
    required this.organization,
  }) : super(key: key);

  final String time;
  final int type;
  final String? airportName;
  final String atcName;
  final String? organization;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4,
      shadowColor: InfiniteFlightAtcTypeColor.colorType[type],
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: const EdgeInsets.all(10),
                child: CircleAvatar(
                  radius: 20,
                  child: Text(atcName.substring(0, 1)),
                ),
              ),
              Container(margin: const EdgeInsets.all(10), child: Text(time, style: const TextStyle(color: Colors.blueGrey))),
              Container(
                padding: const EdgeInsets.all(10),
                margin: const EdgeInsets.only(right: 10),
                decoration: BoxDecoration(
                    color: InfiniteFlightAtcTypeColor.colorType[type], borderRadius: const BorderRadius.all(Radius.circular(20))),
                child: Text(InfiniteFlightAtcType.type[type]!, style: const TextStyle(color: Colors.white, fontSize: 12)),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const LineGradualLineWidget(isReverse: false),
              Text(
                airportName ?? 'unknown',
                style: TextStyle(color: LightTheme.themeColor),
              ),
              const LineGradualLineWidget(isReverse: true),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: const EdgeInsets.all(10),
                  child: Text(atcName, style: const TextStyle(color: Colors.blueGrey, fontSize: 12, fontWeight: FontWeight.w400))),
              Container(
                  margin: const EdgeInsets.all(10),
                  child: Text(
                    'Org: ${organization ?? 'unknown'}',
                    style: const TextStyle(
                      color: Colors.blue,
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                    ),
                  )),
            ],
          )
        ],
      ),
    );
  }
}
