import 'dart:math';

import 'package:flutter/material.dart';

import '../../core/Common.dart';

class CustomerPainter extends StatefulWidget {
  const CustomerPainter({Key? key}) : super(key: key);

  @override
  _CustomerPainterState createState() => _CustomerPainterState();
}

class _CustomerPainterState extends State<CustomerPainter> with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  final snowflakeList = List.generate(100, (index) => Snowflake());

  @override
  void initState() {
    _controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 800));
    _animation = Tween(begin: 0.0, end: 100.0).animate(_controller);
    _controller.repeat();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Log.logger.i(_animation.value);
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomRight,
              colors: [Colors.blue, Colors.white],
              stops: [0.6, 1]),
        ),
        constraints: const BoxConstraints.expand(),
        child: AnimatedBuilder(
          animation: _controller,
          builder: (BuildContext context, Widget? child) {
            snowflakeList.forEach((element) {
              element.fall();
            });
            return CustomPaint(
              //定义裁切路径
              painter: CustomerPainterViewer(snowflakeList),
            );
          },
        ),
      ),
    );
  }
}

class CustomerPainterViewer extends CustomPainter {
  late List<Snowflake> snowflake;
  CustomerPainterViewer(this.snowflake);
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()..color = Colors.white;
    canvas.drawCircle(size.center(Offset(0, size.height * 0.3)), 50, paint);
    canvas.drawOval(Rect.fromCenter(center: size.center(Offset(0, size.height * 0.4)), width: 130, height: 140), paint);
    snowflake.forEach((element) {
      canvas.drawCircle(Offset(element.x, element.y), element.radius, paint);
    });
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}

class Snowflake {
  double x = Random().nextDouble() * 400;
  double y = Random().nextDouble() * 800;
  double speed = Random().nextDouble() * 2 + 4;
  double radius = Random().nextDouble() * 2 + 2;
  void fall() {
    y += speed;
    if (y > 800) {
      y = 0;
      x = Random().nextDouble() * 400;
      speed = Random().nextDouble() * 2 + 4;
      radius = Random().nextDouble() * 2 + 2;
    }
  }
}
