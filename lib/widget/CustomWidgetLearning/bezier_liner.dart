import 'package:flutter/material.dart';

import '../../core/Common.dart';

class AnimatedButton extends StatefulWidget {
  const AnimatedButton({Key? key, required this.function, this.width = 120.0, this.height = 60.0}) : super(key: key);

  final Future<dynamic> Function() function;
  final double? width;
  final double? height;
  @override
  _AnimatedButtonState createState() => _AnimatedButtonState();
}

class _AnimatedButtonState extends State<AnimatedButton> with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    _controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 2), lowerBound: 0.0, upperBound: 360.0);
    _controller.repeat();

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        ClipPath.shape(
          shape: const StadiumBorder(),
          child: Material(
            color: Colors.amber,
            child: InkWell(
              customBorder: const StadiumBorder(),
              onTap: () => () => Log.logger.i('he'),
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 400),
                height: widget.height,
                width: widget.width,
                child: Container(),
              ),
            ),
          ),
        ),
        _AnimatedIcon(),
      ],
    );
  }

  AnimatedBuilder _AnimatedIcon() {
    return AnimatedBuilder(
      animation: _controller,
      builder: (BuildContext context, Widget? child) {
        return Center(
          child: Transform.rotate(
            angle: _controller.value,
            child: const Icon(Icons.autorenew, size: 40),
          ),
        );
      },
    );
  }
}
