import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/request/modal/OnlineFlightPlanBean.dart';
import 'package:ifgs/stylesheet/widgetstylesheet/FlightFirstCardStyle.dart';
import 'package:ifgs/widget/PublicWidget/LineGradualLineWidget.dart';
import 'package:provider/provider.dart';

class FlightCardFirst extends StatefulWidget {
  @override
  _FlightCardFirstState createState() => _FlightCardFirstState();

  FlightCardFirst({Key? key}) : super(key: key);
}

class _FlightCardFirstState extends State<FlightCardFirst> {
  var departure = '';
  var arrival = '';
  var route = '';
  var departureTimeHHMM = '.....';
  var departureTimeMMDD = '......';
  var arrivalTimeHHMM = '.....';
  var arrivalTimeMMDD = '.....';
  late List<WholeRangePosition> wholeRangePosition;

  Future<void> setFlightBaseInfo(OnlineFlightPlanBean onlineFlightPlanBean) async {
    departure = onlineFlightPlanBean.departure;
    arrival = onlineFlightPlanBean.arrival;
    route = onlineFlightPlanBean.routeInfo;
    wholeRangePosition = onlineFlightPlanBean.wholeRangePosition;
    departure = departure;
    arrival = arrival;
    route = route;
  }

  void setEstArrivalTime(CalculateArrivalTime calculateArrivalTime) {
    var now = DateTime.now();
    final departureTime = calculateArrivalTime.departureTime;
    final remainingRange = calculateArrivalTime.remainingRange;
    final speed = calculateArrivalTime.speed;
    final estArrivalTime = calculateArrivalTime.estArrivalTime;
    if (remainingRange > 1 && speed >= 60) {
      setState(() {
        var departureMinute = departureTime.minute < 10 ? '0' + departureTime.minute.toString() : departureTime.minute.toString();
        var arrivalMinute = estArrivalTime.minute < 10 ? '0' + estArrivalTime.minute.toString() : estArrivalTime.minute.toString();
        departureTimeHHMM = '${departureTime.timeZoneName} -/- ${departureTime.hour.toString()}:${departureMinute.toString()}';
        departureTimeMMDD = '${departureTime.year}-${departureTime.month}-${departureTime.day}';
        arrivalTimeHHMM = '${departureTime.timeZoneName} -/- ${estArrivalTime.hour.toString()}:${arrivalMinute.toString()}';
        arrivalTimeMMDD = '${estArrivalTime.year}-${estArrivalTime.month}-${estArrivalTime.day}';
      });
    } else {
      setState(() {
        var departureMinute = departureTime.minute < 10 ? '0' + departureTime.minute.toString() : departureTime.minute.toString();
        departureTimeHHMM = '${departureTime.timeZoneName} -/- ${departureTime.hour.toString()}:${departureMinute.toString()}';
        departureTimeMMDD = '${departureTime.year}-${departureTime.month}-${departureTime.day}';
        arrivalTimeHHMM = remainingRange.toInt() == 0 ? S.current.FlightInfoPageViewArrived : S.current.FlightInfoPageViewReached;
        arrivalTimeMMDD = '${now.year}-${now.month}-${now.day}';
      });
    }
  }

  Future getEstimateTime(String flightId, double speed) async {
    final res = await InfiniteFlightLiveDataApi.getAirwayLine(flightId);
    try {
      if (res.isNotEmpty) {
        final _departureTime = DateTime.parse(FlightJudgment.getDepartureTime(res)).toLocal();
        final _wholeRange = CalculateMethod.entireWholeDistance(wholeRangePosition);
        final _alreadyFlewRange = CalculateMethod.entireDistance(res);
        final _diffRange = _wholeRange - _alreadyFlewRange;
        final _now = DateTime.now();
        final _remainFlightTime = ((_diffRange / speed) * 60 * 60).ceil();
        final _estArrivalTime = _now.add(Duration(seconds: _remainFlightTime));
        setEstArrivalTime(CalculateArrivalTime(
            remainingRange: _remainFlightTime, departureTime: _departureTime, estArrivalTime: _estArrivalTime, speed: speed));
      }else {
        setState(() {
          departureTimeHHMM = 'unset';
          departureTimeMMDD = 'unset';
          arrivalTimeHHMM = 'unset';
          arrivalTimeMMDD = 'unset';
        });
      }
    } catch (e) {
      if (mounted) {
        setState(() {
          departureTimeHHMM = 'unset';
          departureTimeMMDD = 'unset';
          arrivalTimeHHMM = 'unset';
          arrivalTimeMMDD = 'unset';
        });
      }
    }
  }

  /// 复制航路
  void copyFlightRoute(bool shouldShareAirway) {
    Clipboard.setData(ClipboardData(text: route));
    UIViewProvider.showScaffoldSuccess(ScaffoldMessageConfig(message: 'Copied!'));
    final textButton = TextButton(
      child: const Text('Ok'),
      onPressed: () {
        Navigator.of(context).pop();
        EventMethod.schemeLauncher(Links.wechatSchemeUrl);
      },
    );
    final tips = 'Airway has copied into clipboard, wanna share this airway to WeChat?';
    if (shouldShareAirway) {
      UIViewProvider.showConfirmDialog(context, tips, textButton);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final onlineFlightPlanBean = Provider.of<FlightInfoStateProvider>(context).onlinePlan;
    final serverType = Provider.of<FlightInfoStateProvider>(context).serverType;
    final flightInfoEntity = Provider.of<FlightInfoStateProvider>(context).flightInfoEntity;
    setFlightBaseInfo(onlineFlightPlanBean);
    getEstimateTime(flightInfoEntity.flightId, flightInfoEntity.speed);
    var livery = flightInfoEntity.livery;
    return Card(
      shadowColor: Colors.blueGrey,
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: const BoxDecoration(
          image: DecorationImage(image: AssetImage('images/basic/bg_blue.png'), fit: BoxFit.cover),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () async {
                    Navigator.pop(context);
                    await Get.toNamed(RouterSheet.airportScreen, arguments: [departure, serverType]);
                  },
                  child: Text('${S.current.FlightInfoPageViewDeparture}-$departure', style: FlightFirstCardStyle.ICAOTextStyle),
                ),
                GestureDetector(
                  onTap: () async {
                    await Get.toNamed(RouterSheet.airportScreen, arguments: [arrival, serverType]);
                  },
                  child: Text('${S.current.FlightInfoPageViewArrival}-$arrival', style: FlightFirstCardStyle.ICAOTextStyle),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(S.current.FlightInfoPageViewOnTime, style: FlightFirstCardStyle.onTimeTextTextStyle),
                    Text('$departureTimeHHMM', style: FlightFirstCardStyle.departureTimeTextStyle),
                    Text('$departureTimeMMDD', style: FlightFirstCardStyle.departureTimeTextStyle),
                  ],
                ),
                Column(
                  children: [
                    Row(children: <Widget>[
                      const LineGradualLineWidget(
                        isReverse: true,
                        width: 25,
                      ),
                      Image.asset('images/basic/airplane_white.png', width: 30),
                      const LineGradualLineWidget(
                        isReverse: false,
                        width: 25,
                      ),
                    ]),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(S.current.FlightPlanWidgetEstimate, style: FlightFirstCardStyle.estTextTextStyle),
                    Text('$arrivalTimeHHMM', style: FlightFirstCardStyle.arrivalTimeTextStyle),
                    Text('$arrivalTimeMMDD', style: FlightFirstCardStyle.arrivalTimeTextStyle)
                  ],
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('${S.current.Airline}: ${livery[0].liveryName ?? 'unknown'}',
                    style: FlightFirstCardStyle.liveryNameTextStyle, softWrap: true, textAlign: TextAlign.left),
                GestureDetector(
                  onTap: () => copyFlightRoute(false),
                  onLongPress: () => copyFlightRoute(true),
                  child: const Icon(Icons.copy, color: Colors.white),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class CalculateArrivalTime {
  final int remainingRange;
  final DateTime departureTime;
  final DateTime estArrivalTime;
  final double speed;

  CalculateArrivalTime({required this.remainingRange, required this.departureTime, required this.estArrivalTime, required this.speed});
}