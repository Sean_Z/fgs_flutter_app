import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/Color.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/pageview/InfiniteFlightAchievementPageView.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';
import 'package:provider/provider.dart';

class FlightCardSecondWidget extends StatefulWidget {
  @override
  _FlightCardSecondWidgetState createState() => _FlightCardSecondWidgetState();

  const FlightCardSecondWidget({Key? key}) : super(key: key);
}

class _FlightCardSecondWidgetState extends State<FlightCardSecondWidget> {
  late String iconAddress;
  late Color color;
  late String liveryName;
  var flewTime = '0';
  var flewRange = 0.0;

  Future getFlewTime(List<AirwayLineListBean> airwayLine) async {
    if(airwayLine.isNotEmpty) {
      final departureTime = DateTime.parse(FlightJudgment.getDepartureTime(airwayLine));
      final currentTime = DateTime.now();
      final diff = currentTime.difference(departureTime);
      final range = CalculateMethod.entireDistance(airwayLine);
      setState(() {
        flewRange = range.truncateToDouble();
        flewTime = diff.inHours.toString() + (diff.inMinutes.toInt() != 0 ? '.' : '') + diff.inMinutes.toString().substring(0, 1);
      });
    }
  }

  /// 生命周期开始 设置初始化的flight对象
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final airwayLineEntity = Provider.of<FlightInfoStateProvider>(context).airwayLineEntity;
    final flightInfoEntity = Provider.of<FlightInfoStateProvider>(context).flightInfoEntity;
    getFlewTime(airwayLineEntity);
    final verticalSpeed = flightInfoEntity.verticalSpeed;
    final altitude = flightInfoEntity.altitude;
    final speed = flightInfoEntity.speed;
    final flightStatus = StatusJudgment.getStatus(verticalSpeed, altitude, speed);
    final flewTimeText = '${S.current.AlreadyFlownTime} $flewTime hours';
    final flewRangeText = '${S.current.AlreadyFlownRange} $flewRange nautical miles';
    final textStyle = const TextStyle(color: Colors.white);
    return Container(
      child: Card(
        shadowColor: Colors.blueGrey,
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        clipBehavior: Clip.antiAlias,
        child: Container(
          color: flightStatus.color,
          width: Get.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(height: 5),
                  Text(flightStatus.status, style: textStyle.merge(const TextStyle(fontSize: 18))),
                  const SizedBox(height: 5),
                  Text(flewTimeText, style: textStyle),
                  const SizedBox(height: 5),
                  Text(flewRangeText, style: textStyle)
                ],
              ),
              const SizedBox(height: 8),
              GestureDetector(
                onTap: () {
                  if (flightInfoEntity.displayName != 'Unknown') {
                    Get.to(() => InfiniteFlightAchievementPageView(), arguments: flightInfoEntity.displayName);
                  } else {
                    UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'IFC Account missed!'));
                  }
                },
                child: Container(
                    color: HexColor.fromHex('093B6D'),
                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                    alignment: Alignment.center,
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      spacing: 8,
                      children: [
                        Text(
                            '${S.current.Pilot}: ${flightInfoEntity.displayName == '' ? 'unknown' : flightInfoEntity.displayName}',
                            style: const TextStyle(color: Colors.greenAccent)),
                        const Icon(Icons.info, color: Colors.white),
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
