import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/Color.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:ifgs/request/modal/OnlineFlightPlanBean.dart';
import 'package:latlong2/latlong.dart' as osm_l;
import 'package:provider/provider.dart';

import 'OsmInfoWindow.dart';

GlobalKey<_FlightMapCardWidgetState> flightMapCardKey = GlobalKey();

class FlightMapCardWidget extends StatefulWidget {
  @override
  _FlightMapCardWidgetState createState() => _FlightMapCardWidgetState();

  const FlightMapCardWidget({Key? key}) : super(key: key);
}

class _FlightMapCardWidgetState extends State<FlightMapCardWidget> {
  var _markerList = <Marker>[];

  final _osmMapController = MapController();

  /// 在线计划对象
  late OnlineFlightPlanBean _onlinePlan;

  /// 航线
  var _polyline = <Polyline>[];

  /// called by IndexPageView RefreshIndicator
  Future<void> refresh(FgsOnlineFlightData fgsOnlineFlightData) async {
    await _setMarker(context);
    await _renderPolyline(context);
  }

  /// render target aircraft icon
  Future<void> _setMarker(BuildContext context) async {
    final flightInfoEntity = Provider.of<FlightInfoStateProvider>(context, listen: false).flightInfoEntity;
    var location = flightInfoEntity.position;
    var latLngMarker = Marker(
        point: osm_l.LatLng(location.latitude, location.longitude),
        alignment: Alignment.center,
        child: InkWell(
            child: RotationTransition(
          turns: AlwaysStoppedAnimation(flightInfoEntity.angle / 360),
          child: const Icon(
            Icons.airplanemode_active,
            color: Colors.blue,
          ),
        )));
    final _tempMarkerList = [latLngMarker];
    setState(() {
      _markerList = _tempMarkerList;
    });
  }

  Future<void> _renderPolyline(BuildContext context) async {
    try {

      final airwayLineResponse = Provider.of<FlightInfoStateProvider>(context, listen: false).airwayLineEntity;
      final onlineFlightBean = Provider.of<FlightInfoStateProvider>(context, listen: false).onlinePlan;
      var wholeAirwayLineResponse = onlineFlightBean.wholeRangePosition;

      /// WholeAirway should be overwrite by airwayLine when fetch wholeAirway
      /// has [Exception] caused by Infinite Flight server
      if (onlineFlightBean.departure == 'unknown') {
        final temp = <WholeRangePosition>[];
        airwayLineResponse.forEach((element) {
          temp.add(WholeRangePosition.fromJson(element.toJson()));
        });
        wholeAirwayLineResponse = temp;
      }
      final airwayPointList = <osm_l.LatLng>[];
      final wholeAirwayLinePointList = <osm_l.LatLng>[];
      for (var item in airwayLineResponse) {
        airwayPointList.add(osm_l.LatLng(item.latitude, item.longitude));
      }

      /// Render flight plan polyline
      for (var _item in wholeAirwayLineResponse) {
        wholeAirwayLinePointList.add(osm_l.LatLng(_item.latitude, _item.longitude));
      }
      final tempPolyline = <Polyline>[];
      final singleAirwayLinPolyLine = Polyline(
        points: airwayPointList,
        gradientColors: [Colors.yellowAccent, Colors.blueAccent],
        colorsStop: ColorCommonMethod.colorStop(airwayLineResponse),
        strokeWidth: 3,
      );
      final singleWholeAirwayLinPolyLine = Polyline(
        points: wholeAirwayLinePointList,
        color: Colors.greenAccent,
        strokeWidth: 1,
      );
      tempPolyline.add(singleAirwayLinPolyLine);
      tempPolyline.add(singleWholeAirwayLinPolyLine);
      setState(() {
        _polyline = tempPolyline;
      });
      _onlinePlan = onlineFlightBean;
      if (wholeAirwayLinePointList.isNotEmpty) {
        final cameraFit = CameraFit.bounds(
            bounds: LatLngBounds(
              osm_l.LatLng(wholeAirwayLinePointList[0].latitude, wholeAirwayLinePointList[0].longitude),
              osm_l.LatLng(wholeAirwayLinePointList.last.latitude, wholeAirwayLinePointList.last.longitude),
            ));
        _osmMapController.fitCamera(cameraFit);
      }
    }catch(E) {
      return null;
    }
  }

  /// Set Flight Info and render aircraft icon
  /// Flight airway, and flight plan airway polyline
  void initFlightMap() {
    _setMarker(context);
    _renderPolyline(context);
  }

  @override
  void initState() {
    super.initState();
    initFlightMap();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final flightEntity = Provider.of<FlightInfoStateProvider>(context).flightInfoEntity;
    return Container(
        height: 250,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
        child: FlutterMap(
          mapController: _osmMapController,
          options: MapOptions(
            initialCenter: osm_l.LatLng(flightEntity.position.latitude, flightEntity.position.longitude),
            interactionOptions: const InteractionOptions(
                enableMultiFingerGestureRace: false,
                flags: InteractiveFlag.none
            ),
            initialZoom: 5,
          ),
          children: [
            TileLayer(
              urlTemplate: Links.osmMapUrl,
              additionalOptions: {
                'id': Links.getOsmTheme(OsmMapStyle.dark),
              },
            ),
            PolylineLayer(polylines: _polyline),
            PopupMarkerLayer(
              options: PopupMarkerLayerOptions(
                markers: _markerList,
                // markerRotateAlignment: PopupMarkerLayerOptions.rotationAlignmentFor(AnchorAlign.top),
                selectedMarkerBuilder: (BuildContext context, Marker marker) {
                  return OsmInfoWindow(targetInfo: flightEntity, onlinePlanBean: _onlinePlan);
                },
              ),
            ),
          ],
        ));
  }
}
