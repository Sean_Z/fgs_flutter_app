import 'package:flutter/material.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:material_floating_search_bar_2/material_floating_search_bar_2.dart';

class SearchBarWidget extends StatefulWidget {
  @override
  _SearchBarWidgetState createState() => _SearchBarWidgetState();

  SearchBarWidget({Key? key, required this.searchResult, required this.searchBarCallBack, required this.functionsCallback})
      : super(key: key);
  final ValueChanged searchBarCallBack;
  final ValueChanged functionsCallback;
  final List<FgsOnlineFlightData> searchResult;
}

class _SearchBarWidgetState extends State<SearchBarWidget> {
  final _floatingSearchBarController = FloatingSearchBarController();
  List<FgsOnlineFlightData> copyData = [];
  List<FgsOnlineFlightData> searchResult = [];
  var isHasData = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    searchResult = widget.searchResult;
    final isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return FloatingSearchBar(
      backgroundColor: Theme.of(context).cardTheme.color,
      iconColor: Theme.of(context).iconTheme.color,
      queryStyle: Theme.of(context).textTheme.bodyMedium,
      hintStyle: Theme.of(context).textTheme.bodyMedium,
      hint: 'Search...',
      scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
      transitionDuration: const Duration(milliseconds: 500),
      transitionCurve: Curves.easeInOut,
      controller: _floatingSearchBarController,
      physics: const BouncingScrollPhysics(),
      axisAlignment: isPortrait ? 0.0 : -1.0,
      openAxisAlignment: 0.0,
      debounceDelay: const Duration(milliseconds: 200),
      transition: CircularFloatingSearchBarTransition(),
      onQueryChanged: (String query) {
        try {
          copyData = searchResult
              .where((element) =>
                  element.callSign.trim().toLowerCase().contains(query.toLowerCase()) ||
                  element.displayName.trim().toLowerCase().contains(query.toLowerCase()))
              .toList()
              .sublist(0, 10);
        } catch (e) {
          /// 超出最大长度就不执行sublist方法
          copyData = searchResult
              .where((element) =>
                  element.callSign.trim().toLowerCase().contains(query.toLowerCase()) ||
                  element.displayName.toString().trim().toLowerCase().contains(query.toLowerCase()))
              .toList();
        }
        setState(() {});
      },
      actions: [
        FloatingSearchBarAction(
          showIfOpened: false,
          child: PopupMenuButton(
            onSelected: (value) => widget.functionsCallback(value),
            icon: Icon(Icons.menu, color: Theme.of(context).iconTheme.color),
            iconSize: 30,
            itemBuilder: (BuildContext context) {
              return <PopupMenuEntry>[
                PopupMenuItem(value: 4, child: Text('Expert Server', style: Theme.of(context).textTheme.bodyMedium)),
                PopupMenuItem(value: 5, child: Text('Training Server', style: Theme.of(context).textTheme.bodyMedium)),
                PopupMenuItem(value: 6, child: Text('Casual Server', style: Theme.of(context).textTheme.bodyMedium)),
              ];
            },
          ),
        ),
        FloatingSearchBarAction.searchToClear(
          showIfClosed: false,
          duration: const Duration(microseconds: 300),
        ),
      ],
      builder: (context, transition) {
        return ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Material(
            color: Colors.white,
            elevation: 12.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: copyData.map((FgsOnlineFlightData item) {
                return Container(
                  child: ListBody(
                    children: <Widget>[
                      Container(
                          height: 50,
                          child: TextButton(
                              onPressed: () => {
                                    widget.searchBarCallBack(item.callSign == '' ? 'unknown' : item.callSign),
                                    FocusScope.of(context).requestFocus(FocusNode()),
                                    _floatingSearchBarController.close()
                                  },
                              child: Text(item.callSign == '' ? 'unknown' : item.callSign, style: const TextStyle(color: Colors.grey))))
                    ],
                  ),
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }
}
