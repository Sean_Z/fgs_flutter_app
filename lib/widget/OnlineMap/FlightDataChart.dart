import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:ifgs/constant/AirlineListMap.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart' as f_chart;
import 'package:syncfusion_flutter_charts/charts.dart';

class FlightDataChart extends StatefulWidget {
  const FlightDataChart({Key? key}) : super(key: key);

  @override
  State<FlightDataChart> createState() => _FlightDataChartState();
}

class _FlightDataChartState extends State<FlightDataChart> {
  final GlobalKey<f_chart.SfCartesianChartState> _chartKey = GlobalKey<f_chart.SfCartesianChartState>();
  final GlobalKey<ScaffoldState> _wholeKey = GlobalKey<ScaffoldState>();
  bool _localValue = false;
  String _localLabel = 'Intl';

  void onLocalChange(bool state, FgsOnlineFlightData flightInfoEntity) {
    setState(() {
      _localValue = state;
    });
    getFlightInfo(flightInfoEntity);
  }

  Map<String, String> getFlightInfo(FgsOnlineFlightData flightInfoEntity) {
    if (_localValue) {
      setState(() {
        _localLabel = 'Asia';
      });
      return {
        'altitude': (flightInfoEntity.altitude.toDouble() * 0.3048000).toInt().toString() + ' m',
        'verticalSpeed': (flightInfoEntity.verticalSpeed.toDouble() * 0.3048000).toInt().toString() + ' m/min',
        'groundSpeed': (flightInfoEntity.speed.toDouble() * 1.852).toInt().toString() + ' km/h',
      };
    } else {
      setState(() {
        _localLabel = 'Intl';
      });
      return {
        'altitude': flightInfoEntity.altitude.toInt().toString() + ' ft',
        'verticalSpeed': flightInfoEntity.verticalSpeed.toInt().toString() + ' ft/min',
        'groundSpeed': flightInfoEntity.speed.toInt().toString() + ' kts',
      };
    }
  }

  Future<void> _saveImage() async {
    final data = await _chartKey.currentState!.toImage(pixelRatio: 3.0);
    final bytes = await data.toByteData(format: ImageByteFormat.png);
    final image = bytes!.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes);
    UIViewProvider.saveImageToGallery(image);
    UIViewProvider.showScaffoldSuccess(ScaffoldMessageConfig(message: 'saved!'));
  }

  @override
  Widget build(BuildContext context) {
    final flightInfoEntity = Provider.of<FlightInfoStateProvider>(context).flightInfoEntity;
    final airwayLineEntity = Provider.of<FlightInfoStateProvider>(context).airwayLineEntity;
    final _chartTileStyle = const TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 14,
      color: Colors.white,
    );
    final _chartSubTileStyle = const TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w600,
      fontSize: 14,
    );
    final _speedList = <SpeedClass>[];
    final _altitudeList = <AltitudeClass>[];
    airwayLineEntity.forEach((element) {
      _speedList.add(SpeedClass.fromJson({'speed': element.groundSpeed, 'time': element.time}));
      _altitudeList.add(AltitudeClass.fromJson({'altitude': element.altitude, 'time': element.time}));
    });
    return Scaffold(
      key: _wholeKey,
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            color: Colors.indigoAccent,
            padding: const EdgeInsets.all(8.0),
            child: Flex(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              direction: Axis.horizontal,
              children: [
                Flexible(
                  flex: 1,
                  child: Container(
                    padding: const EdgeInsets.all(4),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      color: Colors.white70,
                    ),
                    constraints: const BoxConstraints(
                      maxWidth: 120,
                      maxHeight: 30,
                      minWidth: 120,
                      minHeight: 30,
                    ),
                    child: Image.network(
                      AirlineListMap.getIcaoCode(flightInfoEntity.livery[0].liveryName!),
                    ),
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: Text(
                    flightInfoEntity.callSign,
                    style: _chartTileStyle,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Flexible(
                  flex: 3,
                  child: Text(
                    flightInfoEntity.livery[0].aircraftName,
                    style: _chartTileStyle,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: IconButton(
                    icon: const Icon(Icons.cloud_download, color: Colors.amberAccent),
                    onPressed: () => _saveImage(),
                  ),
                )
              ],
            ),
          ),
          const Divider(height: 1, color: Colors.white),
          Container(
            color: Colors.indigoAccent,
            padding: const EdgeInsets.only(top: 8, bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Text('Altitude', style: _chartSubTileStyle),
                    Text(getFlightInfo(flightInfoEntity)['altitude']!, style: _chartSubTileStyle),
                  ],
                ),
                Column(
                  children: [
                    Text('V/S', style: _chartSubTileStyle),
                    Text(getFlightInfo(flightInfoEntity)['verticalSpeed']!, style: _chartSubTileStyle),
                  ],
                ),
                Column(
                  children: [
                    Text('G/S', style: _chartSubTileStyle),
                    Text(getFlightInfo(flightInfoEntity)['groundSpeed']!, style: _chartSubTileStyle),
                  ],
                ),
                Wrap(
                  direction: Axis.horizontal,
                  alignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Switch(
                      value: _localValue,
                      activeColor: Colors.blue,
                      activeTrackColor: Colors.yellow,
                      inactiveThumbImage: const AssetImage('images/fgsLogo.png'),
                      inactiveTrackColor: Colors.greenAccent,
                      inactiveThumbColor: Colors.blueAccent,
                      onChanged: (bool state) => onLocalChange(state, flightInfoEntity),
                    ),
                    Text(_localLabel, style: const TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600)),
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: f_chart.SfCartesianChart(
              key: _chartKey,
              backgroundColor: Colors.indigoAccent,
              legend: const Legend(isVisible: true, position: LegendPosition.bottom, textStyle: TextStyle(color: Colors.white)),
              trackballBehavior: TrackballBehavior(
                /// 纵向选择指示器
                lineType: TrackballLineType.vertical,
                activationMode: ActivationMode.singleTap,
                enable: true,

                /// 工具提示位置(顶部)
                tooltipAlignment: ChartAlignment.near,

                /// 工具提示模式(全部分组)
                tooltipDisplayMode: TrackballDisplayMode.groupAllPoints,
              ),
              primaryXAxis: NumericAxis(isVisible: false),
              primaryYAxis: NumericAxis(labelStyle: const TextStyle(color: Colors.white)),
              axes: [
                f_chart.DateTimeAxis(
                  name: 'timeAxis',
                  opposedPosition: false,
                  dateFormat: DateFormat('H'),
                  majorGridLines: const f_chart.MajorGridLines(color: Colors.transparent),
                  labelStyle: const TextStyle(color: Colors.white, fontSize: 10),
                  labelRotation: 45,
                  interval: 1,
                  maximumLabels: 1,
                )
              ],
              series: <f_chart.ChartSeries>[
                f_chart.LineSeries<SpeedClass, DateTime>(
                    name: 'Speed',
                    animationDuration: 2000,
                    color: Colors.greenAccent,
                    dataSource: _speedList,
                    yAxisName: 'Speed',
                    xAxisName: 'timeAxis',
                    yValueMapper: (SpeedClass _speedList, _) => _speedList.speed,
                    xValueMapper: (SpeedClass _speedList, _) => DateTime.parse(_speedList.time)),
                f_chart.LineSeries<AltitudeClass, DateTime>(
                    name: 'Altitude',
                    animationDuration: 2000,
                    color: Colors.limeAccent,
                    dataSource: _altitudeList,
                    yAxisName: 'Altitude',
                    xAxisName: 'timeAxis',
                    yValueMapper: (AltitudeClass _altitudeList, _) => _altitudeList.altitude / 100,
                    xValueMapper: (AltitudeClass _altitudeList, _) => DateTime.parse(_altitudeList.time))
              ],
            ),
          )
        ],
      ),
    );
  }
}

class SpeedClass {
  late double speed;
  late String time;

  SpeedClass(this.speed, this.time);

  SpeedClass.fromJson(Map<String, Object> json) {
    speed = double.parse(json['speed'].toString());
    time = json['time'] as String;
  }
}

class AltitudeClass {
  late double altitude;
  late String time;

  AltitudeClass(this.altitude, this.time);

  AltitudeClass.fromJson(Map<String, Object> json) {
    altitude = double.parse(json['altitude'].toString());
    time = json['time'] as String;
  }
}
