import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/constant/AirlineListMap.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:ifgs/request/modal/OnlineFlightPlanBean.dart';
import 'package:provider/provider.dart';

class OsmInfoWindow extends StatefulWidget {
  const OsmInfoWindow({
    Key? key,
    required FgsOnlineFlightData targetInfo,
    OnlineFlightPlanBean? onlinePlanBean,
  })  : _targetInfo = targetInfo,
        _onlinePlanBean = onlinePlanBean,
        super(key: key);

  final FgsOnlineFlightData _targetInfo;
  final OnlineFlightPlanBean? _onlinePlanBean;

  @override
  _OsmInfoWindowState createState() => _OsmInfoWindowState();
}

class _OsmInfoWindowState extends State<OsmInfoWindow> {
  String _getText() {
    if (widget._onlinePlanBean == null) {
      return DataHandler.getLiveryName(widget._targetInfo.livery);
    } else {
      return '${widget._onlinePlanBean!.departure} - ${widget._onlinePlanBean!.arrival}';
    }
  }

  @override
  Widget build(BuildContext context) {
    final _status = StatusJudgment.getStatus(widget._targetInfo.verticalSpeed, widget._targetInfo.altitude, widget._targetInfo.speed);
    final _statusColor = _status.color;
    return InkWell(
      splashColor: _statusColor,
      onTap: () {
        Provider.of<FlightInfoStateProvider>(context, listen: false).setFlightInfoEntity(widget._targetInfo);
        Get.toNamed(RouterSheet.flightInfo);
      },
      child: Container(
        height: 120,
        width: 350,
        child: Card(
          shadowColor: _statusColor,
          color: _statusColor,
          child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        children: [
                          Container(
                            constraints: const BoxConstraints(maxWidth: 100),
                            child: UIViewProvider.getAnimatedColorText(widget._targetInfo.callSign, [Colors.white, _statusColor]),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 8),
                            padding: const EdgeInsets.all(2),
                            width: 60,
                            height: 20,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(4)),
                              color: Colors.white70,
                            ),
                            child: Image.network(
                              AirlineListMap.getIcaoCode(DataHandler.getLiveryName(widget._targetInfo.livery)),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        constraints: const BoxConstraints(maxWidth: 100),
                        child: Text(
                          _getText(),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                      Container(
                          padding: const EdgeInsets.all(4),
                          child: UIViewProvider.getAnimatedColorText(_status.status, [Colors.white, _statusColor])),
                    ],
                  ),
                  Container(
                    width: 150,
                    height: 120,
                    child: UIViewProvider.getAirlineAvatar(
                      context,
                      widget._targetInfo.liveryId,
                      widget._targetInfo.aircraftId,
                      BoxFit.fill,
                    ),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
