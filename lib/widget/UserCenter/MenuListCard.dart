import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';

class MenuListCard extends StatefulWidget {
  const MenuListCard({
    Key? key,
  }) : super(key: key);

  @override
  State<MenuListCard> createState() => _MenuListCardState();
}

class _MenuListCardState extends State<MenuListCard> {
  var _pageIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        height: 70,
        child: PageView(
          onPageChanged: (int index) {
            setState(() {
              _pageIndex = index;
            });
          },
          physics: const BouncingScrollPhysics(),
          children: [
            Flex(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              direction: Axis.horizontal,
              children: _BuildMenu(),
            ),
            Flex(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              direction: Axis.horizontal,
              children: _BuildMenu(),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _BuildMenu() {
    final _firstMenuList = [
      MenuListBean(
        RouterSheet.airportScreen,
        Icons.local_airport_outlined,
        Colors.blue,
        S.current.MenuListAirport,
      ),
      MenuListBean(
        RouterSheet.airportScreen,
        Icons.favorite,
        Colors.deepOrange,
        S.current.AboutMeFragmentFavoriteRoute,
      ),
      MenuListBean(
        RouterSheet.ffp,
        Icons.style,
        Colors.deepOrangeAccent,
        S.current.AboutMeFragmentFFP,
      ),
      MenuListBean(
        RouterSheet.certificatePackage,
        Icons.recent_actors,
        Colors.green,
        S.current.AboutMeFragmentCardPack,
      ),
    ];
    final _secondMenuList = [
      MenuListBean(
        RouterSheet.osm,
        Icons.map,
        Colors.blue,
        S.current.MenuListMap,
      ),
      MenuListBean(
        RouterSheet.airportScreen,
        Icons.alt_route,
        Colors.indigo,
        S.current.MenuListPlan,
      ),
      MenuListBean(
        RouterSheet.atcMonitor,
        Icons.api_outlined,
        Colors.blueAccent,
        S.current.MenuListFlightATC,
      ),
    ];
    final widgetList = <Flexible>[];
    final _menuList = _pageIndex == 0 ? _firstMenuList : _secondMenuList;
    _menuList.forEach((element) {
      widgetList.add(
        Expanded(
          flex: 1,
          child: InkWell(
            onTap: () => Get.toNamed(element.route),
            child: Wrap(
              spacing: 4,
              direction: Axis.vertical,
              runAlignment: WrapAlignment.center,
              alignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Icon(
                  element.icon,
                  size: 30,
                  color: element.color,
                ),
                Text(
                  element.title,
                  style: const TextStyle(fontSize: 12),
                ),
              ],
            ),
          ),
        ),
      );
    });
    return widgetList;
  }
}

class MenuListBean {
  MenuListBean(this.route, this.icon, this.color, this.title);
  final String route;
  final IconData icon;
  final Color color;
  final String title;
}
