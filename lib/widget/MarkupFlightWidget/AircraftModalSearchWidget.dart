import 'package:flutter/material.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/request/modal/infinite_flight_livery_res_entity.dart';

import '../../constant/StaticData.dart';

class AircraftModalSearchModal extends SearchDelegate {
  @override
  List<Widget> buildActions(BuildContext context) {
    return [Container()];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
      onPressed: () {
        close(context, 'result');
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Text('Search result：$query');
  }

  static List<InfiniteFlightLiveryResResult> filterDuplicateModalArray(List<InfiniteFlightLiveryResResult> data) {
    final ids = data.map((e) => e.aircraftName).toSet();
    data.retainWhere((x) => ids.remove(x.aircraftName));
    return data;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final rawAircraftList = filterDuplicateModalArray(StaticData.aircraftJson);
    final suggestionList =
        query.isEmpty ? rawAircraftList : rawAircraftList.where((input) => (input.aircraftName).contains(query)).toList();
    return ListView(
      children: suggestionList.map((e) {
        return ListTile(
          onTap: () => close(context, e.aircraftName),
          leading: SizedBox(
            width: 60,
            height: 30,
            child: UIViewProvider.getAirlineLogo(e.liveryName),
          ),
          title: Text(
            e.aircraftName,
            textAlign: TextAlign.end,
          ),
        );
      }).toList(),
    );
  }
}
