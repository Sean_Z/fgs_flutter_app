import 'package:flutter/material.dart';
import 'package:ifgs/core/UIViewProvider.dart';

import '../../constant/StaticData.dart';

class LiverySearchWidget extends SearchDelegate {
  @override
  List<Widget> buildActions(BuildContext context) {
    return [Container()];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
      onPressed: () {
        close(context, 'result');
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Text('Search result：$query');
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final rawLiveryList = StaticData.aircraftJson;
    final suggestionList =
        query.isEmpty ? rawLiveryList : rawLiveryList.where((input) => (input.liveryName).contains(query)).toList();
    return ListView(
      children: suggestionList.map((e) {
        return ListTile(
          onTap: () => close(context, e.liveryName),
          leading: SizedBox(
            width: 60,
            height: 30,
            child: UIViewProvider.getAirlineLogo(e.liveryName),
          ),
          title: Text(
            e.liveryName,
            textAlign: TextAlign.end,
          ),
        );
      }).toList(),
    );
  }
}
