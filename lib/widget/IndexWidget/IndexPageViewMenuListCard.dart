import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/viewmodal/IndexPageViewModal.dart';

class IndexPageViewMenuListCard extends StatelessWidget {
  const IndexPageViewMenuListCard({
    Key? key,
  }) : super(key: key);

  List<Widget> _MenuItemBuilder(BuildContext context) {
    final list1 = <Widget>[];

    final menuListInIndexPageView = [
      MenuListBean(Icons.map, '${S.current.MenuListMap}', Colors.blue, () => Get.toNamed(RouterSheet.osm)),
      MenuListBean(Icons.history, '${S.current.AboutMeFragmentHistory}', Colors.amberAccent, () => Get.toNamed(RouterSheet.historyList)),
      MenuListBean(Icons.emoji_events, '${S.current.MenuListFlightIfAchievement}', Colors.amber,
              () => Get.toNamed(RouterSheet.infiniteFlightAchievement)),
      MenuListBean(Icons.api_outlined, '${S.current.MenuListFlightATC}', Colors.blueAccent, () => Get.toNamed(RouterSheet.atcMonitor)),
    ];

    menuListInIndexPageView.forEach((element) {
      final mapIcon = Icon(element.icon, size: 30, color: element.iconColor);
      final _menuItem = Flexible(
        fit: FlexFit.tight,
        child: GestureDetector(
            onTap: element.routerMethod,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [mapIcon, Text(element.iconName, style: Theme.of(context).textTheme.bodySmall, textAlign: TextAlign.center)],
            )),
      );
      list1.add(_menuItem);
    });
    return list1;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
      child: Container(
        height: 70,
        child: PageView(
          children: [
            Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: _MenuItemBuilder(context),
            ),
          ],
        ),
      ),
    );
  }
}
