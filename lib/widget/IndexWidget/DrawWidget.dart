import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/constant/ConfigData.dart' as config;
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:ifgs/viewmodal/IndexDrawViewModal.dart';

class IndexDrawer extends StatefulWidget {
  final String email;

  const IndexDrawer({super.key, required this.email});

  @override
  State<IndexDrawer> createState() => _IndexDrawerState();
}

class _IndexDrawerState extends State<IndexDrawer> {
  var menuList = <Widget>[];
  var indexDrawerList = <DrawListBean>[
    DrawListBean(
      icon: Icons.home,
      title: '${S.current.MenuListAbout}',
      iconColor: Colors.deepPurpleAccent,
      route: RouterSheet.userCenter,
    ),
    DrawListBean(
      icon: Icons.article,
      title: '${S.current.MenuListAviationNews}',
      iconColor: Colors.blue,
      route: RouterSheet.aviationNews,
    ),
    DrawListBean(
      icon: Icons.architecture,
      title: '${S.current.MenuListDrawFpl}',
      iconColor: Colors.blueAccent,
      route: RouterSheet.drawPlan,
    ),
    DrawListBean(
      icon: Icons.import_contacts,
      title: '${S.current.MenuListChart}',
      iconColor: Colors.teal,
      route: RouterSheet.charts,
    ),
    DrawListBean(
      icon: Icons.psychology_rounded,
      title: '${S.current.MenuListAircraftAdvice}',
      iconColor: Colors.deepOrangeAccent,
      route: RouterSheet.aircraftAdvice,
    ),
    DrawListBean(
      icon: Icons.book,
      title: '${S.current.MenuListPlan}',
      iconColor: Colors.amberAccent,
      route: RouterSheet.flightPlanGenerator,
    ),
    DrawListBean(
      icon: Icons.flight_takeoff,
      title: '${S.current.MenuListAirport}',
      iconColor: Colors.greenAccent,
      route: RouterSheet.airportScreen,
    ),
    DrawListBean(
      icon: Icons.connecting_airports,
      title: '${S.current.AirportAdvice}',
      iconColor: Colors.deepPurpleAccent,
      route: RouterSheet.airportAdvice,
    ),
    DrawListBean(
      icon: CupertinoIcons.settings_solid,
      title: '${S.current.DrawerMenuListSetting}',
      iconColor: Colors.grey,
      route: RouterSheet.setting,
    ),
  ];

  @override
  void initState() {
    super.initState();
    Future.microtask(() => renderListItem(context));
    initLang();
  }

  Future<void> initLang() async {
    final lang = await StorageUtil.getStringItem('lang') ?? 'en';
    lang == 'en' ? UIAdapter.changeLocalLangToEN() : UIAdapter.changeLocalLangToCN();
    await Get.forceAppUpdate();
  }

  void renderListItem(BuildContext context) {
    for (var i = 0; i < indexDrawerList.length; ++i) {
      final item = indexDrawerList[i];
      if (!GetPlatform.isMobile && config.RouterConfig.platformUnSupportList.contains(item.route)) {
        continue;
      }
      menuList.add(InkWell(
        onTap: () {
          if (config.RouterConfig.wipRouteList.contains(item.route)) {
            UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'Working in progress!'));
          } else {
            Navigator.of(context).pop();
            Get.toNamed(item.route);
          }
        },
        child: Container(
          margin: const EdgeInsets.only(bottom: 20),
          height: 30,
          child: ListTile(
            enabled: false,
            title: Text(item.title, style: Theme.of(context).textTheme.bodyLarge),
            leading: Icon(
              item.icon,
              size: 30,
              color: item.iconColor,
            ),
          ),
        ),
      ));
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: Material(
          color: Theme.of(context).cardTheme.color,
          child: ListView(
            controller: ScrollController(),
            children: <Widget>[
              Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage('images/basic/drawerside.jpg'),
                  fit: BoxFit.fitWidth,
                )),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 40, left: 10),
                      child: GestureDetector(
                        onTap: () => Get.toNamed(RouterSheet.userCenter),
                        child: UIViewProvider.getUserAvatar(),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, bottom: 8, left: 8),
                      child: Text(
                        widget.email,
                        style: const TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    )
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: menuList,
              ),
              GestureDetector(
                onTap: () => RouterMethod.logout(),
                child: ListTile(
                  enabled: false,
                  title: Text('${S.current.LogOut}', style: Theme.of(context).textTheme.bodyLarge),
                  leading: Icon(Icons.logout, color: Theme.of(context).iconTheme.color),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
