import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';

import '../../pageview/BoundAircraftCardWidget.dart';

final fgsBoundKey = GlobalKey<_FgsBoundFlightWidgetState>();

class FgsBoundFlightWidget extends StatefulWidget {
  const FgsBoundFlightWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<FgsBoundFlightWidget> createState() => _FgsBoundFlightWidgetState();
}

class _FgsBoundFlightWidgetState extends State<FgsBoundFlightWidget> {
  List<List<FgsOnlineFlightData>>? _data;

  Future initData() async {
    final response = await InfiniteFlightLiveDataApi.getFgsMemberFlight();
    setState(() {
      _data = response;
    });
  }

  @override
  void initState() {
    initData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      margin: const EdgeInsets.only(top: 4, bottom: 4),
      child: BoundAircraftCardWidget(
        isWidget: true,
        title: 'FGS Flight',
        serverType: 0,
        isInbound: true,
        outBoundFlight: _data?[1],
        inBoundFlight: _data?.first,
        context: context,
      ),
    );
  }
}
