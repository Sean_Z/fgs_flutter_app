import 'package:get/get.dart';
import 'package:ifgs/request/modal/infinte_flight_airport_entity.dart';
import 'package:ifgs/util/FileUtil.dart';
import 'package:material_floating_search_bar_2/material_floating_search_bar_2.dart';

class AirportSearchBarViewController extends GetxController {
  final FloatingSearchBarController floatingSearchBarController = FloatingSearchBarController();
  late List<InfiniteFlightAirportResult> copyData = [];
  late List<InfiniteFlightAirportResult> airportStaticData = [];

  Future<void> initCopyData()async {
    final _airportStaticData = await FileGetter.airportStaticData;
    airportStaticData = _airportStaticData?.result ?? [];
    copyData = _airportStaticData?.result?.sublist(0, 10) ?? [];
    update();
  }

  @override
  void onReady() {
    initCopyData();
    super.onReady();
  }
}