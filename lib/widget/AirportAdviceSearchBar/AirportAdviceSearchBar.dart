import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/request/modal/infinte_flight_airport_entity.dart';
import 'package:ifgs/widget/AirportAdviceSearchBar/AirportSearchBarViewController.dart';
import 'package:material_floating_search_bar_2/material_floating_search_bar_2.dart';

class AirportAdviceSearchBar extends StatelessWidget {
  AirportAdviceSearchBar({super.key, required this.onItemClicked});
  final viewController = Get.put(AirportSearchBarViewController());
  final void Function(InfiniteFlightAirportResult airportResult) onItemClicked;

  void onQueryChanged(String icao) {
    final filteredData = viewController.airportStaticData.where((element) => (element.icao?.toLowerCase() ?? '').contains(icao.toLowerCase())).toList();
    viewController.copyData = filteredData.length > 10 ? filteredData.sublist(0, 10) : filteredData;
    if(icao =='') viewController.copyData = viewController.airportStaticData.sublist(0, 10);
    viewController.update();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder(builder: (AirportSearchBarViewController airportSearchBarViewController) {
      return SizedBox(
        height: Get.height,
        child: FloatingSearchBar(
          backgroundColor: Colors.white,
          backdropColor: Colors.black12,
          controller: viewController.floatingSearchBarController,
          automaticallyImplyBackButton: false,
          onQueryChanged: onQueryChanged,
          builder: (BuildContext context, Animation<double> transition) {
            return ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Material(
                color: Colors.white,
                elevation: 12.0,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: viewController.copyData.map((InfiniteFlightAirportResult item) {
                    return Container(
                      child: ListBody(
                        children: <Widget>[
                          Container(
                              height: 50,
                              child: TextButton(
                                  onPressed: () => {
                                        onItemClicked(item),
                                        FocusScope.of(context).requestFocus(FocusNode()),
                                    viewController.floatingSearchBarController.close()
                                      },
                                  child: Text('${item.icao ??''} ${item.has3dBuildings == true ? '3D' : ''}', style: const TextStyle(color: Colors.grey))))
                        ],
                      ),
                    );
                  }).toList(),
                ),
              ),
            );
          },
        ),
      );
    });
  }
}
