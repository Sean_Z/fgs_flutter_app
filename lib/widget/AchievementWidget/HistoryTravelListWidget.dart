import 'package:flutter/material.dart';
import 'package:ifgs/plugin/ticket/TicketIndex.dart';
import 'package:ifgs/stylesheet/Theme.dart';

class HistoryTravelListWidget extends StatefulWidget {
  HistoryTravelListWidget({Key? key}) : super(key: key);
  @override
  _HistoryTravelListWidgetState createState() =>
      _HistoryTravelListWidgetState();
}

class _HistoryTravelListWidgetState extends State<HistoryTravelListWidget> {
  Widget CustomerDivider(bool isReverse) {
    return Container(
      margin: const EdgeInsets.only(left: 5, right: 5),
      height: 1,
      width: 50,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
            Colors.white,
            LightTheme.themeColor,
          ],
              begin: !isReverse
                  ? FractionalOffset.centerLeft
                  : FractionalOffset.centerRight,
              end: !isReverse
                  ? FractionalOffset.centerRight
                  : FractionalOffset.centerLeft)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 10), child: TicketIndex());
  }
}
