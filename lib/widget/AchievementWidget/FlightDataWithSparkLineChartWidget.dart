import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';

class FlightDataWithSparkLineChartWidget extends StatelessWidget {
  const FlightDataWithSparkLineChartWidget({
    Key? key,
    required this.baseColor,
    required this.title,
    required this.value,
    required this.backgroundImageName,
    required this.data,
  }) : super(key: key);

  final Color baseColor;
  final String title;
  final String value;
  final String backgroundImageName;
  final List<double> data;

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: const EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 32),
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage('images/background/$backgroundImageName'),
            ),
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Wrap(
                      spacing: 8,
                      children: [
                        Icon(
                          Icons.access_time,
                          color: baseColor,
                        ),
                        Text(
                          title,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: baseColor,
                          ),
                        )
                      ],
                    ),
                    Text(value,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: baseColor,
                        )),
                  ],
                ),
              ),
              SizedBox(
                height: 100,
                child: SfSparkLineChart(
                  width: 1,
                  highPointColor: Colors.greenAccent,
                  labelStyle: const TextStyle(color: Colors.white),
                  color: baseColor,
                  marker: const SparkChartMarker(
                    color: Colors.amber,
                    size: 2,
                    borderWidth: 1,
                    displayMode: SparkChartMarkerDisplayMode.all,
                  ),
                  data: data,
                ),
              )
            ],
          ),
        ));
  }
}
