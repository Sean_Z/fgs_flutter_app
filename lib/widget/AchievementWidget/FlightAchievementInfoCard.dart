import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';

class FlightAchievementInfoCard extends StatelessWidget {
  const FlightAchievementInfoCard({
    Key? key,
    required int totalLandings,
    required int atcOperationCount,
    required int totalViolations,
  })  : _totalLandings = totalLandings,
        _atcOperationCount = atcOperationCount,
        _totalViolations = totalViolations,
        super(key: key);

  final int _totalLandings;
  final int _atcOperationCount;
  final int _totalViolations;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.fromLTRB(16, 0, 16, 16),
      child: Stack(
        alignment: Alignment.topLeft,
        children: [
          Image.asset('images/background/md.jpg'),
          _BuildTitle(),
          Positioned(left: 10, top: 50, width: 120, child: _BuildLandingCircle()),
          Positioned(width: 80, left: 150, top: 80, child: _BuildAtcCircle()),
          Positioned(width: 90, right: 20, top: 40, child: _BuildVioCircle())
        ],
      ),
    );
  }

  Container _BuildVioCircle() {
    return Container(
        height: 90,
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.redAccent,
        ),
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          direction: Axis.vertical,
          children: [
            const Icon(Icons.gpp_bad, color: Colors.lightBlueAccent),
            Text(
              'Violations \n $_totalViolations',
              style: const TextStyle(height: 1.5, color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ],
        ));
  }

  Container _BuildAtcCircle() {
    return Container(
        height: 80,
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.deepOrangeAccent,
        ),
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          direction: Axis.vertical,
          children: [
            const Icon(Icons.record_voice_over, color: Colors.lightBlueAccent),
            Text(
              'ATC Ops \n $_atcOperationCount',
              style: const TextStyle(
                height: 1.5,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ));
  }

  Container _BuildLandingCircle() {
    return Container(
        alignment: Alignment.center,
        height: 110,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.teal,
        ),
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          direction: Axis.vertical,
          runSpacing: 8,
          children: [
            const Icon(Icons.flight_land, color: Colors.cyanAccent),
            Text(
              'Total Landing\n $_totalLandings',
              style: const TextStyle(
                height: 1.5,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ));
  }

  Padding _BuildTitle() {
    final baseInfoTextStyle = const TextStyle(
      fontSize: 16,
      color: Colors.white,
      fontWeight: FontWeight.w600,
    );
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        spacing: 8,
        children: [
          const Icon(Icons.info, color: Colors.white),
          AnimatedTextKit(animatedTexts: [
            TypewriterAnimatedText(
              'Base Info',
              speed: const Duration(milliseconds: 100),
              textStyle: baseInfoTextStyle,
            ),
          ])
        ],
      ),
    );
  }
}
