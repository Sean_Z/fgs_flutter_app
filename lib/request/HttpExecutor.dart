import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/SecruityUtil.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:oktoast/oktoast.dart';

import 'Router/RouteSheet.dart';

class HttpExecutor {
  static final dio = Dio();

  static void _toLogin() {
    Get.offAllNamed(RouterSheet.login);
    UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'server down', icon: const Icon(Icons.error_outline)));
    CustomViewProvider.hideMaterialDialogLoading();
    CustomViewProvider.hideMaterialLoading();
  }

  /// token白名单中的请求
  static Future<Map<String, dynamic>> _postRequestWithoutToken(String methodName, Object requestData) async {
    final encryptReqData = {'data': await SecurityUtil.rsaEncrypt(jsonEncode(requestData))};
    final _requestData = HttpConfig.encryptList.contains('/$methodName') ? encryptReqData : requestData;
    final url = FgsServer.BASE_URL + methodName;
    final option = Options(headers: {'appVersion': AppVersionInfo.appVersion});
    final res = await dio.post(url, data: _requestData, options: option);
    return json.decode(res.toString()) as Map<String, dynamic>;
  }

  /// 有token校验的post请求
  static Future<Map<String, dynamic>> _postRequestWithToken(String methodName, Object requestData, {bool shouldStopLoading = false}) async {
    if (!shouldStopLoading) {
      CustomViewProvider.hideMaterialLoading();
    }

    /// 取token
    final nodeToken = await StorageUtil.getStringItem('nodeToken');
    final accessToken = await StorageUtil.getStringItem('accessToken');
    final refreshToken = await StorageUtil.getStringItem('refreshToken');
    final tokenList = [nodeToken, accessToken, refreshToken];
    if (tokenList.contains(null)) {
      _toLogin();
      return {};
    }
    final baseOption = Options(
      contentType: 'application/json',
      responseType: ResponseType.plain,
      receiveTimeout: const Duration(seconds: 10),
      headers: {
        'Connection': 'Keep-Alive',
        'accessToken': accessToken,
        'refreshToken': refreshToken,
        'nodeToken': nodeToken,
        'appVersion': AppVersionInfo.appVersion,
      },
    );
    final url = FgsServer.BASE_URL + methodName;
    final encryptReqData = {'data': await SecurityUtil.rsaEncrypt(jsonEncode(requestData))};
    final _requestData = HttpConfig.encryptList.contains('/$methodName') ? encryptReqData : requestData;
    final res = await dio.post(url, data: _requestData, options: baseOption);
    if (res.toString().contains('JsonWebTokenError') || res.toString().contains('token已失效，请重新登录')) {
      _toLogin();
      return {};
    } else {
      return json.decode(res.toString()) as Map<String, dynamic>;
    }
  }

  static Future<Map<String, dynamic>> post(String method, Object requestData, {bool shouldStopLoading = false}) async {
    try {
      if (HttpConfig.tokenWhiteList.contains('/$method')) {
        return await _postRequestWithoutToken(method, requestData);
      } else {
        return await _postRequestWithToken(method, requestData, shouldStopLoading: shouldStopLoading);
      }
    } catch (e) {
      UIViewProvider.showToast('network error', position: ToastPosition.bottom);
      CustomViewProvider.hideMaterialLoading();
      throw Exception(e);
    } finally {
      CustomViewProvider.hideMaterialLoading();
      CustomViewProvider.hideMaterialDialogLoading();
    }
  }

  /// token白名单中的get请求
  static Future _getRequestWithoutToken(String method, String requestData) async {
    try {
      var url = FgsServer.BASE_URL + method;
      if (requestData != '') url = FgsServer.BASE_URL + method + '?$requestData';
      final option = Options(headers: {'appVersion': AppVersionInfo.appVersion});
      final res = await dio.get(url, options: option);
      if (json.decode(res.toString())['status'] == 1) {
        final message = json.decode(res.toString())['message'].toString();
        UIViewProvider.showToast(message);
      }
      return json.decode(res.toString());
    } catch (e) {
      throw HttpException(e.toString());
    }
  }

  static Future get(String method, String requestData,
      {bool shouldStopLoading = false, ResponseType responseType = ResponseType.plain, int fileSize = 100}) async {
    dio.options.responseType = responseType;
    try {
      if (HttpConfig.tokenWhiteList.contains('/$method')) {
        return await _getRequestWithoutToken(method, requestData);
      } else {
        /// 取token
        final nodeToken = await StorageUtil.getStringItem('nodeToken');
        final accessToken = await StorageUtil.getStringItem('accessToken');
        final refreshToken = await StorageUtil.getStringItem('refreshToken');
        final tokenList = [nodeToken, accessToken, refreshToken];
        if (tokenList.contains(null)) {
          _toLogin();
        } else {
          final _baseOption = Options(
            responseType: responseType,
            receiveTimeout: const Duration(seconds: 10),
            headers: {
              'accessToken': accessToken,
              'refreshToken': refreshToken,
              'nodeToken': nodeToken,
              'appVersion': AppVersionInfo.appVersion,
            },
          );

          final _encryptReq = await SecurityUtil.rsaEncrypt(jsonEncode(requestData));
          final _requestData = HttpConfig.encryptList.contains('/$method') ? _encryptReq : requestData;
          final url = FgsServer.BASE_URL + '$method?' + _requestData;
          final res = await dio.get(url, options: _baseOption);
          if (!shouldStopLoading) {
            CustomViewProvider.hideMaterialLoading();
          }
          if (res.toString().contains('JsonWebTokenError') || res.toString().contains('token已失效，请重新登录')) {
            _toLogin();
          } else {
            if (responseType == ResponseType.plain) {
              return json.decode(res.toString());
            } else {
              return res;
            }
          }
        }
      }
    } catch (e) {
      CustomViewProvider.hideMaterialDialogLoading();
      CustomViewProvider.hideMaterialLoading();
      throw HttpException(e.toString());
    }
  }
}
