class AirwayLineListBean {
  late double latitude;
  late double longitude;
  late String time;
  late double altitude;
  late double groundSpeed;

  AirwayLineListBean({required this.latitude, required this.longitude, required this.time, required this.altitude, required this.groundSpeed});

  AirwayLineListBean.fromJson(dynamic json) {
    latitude = double.parse((json['latitude'] ?? 0.0).toString());
    longitude = double.parse((json['longitude'] ?? 0.0).toString());
    time = (json['time'] ?? '') as String;
    altitude = double.parse((json['altitude'] ?? 0.0).toString());
    groundSpeed = double.parse((json['groundSpeed'] ?? 0.0).toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['time'] = time;
    map['altitude'] = altitude;
    map['groundSpeed'] = groundSpeed;
    return map;
  }
}
