/// errorCode : 0
/// result : {"airportIcao":"VTBS","inboundFlightsCount":40,"inboundFlights":["4f559855-fecc-4a8a-a95e-1d097eed9b72",null],"outboundFlightsCount":19,"outboundFlights":["59e9509b-214a-4f8c-9d45-29c4f7ea01d7",null],"atcFacilities":[{"frequencyId":"23bea566-20a0-2858-a40e-179d0699afc1","userId":"05328fc4-b651-45e9-8e1f-328095329484","username":"Rhys_V","virtualOrganization":null,"airportName":"VTBS","type":4,"latitude":13.680815,"longitude":100.74768,"startTime":"2021-02-08 09:59:58Z"}]}

class InfiniteFlightAirportStateBean {
  int? _errorCode;
  Result? _result;

  int? get errorCode => _errorCode;
  Result? get result => _result;

  InfiniteFlightAirportStateBean({int? errorCode, Result? result}) {
    _errorCode = errorCode;
    _result = result;
  }

  InfiniteFlightAirportStateBean.fromJson(dynamic json) {
    _errorCode = json['errorCode'] as int;
    _result = json['result'] != null ? Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errorCode'] = _errorCode;
    if (_result != null) {
      map['result'] = _result?.toJson();
    }
    return map;
  }
}

/// airportIcao : "VTBS"
/// inboundFlightsCount : 40
/// inboundFlights : ["4f559855-fecc-4a8a-a95e-1d097eed9b72",null]
/// outboundFlightsCount : 19
/// outboundFlights : ["59e9509b-214a-4f8c-9d45-29c4f7ea01d7",null]
/// atcFacilities : [{"frequencyId":"23bea566-20a0-2858-a40e-179d0699afc1","userId":"05328fc4-b651-45e9-8e1f-328095329484","username":"Rhys_V","virtualOrganization":null,"airportName":"VTBS","type":4,"latitude":13.680815,"longitude":100.74768,"startTime":"2021-02-08 09:59:58Z"}]

class Result {
  String? _airportIcao;
  int? _inboundFlightsCount;
  List? _inboundFlights;
  int? _outboundFlightsCount;
  List? _outboundFlights;
  List<AtcFacilities>? _atcFacilities;

  String? get airportIcao => _airportIcao;
  int? get inboundFlightsCount => _inboundFlightsCount;
  List? get inboundFlights => _inboundFlights;
  int? get outboundFlightsCount => _outboundFlightsCount;
  List? get outboundFlights => _outboundFlights;
  List<AtcFacilities>? get atcFacilities => _atcFacilities;

  Result(
      {String? airportIcao,
      int? inboundFlightsCount,
      List<String>? inboundFlights,
      int? outboundFlightsCount,
      List<String>? outboundFlights,
      List<AtcFacilities>? atcFacilities}) {
    _airportIcao = airportIcao;
    _inboundFlightsCount = inboundFlightsCount;
    _inboundFlights = inboundFlights;
    _outboundFlightsCount = outboundFlightsCount;
    _outboundFlights = outboundFlights;
    _atcFacilities = atcFacilities;
  }

  Result.fromJson(dynamic json) {
    _airportIcao = json['airportIcao'] as String;
    _inboundFlightsCount = json['inboundFlightsCount'] as int;
    _inboundFlights = json['inboundFlights'] != null ? json['inboundFlights'] as List : [];
    _outboundFlightsCount = json['outboundFlightsCount'] as int;
    _outboundFlights = json['outboundFlights'] != null ? json['outboundFlights'] as List : [];
    if (json['atcFacilities'] != null) {
      _atcFacilities = [];
      json['atcFacilities'].forEach((v) {
        _atcFacilities?.add(AtcFacilities.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['airportIcao'] = _airportIcao;
    map['inboundFlightsCount'] = _inboundFlightsCount;
    map['inboundFlights'] = _inboundFlights;
    map['outboundFlightsCount'] = _outboundFlightsCount;
    map['outboundFlights'] = _outboundFlights;
    if (_atcFacilities != null) {
      map['atcFacilities'] = _atcFacilities?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// frequencyId : "23bea566-20a0-2858-a40e-179d0699afc1"
/// userId : "05328fc4-b651-45e9-8e1f-328095329484"
/// username : "Rhys_V"
/// virtualOrganization : null
/// airportName : "VTBS"
/// type : 4
/// latitude : 13.680815
/// longitude : 100.74768
/// startTime : "2021-02-08 09:59:58Z"

class AtcFacilities {
  String? _frequencyId;
  String? _userId;
  String? _username;
  dynamic _virtualOrganization;
  String? _airportName;
  int? _type;
  double? _latitude;
  double? _longitude;
  String? _startTime;

  String? get frequencyId => _frequencyId;
  String? get userId => _userId;
  String? get username => _username;
  dynamic get virtualOrganization => _virtualOrganization;
  String? get airportName => _airportName;
  int? get type => _type;
  double? get latitude => _latitude;
  double? get longitude => _longitude;
  String? get startTime => _startTime;

  AtcFacilities(
      {String? frequencyId,
      String? userId,
      String? username,
      dynamic virtualOrganization,
      String? airportName,
      int? type,
      double? latitude,
      double? longitude,
      String? startTime}) {
    _frequencyId = frequencyId;
    _userId = userId;
    _username = username;
    _virtualOrganization = virtualOrganization;
    _airportName = airportName;
    _type = type;
    _latitude = latitude;
    _longitude = longitude;
    _startTime = startTime;
  }

  AtcFacilities.fromJson(dynamic json) {
    _frequencyId = json['frequencyId'] as String;
    _userId = json['userId'] as String;
    _username = (json['username'] ?? 'unknown') as String;
    _virtualOrganization = json['virtualOrganization'];
    _airportName = json['airportName'] as String;
    _type = json['type'] as int;
    _latitude = double.parse(json['latitude'].toString());
    _longitude = double.parse(json['longitude'].toString());
    _startTime = json['startTime'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['frequencyId'] = _frequencyId;
    map['userId'] = _userId;
    map['username'] = _username;
    map['virtualOrganization'] = _virtualOrganization;
    map['airportName'] = _airportName;
    map['type'] = _type;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['startTime'] = _startTime;
    return map;
  }
}
