import 'dart:convert';

import 'package:ifgs/generated/json/base/json_field.dart';
import 'package:ifgs/generated/json/flight_log_res_entity.g.dart';

@JsonSerializable()
class FlightLogResEntity {
  late int endRow;
  late bool hasNextPage;
  late bool hasPreviousPage;
  late bool isFirstPage;
  late bool isLastPage;
  late List<FlightLogResList> list;
  late int navigateFirstPage;
  late int navigateLastPage;
  late int navigatePages;
  late List<int> navigatepageNums;
  late int nextPage;
  late int pageNum;
  late int pageSize;
  late int pages;
  late int prePage;
  late int size;
  late int startRow;
  late int total;

  FlightLogResEntity();

  factory FlightLogResEntity.fromJson(Map<String, dynamic> json) => $FlightLogResEntityFromJson(json);

  Map<String, dynamic> toJson() => $FlightLogResEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class FlightLogResList {
  late String aircraft;
  late String airwayPoint;
  late String arrival;
  late String arrivalAirportName;
  late String arrivalTime;
  late String boardingTime;
  late int createDate;
  late int delay;
  late String departure;
  late String departureAirportName;
  late String departureTime;
  late int distance;
  late int favorite;
  late String flightId;
  late String flightNumber;
  late String flightTime;
  late String livery;
  late int logId;
  late int modifyDate;
  late String pilot;
  late int rate;
  late String route;
  late int searchFavorite;
  late double dayTime;
  late double nightTime;

  FlightLogResList();

  factory FlightLogResList.fromJson(Map<String, dynamic> json) => $FlightLogResListFromJson(json);

  Map<String, dynamic> toJson() => $FlightLogResListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
