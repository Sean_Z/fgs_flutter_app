import 'dart:convert';

import 'package:ifgs/generated/json/AircraftInfoEntity.g.dart';
import 'package:ifgs/generated/json/base/json_field.dart';

@JsonSerializable()
class AircraftInfoEntity {
  late int errorCode;
  late List<AircraftInfoResult> result;

  AircraftInfoEntity();

  factory AircraftInfoEntity.fromJson(Map<String, dynamic> json) => $AircraftInfoEntityFromJson(json);

  Map<String, dynamic> toJson() => $AircraftInfoEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class AircraftInfoResult {
  late String id;
  late String aircraftID;
  late String aircraftName;
  late String liveryName;

  AircraftInfoResult();

  factory AircraftInfoResult.fromJson(Map<String, dynamic> json) => $AircraftInfoResultFromJson(json);

  Map<String, dynamic> toJson() => $AircraftInfoResultToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
