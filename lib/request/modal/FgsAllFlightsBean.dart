class FgsAllFlightsBean {
  late List<FgsOnlineFlightData> data;
  late String user;

  FgsAllFlightsBean({required this.data, required this.user});

  FgsAllFlightsBean.fromJson(dynamic json) {
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data.add(FgsOnlineFlightData.fromJson(v));
      });
    }
    user = (json['user'] ?? '') as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['data'] = data.map((v) => v.toJson()).toList();
    map['user'] = user;
    return map;
  }
}

class FgsOnlineFlightData {
  late Position position;
  late double angle;
  late String liveryId;
  late String flightId;
  late String aircraftId;
  late double altitude;
  late String callSign;
  late String displayName;
  late double speed;
  late String userId;
  late double verticalSpeed;
  late String departure;
  late String arrival;
  late List<Livery> livery;

  FgsOnlineFlightData(
      {required this.position,
      required this.angle,
      required this.liveryId,
      required this.flightId,
      required this.aircraftId,
      required this.altitude,
      required this.callSign,
      required this.displayName,
      required this.speed,
      required this.userId,
      required this.verticalSpeed,
      required this.departure,
      required this.arrival,
      required this.livery});

  FgsOnlineFlightData.fromJson(dynamic json) {
    position = Position.fromJson(json['position']);
    angle = double.parse(json['angle'].toString());
    liveryId = json['liveryId'] as String;
    flightId = json['flightId'] as String;
    aircraftId = json['aircraftId'] as String;
    altitude = double.parse(json['altitude'].toString());
    callSign = (json['callSign'] ?? 'unknown') as String;
    displayName = json['displayName'] == '' ? 'unknown' : json['displayName'] as String;
    speed = double.parse(json['speed'].toString());
    userId = json['userId'] as String;
    departure = (json['departure'] ?? 'unset') as String;
    arrival = (json['arrival'] ?? 'unset') as String;
    verticalSpeed = double.parse(json['verticalSpeed'].toString());
    if (json['livery'] != null) {
      livery = [];
      json['livery'].forEach((v) {
        livery.add(Livery.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['position'] = position.toJson();
    map['angle'] = angle;
    map['liveryId'] = liveryId;
    map['flightId'] = flightId;
    map['aircraftId'] = aircraftId;
    map['altitude'] = altitude;
    map['callSign'] = callSign;
    map['displayName'] = displayName;
    map['speed'] = speed;
    map['userId'] = userId;
    map['departure'] = departure;
    map['arrival'] = arrival;
    map['verticalSpeed'] = verticalSpeed;
    map['livery'] = livery.map((v) => v.toJson()).toList();
    return map;
  }
}

class Livery {
  late String aircraftId;
  late String aircraftName;
  late String liveryId;
  String? liveryName;

  Livery({required this.aircraftId, required this.aircraftName, required this.liveryId, required this.liveryName});

  Livery.fromJson(dynamic json) {
    aircraftId = json['AircraftId'] as String;
    aircraftName = json['AircraftName'] as String;
    liveryId = json['LiveryId'] as String;
    liveryName = json['LiveryName'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['AircraftId'] = aircraftId;
    map['AircraftName'] = aircraftName;
    map['LiveryId'] = liveryId;
    map['LiveryName'] = liveryName;
    return map;
  }
}

class Position {
  late double latitude;
  late double longitude;

  Position({required this.latitude, required this.longitude});

  Position.fromJson(dynamic json) {
    latitude = double.parse(json['latitude'].toString());
    longitude = double.parse(json['longitude'].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    return map;
  }
}
