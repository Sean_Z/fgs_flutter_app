/// updateinfo : "fix issues"
/// issue : "UI"
/// id : 1
/// message : "success"
/// version : "1.0.3"
/// platform : "ios/android/web"
/// status : 0

class AppInfoBean {
  String? _updateInfo;
  String? _issue;
  int? _id;
  String? _message;
  String? _version;
  String? _platform;
  int? _status;

  String? get updateInfo => _updateInfo;
  String? get issue => _issue;
  int? get id => _id;
  String? get message => _message;
  String? get version => _version;
  String? get platform => _platform;
  int? get status => _status;

  AppInfoBean(
      {String? updateInfo, String? issue, int? id, String? message, String? version, String? platform, int? status}) {
    _updateInfo = updateInfo;
    _issue = issue;
    _id = id;
    _message = message;
    _version = version;
    _platform = platform;
    _status = status;
  }

  AppInfoBean.fromJson(dynamic json) {
    _updateInfo = (json['updateinfo'] ?? '') as String;
    _issue = (json['issue'] ?? '') as String;
    _id = json['id'] as int;
    _message = json['message'] as String;
    _version = json['version'] as String;
    _platform = json['platform'] as String;
    _status = json['status'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['updateinfo'] = _updateInfo;
    map['issue'] = _issue;
    map['id'] = _id;
    map['message'] = _message;
    map['version'] = _version;
    map['platform'] = _platform;
    map['status'] = _status;
    return map;
  }
}
