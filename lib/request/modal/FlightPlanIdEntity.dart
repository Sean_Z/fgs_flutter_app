import 'dart:convert';

import 'package:ifgs/generated/json/FlightPlanIdEntity.g.dart';
import 'package:ifgs/generated/json/base/json_field.dart';

@JsonSerializable()
class FlightPlanIdEntity {
	late int id;
	late String fromICAO;
	late String toICAO;
	late String fromName;
	late String toName;
	dynamic flightNumber;
	late double distance;
	late int maxAltitude;
	late int waypoints;
	late int popularity;
	late String notes;
	late String encodedPolyline;
	late String createdAt;
	late String updatedAt;
	late List<String> tags;
	late FlightPlanIdUser user;

	FlightPlanIdEntity();

	factory FlightPlanIdEntity.fromJson(Map<String, dynamic> json) => $FlightPlanIdEntityFromJson(json);

	Map<String, dynamic> toJson() => $FlightPlanIdEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class FlightPlanIdUser {
	late int id;
	late String username;
	late String gravatarHash;
	dynamic location;

	FlightPlanIdUser();

	factory FlightPlanIdUser.fromJson(Map<String, dynamic> json) => $FlightPlanIdUserFromJson(json);

	Map<String, dynamic> toJson() => $FlightPlanIdUserToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}