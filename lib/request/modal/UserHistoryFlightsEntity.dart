import 'dart:convert';

import 'package:ifgs/generated/json/UserHistoryFlightsEntity.g.dart';
import 'package:ifgs/generated/json/base/json_field.dart';

@JsonSerializable()
class UserHistoryFlightsEntity {
	late int errorCode;
	late UserHistoryFlightsResult result;

	UserHistoryFlightsEntity();

	factory UserHistoryFlightsEntity.fromJson(Map<String, dynamic> json) => $UserHistoryFlightsEntityFromJson(json);

	Map<String, dynamic> toJson() => $UserHistoryFlightsEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class UserHistoryFlightsResult {
	late int pageIndex;
	late int totalPages;
	late int totalCount;
	late bool hasPreviousPage;
	late bool hasNextPage;
	late List<UserHistoryFlightsResultData> data;

	UserHistoryFlightsResult();

	factory UserHistoryFlightsResult.fromJson(Map<String, dynamic> json) => $UserHistoryFlightsResultFromJson(json);

	Map<String, dynamic> toJson() => $UserHistoryFlightsResultToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class UserHistoryFlightsResultData {
	late String id;
	late String created;
	late String userId;
	late String aircraftId;
	late String liveryId;
	late String callsign;
	late String server;
	late double dayTime;
	late double nightTime;
	late double totalTime;
	late int landingCount;
	late String originAirport;
	String destinationAirport = 'Unknown';
	late int xp;
	late int worldType;
	late List<dynamic> violations;

	UserHistoryFlightsResultData();

	factory UserHistoryFlightsResultData.fromJson(Map<String, dynamic> json) => $UserHistoryFlightsResultDataFromJson(json);

	Map<String, dynamic> toJson() => $UserHistoryFlightsResultDataToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}