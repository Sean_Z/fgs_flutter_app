import 'dart:convert';

import 'package:ifgs/generated/json/InfiniteFlightAllFlightsEntity.g.dart';
import 'package:ifgs/generated/json/base/json_field.dart';

@JsonSerializable()
class InfiniteFlightAllFlightsEntity {
  late int errorCode;
  late List<InfiniteFlightAllFlightsEntityResult> result;

  InfiniteFlightAllFlightsEntity();

  factory InfiniteFlightAllFlightsEntity.fromJson(Map<String, dynamic> json) => $InfiniteFlightAllFlightsEntityFromJson(json);

  Map<String, dynamic> toJson() => $InfiniteFlightAllFlightsEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class InfiniteFlightAllFlightsEntityResult {
  late String username = '';
  late String callsign;
  late double latitude;
  late double longitude;
  late double altitude;
  late double speed;
  late double verticalSpeed;
  late double track;
  late String lastReport;
  late String flightId;
  late String userId;
  late String aircraftId;
  late String liveryId;
  late double heading;

  InfiniteFlightAllFlightsEntityResult();

  factory InfiniteFlightAllFlightsEntityResult.fromJson(Map<String, dynamic> json) => $InfiniteFlightAllFlightsEntityResultFromJson(json);

  Map<String, dynamic> toJson() => $InfiniteFlightAllFlightsEntityResultToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
