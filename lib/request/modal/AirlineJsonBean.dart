/// airline_name : "Airbus - Old"
/// icao_code : "AIB"

class AirlineJsonBean {
  AirlineJsonBean({
    String? airlineName,
    String? icaoCode,
  }) {
    _airlineName = airlineName;
    _icaoCode = icaoCode;
  }

  AirlineJsonBean.fromJson(dynamic json) {
    _airlineName = json['airline_name'] as String;
    _icaoCode = json['icao_code'] as String;
  }
  String? _airlineName;
  String? _icaoCode;

  String? get airlineName => _airlineName;
  String? get icaoCode => _icaoCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['airline_name'] = _airlineName;
    map['icao_code'] = _icaoCode;
    return map;
  }
}
