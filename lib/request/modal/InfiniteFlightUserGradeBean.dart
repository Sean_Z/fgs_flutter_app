class InfiniteFlightUserGradeBean {
  int? _errorCode;
  Result? _result;

  int? get errorCode => _errorCode;
  Result? get result => _result;

  InfiniteFlightUserGradeBean({int? errorCode, Result? result}) {
    _errorCode = errorCode;
    _result = result;
  }

  InfiniteFlightUserGradeBean.fromJson(dynamic json) {
    _errorCode = json['errorCode'] as int;
    _result = json['result'] != null ? Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errorCode'] = _errorCode;
    if (_result != null) {
      map['result'] = _result?.toJson();
    }
    return map;
  }
}

class Result {
  int? _total12MonthsViolations;
  GradeDetails? _gradeDetails;
  int? _totalXP;
  int? _atcOperations;
  int? _atcRank;
  String? _lastLevel1ViolationDate;
  String? _lastReportViolationDate;
  ViolationCountByLevel? _violationCountByLevel;
  List? _roles;
  String? _userId;
  String? _virtualOrganization;
  String? _discourseUsername;
  List<dynamic>? _groups;
  int? _errorCode;

  int? get total12MonthsViolations => _total12MonthsViolations;
  GradeDetails? get gradeDetails => _gradeDetails;
  int? get totalXP => _totalXP;
  int? get atcOperations => _atcOperations;
  int? get atcRank => _atcRank;
  String? get lastLevel1ViolationDate => _lastLevel1ViolationDate;
  String? get lastReportViolationDate => _lastReportViolationDate;
  ViolationCountByLevel? get violationCountByLevel => _violationCountByLevel;
  List? get roles => _roles;
  String? get userId => _userId;
  String? get virtualOrganization => _virtualOrganization;
  String? get discourseUsername => _discourseUsername;
  List<dynamic>? get groups => _groups;
  int? get errorCode => _errorCode;

  Result(
      {int? total12MonthsViolations,
      GradeDetails? gradeDetails,
      int? totalXP,
      int? atcOperations,
      int? atcRank,
      String? lastLevel1ViolationDate,
      String? lastReportViolationDate,
      ViolationCountByLevel? violationCountByLevel,
      List? roles,
      String? userId,
      String? virtualOrganization,
      String? discourseUsername,
      List<dynamic>? groups,
      int? errorCode}) {
    _total12MonthsViolations = total12MonthsViolations;
    _gradeDetails = gradeDetails;
    _totalXP = totalXP;
    _atcOperations = atcOperations;
    _atcRank = atcRank;
    _lastLevel1ViolationDate = lastLevel1ViolationDate;
    _lastReportViolationDate = lastReportViolationDate;
    _violationCountByLevel = violationCountByLevel;
    _roles = roles;
    _userId = userId;
    _virtualOrganization = virtualOrganization;
    _discourseUsername = discourseUsername;
    _groups = groups;
    _errorCode = errorCode;
  }

  Result.fromJson(dynamic json) {
    _total12MonthsViolations = json['total12MonthsViolations'] as int;
    _gradeDetails = json['gradeDetails'] != null ? GradeDetails.fromJson(json['gradeDetails']) : null;
    _totalXP = json['totalXP'] as int;
    _atcOperations = json['atcOperations'] as int;
    _atcRank = json['atcRank'] as int;
    _lastLevel1ViolationDate = json['lastLevel1ViolationDate'] as String;
    _lastReportViolationDate = json['lastReportViolationDate'] as String;
    _violationCountByLevel = json['violationCountByLevel'] != null ? ViolationCountByLevel.fromJson(json['violationCountByLevel']) : null;
    _roles = json['roles'] != null ? json['roles'] as List : [];
    _userId = json['userId'] as String;
    _virtualOrganization = (json['virtualOrganization'] ?? 'unknown') as String;
    _discourseUsername = (json['discourseUsername'] ?? 'unknown') as String;
    _groups = json['groups'] as List;
    _errorCode = json['errorCode'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['total12MonthsViolations'] = _total12MonthsViolations;
    if (_gradeDetails != null) {
      map['gradeDetails'] = _gradeDetails?.toJson();
    }
    map['totalXP'] = _totalXP;
    map['atcOperations'] = _atcOperations;
    map['atcRank'] = _atcRank;
    map['lastLevel1ViolationDate'] = _lastLevel1ViolationDate;
    map['lastReportViolationDate'] = _lastReportViolationDate;
    if (_violationCountByLevel != null) {
      map['violationCountByLevel'] = _violationCountByLevel?.toJson();
    }
    map['roles'] = _roles;
    map['userId'] = _userId;
    map['virtualOrganization'] = _virtualOrganization;
    map['discourseUsername'] = _discourseUsername;
    if (_groups != null) {
      map['groups'] = _groups?.map((v) => v.toJson()).toList();
    }
    map['errorCode'] = _errorCode;
    return map;
  }
}

class ViolationCountByLevel {
  int? _level1;
  int? _level2;
  int? _level3;

  int? get level1 => _level1;
  int? get level2 => _level2;
  int? get level3 => _level3;

  ViolationCountByLevel({int? level1, int? level2, int? level3}) {
    _level1 = level1;
    _level2 = level2;
    _level3 = level3;
  }

  ViolationCountByLevel.fromJson(dynamic json) {
    _level1 = json['level1'] as int;
    _level2 = json['level2'] as int;
    _level3 = json['level3'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['level1'] = _level1;
    map['level2'] = _level2;
    map['level3'] = _level3;
    return map;
  }
}

class GradeDetails {
  List<Grades>? _grades;
  int? _gradeIndex;
  List<RuleDefinitions>? _ruleDefinitions;

  List<Grades>? get grades => _grades;
  int? get gradeIndex => _gradeIndex;
  List<RuleDefinitions>? get ruleDefinitions => _ruleDefinitions;

  GradeDetails({List<Grades>? grades, int? gradeIndex, List<RuleDefinitions>? ruleDefinitions}) {
    _grades = grades;
    _gradeIndex = gradeIndex;
    _ruleDefinitions = ruleDefinitions;
  }

  GradeDetails.fromJson(dynamic json) {
    if (json['grades'] != null) {
      _grades = [];
      json['grades'].forEach((v) {
        _grades?.add(Grades.fromJson(v));
      });
    }
    _gradeIndex = json['gradeIndex'] as int;
    if (json['ruleDefinitions'] != null) {
      _ruleDefinitions = [];
      json['ruleDefinitions'].forEach((v) {
        _ruleDefinitions?.add(RuleDefinitions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_grades != null) {
      map['grades'] = _grades?.map((v) => v.toJson()).toList();
    }
    map['gradeIndex'] = _gradeIndex;
    if (_ruleDefinitions != null) {
      map['ruleDefinitions'] = _ruleDefinitions?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class RuleDefinitions {
  String? _name;
  String? _property;
  int? _operator;
  double? _period;
  int? _order;
  int? _group;

  String? get name => _name;
  String? get property => _property;
  int? get operator => _operator;
  double? get period => _period;
  int? get order => _order;
  int? get group => _group;

  RuleDefinitions({String? name, String? property, int? operator, double? period, int? order, int? group}) {
    _name = name;
    _property = property;
    _operator = operator;
    _period = period;
    _order = order;
    _group = group;
  }

  RuleDefinitions.fromJson(dynamic json) {
    _name = json['name'] as String;
    _property = json['property'] as String;
    _operator = json['operator'] as int;
    _period = double.parse(json['period'].toString());
    _order = json['order'] as int;
    _group = json['group'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['name'] = _name;
    map['property'] = _property;
    map['operator'] = _operator;
    map['period'] = _period;
    map['order'] = _order;
    map['group'] = _group;
    return map;
  }
}

class Grades {
  List<Rules>? _rules;
  int? _index;
  String? _name;
  int? _state;

  List<Rules>? get rules => _rules;
  int? get index => _index;
  String? get name => _name;
  int? get state => _state;

  Grades({List<Rules>? rules, int? index, String? name, int? state}) {
    _rules = rules;
    _index = index;
    _name = name;
    _state = state;
  }

  Grades.fromJson(dynamic json) {
    if (json['rules'] != null) {
      _rules = [];
      json['rules'].forEach((v) {
        _rules?.add(Rules.fromJson(v));
      });
    }
    _index = json['index'] as int;
    _name = json['name'] as String;
    _state = json['state'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_rules != null) {
      map['rules'] = _rules?.map((v) => v.toJson()).toList();
    }
    map['index'] = _index;
    map['name'] = _name;
    map['state'] = _state;
    return map;
  }
}

class Rules {
  int? _ruleIndex;
  double? _referenceValue;
  double? _userValue;
  int? _state;
  String? _userValueString;
  String? _referenceValueString;
  Definition? _definition;

  int? get ruleIndex => _ruleIndex;
  double? get referenceValue => _referenceValue;
  double? get userValue => _userValue;
  int? get state => _state;
  String? get userValueString => _userValueString;
  String? get referenceValueString => _referenceValueString;
  Definition? get definition => _definition;

  Rules(
      {int? ruleIndex, double? referenceValue, double? userValue, int? state, String? userValueString, String? referenceValueString, Definition? definition}) {
    _ruleIndex = ruleIndex;
    _referenceValue = referenceValue;
    _userValue = userValue;
    _state = state;
    _userValueString = userValueString;
    _referenceValueString = referenceValueString;
    _definition = definition;
  }

  Rules.fromJson(dynamic json) {
    _ruleIndex = json['ruleIndex'] as int;
    _referenceValue = double.parse(json['referenceValue'].toString());
    _userValue = double.parse(json['userValue'].toString());
    _state = json['state'] as int;
    _userValueString = json['userValueString'] as String;
    _referenceValueString = json['referenceValueString'] as String;
    _definition = json['definition'] != null ? Definition.fromJson(json['definition']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['ruleIndex'] = _ruleIndex;
    map['referenceValue'] = _referenceValue;
    map['userValue'] = _userValue;
    map['state'] = _state;
    map['userValueString'] = _userValueString;
    map['referenceValueString'] = _referenceValueString;
    if (_definition != null) {
      map['definition'] = _definition?.toJson();
    }
    return map;
  }
}

class Definition {
  String? _name;
  String? _property;
  int? _operator;
  double? _period;
  int? _order;
  int? _group;

  String? get name => _name;
  String? get property => _property;
  int? get operator => _operator;
  double? get period => _period;
  int? get order => _order;
  int? get group => _group;

  Definition({String? name, String? description, String? property, int? operator, double? period, int? order, int? group}) {
    _name = name;
    _property = property;
    _operator = operator;
    _period = period;
    _order = order;
    _group = group;
  }

  Definition.fromJson(dynamic json) {
    _name = json['name'] as String;
    _property = json['property'] as String;
    _operator = json['operator'] as int;
    _period = double.parse(json['period'].toString());
    _order = json['order'] as int;
    _group = json['group'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['name'] = _name;
    map['property'] = _property;
    map['operator'] = _operator;
    map['period'] = _period;
    map['order'] = _order;
    map['group'] = _group;
    return map;
  }
}
