import 'dart:convert';

import 'package:ifgs/generated/json/base/json_field.dart';

import '../../generated/json/FlightPlanDetailEntity.g.dart';

@JsonSerializable()
class FlightPlanDetailEntity {
	late int id;
	late String fromICAO;
	late String toICAO;
	late String fromName;
	late String toName;
	dynamic flightNumber;
	late double distance;
	late int maxAltitude;
	late int waypoints;
	late int likes;
	late int downloads;
	late int popularity;
	late String notes;
	late String encodedPolyline;
	late String createdAt;
	late String updatedAt;
	late List<String> tags;
	late FlightPlanDetailUser user;
	late FlightPlanDetailRoute route;

	FlightPlanDetailEntity();

	factory FlightPlanDetailEntity.fromJson(Map<String, dynamic> json) => $FlightPlanDetailEntityFromJson(json);

	Map<String, dynamic> toJson() => $FlightPlanDetailEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class FlightPlanDetailUser {
	late int id;
	late String username;
	late String gravatarHash;
	dynamic location;

	FlightPlanDetailUser();

	factory FlightPlanDetailUser.fromJson(Map<String, dynamic> json) => $FlightPlanDetailUserFromJson(json);

	Map<String, dynamic> toJson() => $FlightPlanDetailUserToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class FlightPlanDetailRoute {
	late List<FlightPlanDetailRouteNodes> nodes;

	FlightPlanDetailRoute();

	factory FlightPlanDetailRoute.fromJson(Map<String, dynamic> json) => $FlightPlanDetailRouteFromJson(json);

	Map<String, dynamic> toJson() => $FlightPlanDetailRouteToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class FlightPlanDetailRouteNodes {
	late String type;
	late String ident;
	late String name;
	late double lat;
	late double lon;
	late int alt;
	dynamic via;

	FlightPlanDetailRouteNodes();

	factory FlightPlanDetailRouteNodes.fromJson(Map<String, dynamic> json) => $FlightPlanDetailRouteNodesFromJson(json);

	Map<String, dynamic> toJson() => $FlightPlanDetailRouteNodesToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}