import 'dart:convert';

import 'package:ifgs/generated/json/base/json_field.dart';

import '../../generated/json/LoginResponseEntity.g.dart';

@JsonSerializable()
class LoginResponseEntity {
  late String accessToken;
  late String currentPosition;
  late String email;
  late String infiniteFlightUserId;
  late String infiniteFlightUserName;
  late String message;
  late String position;
  late String refreshToken;
  late int status;
  late String username;
  late String authToken;
  late String nodeToken;

  LoginResponseEntity();

  factory LoginResponseEntity.fromJson(Map<String, dynamic> json) => $LoginResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $LoginResponseEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
