/// data : [{"airportName":"天津滨海国际机场","cloudHight":300,"dewpoint":21,"icao":"ZBTJ","lat":39.12,"lng":117.35,"rd":0,"stationWeatherDesc":null,"stationWeatherImg":null,"status":"1","temperature":21,"type":1,"visibility":4500,"weatherCode":"rain","weatherDesc":"小雨 / 轻雾","windDirection":10,"windSpeed":6,"zoom":4},{"airportName":"昌都邦达机场","cloudHight":null,"dewpoint":4,"icao":"ZUBD","lat":30.55,"lng":97.11,"rd":0,"stationWeatherDesc":null,"stationWeatherImg":null,"status":"1","temperature":12,"type":2,"visibility":9999,"weatherCode":"sun","weatherDesc":"晴","windDirection":160,"windSpeed":2,"zoom":4},{"airportName":"拉萨贡嘎机场","cloudHight":1200,"dewpoint":9,"icao":"ZULS","lat":29.3,"lng":90.91,"rd":0,"stationWeatherDesc":null,"stationWeatherImg":null,"status":"1","temperature":19,"type":1,"visibility":9999,"weatherCode":"sun","weatherDesc":"晴","windDirection":30,"windSpeed":2,"zoom":4}]
/// timeFormat : "2021-07-29 14:30"
/// time : 1627540200000

class WorldWeatherBean {
  List<Data>? _data;
  String? _timeFormat;
  int? _time;

  List<Data>? get data => _data;
  String? get timeFormat => _timeFormat;
  int? get time => _time;

  WorldWeatherBean({List<Data>? data, String? timeFormat, int? time}) {
    _data = data;
    _timeFormat = timeFormat;
    _time = time;
  }

  WorldWeatherBean.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
    _timeFormat = json['timeFormat'] as String;
    _time = json['time'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['timeFormat'] = _timeFormat;
    map['time'] = _time;
    return map;
  }
}

/// airportName : "天津滨海国际机场"
/// cloudHight : 300
/// dewpoint : 21
/// icao : "ZBTJ"
/// lat : 39.12
/// lng : 117.35
/// rd : 0
/// stationWeatherDesc : null
/// stationWeatherImg : null
/// status : "1"
/// temperature : 21
/// type : 1
/// visibility : 4500
/// weatherCode : "rain"
/// weatherDesc : "小雨 / 轻雾"
/// windDirection : 10
/// windSpeed : 6
/// zoom : 4

class Data {
  String? _airportName;
  double? _cloudHight;
  double? _dewpoint;
  String? _icao;
  double? _lat;
  double? _lng;
  int? _rd;
  dynamic _stationWeatherDesc;
  dynamic _stationWeatherImg;
  String? _status;
  double? _temperature;
  int? _type;
  double? _visibility;
  String? _weatherCode;
  String? _weatherDesc;
  double? _windDirection;
  double? _windSpeed;
  int? _zoom;

  String? get airportName => _airportName;
  double? get cloudHight => _cloudHight;
  double? get dewpoint => _dewpoint;
  String? get icao => _icao;
  double? get lat => _lat;
  double? get lng => _lng;
  int? get rd => _rd;
  dynamic get stationWeatherDesc => _stationWeatherDesc;
  dynamic get stationWeatherImg => _stationWeatherImg;
  String? get status => _status;
  double? get temperature => _temperature;
  int? get type => _type;
  double? get visibility => _visibility;
  String? get weatherCode => _weatherCode;
  String? get weatherDesc => _weatherDesc;
  double? get windDirection => _windDirection;
  double? get windSpeed => _windSpeed;
  int? get zoom => _zoom;

  Data(
      {String? airportName,
      double? cloudHight,
      double? dewpoint,
      String? icao,
      double? lat,
      double? lng,
      int? rd,
      dynamic stationWeatherDesc,
      dynamic stationWeatherImg,
      String? status,
      double? temperature,
      int? type,
      double? visibility,
      String? weatherCode,
      String? weatherDesc,
      double? windDirection,
      double? windSpeed,
      int? zoom}) {
    _airportName = airportName;
    _cloudHight = cloudHight;
    _dewpoint = dewpoint;
    _icao = icao;
    _lat = lat;
    _lng = lng;
    _rd = rd;
    _stationWeatherDesc = stationWeatherDesc;
    _stationWeatherImg = stationWeatherImg;
    _status = status;
    _temperature = temperature;
    _type = type;
    _visibility = visibility;
    _weatherCode = weatherCode;
    _weatherDesc = weatherDesc;
    _windDirection = windDirection;
    _windSpeed = windSpeed;
    _zoom = zoom;
  }

  Data.fromJson(dynamic json) {
    _airportName = json['airportName'] as String;
    _cloudHight = double.parse((json['cloudHight'] ?? 12000).toString());
    _dewpoint = double.parse((json['dewpoint'] ?? 0.0).toString());
    _icao = json['icao'] as String;
    _lat = double.parse(json['lat'].toString());
    _lng = double.parse(json['lng'].toString());
    _rd = json['rd'] as int;
    _stationWeatherDesc = json['stationWeatherDesc'];
    _stationWeatherImg = json['stationWeatherImg'];
    _status = json['status'] as String;
    _temperature = double.parse((json['temperature'] ?? 0.0).toString());
    _type = json['type'] as int;
    _visibility = double.parse((json['visibility'] ?? 0.0).toString());
    _weatherCode = (json['weatherCode'] ?? 'unknown') as String;
    _weatherDesc = (json['weatherDesc'] ?? 'unknown') as String;
    _windDirection = double.parse((json['windDirection'] ?? 0.0).toString());
    _windSpeed = double.parse((json['windSpeed'] ?? 0.0).toString());
    _zoom = json['zoom'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['airportName'] = _airportName;
    map['cloudHight'] = _cloudHight;
    map['dewpoint'] = _dewpoint;
    map['icao'] = _icao;
    map['lat'] = _lat;
    map['lng'] = _lng;
    map['rd'] = _rd;
    map['stationWeatherDesc'] = _stationWeatherDesc;
    map['stationWeatherImg'] = _stationWeatherImg;
    map['status'] = _status;
    map['temperature'] = _temperature;
    map['type'] = _type;
    map['visibility'] = _visibility;
    map['weatherCode'] = _weatherCode;
    map['weatherDesc'] = _weatherDesc;
    map['windDirection'] = _windDirection;
    map['windSpeed'] = _windSpeed;
    map['zoom'] = _zoom;
    return map;
  }
}
