import 'dart:convert';

import 'package:ifgs/generated/json/base/json_field.dart';

import '../../generated/json/FgsUserInfoBean.g.dart';

@JsonSerializable()
class FgsUserInfoBeanEntity {
  late int endRow;
  late bool hasNextPage;
  late bool hasPreviousPage;
  late bool isFirstPage;
  late bool isLastPage;
  late List<FgsUserInfoBeanList> list;
  late int navigateFirstPage;
  late int navigateLastPage;
  late int navigatePages;
  late List<int> navigatepageNums;
  late int nextPage;
  late int pageNum;
  late int pageSize;
  late int pages;
  late int prePage;
  late int size;
  late int startRow;
  late int total;

  FgsUserInfoBeanEntity();

  factory FgsUserInfoBeanEntity.fromJson(Map<String, dynamic> json) => $FgsUserInfoBeanEntityFromJson(json);

  Map<String, dynamic> toJson() => $FgsUserInfoBeanEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class FgsUserInfoBeanList {
  late String country;
  late int createTime;
  late String infiniteFlightUserName;
  late String mail;
  late String position;
  late int updateTime;
  late String userId;
  late String username;

  FgsUserInfoBeanList();

  factory FgsUserInfoBeanList.fromJson(Map<String, dynamic> json) => $FgsUserInfoBeanListFromJson(json);

  Map<String, dynamic> toJson() => $FgsUserInfoBeanListToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
