import 'dart:convert';

import 'package:ifgs/generated/json/base/json_field.dart';
import 'package:ifgs/generated/json/infinte_flight_airport_entity.g.dart';

export 'package:ifgs/generated/json/infinte_flight_airport_entity.g.dart';

@JsonSerializable()
class InfiniteFlightAirportEntity {
	int? errorCode = 0;
	List<InfiniteFlightAirportResult>? result = [];

	InfiniteFlightAirportEntity.InfiniteFlightAirportEntity();

	factory InfiniteFlightAirportEntity.fromJson(Map<String, dynamic> json) => $InfinteFlightAirportEntityFromJson(json);

	Map<String, dynamic> toJson() => $InfinteFlightAirportEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class InfiniteFlightAirportResult {
	String? icao = '';
	String? iata = '';
	String? name = '';
	String? city = '';
	String? state = '';
	InfiniteFlightAirportResultCountry? country;
	int? clazz = 0;
	int? frequenciesCount = 0;
	int? elevation = 0;
	double? latitude;
	double? longitude;
	String? timezone = '';
	bool? has3dBuildings = false;
	bool? hasJetbridges = false;
	bool? hasSafedockUnits = false;
	bool? hasTaxiwayRouting = false;

	InfiniteFlightAirportResult.InfiniteFlightAirportResult();

	factory InfiniteFlightAirportResult.fromJson(Map<String, dynamic> json) => $InfinteFlightAirportResultFromJson(json);

	Map<String, dynamic> toJson() => $InfiniteFlightAirportResultToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class InfiniteFlightAirportResultCountry {
	int? id = 0;
	String? name = '';
	String? isoCode = '';

	InfiniteFlightAirportResultCountry.InfiniteFlightAirportResultCountry();

	factory InfiniteFlightAirportResultCountry.fromJson(Map<String, dynamic> json) => $InfinteFlightAirportResultCountryFromJson(json);

	Map<String, dynamic> toJson() => $InfinteFlightAirportResultCountryToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}