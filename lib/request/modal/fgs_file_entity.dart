import 'dart:convert';

import 'package:ifgs/generated/json/base/json_field.dart';
import 'package:ifgs/generated/json/fgs_file_entity.g.dart';

export 'package:ifgs/generated/json/fgs_file_entity.g.dart';

@JsonSerializable()
class FgsFileEntity {
	late double bizType;
	late String createTime;
	late String fileName;
	late double fileSize;
	late String fileType;
	late double id;
	late String modifyTime;
	late String uploader;

	FgsFileEntity();

	factory FgsFileEntity.fromJson(dynamic json) => $FgsFileEntityFromJson(json);

	Map<String, dynamic> toJson() => $FgsFileEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}