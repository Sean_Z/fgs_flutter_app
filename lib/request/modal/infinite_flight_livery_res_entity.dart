import 'dart:convert';

import 'package:ifgs/generated/json/base/json_field.dart';
import 'package:ifgs/generated/json/infinite_flight_livery_res_entity.g.dart';

@JsonSerializable()
class InfiniteFlightLiveryResEntity {
  late int errorCode;
  late List<InfiniteFlightLiveryResResult> result;

  InfiniteFlightLiveryResEntity();

  factory InfiniteFlightLiveryResEntity.fromJson(Map<String, dynamic> json) => $InfiniteFlightLiveryResEntityFromJson(json);

  Map<String, dynamic> toJson() => $InfiniteFlightLiveryResEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class InfiniteFlightLiveryResResult {
  late String id;
  late String aircraftID;
  late String aircraftName;
  late String liveryName;

  InfiniteFlightLiveryResResult();

  factory InfiniteFlightLiveryResResult.fromJson(Map<String, dynamic> json) => $InfiniteFlightLiveryResResultFromJson(json);

  Map<String, dynamic> toJson() => $InfiniteFlightLiveryResResultToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
