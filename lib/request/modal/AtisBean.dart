/// status : 1
/// atis : "ATC is Offline, No ATIS information!"

class AtisBean {
  late int status;
  late String atis;

  AtisBean({required this.status, required this.atis});

  AtisBean.fromJson(dynamic json) {
    status = json['status'] as int;
    atis = json['atis'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['atis'] = atis;
    return map;
  }
}
