class InfiniteFlightOnlinePlane {
  int? errorCode;
  Result? result;

  InfiniteFlightOnlinePlane({this.errorCode, this.result});

  InfiniteFlightOnlinePlane.fromJson(dynamic json) {
    errorCode = json['errorCode'] as int;
    result = json['result'] != null ? Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errorCode'] = errorCode;
    if (result != null) {
      map['result'] = result?.toJson();
    }
    return map;
  }
}

class Result {
  String? flightPlanId;
  String? flightId;
  late List waypoints;
  String? lastUpdate;
  List<FlightPlanItems>? flightPlanItems;

  Result({this.flightPlanId, this.flightId, required this.waypoints, this.lastUpdate, this.flightPlanItems});

  Result.fromJson(dynamic json) {
    flightPlanId = json['flightPlanId'] as String;
    flightId = json['flightId'] as String;
    waypoints = json['waypoints'] as List;
    lastUpdate = json['lastUpdate'] as String;
    if (json['flightPlanItems'] != null) {
      flightPlanItems = [];
      json['flightPlanItems'].forEach((v) {
        flightPlanItems?.add(FlightPlanItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['flightPlanId'] = flightPlanId;
    map['flightId'] = flightId;
    map['waypoints'] = waypoints;
    map['lastUpdate'] = lastUpdate;
    if (flightPlanItems != null) {
      map['flightPlanItems'] = flightPlanItems?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class FlightPlanItems {
  String? name;
  int? type;
  dynamic children;
  dynamic identifier;
  double? altitude;
  Location? location;

  FlightPlanItems({this.name, this.type, this.children, this.identifier, this.altitude, this.location});

  FlightPlanItems.fromJson(dynamic json) {
    name = (json['name'] ?? 'unknown') as String;
    type = json['type'] as int;
    children = json['children'];
    identifier = json['identifier'];
    altitude = double.parse(json['altitude'].toString());
    location = json['location'] != null ? Location.fromJson(json['location']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['name'] = name;
    map['type'] = type;
    map['children'] = children;
    map['identifier'] = identifier;
    map['altitude'] = altitude;
    if (location != null) {
      map['location'] = location?.toJson();
    }
    return map;
  }
}

class Location {
  double? latitude;
  double? longitude;

  Location({this.latitude, this.longitude});

  Location.fromJson(dynamic json) {
    latitude = double.parse(json['latitude'].toString());
    longitude = double.parse(json['longitude'].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    return map;
  }
}
