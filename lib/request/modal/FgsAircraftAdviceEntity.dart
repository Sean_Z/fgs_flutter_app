import 'dart:convert';

import 'package:ifgs/generated/json/base/json_field.dart';
import 'package:ifgs/generated/json/fgs_aircraft_advice_entity.g.dart';

export 'package:ifgs/generated/json/fgs_aircraft_advice_entity.g.dart';

@JsonSerializable()
class FgsAircraftAdviceEntity {
	double? id;
	String? model = '';
	@JSONField(name: 'takeoff_trim')
	String? takeoffTrim = '';
	@JSONField(name: 'landing_trim')
	String? landingTrim = '';
	@JSONField(name: 'cruising_speed')
	double? cruisingSpeed;
	@JSONField(name: 'landing_flaps')
	String? landingFlaps = '';
	@JSONField(name: 'takeoff_flaps')
	String? takeoffFlaps = '';
	@JSONField(name: 'max_passenger')
	String? maxPassenger;
	@JSONField(name: 'max_cargo')
	String? maxCargo = '';
	String? mlw = '';
	String? mtow = '';
	@JSONField(name: 'range_time')
	String? rangeTime = '';
	@JSONField(name: 'takeoff_distance')
	String? takeoffDistance;
	@JSONField(name: 'landing_distance')
	String? landingDistance;
	String? company = '';
	String? remark = '';
	@JSONField(name: 'create_time')
	String? createTime = '';
	@JSONField(name: 'modify_time')
	String? modifyTime = '';

	FgsAircraftAdviceEntity();

	factory FgsAircraftAdviceEntity.fromJson(dynamic json) => $FgsAircraftAdviceEntityFromJson(json);

	Map<String, dynamic> toJson() => $FgsAircraftAdviceEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}