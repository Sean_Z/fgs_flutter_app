import 'dart:convert';

import 'package:ifgs/generated/json/base/json_field.dart';
import 'package:ifgs/generated/json/infinite_flight_history_flight_entity.g.dart';

@JsonSerializable()
class InfiniteFlightHistoryFlightEntity {
  late int errorCode;
  late InfiniteFlightHistoryFlightResult result;

  InfiniteFlightHistoryFlightEntity();

  factory InfiniteFlightHistoryFlightEntity.fromJson(Map<String, dynamic> json) => $InfiniteFlightHistoryFlightEntityFromJson(json);

  Map<String, dynamic> toJson() => $InfiniteFlightHistoryFlightEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class InfiniteFlightHistoryFlightResult {
  late int pageIndex;
  late int totalPages;
  late int totalCount;
  late bool hasPreviousPage;
  late bool hasNextPage;
  late List<InfiniteFlightHistoryFlightResultData> data;

  InfiniteFlightHistoryFlightResult();

  factory InfiniteFlightHistoryFlightResult.fromJson(Map<String, dynamic> json) => $InfiniteFlightHistoryFlightResultFromJson(json);

  Map<String, dynamic> toJson() => $InfiniteFlightHistoryFlightResultToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class InfiniteFlightHistoryFlightResultData {
  late String id;
  late String created;
  late String userId;
  late String aircraftId;
  late String liveryId;
  late String callsign;
  late String server;
  late double dayTime;
  late int nightTime;
  late double totalTime;
  late int landingCount;
  late String originAirport;
  late String destinationAirport;
  late int xp;

  InfiniteFlightHistoryFlightResultData();

  factory InfiniteFlightHistoryFlightResultData.fromJson(Map<String, dynamic> json) => $InfiniteFlightHistoryFlightResultDataFromJson(json);

  Map<String, dynamic> toJson() => $InfiniteFlightHistoryFlightResultDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
