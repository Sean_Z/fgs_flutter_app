/// errorCode : 0
/// result : [{"onlineFlights":2449,"violations":102,"xp":572128,"landingCount":898,"flightTime":45983,"atcOperations":548,"atcRank":7,"grade":5,"hash":"5F0973A9","violationCountByLevel":{"level1":102,"level2":0,"level3":0},"roles":[1,2,64],"userId":"2a11e620-1cc1-4ac6-90d1-18c4ed9cb913","virtualOrganization":null,"discourseUsername":"Cameron","groups":["8c93a113-0c6c-491f-926d-1361e43a5833","d07afad8-79df-4363-b1c7-a5a1dde6e3c8","df0f6341-5f6a-40ef-8b73-087a0ec255b5"],"errorCode":0},{"onlineFlights":21,"violations":0,"xp":29984,"landingCount":24,"flightTime":2717,"atcOperations":0,"atcRank":null,"grade":1,"hash":"56099EA4","userId":"66e362c0-894b-495b-93a6-75f9befa502d","virtualOrganization":null,"discourseUsername":null,"violationCountByLevel":{"level1":22,"level2":0,"level3":0},"roles":[64],"groups":[],"errorCode":0}]

class InfiniteFlightUserStateBean {
  int? _errorCode;
  List<Result>? _result;

  int? get errorCode => _errorCode;
  List<Result>? get result => _result;

  InfiniteFlightUserStateBean({int? errorCode, List<Result>? result}) {
    _errorCode = errorCode;
    _result = result;
  }

  InfiniteFlightUserStateBean.fromJson(dynamic json) {
    _errorCode = json['errorCode'] as int;
    if (json['result'] != null) {
      _result = [];
      json['result'].forEach((v) {
        _result?.add(Result.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errorCode'] = _errorCode;
    if (_result != null) {
      map['result'] = _result?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// onlineFlights : 2449
/// violations : 102
/// xp : 572128
/// landingCount : 898
/// flightTime : 45983
/// atcOperations : 548
/// atcRank : 7
/// grade : 5
/// hash : "5F0973A9"
/// violationCountByLevel : {"level1":102,"level2":0,"level3":0}
/// roles : [1,2,64]
/// userId : "2a11e620-1cc1-4ac6-90d1-18c4ed9cb913"
/// virtualOrganization : null
/// discourseUsername : "Cameron"
/// groups : ["8c93a113-0c6c-491f-926d-1361e43a5833","d07afad8-79df-4363-b1c7-a5a1dde6e3c8","df0f6341-5f6a-40ef-8b73-087a0ec255b5"]
/// errorCode : 0

class Result {
  int? _onlineFlights;
  int? _violations;
  int? _xp;
  int? _landingCount;
  int? _flightTime;
  int? _atcOperations;
  int? _atcRank;
  int? _grade;
  String? _hash;
  ViolationCountByLevel? _violationCountByLevel;
  List<int>? _roles;
  String? _userId;
  dynamic _virtualOrganization;
  String? _discourseUsername;
  List<String>? _groups;
  int? _errorCode;

  int? get onlineFlights => _onlineFlights;
  int? get violations => _violations;
  int? get xp => _xp;
  int? get landingCount => _landingCount;
  int? get flightTime => _flightTime;
  int? get atcOperations => _atcOperations;
  int? get atcRank => _atcRank;
  int? get grade => _grade;
  String? get hash => _hash;
  ViolationCountByLevel? get violationCountByLevel => _violationCountByLevel;
  List<int>? get roles => _roles;
  String? get userId => _userId;
  dynamic get virtualOrganization => _virtualOrganization;
  String? get discourseUsername => _discourseUsername;
  List<String>? get groups => _groups;
  int? get errorCode => _errorCode;

  Result(
      {int? onlineFlights,
      int? violations,
      int? xp,
      int? landingCount,
      int? flightTime,
      int? atcOperations,
      int? atcRank,
      int? grade,
      String? hash,
      ViolationCountByLevel? violationCountByLevel,
      List<int>? roles,
      String? userId,
      dynamic virtualOrganization,
      String? discourseUsername,
      List<String>? groups,
      int? errorCode}) {
    _onlineFlights = onlineFlights;
    _violations = violations;
    _xp = xp;
    _landingCount = landingCount;
    _flightTime = flightTime;
    _atcOperations = atcOperations;
    _atcRank = atcRank;
    _grade = grade;
    _hash = hash;
    _violationCountByLevel = violationCountByLevel;
    _roles = roles;
    _userId = userId;
    _virtualOrganization = virtualOrganization;
    _discourseUsername = discourseUsername;
    _groups = groups;
    _errorCode = errorCode;
  }

  Result.fromJson(dynamic json) {
    _onlineFlights = json['onlineFlights'] as int;
    _violations = json['violations'] as int;
    _xp = json['xp'] as int;
    _landingCount = json['landingCount'] as int;
    _flightTime = json['flightTime'] as int;
    _atcOperations = json['atcOperations'] as int;
    _atcRank = json['atcRank'] as int;
    _grade = json['grade'] as int;
    _hash = json['hash'] as String;
    _violationCountByLevel = json['violationCountByLevel'] != null ? ViolationCountByLevel.fromJson(json['violationCountByLevel']) : null;
    _roles = json['roles'] != null ? json['roles'] as List<int> : [1];
    _userId = json['userId'] as String;
    _virtualOrganization = json['virtualOrganization'];
    _discourseUsername = json['discourseUsername'] as String;
    _groups = json['groups'] != null ? json['groups'] as List<String> : [''];
    _errorCode = json['errorCode'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['onlineFlights'] = _onlineFlights;
    map['violations'] = _violations;
    map['xp'] = _xp;
    map['landingCount'] = _landingCount;
    map['flightTime'] = _flightTime;
    map['atcOperations'] = _atcOperations;
    map['atcRank'] = _atcRank;
    map['grade'] = _grade;
    map['hash'] = _hash;
    if (_violationCountByLevel != null) {
      map['violationCountByLevel'] = _violationCountByLevel?.toJson();
    }
    map['roles'] = _roles;
    map['userId'] = _userId;
    map['virtualOrganization'] = _virtualOrganization;
    map['discourseUsername'] = _discourseUsername;
    map['groups'] = _groups;
    map['errorCode'] = _errorCode;
    return map;
  }
}

/// level1 : 102
/// level2 : 0
/// level3 : 0

class ViolationCountByLevel {
  int? _level1;
  int? _level2;
  int? _level3;

  int? get level1 => _level1;
  int? get level2 => _level2;
  int? get level3 => _level3;

  ViolationCountByLevel({int? level1, int? level2, int? level3}) {
    _level1 = level1;
    _level2 = level2;
    _level3 = level3;
  }

  ViolationCountByLevel.fromJson(dynamic json) {
    _level1 = json['level1'] as int;
    _level2 = json['level2'] as int;
    _level3 = json['level3'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['level1'] = _level1;
    map['level2'] = _level2;
    map['level3'] = _level3;
    return map;
  }
}
