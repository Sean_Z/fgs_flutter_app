/// passengerName : ""
/// origin : {"code":"","city":""}
/// destination : {"code":"","city":""}
/// duration : ""
/// boardingTime : ""
/// departs : ""
/// arrives : ""
/// gate : ""
/// zone : 1
/// seat : ""
/// flightClass : ""
/// flightNumber : ""

class FlightLogCardBean {
  String? passengerName;
  Origin? origin;
  Destination? destination;
  String? duration;
  String? boardingTime;
  String? departs;
  String? arrives;
  String? gate;
  int? zone;
  String? seat;
  String? flightClass;
  String? flightNumber;

  FlightLogCardBean(
      {this.passengerName,
      this.origin,
      this.destination,
      this.duration,
      this.boardingTime,
      this.departs,
      this.arrives,
      this.gate,
      this.zone,
      this.seat,
      this.flightClass,
      this.flightNumber});

  FlightLogCardBean.fromJson(dynamic json) {
    passengerName = json['passengerName'] as String;
    origin = json['origin'] != null ? Origin.fromJson(json['origin']) : null;
    destination = json['destination'] != null ? Destination.fromJson(json['destination']) : null;
    duration = json['duration'] as String;
    boardingTime = json['boardingTime'] as String;
    departs = json['departs'] as String;
    arrives = json['arrives'] as String;
    gate = json['gate'] as String;
    zone = json['zone'] as int;
    seat = json['seat'] as String;
    flightClass = json['flightClass'] as String;
    flightNumber = json['flightNumber'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['passengerName'] = passengerName;
    if (origin != null) {
      map['origin'] = origin?.toJson();
    }
    if (destination != null) {
      map['destination'] = destination?.toJson();
    }
    map['duration'] = duration;
    map['boardingTime'] = boardingTime;
    map['departs'] = departs;
    map['arrives'] = arrives;
    map['gate'] = gate;
    map['zone'] = zone;
    map['seat'] = seat;
    map['flightClass'] = flightClass;
    map['flightNumber'] = flightNumber;
    return map;
  }
}

/// code : ""
/// city : ""

class Destination {
  String? code;
  String? city;

  Destination({this.code, this.city});

  Destination.fromJson(dynamic json) {
    code = json['code'] as String;
    city = json['city'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['code'] = code;
    map['city'] = city;
    return map;
  }
}

/// code : ""
/// city : ""

class Origin {
  String? code;
  String? city;

  Origin({this.code, this.city});

  Origin.fromJson(dynamic json) {
    code = json['code'] as String;
    city = json['city'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['code'] = code;
    map['city'] = city;
    return map;
  }
}
