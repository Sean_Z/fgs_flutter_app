/// endRow : 1
/// hasNextPage : false
/// hasPreviousPage : false
/// isFirstPage : true
/// isLastPage : true
/// list : [{"biz_type":1,"create_time":"2021-08-26T11:00:02","file_name":"VQPR_CHARTS.pdf","file_size":2593448,"file_type":"pdf","id":1,"modify_time":"2021-08-26T11:00:02","uploader":"Sean_Zhang"}]
/// navigateFirstPage : 1
/// navigateLastPage : 1
/// navigatePages : 8
/// navigatepageNums : [1]
/// nextPage : 0
/// pageNum : 1
/// pageSize : 10
/// pages : 1
/// prePage : 0
/// size : 1
/// startRow : 1
/// total : 1

class FgsGetFileListBean {
  int? _endRow;
  bool? _hasNextPage;
  bool? _hasPreviousPage;
  bool? _isFirstPage;
  bool? _isLastPage;
  late List<FileList> _list;
  late int _navigateFirstPage;
  late int _navigateLastPage;
  late int _navigatePages;
  late List _navigatepageNums;
  late int _nextPage;
  late int _pageNum;
  late int _pageSize;
  late int _pages;
  late int _prePage;
  late int _size;
  late int _startRow;
  late int _total;

  int? get endRow => _endRow;
  bool? get hasNextPage => _hasNextPage;
  bool? get hasPreviousPage => _hasPreviousPage;
  bool? get isFirstPage => _isFirstPage;
  bool? get isLastPage => _isLastPage;
  List<FileList> get list => _list;
  int get navigateFirstPage => _navigateFirstPage;
  int get navigateLastPage => _navigateLastPage;
  int get navigatePages => _navigatePages;
  List get navigatepageNums => _navigatepageNums;
  int get nextPage => _nextPage;
  int get pageNum => _pageNum;
  int get pageSize => _pageSize;
  int get pages => _pages;
  int get prePage => _prePage;
  int get size => _size;
  int get startRow => _startRow;
  int get total => _total;

  FgsGetFileListBean(
      {int? endRow,
      bool? hasNextPage,
      bool? hasPreviousPage,
      bool? isFirstPage,
      bool? isLastPage,
      required List<FileList> list,
      required int navigateFirstPage,
      required int navigateLastPage,
      required int navigatePages,
      required List navigatepageNums,
      required int nextPage,
      required int pageNum,
      required int pageSize,
      required int pages,
      required int prePage,
      required int size,
      required int startRow,
      required int total}) {
    _endRow = endRow;
    _hasNextPage = hasNextPage;
    _hasPreviousPage = hasPreviousPage;
    _isFirstPage = isFirstPage;
    _isLastPage = isLastPage;
    _list = list;
    _navigateFirstPage = navigateFirstPage;
    _navigateLastPage = navigateLastPage;
    _navigatePages = navigatePages;
    _navigatepageNums = navigatepageNums;
    _nextPage = nextPage;
    _pageNum = pageNum;
    _pageSize = pageSize;
    _pages = pages;
    _prePage = prePage;
    _size = size;
    _startRow = startRow;
    _total = total;
  }

  FgsGetFileListBean.fromJson(dynamic json) {
    _endRow = json['endRow'] as int;
    _hasNextPage = json['hasNextPage'] as bool;
    _hasPreviousPage = json['hasPreviousPage'] as bool;
    _isFirstPage = json['isFirstPage'] as bool;
    _isLastPage = json['isLastPage'] as bool;
    if (json['list'] != null) {
      _list = [];
      json['list'].forEach((v) {
        _list.add(FileList.fromJson(v));
      });
    }
    _navigateFirstPage = json['navigateFirstPage'] as int;
    _navigateLastPage = json['navigateLastPage'] as int;
    _navigatePages = json['navigatePages'] as int;
    _navigatepageNums = json['navigatepageNums'] != null ? json['navigatepageNums'] as List : [];
    _nextPage = json['nextPage'] as int;
    _pageNum = json['pageNum'] as int;
    _pageSize = json['pageSize'] as int;
    _pages = json['pages'] as int;
    _prePage = json['prePage'] as int;
    _size = json['size'] as int;
    _startRow = json['startRow'] as int;
    _total = json['total'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['endRow'] = _endRow;
    map['hasNextPage'] = _hasNextPage;
    map['hasPreviousPage'] = _hasPreviousPage;
    map['isFirstPage'] = _isFirstPage;
    map['isLastPage'] = _isLastPage;
    map['list'] = _list.map((v) => v.toJson()).toList();
    map['navigateFirstPage'] = _navigateFirstPage;
    map['navigateLastPage'] = _navigateLastPage;
    map['navigatePages'] = _navigatePages;
    map['navigatepageNums'] = _navigatepageNums;
    map['nextPage'] = _nextPage;
    map['pageNum'] = _pageNum;
    map['pageSize'] = _pageSize;
    map['pages'] = _pages;
    map['prePage'] = _prePage;
    map['size'] = _size;
    map['startRow'] = _startRow;
    map['total'] = _total;
    return map;
  }
}

/// biz_type : 1
/// create_time : "2021-08-26T11:00:02"
/// file_name : "VQPR_CHARTS.pdf"
/// file_size : 2593448
/// file_type : "pdf"
/// id : 1
/// modify_time : "2021-08-26T11:00:02"
/// uploader : "Sean_Zhang"

class FileList {
  int? _bizType;
  String? _createTime;
  late String _fileName;
  late int _fileSize;
  String? _fileType;
  int? _id;
  late String _modifyTime;
  String? _uploader;

  int? get bizType => _bizType;
  String? get createTime => _createTime;
  String get fileName => _fileName;
  int get fileSize => _fileSize;
  String? get fileType => _fileType;
  int? get id => _id;
  String get modifyTime => _modifyTime;
  String? get uploader => _uploader;

  FileList(
      {int? bizType,
      String? createTime,
      required String fileName,
      required int fileSize,
      String? fileType,
      int? id,
      required String modifyTime,
      String? uploader}) {
    _bizType = bizType;
    _createTime = createTime;
    _fileName = fileName;
    _fileSize = fileSize;
    _fileType = fileType;
    _id = id;
    _modifyTime = modifyTime;
    _uploader = uploader;
  }

  FileList.fromJson(dynamic json) {
    _bizType = json['biz_type'] as int;
    _createTime = json['create_time'] as String;
    _fileName = json['file_name'] as String;
    _fileSize = json['file_size'] as int;
    _fileType = json['file_type'] as String;
    _id = json['id'] as int;
    _modifyTime = json['modify_time'] as String;
    _uploader = json['uploader'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['biz_type'] = _bizType;
    map['create_time'] = _createTime;
    map['file_name'] = _fileName;
    map['file_size'] = _fileSize;
    map['file_type'] = _fileType;
    map['id'] = _id;
    map['modify_time'] = _modifyTime;
    map['uploader'] = _uploader;
    return map;
  }
}
