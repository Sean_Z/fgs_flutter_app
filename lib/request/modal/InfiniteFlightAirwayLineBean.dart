class InfiniteFlightAirwayLineBean {
  int? errorCode;
  List<Result>? result;

  InfiniteFlightAirwayLineBean({this.errorCode, this.result});

  InfiniteFlightAirwayLineBean.fromJson(dynamic json) {
    errorCode = json['errorCode'] as int;
    if (json['result'] != null) {
      result = [];
      json['result'].forEach((v) {
        result?.add(Result.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errorCode'] = errorCode;
    if (result != null) {
      map['result'] = result?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Result {
  double? latitude;
  double? longitude;
  double? altitude;
  double? track;
  double? groundSpeed;
  String? date;

  Result({this.latitude, this.longitude, this.altitude, this.track, this.groundSpeed, this.date});

  Result.fromJson(dynamic json) {
    latitude = double.parse((json['latitude'] ?? 0.0).toString());
    longitude = double.parse((json['longitude'] ?? 0.0).toString());
    altitude = double.parse((json['altitude'] ?? 0.0).toString());
    track = double.parse((json['track'] ?? 0.0).toString());
    groundSpeed = double.parse((json['groundSpeed'] ?? 0.0).toString());
    date = json['date'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['altitude'] = altitude;
    map['track'] = track;
    map['groundSpeed'] = groundSpeed;
    map['date'] = date;
    return map;
  }
}
