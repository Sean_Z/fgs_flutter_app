import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:ifgs/constant/ConfigData.dart';

import '../core/Common.dart';

class InterfaceUrlMap {
  final String Function(String data, int serverType) atis;
  final String Function(int serverType) flights;
  final String Function(String data, int serverType) route;
  final String Function(String data, int serverType) flightplan;
  final String Function(int serverType) atc;
  final String userStatus;
  final String Function(String data) userGrade;
  final String Function(String data, int serverType) airportStatus;
  final String Function(String sessionValue) worldStatus;
  final String Function(String data) userFlights;
  final String Function(String userId, String flightId) userFlight;
  final String airport;
  final String liveries;
  InterfaceUrlMap({
    required this.atis,
    required this.flights,
    required this.route,
    required this.flightplan,
    required this.atc,
    required this.userStatus,
    required this.userGrade,
    required this.airportStatus,
    required this.worldStatus,
    required this.userFlights,
    required this.userFlight,
    required this.liveries,
    required this.airport
  });
}

final interfaceUrlMap = InterfaceUrlMap(
  atis: (String data, int serverType) {
    final sessionValue = Links.getSessionId(serverType);
    return 'https://api.infiniteflight.com/public/v2/sessions/$sessionValue/airport/$data/atis';
  },
  flights: (int serverType) {
    final sessionValue = Links.getSessionId(serverType);
    return 'https://api.infiniteflight.com/public/v2/sessions/$sessionValue/flights';
  },
  route: (String data, int serverType) {
    final sessionValue = Links.getSessionId(serverType);
    return 'https://api.infiniteflight.com/public/v2/sessions/$sessionValue/flights/$data/route';
  },
  flightplan: (String data, int serverType) {
    final sessionValue = Links.getSessionId(serverType);
    return 'https://api.infiniteflight.com/public/v2/sessions/$sessionValue/flights/$data/flightplan';
  },
  atc: (int serverType) {
    final sessionValue = Links.getSessionId(serverType);
    return 'https://api.infiniteflight.com/public/v2/sessions/$sessionValue/atc';
  },
  userStatus: 'https://api.infiniteflight.com/public/v2/users',
  userGrade: (String data) {
    return 'https://api.infiniteflight.com/public/v2/users/$data';
  },
  airportStatus: (String data, int serverType) {
    final sessionValue = Links.getSessionId(serverType);
    return 'https://api.infiniteflight.com/public/v2/sessions/$sessionValue/airport/$data/status';
  },
  worldStatus: (String sessionValue) {
    return 'https://api.infiniteflight.com/public/v2/sessions/$sessionValue/world';
  },
  userFlights: (String data) {
    return 'https://api.infiniteflight.com/public/v2/users/$data/flights';
  },
  userFlight: (String userId, String flightId) {
    return 'https://api.infiniteflight.com/public/v2/users/$userId/flights/$flightId';
  },
  airport: 'https://api.infiniteflight.com/public/v2/airports',
  liveries: 'https://api.infiniteflight.com/public/v2/aircraft/liveries',
);

class InfiniteFlightLiveDataCaller {
  static final dio = Dio();
  static Future<Map<String, dynamic>> get(String url) async {
    try {
      final option = Options(
        responseType: ResponseType.json,
        contentType: 'application/json;charset=UTF-8',
        headers: {'Authorization': 'Bearer ${KeyManager.infiniteFlightApiKey}'},
      );
      var res = await dio.get(url, options: option);
      return json.decode(res.toString()) as Map<String, dynamic>;
    } on Map<String, dynamic> catch (e) {
      Log.logger.i(e['response'].statusMessage);
      throw Exception(e['response'].statusMessage);
    }
  }

  static Future postLiveData(String userId, Map<String, Object> requestData) async {
    var url = 'https://api.infiniteflight.com/public/v2/user/stats/?apikey=${KeyManager.infiniteFlightApiKey}';
    var res = await dio.post(url, data: requestData);
    return json.decode(res.toString());
  }
}
