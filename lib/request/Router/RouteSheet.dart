import 'package:get/get.dart';
import 'package:ifgs/fragment/AboutMeFragment.dart';
import 'package:ifgs/fragment/OsmMapFragment.dart';
import 'package:ifgs/pageview/ATCMonitorPageView.dart';
import 'package:ifgs/pageview/AircraftAdvicePageView.dart';
import 'package:ifgs/pageview/AirportAdvicePageView.dart';
import 'package:ifgs/pageview/AirportScreenPageView.dart';
import 'package:ifgs/pageview/AviationNewsPageView.dart';
import 'package:ifgs/pageview/CertificatePackagePageView.dart';
import 'package:ifgs/pageview/ChartsManagerPageView.dart';
import 'package:ifgs/pageview/CrewCenterPageView.dart';
import 'package:ifgs/pageview/DrawFplPageView.dart';
import 'package:ifgs/pageview/FlightInfoPageView.dart';
import 'package:ifgs/pageview/FlightReviewPageView.dart';
import 'package:ifgs/pageview/HistoryTravelListPageView.dart';
import 'package:ifgs/pageview/IndexPageView.dart';
import 'package:ifgs/pageview/InfiniteFlightAchievementPageView.dart';
import 'package:ifgs/pageview/LoginPageView.dart';
import 'package:ifgs/pageview/MarkupFlightPageView.dart';
import 'package:ifgs/pageview/PdfViewer.dart';
import 'package:ifgs/pageview/RegisterPageView.dart';
import 'package:ifgs/pageview/SettingPageView.dart';
import 'package:ifgs/pageview/UserPrivatePrivacyPageView.dart';

class MainRouterSheet {
  static final routes = <GetPage>[
    GetPage(name: '/index', page: () => IndexPageView()),
    GetPage(name: '/userCenter', page: () => AboutMeFragment()),
    GetPage(name: '/login', page: () => LoginPageView()),
    GetPage(name: '/crew', page: () => CrewCenterPageView()),
    GetPage(name: '/flightInfo', page: () => FlightInfoPageView()),
    GetPage(name: '/airportScreen', page: () => AirportScreenPageView()),
    GetPage(name: '/atcMonitor', page: () => ATCMonitorPageView()),
    GetPage(name: '/setting', page: () => SettingPageView()),
    GetPage(name: '/historyList', page: () => HistoryTravelListPageView()),
    GetPage(name: '/osm', page: () => const OsmMapFragment()),
    GetPage(name: '/infiniteFlightAchievement', page: () => InfiniteFlightAchievementPageView()),
    GetPage(name: '/certificatePackage', page: () => CertificatePackagePageView()),
    GetPage(name: '/register', page: () => RegisterPageView()),
    GetPage(name: '/drawPlan', page: () => DrawFplPageView()),
    GetPage(name: '/charts', page: () => ChartsManagerPageView()),
    GetPage(name: '/pdfViewer', page: () => PdfViewer()),
    GetPage(name: '/aviationNews', page: () => const AviationNewsPageView()),
    GetPage(name: '/aircraftAdvice', page: () => const AircraftAdvicePageView()),
    GetPage(name: '/userPrivatePrivacy', page: () => const UserPrivatePrivacyPageView()),
    GetPage(name: '/flightReview', page: () => const FlightReviewPageView()),
    GetPage(name: '/markupFlight', page: () => const MarkupFlightPageView()),
    GetPage(name: '/airportAdvice', page: () => AirportAdvicePageView()),
  ];
}

abstract class RouterSheet {
  static const index = '/index';
  static const userCenter = '/userCenter';
  static const login = '/login';
  static const crew = '/crew';
  static const flightList = '/flightList';
  static const flightInfo = '/flightInfo';
  static const flightPlanGenerator = '/flightPlanGenerator';
  static const airportScreen = '/airportScreen';
  static const historyList = '/historyList';
  static const atcMonitor = '/atcMonitor';
  static const achievement = '/achievement';
  static const setting = '/setting';
  static const osm = '/osm';
  static const infiniteFlightAchievement = '/infiniteFlightAchievement';
  static const certificatePackage = '/certificatePackage';
  static const ffp = '/ffp';
  static const register = '/register';
  static const ffpDetail = '/ffpDetail';
  static const drawPlan = '/drawPlan';
  static const charts = '/charts';
  static const pdfViewer = '/pdfViewer';
  static const aviationNews = '/aviationNews';
  static const aircraftAdvice = '/aircraftAdvice';
  static const userPrivatePrivacy = '/userPrivatePrivacy';
  static const flightReview = '/flightReview';
  static const markupFlight = '/markupFlight';
  static const airportAdvice = '/airportAdvice';
}
