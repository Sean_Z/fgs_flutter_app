import 'dart:convert';

import 'package:dio/dio.dart';

class ThirdPartServiceCaller {
  static Future get(String url) async {
    final dio = Dio();
    dio.options = BaseOptions(responseType: ResponseType.plain, contentType: 'application/json;charset=GBK');
    final _response = await dio.get(url);
    final _decodeResult = json.decode(_response.toString());
    return _decodeResult;
  }
}
