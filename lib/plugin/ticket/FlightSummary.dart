import 'package:flutter/material.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/main.dart';
import 'package:ifgs/request/modal/flight_log_res_entity.dart';
import 'package:ifgs/stylesheet/Theme.dart';
import 'package:intl/intl.dart';

enum SummaryTheme { dark, light }

class FlightSummary extends StatelessWidget {
  final FlightLogResList boardingPass;
  final SummaryTheme theme;
  final bool isOpen;

  const FlightSummary({Key? key, required this.boardingPass, this.theme = SummaryTheme.light, this.isOpen = false}) : super(key: key);

  Color get mainTextColor {
    late Color textColor;
    final applicationContext = navKey.currentContext;
    if (theme == SummaryTheme.dark) textColor = Colors.white;
    if (theme == SummaryTheme.light) {
      textColor = UIAdapter.isDarkTheme(applicationContext!) ? Colors.white : const Color(0xFF083e64);
    }
    return textColor;
  }

  Color get secondaryTextColor {
    late Color textColor;
    if (theme == SummaryTheme.dark) textColor = const Color(0xff61849c);
    if (theme == SummaryTheme.light) textColor = const Color(0xFF838383);
    return textColor;
  }

  Color get separatorColor {
    late Color color;
    if (theme == SummaryTheme.light) color = const Color(0xffeaeaea);
    if (theme == SummaryTheme.dark) color = const Color(0xff396583);
    return color;
  }

  TextStyle get bodyTextStyle => TextStyle(color: mainTextColor, fontSize: 13, fontFamily: 'Oswald');

  Widget get favorite => theme == SummaryTheme.light ? Positioned(bottom: 0, right: 16, child: _buildFavorite()) : Container();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          child: Container(
            decoration: _getBackgroundDecoration(context),
            width: double.infinity,
            height: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  _buildLogoHeader(),
                  _buildSeparationLine(),
                  _buildTicketHeader(context),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Stack(
                      children: <Widget>[
                        Align(alignment: Alignment.centerLeft, child: _buildTicketOrigin()),
                        Align(alignment: Alignment.center, child: _buildTicketDuration(context)),
                        Align(alignment: Alignment.centerRight, child: _buildTicketDestination())
                      ],
                    ),
                  ),
                  _buildBottomIcon(),
                ],
              ),
            ),
          ),
        ),
        favorite
      ],
    );
  }

  Widget _buildFavorite() {
    if (boardingPass.searchFavorite == 1) {
      return const Icon(Icons.recommend, color: Colors.amber);
    } else {
      return Container();
    }
  }

  Decoration _getBackgroundDecoration(BuildContext context) {
    if (theme == SummaryTheme.light) {
      return BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        color: UIAdapter.isDarkTheme(context) ? DarkTheme.deepDarkGrey : Colors.white,
      );
    } else {
      return BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        image: const DecorationImage(image: AssetImage('images/ticket/bg_blue.png'), fit: BoxFit.cover),
      );
    }
  }

  Row _buildLogoHeader() {
    final textStyle = TextStyle(
      color: mainTextColor,
      fontFamily: 'OpenSans',
      fontSize: 12,
      fontWeight: FontWeight.bold,
      letterSpacing: 1.5,
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 4.0),
          child: FlutterLogo(size: 16),
        ),
        Text(boardingPass.flightNumber.toUpperCase(), style: textStyle)
      ],
    );
  }

  Container _buildSeparationLine() {
    return Container(
      width: double.infinity,
      height: 1,
      color: separatorColor,
    );
  }

  Row _buildTicketHeader(BuildContext context) {
    var headerStyle = const TextStyle(fontFamily: 'OpenSans', fontWeight: FontWeight.bold, fontSize: 11, color: Color(0xFFe46565));
    final rawDate = DateTime.parse(boardingPass.boardingTime);
    final boardingTime = DateFormat('MMM d, H:mm', 'en_US').format(rawDate);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(boardingPass.pilot.toUpperCase(), style: headerStyle),
        Text('BOARDING $boardingTime', style: headerStyle),
      ],
    );
  }

  Column _buildTicketOrigin() {
    return Column(
      children: <Widget>[
        Text(
          boardingPass.departure,
          style: bodyTextStyle.copyWith(fontSize: 42),
        ),
        Text(
          boardingPass.departureAirportName,
          textAlign: TextAlign.center,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: bodyTextStyle.copyWith(color: secondaryTextColor),
        ),
      ],
    );
  }

  Container _buildTicketDuration(BuildContext context) {
    var planeRoutePath = '';
    if (theme == SummaryTheme.light) {
      planeRoutePath = 'images/ticket/planeroute_${UIAdapter.isDarkTheme(context) ? 'white' : 'blue'}.png';
    }
    if (theme == SummaryTheme.dark) planeRoutePath = 'images/ticket/planeroute_white.png';

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            width: 120,
            height: 58,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Image.asset(planeRoutePath, fit: BoxFit.cover),
                if (theme == SummaryTheme.light)
                  Image.asset('images/ticket/airplane_${UIAdapter.isDarkTheme(context) ? 'white' : 'blue'}.png',
                      height: 20, fit: BoxFit.contain),
                if (theme == SummaryTheme.dark)
                  _AnimatedSlideToRight(
                    isOpen: isOpen,
                    child: Image.asset('images/ticket/airplane_white.png', height: 20, fit: BoxFit.contain),
                  )
              ],
            ),
          ),
          Text(FlightJudgment.formatFlightTime(double.parse(boardingPass.flightTime).toInt()),
              textAlign: TextAlign.center, style: bodyTextStyle),
        ],
      ),
    );
  }

  Widget _buildTicketDestination() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          boardingPass.arrival,
          style: bodyTextStyle.copyWith(fontSize: 42),
        ),
        SizedBox(
          width: 100,
          child: Text(
            boardingPass.arrivalAirportName,
            textAlign: TextAlign.center,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: bodyTextStyle.copyWith(color: secondaryTextColor),
          ),
        )
      ],
    );
  }

  Widget _buildBottomIcon() {
    late IconData icon;
    if (theme == SummaryTheme.light) icon = Icons.keyboard_arrow_down;
    if (theme == SummaryTheme.dark) icon = Icons.keyboard_arrow_up;
    return Icon(
      icon,
      color: mainTextColor,
      size: 18,
    );
  }
}

class _AnimatedSlideToRight extends StatefulWidget {
  final Widget? child;
  final bool isOpen;

  const _AnimatedSlideToRight({Key? key, this.child, required this.isOpen}) : super(key: key);

  @override
  _AnimatedSlideToRightState createState() => _AnimatedSlideToRightState();
}

class _AnimatedSlideToRightState extends State<_AnimatedSlideToRight> with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 1700));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isOpen) _controller.forward(from: 0);
    return SlideTransition(
      position: Tween(begin: const Offset(-2, 0), end: const Offset(0.7, 0))
          .animate(CurvedAnimation(curve: Curves.easeOutQuad, parent: _controller)),
      child: widget.child,
    );
  }
}
