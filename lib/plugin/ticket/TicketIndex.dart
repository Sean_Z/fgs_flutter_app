import 'dart:math';

import 'package:flutter/material.dart';
import 'package:ifgs/api/FgsServiceApi.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';

import '../../core/CustomUIViewProvider.dart';
import '../../request/modal/flight_log_res_entity.dart';
import 'TicketMain.dart';

class TicketIndex extends StatefulWidget {
  @override
  _TicketIndexState createState() => _TicketIndexState();
  TicketIndex({Key? key, this.isReview = false, this.log}) : super(key: key);
  final bool? isReview;
  final FlightLogResEntity? log;
}

class _TicketIndexState extends State<TicketIndex> {
  final ScrollController _scrollController = ScrollController();
  final List<int> _openTickets = [];
  var _isLoadingMore = false;
  List<FlightLogResList> _logList = [];
  var _totalPage = 1;
  var _pageNo = 1;

  Future<void> _getHistoryLogList(bool shouldLoading) async {
    _pageNo = 1;
    _totalPage = 1;
    if (widget.isReview ?? false) {
      setState(() {
        _logList = widget.log!.list.sublist(0, 1);
        _totalPage = 1;
      });
    } else {
      if (shouldLoading) {
        CustomViewProvider.showMaterialLoading('Loading logs....', context);
      }
      final _response = await FgsServiceApi.getFlightLog(1, 10);
      CustomViewProvider.hideMaterialLoading();
      setState(() {
        _logList = _response.list;
        _totalPage = _response.pages;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _getHistoryLogList(true);
    _scrollController.addListener(() async {
      var _initPageSize = 10;
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        _pageNo++;
        if (_pageNo > _totalPage) {
          /// stop count pageNo
          _pageNo = _totalPage;
          UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'no more data!'));
          return;
        }
        setState(() {
          _isLoadingMore = true;
        });
        final _nextPageData = await FgsServiceApi.getFlightLog(_pageNo, _initPageSize);
        _logList.addAll(_nextPageData.list);
        setState(() {
          _isLoadingMore = false;
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
    CustomViewProvider.hideMaterialLoading();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Flex(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        direction: Axis.vertical,
        children: <Widget>[
          Expanded(
            child: RefreshIndicator(
              onRefresh: () => _getHistoryLogList(false),
              child: _MainTicketList(),
            ),
          ),
          _isLoadingMore
              ? Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: const CircularProgressIndicator(),
                )
              : Container(),
        ],
      ),
    );
  }

  ListView _MainTicketList() {
    return ListView.builder(
      controller: _scrollController,
      physics: const BouncingScrollPhysics(),
      itemCount: _logList.length,
      itemBuilder: (BuildContext context, int index) {
        return TicketMain(
          boardingPass: _logList[index],
          onClick: () => _handleClickedTicket(index),
          isReview: false,
        );
      },
    );
  }

  bool _handleClickedTicket(int clickedTicket) {
    // Scroll to ticket position
    // Add or remove the item of the list of open tickets
    _openTickets.contains(clickedTicket) ? _openTickets.remove(clickedTicket) : _openTickets.add(clickedTicket);

    /// single card height is 200 minus 80 offset
    var offset = 200 * clickedTicket - 280;

    // Scroll to the clicked elements
    if (_scrollController.hasClients) {
      _scrollController.animateTo(max(0, offset.toDouble()),
          duration: const Duration(seconds: 1), curve: const Interval(.25, 1, curve: Curves.easeOutQuad));
    }
    // Return true to stop the notification propagation
    return true;
  }
}
