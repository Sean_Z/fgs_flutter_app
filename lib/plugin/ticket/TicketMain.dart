import 'package:flutter/material.dart';

import '../../request/modal/flight_log_res_entity.dart';
import 'FlightBarcode.dart';
import 'FlightDetails.dart';
import 'FlightSummary.dart';
import 'FoldingTicket.dart';

class TicketMain extends StatefulWidget {
  static const double nominalOpenHeight = 400;
  static const double nominalClosedHeight = 160;
  final FlightLogResList boardingPass;
  final VoidCallback onClick;
  final bool isReview;

  const TicketMain({Key? key, required this.boardingPass, required this.onClick, this.isReview = false}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _TicketMainState();
}

class _TicketMainState extends State<TicketMain> {
  late FlightSummary frontCard;
  late FlightSummary topCard;
  late FlightDetails middleCard;
  late FlightBarcode bottomCard;
  late bool _isOpen = widget.isReview == false ? false : true;

  Container get backCard => Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.0),
          color: Theme.of(context).cardTheme.color,
        ),
      );

  @override
  void initState() {
    super.initState();
    topCard = FlightSummary(boardingPass: widget.boardingPass, theme: SummaryTheme.dark, isOpen: _isOpen);
    _isOpen = widget.isReview == false ? false : true;
    frontCard = FlightSummary(boardingPass: widget.boardingPass);
    middleCard = FlightDetails(widget.boardingPass);
    bottomCard = FlightBarcode(route: widget.boardingPass.route);
  }

  @override
  Widget build(BuildContext context) {
    return FoldingTicket(
      entries: _getEntries(),
      isOpen: _isOpen,
      onClick: _handleOnTap,
      boardingPass: widget.boardingPass,
    );
  }

  List<FoldEntry> _getEntries() {
    return [
      FoldEntry(height: 160.0, front: topCard),
      FoldEntry(height: 200.0, front: middleCard, back: frontCard),
      FoldEntry(height: 80.0, front: bottomCard, back: backCard)
    ];
  }

  void _handleOnTap() {
    if (!widget.isReview) {
      widget.onClick();
      setState(() {
        _isOpen = !_isOpen;
        topCard = FlightSummary(
          boardingPass: widget.boardingPass,
          theme: SummaryTheme.dark,
          isOpen: _isOpen,
        );
      });
    }
  }
}
