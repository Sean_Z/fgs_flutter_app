import 'package:flutter/material.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/request/modal/flight_log_res_entity.dart';
import 'package:ifgs/stylesheet/Theme.dart';

class FlightDetails extends StatelessWidget {
  final FlightLogResList boardingPass;

  FlightDetails(this.boardingPass);

  @override
  Widget build(BuildContext context) {
    final titleTextStyle = TextStyle(
        fontFamily: 'OpenSans',
        fontSize: 11,
        height: 1,
        letterSpacing: .2,
        fontWeight: FontWeight.w600,
        color: Theme.of(context).textTheme.bodyMedium?.color
    );

    final contentTextStyle = TextStyle(fontSize: 16, height: 1.8, letterSpacing: .3, color: Theme.of(context).textTheme.bodyMedium?.color);

    return Container(
      decoration: BoxDecoration(
        color: UIAdapter.isDarkTheme(context) ? DarkTheme.deepDarkGrey : Colors.white,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        margin: const EdgeInsets.only(left: 16, right: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Text('Callsign'.toUpperCase(), style: titleTextStyle),
                  Text(boardingPass.flightNumber.toString(), style: contentTextStyle),
                ]),
                Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Text('Class'.toUpperCase(), style: titleTextStyle),
                  Text(FlightJudgment.getFlightHaulType(boardingPass.distance, boardingPass.departure, boardingPass.arrival),
                      style: contentTextStyle),
                ]),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Text('Day Time'.toUpperCase(), style: titleTextStyle),
                  Text('${boardingPass.dayTime.toInt().toString()} mins', style: contentTextStyle),
                ]),
                Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Text('Night Time'.toUpperCase(), style: titleTextStyle),
                  Text('${boardingPass.nightTime.toInt().toString()} mins', style: contentTextStyle),
                ]),
              ],
            ),
            Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                    Text('Aircraft'.toUpperCase(), style: titleTextStyle),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Text(boardingPass.aircraft, style: contentTextStyle),
                    ),
                  ]),
                ),
                Flexible(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Airline'.toUpperCase(), style: titleTextStyle),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Text(
                          boardingPass.livery.toUpperCase(),
                          maxLines: 1,
                          style: contentTextStyle,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
