import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ifgs/core/Common.dart';

class FlightBarcode extends StatelessWidget {
  const FlightBarcode({Key? key, required this.route}) : super(key: key);

  final String route;
  @override
  Widget build(BuildContext context) {
    final Color _barcodeBgColor;
    if (UIAdapter.isDarkTheme(context)) {
      _barcodeBgColor = const Color(0xffafafaf);
    } else {
      _barcodeBgColor = Colors.white;
    }
    return Container(
        width: double.infinity,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(4.0), color: _barcodeBgColor),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 14.0),
          child: MaterialButton(
              onPressed: () {
                Clipboard.setData(ClipboardData(text: route));
              },
              child: Image.asset('images/ticket/barcode.png')),
        ));
  }
}
