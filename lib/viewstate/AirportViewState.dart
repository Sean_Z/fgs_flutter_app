import 'package:flutter/cupertino.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';

class AirportState {
  var airportICAO = 'KLAX';

  List<FgsOnlineFlightData> inBoundFlight = [];

  List<FgsOnlineFlightData> outBoundFlight = [];

  /// Inbound aircraft count
  var inboundAircraftCount = 0;

  /// Count of aircraft which is outbound
  var outboundAircraftCount = 0;

  /// Atis information
  var atisInfo = 'Loading ATIS information .......';
  var rawAtisInfo = 'Loading ATIS information .......';
  var transformedAtisInfo = 'Loading ATIS information .......';
  var transformAtis = false;

  /// Should BoundFlight Widget appear
  var isHasData = false;

  /// Selected server type
  var serverType = 0;

  /// Selected airport default is KLAX
  var searchedAirport = 'KLAX';

  /// IsInbound
  var isInbound = true;

  final TextEditingController airportCodeController = TextEditingController();
  final airportCodeNode = FocusNode();
}
