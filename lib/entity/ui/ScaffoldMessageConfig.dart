import 'package:flutter/material.dart';

class ScaffoldMessageConfig {
  final String message;
  int? duration = 2;
  Icon? icon = const Icon(Icons.info, color: Colors.white);

  ScaffoldMessageConfig({
    required this.message,
    this.duration,
    this.icon
  });
}
