import 'package:flutter/material.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';

extension HexColor on Color {
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

class ColorCommonMethod {
  static List<double> colorStop(List<AirwayLineListBean> airwayLineResponse) {
    final _airwaySpeedList = <double>[];
    final _airwaySpeedListForColorStop = <double>[];
    airwayLineResponse.forEach((element) {
      _airwaySpeedList.add(element.groundSpeed);
    });
    _airwaySpeedList.sort((a, b) => a.compareTo(b));
    if (_airwaySpeedList.isEmpty) {
      _airwaySpeedList.add(0.0);
    }
    final _maxSpeedInRuntime = _airwaySpeedList.last;
    for (var _item in airwayLineResponse) {
      _airwaySpeedListForColorStop.add(_item.groundSpeed / _maxSpeedInRuntime);
    }
    return _airwaySpeedListForColorStop;
  }
}

class StatueColor {
  static final String ground = '#FB8C00';
  static final String climbing = '#69F0AE';
  static final String cruising = '#1565C0';
  static final String descending = '#FF7043';
  static final String unknownColor = '#607D8B';
}

class AirlineFFPCardColorList {
  static final List<List<Color>> chinaEasternColorList = [
    [Colors.blue, const Color(0xEED01608)],
    [Colors.blue[200]!, const Color(0x61D9154C)],
    [Colors.blueAccent, const Color(0xFFFF5200)],
    [Colors.blue, const Color(0x55CA05C7)]
  ];
  static final List<List<Color>> hainanColorList = [
    [Colors.red, const Color(0xEEF44336)],
    [Colors.red[200]!, const Color(0x77E57373)],
    [Colors.orange, const Color(0x66FF9800)],
    [Colors.yellow, const Color(0x55FFEB3B)]
  ];
}
