import 'package:flutter/material.dart';

abstract class StartActivityService {
  /// Main function start all of event while app starting
  void appStartEvent();

  /// Set background for any device bar like chrome
  void systemUiOverlayStyle();

  /// Download/init all of aircraft modal name list
  /// which is fgs has photo for current aircraft
  Future<void> initOwnAircraftPhotoList({bool shouldFileOverwrite = false});
  Future<void> initAirportStaticData({bool shouldFileOverwrite = false});

  Future<void> downloadAircraftAdvice();

  /// Download/init userList in fgs system
  // Future<void> initFgsMemberList();

  /// Download/init current user info in fgs system
  Future<void> initCurrentUserInfo();

  /// Download/init current user info in IF system
  Future<void> initInfiniteFlightUserInfo();

  /// Download user avatar file
  Future<void> initAvatar();

  /// Download/init all of airline and aircraft from IF livery.json
  Future<void> initAirlineAndAircraftJson({bool shouldFileOverwrite = false});

  /// Init language
  Future<void> initLang();

  /// Init theme
  Future<void> initTheme();

  /// Upload all of exception at runtime to fgs server
  Future<void> reportError(Object? error, StackTrace stackTrace);

  /// Catch all of [Exception] at runtime
  void runWithCatchError(Widget appMain);

  /// Download ifgs apk
  Future<void> downloadApk();

  /// Get last app version info
  Future<void> getAppInfo(BuildContext context, bool shouldShowLatestVersion);

  Future<void> initAirportCharts();
}
