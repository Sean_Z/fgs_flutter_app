import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:ifgs/widget/PublicWidget/UIDialogLoading.dart';
import 'package:ifgs/widget/PublicWidget/UILoading.dart';

class CustomViewProvider {
  static OverlayEntry? _materialDialogLoadingOverlayEntry;
  static OverlayEntry? _materialLoadingOverlayEntry;

  static void showMaterialLoading(String content, BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      final overlayState = Overlay.of(context);
      _materialLoadingOverlayEntry = OverlayEntry(builder: (context) {
        return UILoading(content: content);
      });
      overlayState.insert(_materialLoadingOverlayEntry!);
    });
  }

  static void showMaterialDialogLoading(BuildContext context, {String content = 'Loading'}) {
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      final overlayState = Overlay.of(context);
      _materialDialogLoadingOverlayEntry = OverlayEntry(builder: (context) {
        return UIDialogLoading(content: content);
      });
      overlayState.insert(_materialDialogLoadingOverlayEntry!);
    });
  }

  static void hideMaterialDialogLoading() => clearView(_materialDialogLoadingOverlayEntry);

  static void hideMaterialLoading() => clearView(_materialLoadingOverlayEntry);

  /// clear overlay view
  static void clearView(OverlayEntry? overlayEntry) {
    if (overlayEntry?.mounted == true) {
      overlayEntry?.remove();
    }
  }
}
