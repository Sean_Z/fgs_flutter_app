import 'package:flutter/services.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:local_auth/local_auth.dart';

class LocalAuthenticationService {
  static final _auth = LocalAuthentication();
  static Future<bool> authenticate() async {
    try {
      return await _auth.authenticate(
        localizedReason: 'Please use finger print to Login',
        options: const AuthenticationOptions(
          useErrorDialogs: true,
          stickyAuth: true,
        ),
      );
    } on PlatformException catch (e) {
      UIViewProvider.showToast(e.message.toString());
      return false;
    }
  }
}
