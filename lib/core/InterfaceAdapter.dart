import 'package:ifgs/constant/StaticData.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/request/modal/UserHistoryFlightsEntity.dart';
import 'package:ifgs/request/modal/flight_log_res_entity.dart';
import 'package:ifgs/util/FileUtil.dart';

class InterfaceAdapter {
  static Future<FlightLogResEntity> adapterFlightLog(UserHistoryFlightsResult infiniteFlightLog)async {
    final result = FlightLogResEntity();
    final infiniteFlightUsername = await InfoMethod.infiniteFlightUsername;
    result.size = 0;
    final aircraftData = StaticData.aircraftJson;
    final resultList = <FlightLogResList>[];
    final infiniteFlightLogList = infiniteFlightLog.data;
    infiniteFlightLogList.removeWhere((element) => element.destinationAirport == 'Unknown' || element.originAirport == 'Unknown');
    final airportList = await FileGetter.airportStaticData;
    for (var i = 0; i < infiniteFlightLogList.length; i++) {
      var element = infiniteFlightLog.data[i];
      final destAirport = airportList?.result?.where((airport) => airport.icao == element.destinationAirport);
      final deptAirport = airportList?.result?.where((airport) => airport.icao == element.originAirport);
      var destAirportName = element.destinationAirport;
      var deptAirportName = element.originAirport;
      if(destAirport!.isNotEmpty) destAirportName = destAirport.first.name ?? '';
      if(deptAirport!.isNotEmpty) deptAirportName = deptAirport.first.name ?? '';
      final item = FlightLogResList();
      item.logId = i;
      item.aircraft = aircraftData.firstWhere((_element) => element.aircraftId == _element.aircraftID).aircraftName;
      item.rate = 5;
      item.departureTime = element.created;
      item.departure = element.originAirport;
      item.arrival = element.destinationAirport;
      item.airwayPoint = '';
      item.boardingTime = element.created;
      item.favorite = 2;
      item.createDate = 0;
      item.flightTime = element.totalTime.toString();
      item.arrivalAirportName = destAirportName;
      item.departureAirportName = deptAirportName;
      item.distance = 0;
      item.searchFavorite = 0;
      item.delay = 1;
      item.modifyDate = 0;
      item.livery = aircraftData.firstWhere((_element) => element.aircraftId == _element.aircraftID).liveryName;
      item.route = '';
      item.pilot = infiniteFlightUsername;
      item.flightNumber = element.callsign;
      item.dayTime = element.dayTime;
      item.arrivalTime = element.created;
      item.nightTime = element.nightTime;
      resultList.add(item);
    }

    result.pages = infiniteFlightLog.totalPages;
    result.pageSize = 10;
    result.pageNum = 1;
    result.hasNextPage = true;
    result.hasPreviousPage = true;
    result.list = resultList;
    result.total = infiniteFlightLog.totalCount;
    result.startRow = 1;
    result.endRow = 10;
    result.isFirstPage = true;
    result.isLastPage = false;
    result.navigateFirstPage = 0;
    result.navigateFirstPage = 0;
    result.navigatepageNums = [];
    return result;
  }
}