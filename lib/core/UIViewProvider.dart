import 'dart:io';
import 'dart:math';
import 'dart:ui' as ui;

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:ifgs/constant/AirlineListMap.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/util/FileUtil.dart';
import 'package:ifgs/widget/PublicWidget/SkeletonWidget.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:oktoast/oktoast.dart' as ok_toast;
import 'package:oktoast/oktoast.dart';

import 'Color.dart';
import 'Common.dart';

class UIViewProvider {
  /// if photo in own storage use fgs storage
  static FadeInImage getAirlineAvatar(BuildContext context, String liveryId, String aircraftId, BoxFit fit) {
    final _photoId = '$aircraftId:$liveryId';
    if (OwnAircraftPhotoList.ownAircraftPhotoList.contains(_photoId)) {
      return FadeInImage.assetNetwork(
          width: MediaQuery.of(context).size.width,
          height: 120,
          fit: fit,
          placeholder: 'images/basic/imageLoder.gif',
          image: '${Links.fgsStorageBaseUrl}aircraft/$aircraftId:$liveryId.jpg');
    } else {
      return FadeInImage.assetNetwork(
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
          placeholder: 'images/basic/imageLoder.gif',
          imageErrorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
            return Container(
                alignment: Alignment.center,
                color: HexColor.fromHex('093B6D'),
                height: 160,
                width: double.infinity,
                child: const Wrap(
                  direction: Axis.vertical,
                  alignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Icon(Icons.psychology, size: 80),
                    Text('No Aircraft Shot!',
                        style: TextStyle(
                          color: Colors.white60,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ))
                  ],
                ));
          },
          image: 'https://www.infinitex.app/aircraft-livery/$aircraftId/$liveryId.webp');
    }
  }

  static Widget getCacheAircraftImg(BuildContext context, String liveryId, String aircraftId, {BoxFit fitMode = BoxFit.cover}) {
    final _photoId = '$aircraftId:$liveryId';
    if (OwnAircraftPhotoList.ownAircraftPhotoList.contains(_photoId)) {
      return FadeInImage.assetNetwork(
          width: MediaQuery.of(context).size.width,
          fit: fitMode,
          placeholder: 'images/basic/imageLoder.gif',
          imageErrorBuilder: (BuildContext context, Object error, StackTrace? stackTrace) {
            return Image.network(Links.unknownAircraft, height: 120, width: 150, fit: fitMode);
          },
          image: '${Links.fgsStorageBaseUrl}aircraft/$aircraftId:$liveryId.jpg');
    } else {
      return CachedNetworkImage(
        fit: fitMode,
        imageUrl: '${Links.aircraftPhotoBaseLink + aircraftId}/$liveryId.jpg',
        placeholder: (context, url) => Image.asset('images/basic/imageLoder.gif', fit: fitMode),
        errorWidget: (context, url, error) => Image.network(Links.unknownAircraft, height: 120, width: 150, fit: fitMode),
      );
    }
  }

  static FadeInImage getAssetsAircraftImage(BuildContext context, String liveryId, String aircraftId) {
    return UIViewProvider.getAirlineAvatar(
      context,
      liveryId,
      aircraftId,
      BoxFit.fill,
    );
  }

  /// 获取动态颜色文字
  static AnimatedTextKit getAnimatedColorText(String content, List<Color> colors,
      {int speed = 100, double fontSize = 14, FontWeight fontWeight = FontWeight.w600}) {
    return AnimatedTextKit(
      totalRepeatCount: 1,
      animatedTexts: [
        ColorizeAnimatedText(content,
            speed: Duration(milliseconds: speed),
            colors: colors,
            textStyle: TextStyle(
              fontSize: fontSize,
              fontWeight: fontWeight,
            )),
      ],
    );
  }

  static const toastLengthShort = Duration(seconds: 2);
  static const toastLengthLong = Duration(milliseconds: 3500);

  /// 展示Toast
  static void showToast(String msg,
      {ToastPosition? position = ToastPosition.bottom,
      Color? backgroundColor = Colors.white,
      Color? textColor = Colors.black45,
      Duration? time = UIViewProvider.toastLengthShort}) {
    ok_toast.showToast(
      msg,
      radius: 8,
      textPadding: const EdgeInsets.all(8),
      duration: time,
      position: position,
      textStyle: TextStyle(fontSize: 12.0, color: textColor),
      backgroundColor: backgroundColor,
    );
  }

  /// 展示material消息条
  static void showScaffoldMessenger(BuildContext context, String message, {int duration = 1, Color backgroundColor = Colors.black87}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: Duration(seconds: duration),
      backgroundColor: backgroundColor,
      content: Text(
        message,
        style: const TextStyle(fontSize: 16),
      ),
    ));
  }

  static void showScaffoldSuccess(ScaffoldMessageConfig scaffoldSuccessConfig) {
    Get.showSnackbar(GetSnackBar(
      title: 'Info',
      icon: scaffoldSuccessConfig.icon,
      snackStyle: SnackStyle.GROUNDED,
      duration: Duration(seconds: scaffoldSuccessConfig.duration ?? 2),
      animationDuration: const Duration(milliseconds: 500),
      message: scaffoldSuccessConfig.message,
      backgroundColor: Colors.green,
    ));
  }

  static void showScaffoldError(ScaffoldMessageConfig scaffoldMessageConfig) {
    Get.showSnackbar(GetSnackBar(
      title: 'Info',
      icon: scaffoldMessageConfig.icon,
      snackStyle: SnackStyle.GROUNDED,
      duration: Duration(seconds: scaffoldMessageConfig.duration ?? 2),
      animationDuration: const Duration(milliseconds: 500),
      message: scaffoldMessageConfig.message,
      backgroundColor: Colors.redAccent,
    ));
  }

  /// 弹出对话框
  static void showConfirmDialog(BuildContext context, String content, Widget confirmActionButton) {
    showDialog<void>(
      context: context,
      barrierDismissible: false, // false = user must tap button, true = tap outside dialog
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          contentTextStyle: const TextStyle(fontSize: 18, color: Colors.black87),
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
          content: Text(content),
          actions: <Widget>[
            TextButton(
                onPressed: () => Navigator.of(dialogContext).pop(),
                child: const Text(
                  'Cancel',
                  style: TextStyle(fontSize: 15, color: Colors.redAccent),
                )),
            confirmActionButton,
          ],
        );
      },
    );
  }

  static Future<File> get _getUserAvatarFile async => await FileGetter.userAvatar;

  /// 获取用户头像
  static FutureBuilder getUserAvatar({double radius = 50}) {
    return FutureBuilder(
      future: _getUserAvatarFile,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        final avatar = snapshot.data == null ? const AssetImage('images/fgsLogo.png') : FileImage(snapshot.data as File);
        if (snapshot.connectionState == ConnectionState.done) {
          return CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: radius,
            foregroundImage: avatar as ImageProvider,
            onForegroundImageError: (Object exception, StackTrace? stackTrace) => const Icon(Icons.psychology, color: Colors.indigoAccent),
          );
        } else {
          return Container();
        }
      },
    );
  }

  static Widget getSkeletonWidget(int count, bool? isLoading) => SkeletonWidget(count: count, isLoading: isLoading ?? true);

  static void _cacheAirlineLogo(String liveryName) async {
    final dio = Dio();
    final savePath = await FileUtil.appSupportFilePath + '/img/$liveryName.jpg';
    await dio.download(AirlineListMap.getIcaoCode(liveryName), savePath);
  }

  static Future<File?> _getLocalAirlineLogo(String liveryName) async {
    final imgPath = await FileUtil.appSupportFilePath + '/img/$liveryName.jpg';
    if (File(imgPath).existsSync()) {
      return File(imgPath);
    } else {
      _cacheAirlineLogo(liveryName);
      return null;
    }
  }

  /// Get airline logo
  static FutureBuilder getAirlineLogo(String liveryName) {
    _cacheAirlineLogo(liveryName);
    return FutureBuilder(
      future: _getLocalAirlineLogo(liveryName),
      builder: (BuildContext context, AsyncSnapshot snapShot) {
        if (snapShot.connectionState == ConnectionState.done && snapShot.data != null) {
          return Image.file(
            snapShot.data as File,
            color: UIAdapter.isDarkTheme(context) ? Colors.white : null,
          );
        } else {
          return Container();
        }
      },
    );
  }

  static Color getRandomColor() {
    return Color.fromRGBO(Random().nextInt(256), Random().nextInt(256), Random().nextInt(256), 1);
  }

  /// transform picture to bytes
  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    var data = await rootBundle.load(path);
    var codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    var fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!.buffer.asUint8List();
  }

  static void saveImageToGallery(Uint8List data) async {
    final result = await ImageGallerySaver.saveImage(Uint8List.fromList(data), quality: 60, name: 'chart');
    print(result);
  }
}
