import 'package:flutter/material.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';

import 'Color.dart';

class StatusClass {
  late String color;
  late String status;
  StatusClass(this.color, this.status);
}

class StatusInfoBean {
  late Color color;
  late String status;
  StatusInfoBean(this.color, this.status);
}

class StatusJudgment {
  static bool isArrived() {
    return false;
  }

  static StatusClass _changeAircraftStatus(double verticalSpeed, double altitude, double speed) {
    final statusInfoBean = StatusClass('', '');
    if (verticalSpeed >= 1000) {
      statusInfoBean.color = StatueColor.climbing;
      statusInfoBean.status = S.current.FlightJudgmentClimbing;
    } else if (speed.toInt() > 0 && speed <= 30) {
      statusInfoBean.color = StatueColor.ground;
      statusInfoBean.status = S.current.FlightJudgmentTaxing;
    } else if (verticalSpeed.abs() <= 400 && altitude >= 20000) {
      statusInfoBean.color = StatueColor.cruising;
      statusInfoBean.status = S.current.FlightJudgmentCruising;
    } else if (verticalSpeed <= -400) {
      statusInfoBean.color = StatueColor.descending;
      statusInfoBean.status = S.current.FlightJudgmentDescending;
    } else if (verticalSpeed.abs() <= 400 && speed.toInt() == 0) {
      statusInfoBean.color = StatueColor.ground;
      statusInfoBean.status = S.current.FlightJudgmentOnGround;
    } else {
      statusInfoBean.color = StatueColor.cruising;
      statusInfoBean.status = S.current.FlightJudgmentCruising;
    }
    return statusInfoBean;
  }

  static StatusInfoBean getStatus(double verticalSpeed, double altitude, double speed) {
    var statusInfo = StatusInfoBean(Colors.white, '');
    var flightStatusBean = _changeAircraftStatus(
      double.parse(verticalSpeed.toString()),
      double.parse(altitude.toString()),
      double.parse(speed.toString()),
    );
    statusInfo.status = flightStatusBean.status;
    statusInfo.color = HexColor.fromHex(flightStatusBean.color);
    return statusInfo;
  }
}

class FlightJudgment {
  static String getDepartureTime(List<AirwayLineListBean> res) {
    var departureTime = res[0].time;
    if (res.isEmpty) {
      return DateTime.now().toString();
    }
    if (res.length == 1) {
      return res[0].time;
    }
    for (var i = 0; i < res.length; i++) {
      if (res[i].altitude > res[0].altitude) {
        departureTime = res[i].time;
        break;
      }
    }
    return departureTime;
  }

  static String getArrivalTime(List<AirwayLineListBean> res, int speed) {
    var arrivalTime = '';
    for (var i = res.length; i < res.length; i--) {
      if (res[i].groundSpeed.floor() == 0) {
        arrivalTime = res[i].time;
      }
    }
    return arrivalTime;
  }

  static String getFlightHaulType(int range, String departure, String arrival) {
    if (range > 8000) {
      return 'Super Long';
    } else if (range > 5000) {
      return 'Long';
    } else if (range > 1000) {
      return 'Middle Long';
    } else if (range > 500) {
      return 'Middle';
    } else if (range > 100) {
      return 'Short';
    } else if (departure == arrival) {
      return 'Circle training';
    } else {
      return 'Commuter';
    }
  }

  /// Format time to H:M
  static String formatFlightTime(int time) {
    var timeFormat = Duration(minutes: time);
    var parts = timeFormat.toString().split(':');
    return '${parts[0]}H ${parts[1]}M';
  }
}

class CalculateRank {
  /// Get current pilot rank
  static String getRank(double flightTime) {
    if (flightTime > 50000) {
      return 'Chief Pilot';
    } else if (flightTime > 30000) {
      return 'Instruct Pilot';
    } else if (flightTime > 10000) {
      return 'Captain';
    } else if (flightTime > 5000) {
      return 'Curise Captain';
    } else if (flightTime > 1000) {
      return 'First Officer';
    } else if (flightTime >= 10) {
      return 'Second Officer';
    } else {
      return 'Commander';
    }
  }

  /// Get next rank role name and nextRankLimitValue
  static RankBean nextRank(String currentRole) {
    final rankList = ['Second Officer', 'First Officer', 'Curise Captain', 'Captain', 'Instruct Pilot', 'Chief Pilot'];
    final _rankReachedValueList = [10.0, 1000.0, 5000.0, 10000.0, 30000.0, 50000.0];
    final _currentIndex = rankList.indexOf(currentRole);
    final _nextRankLimitValue = _rankReachedValueList[_currentIndex + 1];
    return RankBean(nextRole: rankList[_currentIndex + 1], nextRankLimitValue: _nextRankLimitValue);
  }

  static String getFgsRankClassName(int rank) {
    switch (rank) {
      case 0:
        return 'Second Officer';
      case 1:
        return 'First Officer';
      case 2:
        return 'Curise Captain';
      case 3:
        return 'Captain';
      case 4:
        return 'Instruct Pilot';
      case 5:
        return 'Chief Pilot';
      default:
        return 'rookie';
    }
  }
}

class DataHandler {
  static int parseAppVersion(String version) {
    try {
      return int.parse(version.replaceAll('.', ''));
    } on FormatException catch (e) {
      throw FormatException(e.message);
    }
  }

  static String trimAllInString(String str) {
    return str.replaceAll(RegExp(r'\s+\b|\b\s'), '');
  }

  /// livery could be empty
  /// we have to give a unknown value when it happened
  static String getLiveryName(List<Livery> livery) {
    if (livery.isEmpty) {
      return 'FGS Airline';
    } else {
      return livery[0].liveryName ?? 'FGS Airline';
    }
  }
}

class RankBean {
  RankBean({required this.nextRole, required this.nextRankLimitValue});
  final String nextRole;
  final double nextRankLimitValue;
}
