import 'package:get/get.dart';
import 'package:ifgs/stylesheet/Theme.dart';

class GetThemeController extends GetxController {
  void setTheme(bool isDarkMode) {
    final currentTheme = isDarkMode ? DarkTheme.themeConf : LightTheme.themeConf;
    Get.changeTheme(currentTheme);
  }
}
