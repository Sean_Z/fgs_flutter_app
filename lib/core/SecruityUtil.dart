import 'package:encrypt/encrypt.dart';
import 'package:flutter/services.dart';
import 'package:pointycastle/asymmetric/api.dart';

class SecurityUtil {
  static Future<String> rsaEncrypt(String content) async {
    final _publicKey = await rootBundle.loadString('key/public_key');
    final rsaKeyParser = RSAKeyParser();
    final _encryptor = Encrypter(
        RSA(publicKey: rsaKeyParser.parse(_publicKey) as RSAPublicKey));
    return _encryptor.encrypt(content).base64;
  }
}
