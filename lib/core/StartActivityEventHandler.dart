import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/constant/StaticData.dart';
import 'package:ifgs/core/GetController.dart';
import 'package:ifgs/core/StartActivityService.dart';
import 'package:ifgs/request/HttpExecutor.dart';
import 'package:ifgs/request/modal/AppInfoBean.dart';
import 'package:ifgs/util/FileUtil.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:ota_update/ota_update.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constant/AirlineInfo.dart';
import '../request/modal/AirlineJsonBean.dart';
import 'AlgorithmCore.dart';
import 'Common.dart';
import 'UIViewProvider.dart';

class StartActivityEventHandler implements StartActivityService {
  final themeChanger = Get.put(GetThemeController());

  @override
  void systemUiOverlayStyle() {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    final systemUiOverlayStyle = const SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }

  @override
  Future<void> initOwnAircraftPhotoList({bool shouldFileOverwrite = false}) async {
    if (GetPlatform.isMobile) {
      final savePath = await FileUtil.appSupportFilePath + '/static/ownAircraftPhotoList.json';
      if (!File(savePath).existsSync() || shouldFileOverwrite) {
        final dio = Dio();
        await dio.download('${Links.fgsStorageBaseUrl}static/ownAircraftPhotoList.json', savePath);
      }
      final list = <String>[];
      json.decode(File(savePath).readAsStringSync()).forEach((element) {
        list.add(element.toString());
      });
      OwnAircraftPhotoList.ownAircraftPhotoList = list;
    }
  }


  @override
  Future<void> initAirportStaticData({bool shouldFileOverwrite = false}) async {
    if (GetPlatform.isMobile) {
      final savePath = await FileUtil.appSupportFilePath + '/static/airport.json';
      if (!File(savePath).existsSync() || shouldFileOverwrite) {
        final response = await InfiniteFlightLiveDataApi.getAirport();
        final resString = response.toJson();
        await FileUtil.writeFile(resString, savePath);
        StaticData.airportStaticData = response.result ?? [];
      }
    }
  }

  @override
  Future<void> initCurrentUserInfo() async {
    final savePath = await FileUtil.appSupportFilePath + '/static/userInfo.json';
    final dio = Dio();
    await dio.download('${Links.fgsStorageBaseUrl}static/userInfo.json', savePath);
  }

  /// 初始化IF 账户信息
  @override
  Future<void> initInfiniteFlightUserInfo() async {
    final path = await FileUtil.appSupportFilePath + '/static/infiniteFlightUserInfo.json';
    if (!File(path).existsSync()) {
      final username = await InfoMethod.infiniteFlightUsername;
      final response = await InfiniteFlightLiveDataApi.getInfiniteFlightUserInfoByUserName(username);
      await FileUtil.writeFile(response, path);
    }
  }

  /// 初始化用户头像
  @override
  Future<void> initAvatar() async {
    final username = await InfoMethod.getUserName;
    final avatarPath = await FileUtil.appSupportFilePath + '/profile/$username.jpg';
    if (!File(avatarPath).existsSync()) {
      final dio = Dio();
      await dio.download(
        '${Links.fgsStorageBaseUrl}profile/$username.jpg',
        await FileUtil.appSupportFilePath + '/profile/$username.jpg',
      );
    }
  }

  /// 初始化航司机型静态数据
  @override
  Future<void> initAirlineAndAircraftJson({bool shouldFileOverwrite = false}) async {
    final aircraftRes = await InfiniteFlightLiveDataApi.getInfiniteFlightLivery();
    StaticData.aircraftJson = aircraftRes;
    if (GetPlatform.isMobile) {
      final _dio = Dio();
      final _airlineSavePath = await FileUtil.appSupportFilePath + '/static/airlineJson.json';
      if (!File(_airlineSavePath).existsSync() || shouldFileOverwrite) {
        await _dio.download('${Links.fgsStorageBaseUrl}static/airlineJson.json', _airlineSavePath);
      }
      final liveryList = <AirlineJsonBean>[];
      final liveryData = File(_airlineSavePath).readAsStringSync();
      json.decode(liveryData).forEach((element) {
        liveryList.add(AirlineJsonBean.fromJson(element));
      });
      StaticData.airlineJson = liveryList;
    } else {
      final _liveryList = <AirlineJsonBean>[];
      AirlineInfo.airlineInfo.forEach((element) {
        _liveryList.add(AirlineJsonBean.fromJson(element));
      });
      StaticData.airlineJson = _liveryList;
    }
  }

  /// 启动设置语言
  @override
  Future<void> initLang() async {
    final lang = await StorageUtil.getStringItem('lang') ?? 'en';
    lang == 'en' ? UIAdapter.changeLocalLangToEN() : UIAdapter.changeLocalLangToCN();
    await Get.forceAppUpdate();
  }

  /// 设置初始化主题
  @override
  Future<void> initTheme() async {
    final startActivityEventHandler = StartActivityEventHandler();
    final _isDarkMode = await StorageUtil.getBoolItem('isDarkMode') ?? false;
    startActivityEventHandler.themeChanger.setTheme(_isDarkMode);
  }

  /// 集中处理异常信息
  @override
  Future<void> reportError(Object? error, StackTrace stackTrace) async {
    final ignoreIssue = ['Http status error [429]', 'initialized'];
    if (ignoreIssue.contains(error.toString())) {
      Log.logger.i(error.toString());
    }
  }

  @override
  void runWithCatchError(Widget appMain) {
    // 捕获并上报 Flutter 异常
    FlutterError.onError = (FlutterErrorDetails details) async {
      FlutterError.dumpErrorToConsole(details);
    };
    runZonedGuarded<Future<Null>>(() async {
      runApp(appMain);
    }, (error, stackTrace) async {
      await reportError(error, stackTrace);
    });
  }

  @override
  Future<void> downloadApk() async {
    try {
      OtaUpdate().execute(Links.androidReleaseApkUrl, destinationFilename: 'ifgs.apk').listen((OtaEvent event) {});
    } catch (e) {
      print('Failed to make OTA update. Details: $e');
    }
  }

  /// do app version check when Main Activity start
  @override
  Future<void> getAppInfo(BuildContext context, bool shouldShowLatestVersion) async {
    final appInfoBean = AppInfoBean.fromJson(await HttpExecutor.get('common/getAppInfo', ''));
    final appVersion = appInfoBean.version;
    final updateInfo = appInfoBean.updateInfo;
    final updateInfoList = updateInfo!.replaceAll(',', '\n');
    final currentAppVersion = AppVersionInfo.appVersion;
    if (DataHandler.parseAppVersion(appVersion ?? '1') > DataHandler.parseAppVersion(currentAppVersion)) {
      UIViewProvider.showToast('new version $appVersion is found', time: UIViewProvider.toastLengthLong);
      await showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
            title: Row(
              children: [
                const Icon(Icons.update, size: 24),
                const SizedBox(width: 8),
                Text('Update App', style: TextStyle(color: Theme.of(context).colorScheme.secondary)),
              ],
            ),
            content: Container(
              constraints: const BoxConstraints(maxWidth: 100),
              child: Text(
                updateInfoList,
                style: TextStyle(fontSize: 15, color: Theme.of(context).colorScheme.secondary),
              ),
            ),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    StorageUtil.setBoolItem('isIgnoreUpdate', true);
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Text(
                    'Cancel',
                    style: TextStyle(fontSize: 15, color: Colors.redAccent),
                  )),
              TextButton(
                  onPressed: () async {
                    Navigator.of(context).pop();
                    StorageUtil.remove('isIgnoreUpdate');
                    if (Platform.isAndroid) {
                      UIViewProvider.showToast('Start download new version on background');
                      await downloadApk();
                    } else if (Platform.isIOS) {
                      await canLaunchUrl(Uri(host: Links.testflightSchemeUrl))
                          ? await launchUrl(Uri(host: Links.testflightSchemeUrl))
                          : throw 'Could not launch ${Links.testflightSchemeUrl}';
                      UIViewProvider.showToast('Go Test Flight to update ifgs');
                    } else {
                      UIViewProvider.showToast('Delete cache to update web app');
                    }
                  },
                  child: const Text('ok')),
            ],
          );
        },
      );
    } else {
      if (shouldShowLatestVersion) {
        UIViewProvider.showToast('Your current app version ${AppVersionInfo.appVersion} is the latest version');
      }
    }
  }

  @override
  Future<void> initAirportCharts()async {
    final savePath = await FileUtil.appSupportFilePath + '/static/airportCharts.json';
    final dio = Dio();
    await dio.download('${Links.fgsStorageBaseUrl}static/airportCharts.json', savePath);
  }


  @override
  Future<void> downloadAircraftAdvice()async {
    final savePath = await FileUtil.appSupportFilePath + '/static/aircraftAdvice.json';
    final dio = Dio();
    await dio.download('${Links.fgsStorageBaseUrl}static/aircraftAdvice.json', savePath);
  }

  /// main start event
  @override
  void appStartEvent() async {
    await initTheme();
    await initLang();
    await Future.wait([
      initAvatar(),
      initOwnAircraftPhotoList(),
      initAirportCharts(),
      initAirlineAndAircraftJson(),
      initCurrentUserInfo(),
      initAirportStaticData()
    ]);
  }
}
