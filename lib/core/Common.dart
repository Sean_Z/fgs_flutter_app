import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/generated/l10n.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';
import 'package:ifgs/request/modal/OnlineFlightPlanBean.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher.dart';

import '../main.dart';

class Log {
  static Logger logger = Logger(
    printer: PrettyPrinter(colors: true, printEmojis: true, printTime: true),
  );
}

class EventMethod {
  static void handleException(String error) {
    UIViewProvider.showToast(error);
  }

  static void schemeLauncher(String urlString) async {
    if (await canLaunchUrl(Uri(host: urlString))) {
      await launchUrl(Uri(host: urlString));
    } else {
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'could not launch $urlString'));
    }
  }
}

class InfoMethod {
  static Future<String> get getUserName async {
    return await StorageUtil.getStringItem('username') ?? '';
  }

  static Future<String> get infiniteFlightUsername async {
    return await StorageUtil.getStringItem('infiniteFlightUserName') ?? 'Sean_Zhang';
  }
}

class RouterMethod {
  static T getRouterParameter<T>() => Get.arguments as T;

  static void logout() {
    StorageUtil.remove('nodeToken');
    StorageUtil.remove('accessToken');
    StorageUtil.remove('refreshToken');
    Get.offAllNamed('/login');
  }
}

class CalculateMethod {
  /// calculate distance between two waypoints
  static double getDistance(AirwayLineListBean point1, AirwayLineListBean point2) {
    final radLat1 = point1.latitude * math.pi / 180.0;
    final radLat2 = point2.latitude * math.pi / 180.0;
    final a = radLat1 - radLat2;
    final b = point1.longitude * math.pi / 180.0 - point2.longitude * math.pi / 180.0;
    var s = 2 * math.asin(math.sqrt(math.pow(math.sin(a / 2), 2) + math.cos(radLat1) * math.cos(radLat2) * math.pow(math.sin(b / 2), 2)));
    // EARTH_RADIUS
    s = s * 6371.137;
    s = (s * 10000).round() / 10000;
    return s;
  }

  /// calculate distance between two waypoints
  static double getWholeDistance(WholeRangePosition point1, WholeRangePosition point2) {
    final radLat1 = point1.latitude * math.pi / 180.0;
    final radLat2 = point2.latitude * math.pi / 180.0;
    final a = radLat1 - radLat2;
    final b = point1.longitude * math.pi / 180.0 - point2.longitude * math.pi / 180.0;
    var s = 2 * math.asin(math.sqrt(math.pow(math.sin(a / 2), 2) + math.cos(radLat1) * math.cos(radLat2) * math.pow(math.sin(b / 2), 2)));
    s = s * 6371.137;
    s = (s * 10000).round() / 10000;
    return s;
  }

  /// calculate whole distance for all of waypoints
  static double entireDistance(List<AirwayLineListBean> pointList) {
    var temp = <Map<String, AirwayLineListBean>>[];
    var count = 0.0;
    for (var i = 0; i < pointList.length - 2; i++) {
      temp.add({'point1': pointList[i], 'point2': pointList[i + 2]});
      count = (count + getDistance(temp[i]['point1']!, temp[i]['point2']!));
    }
    return count / 2 / 1.852;
  }

  /// calculate whole distance for all of waypoints
  static double entireWholeDistance(List<WholeRangePosition> pointList) {
    var temp = <Map<String, WholeRangePosition>>[];
    var count = 0.0;
    for (var i = 0; i < pointList.length - 2; i++) {
      temp.add({'point1': pointList[i], 'point2': pointList[i + 2]});
      count = (count + getWholeDistance(temp[i]['point1']!, temp[i]['point2']!));
    }
    return count / 2 / 1.852;
  }
}

class UIAdapter {
  static bool isDarkTheme(BuildContext context) => Get.theme.textTheme.bodyLarge?.color == Colors.white;

  static Color setDividerColor(BuildContext context) => UIAdapter.isDarkTheme(context) ? Colors.grey : Colors.white24;

  /// Set Container color in dark mode
  static Color setContainerColor(BuildContext context) => UIAdapter.isDarkTheme(context) ? Colors.black87 : Colors.white;

  /// reverse [setContainerColor]
  static Color setContainerColorReverse(BuildContext context) => UIAdapter.isDarkTheme(context) ? Colors.white : Colors.black87;

  /// Set Container color with primary color in light mode
  static Color setContainerPrimaryColor(BuildContext context) =>
      UIAdapter.isDarkTheme(context) ? Colors.black87 : Theme.of(context).primaryColor;

  /// Set font color in dark mode
  static Color setFontPrimaryColor(BuildContext context) =>
      UIAdapter.isDarkTheme(context) ? Colors.white70 : Theme.of(context).colorScheme.secondary;

  /// Set Colors.white in dark mode / Colors.secondary in light
  static Color setFontPrimaryColorWhite(BuildContext context) =>
      UIAdapter.isDarkTheme(context) ? Colors.white : Theme.of(context).colorScheme.secondary;

  /// Set Colors.white in dark mode / Colors.blueGrey in light
  static Color setFontBlueGreyColor(BuildContext context) => UIAdapter.isDarkTheme(context) ? Colors.white : Colors.blueGrey;

  /// Set Colors.white in dark mode / Colors.black87 in light
  static Color setFontColor(BuildContext context) => UIAdapter.isDarkTheme(context) ? Colors.white70 : Colors.black87;

  /// Set Icon color in dark mode / light mode
  static Color setIconColor(BuildContext context) => UIAdapter.isDarkTheme(context) ? Colors.white : Theme.of(context).primaryColor;

  /// Set localLanguage to EN
  static void changeLocalLangToEN() => {S.load(const Locale('en', 'EN')), StorageUtil.setStringItem('lang', 'en')};

  /// Set localLanguage to CN
  static void changeLocalLangToCN() => {S.load(const Locale('zh', 'CN')), StorageUtil.setStringItem('lang', 'cn')};
}

BuildContext getApplicationContext() => navKey.currentState!.context;
