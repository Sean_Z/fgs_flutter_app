import 'package:shared_preferences/shared_preferences.dart';
import 'package:tencent_cos/tencent_cos.dart';

class StorageUtil {
  static Future<COSClient> get tencentCosInstance async {
    return COSClient(COSConfig(
      'AKIDLds320iRSlpuoRvMdna9WfXJI9oPs48v',
      'QgqptoaP12tY0d4HOH9uc73sTqxxlOoD',
      'fgs-1300812707',
      'ap-shanghai',
    ));
  }

  static void setBoolItem(String key, bool value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool(key, value);
  }

  static void setDoubleItem(String key, double value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setDouble(key, value);
  }

  static void setIntItem(String key, int value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setInt(key, value);
  }

  static void setStringItem(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  static void setStringListItem(String key, List<String> value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setStringList(key, value);
  }

  static Future<bool?> getBoolItem(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var value = prefs.getBool(key);
    return value;
  }

  static Future<double?> getDoubleItem(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var value = prefs.getDouble(key);
    return value;
  }

  static Future<int?> getIntItem(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var value = prefs.getInt(key);
    return value;
  }

  static Future<String?> getStringItem(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var value = prefs.getString(key);
    return value;
  }

  static Future<List<String>?> getStringListItem(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var value = prefs.getStringList(key);
    return value;
  }

  static void remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove(key);
  }

  static void clear() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  /// [localFilePath] path with appSupportFilePath
  /// cosPath. if path is not exist in cos, it will be created automatically
  static Future<void> saveIntoTencentCos(String localFilePath, String cosPath) async {
    var token = '';
    final cosInstance = await tencentCosInstance;
    await cosInstance.putObject(cosPath, localFilePath, token: token);
  }

  static Future<void> queryCosFile(String cosPath)async {
    final cosInstance = await tencentCosInstance;
    await cosInstance.listObject(prefix: cosPath);
  }
}
