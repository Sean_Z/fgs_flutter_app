import 'package:flutter/services.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/Common.dart';

class NativeMethod {
  static dynamic nativeBridge(String method, String platform, [dynamic param]) {
    print(platform);
    if (platform == 'android') {
      const platform = NativeMethodName.androidPlatformChannelName;
      switch (method) {
        case NativeMethodName.showToast:
          platform.invokeMethod('showToast', {'msg': param, 'className': 'io.flutter.plugins.UiMethod'});
          break;
        case NativeMethodName.getWifiSSID:
          return platform.invokeMethod('getWIFISSID', {'className': 'io.flutter.plugins.HardwareInfo'});
      }
    }
  }

  static Future nativeBridgeWithData(String method, String platform, [dynamic param]) async {
    print(platform);
    if (platform == 'android') {
      const platform = NativeMethodName.androidPlatformChannelName;
      switch (method) {
        case NativeMethodName.getBatteryInfo:
          try {
            var result = await platform.invokeMethod('getBatteryInfo', {'className': 'io.flutter.plugins.HardwareInfo'});
            return result;
          } on PlatformException catch (e) {
            print(e);
          }
          break;
        case NativeMethodName.getCpuInfo:
          try {
            final _cpuInfo = await platform.invokeMethod('getCpuInfo', {'className': 'io.flutter.plugins.HardwareInfo'});
            return _cpuInfo;
          } on PlatformException catch (e) {
            print(e);
          }
          break;
      }
    }
  }

  static Future<List<String>> getCpuInfo() async {
    return await NativeMethod.nativeBridgeWithData(NativeMethodName.getCpuInfo, 'android') as List<String>;
  }

  static Future<String> getWifiSSID() async {
    Log.logger.i(await NativeMethod.nativeBridge(NativeMethodName.getWifiSSID, 'android'));
    return await NativeMethod.nativeBridge(NativeMethodName.getWifiSSID, 'android') as String;
  }
}
