class TimeUtil {
  static String YYYY_MM_DD_HH_MM_SS() {
    var currentTime = DateTime.now();
    var month = currentTime.month < 10 ? '0' + currentTime.month.toString() : currentTime.month.toString();
    var day = currentTime.day < 10 ? '0' + currentTime.day.toString() : currentTime.day.toString();
    var hour = currentTime.day < 10 ? '0' + currentTime.hour.toString() : currentTime.hour.toString();
    var minute = currentTime.minute < 10 ? '0' + currentTime.minute.toString() : currentTime.minute.toString();
    var second = currentTime.second < 10 ? '0' + currentTime.second.toString() : currentTime.second.toString();
    return '${currentTime.year}-$month-$day $hour:$minute:$second';
  }

  static int getTimeZoneByLongitude() {
    int timeZone;
    var currentLong = 123.43900299072266;
    var shangValue = currentLong / 15;
    var remainder = (currentLong % 15).abs();
    if (remainder <= 7.5) {
      timeZone = shangValue.toInt();
    } else {
      timeZone = shangValue.toInt() + 1;
    }
    return timeZone;
  }
}
