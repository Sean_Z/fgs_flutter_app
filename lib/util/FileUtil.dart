import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/request/modal/AirlineJsonBean.dart';
import 'package:ifgs/request/modal/FgsAircraftAdviceEntity.dart';
import 'package:ifgs/request/modal/FgsUserInfoBean.dart';
import 'package:ifgs/request/modal/InfiniteFlightUserInfoBean.dart';
import 'package:ifgs/request/modal/fgs_file_entity.dart';
import 'package:ifgs/request/modal/infinte_flight_airport_entity.dart';
import 'package:ifgs/util/DirectoryUtil.dart';
import 'package:ifgs/util/StorageUtil.dart';

class FileUtil {
  static Future<String> get appSupportFilePath async {
    return (await PathProvider.cacheFileDirectory).path;
  }

  static Future<File> getLocalFile(String filePath) async {
    final path = await appSupportFilePath;
    return File('$path/$filePath');
  }

  static Future<void> writeFile(Object content, String filePath) async {
    final file = File(filePath);
    await file.writeAsString(json.encode(content));
  }

  static Future<void> uploadFiles(FormData requestData, String uploadInterfaceUrl, {String uploadTargetPath = 'China'}) async {
    final dio = Dio();
    await dio.post(
      uploadInterfaceUrl,
      onSendProgress: (int count, int total) {
        final sendProgress = count / total;
        Log.logger.i(sendProgress);
      },
      data: requestData,
      options: Options(headers: {
        'shouldFileUploadToCos': 0,
        'cosTargetPath': uploadTargetPath,
        'Content-Type': 'multipart/form-data; boundary=${requestData.boundary}',
        'nodeToken': await StorageUtil.getStringItem('nodeToken'),
        'accessToken': await StorageUtil.getStringItem('accessToken'),
        'refreshToken': await StorageUtil.getStringItem('refreshToken'),
      }),
    );
  }
}

class FileGetter {
  static Future<FgsUserInfoBeanEntity?> get userInfo async {
    try {
      final userInfoPath = await FileUtil.appSupportFilePath + '/static/userInfo.json';
      return FgsUserInfoBeanEntity.fromJson(json.decode(File(userInfoPath).readAsStringSync()) as Map<String, dynamic>);
    } catch (e) {
      Log.logger.i('user info cache does not exist');
    }
    return null;
  }

  static Future<InfiniteFlightAirportEntity?> get airportStaticData async {
    try {
      final filePath = await FileUtil.appSupportFilePath + '/static/airport.json';
      final resString = File(filePath).readAsStringSync();
      final map = json.decode(resString) as Map<String, dynamic>;
      return InfiniteFlightAirportEntity.fromJson(map);
    } catch (e) {
      Log.logger.i('airport info cache does not exist');
    }
    return null;
  }

  static Future<List<FgsAircraftAdviceEntity>?> get aircraftAdviceStaticData async {
    try {
      final filePath = await FileUtil.appSupportFilePath + '/static/aircraftAdvice.json';
      final resString = File(filePath).readAsStringSync();
      final map = json.decode(resString);
      final result = <FgsAircraftAdviceEntity>[];
      map.forEach((element) => {result.add(FgsAircraftAdviceEntity.fromJson(element))});
      return result;
    } catch (e) {
      Log.logger.i('airport info cache does not exist');
    }
    return null;
  }

  static Future<List<FgsFileEntity>?> get getAirportCharts async {
    try {
      final userInfoPath = await FileUtil.appSupportFilePath + '/static/airportCharts.json';
      final data = File(userInfoPath).readAsStringSync();
      final res = <FgsFileEntity>[];
      json.decode(data).forEach((element) {
        res.add(FgsFileEntity.fromJson(element));
      });
      return res;
    } catch (e) {
      Log.logger.i('airport charts cache does not exist');
    }
    return null;
  }

  static Future<File> get userAvatar async {
    final username = await InfoMethod.getUserName;
    final avatarPath = await FileUtil.appSupportFilePath + '/profile/$username.jpg';
    return File(avatarPath);
  }

  static Future<InfiniteFlightUserInfoBean> get infiniteFlightUserInfo async {
    final path = await FileUtil.appSupportFilePath + '/static/infiniteFlightUserInfo.json';
    final data = File(path).readAsStringSync();
    return InfiniteFlightUserInfoBean.fromJson(json.decode(data));
  }

  static Future<List<AirlineJsonBean>> get airlineJson async {
    final path = await FileUtil.appSupportFilePath + '/static/airlineJson.json';
    final data = File(path).readAsStringSync();
    return json.decode(data).map((element) => AirlineJsonBean.fromJson(element)) as List<AirlineJsonBean>;
  }
}
