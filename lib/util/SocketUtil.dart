import 'dart:convert';
import 'dart:io';

class SocketUtil {
  static String host = '192.168.1.110';
  static int port = 15000;

  static void server() {
    // listen forever & send response
    RawDatagramSocket.bind(InternetAddress('113.215.165.139'), port).then((socket) {
      socket.listen((RawSocketEvent event) {
        if (event == RawSocketEvent.read) {
          var dg = socket.receive();
          if (dg == null) return;
          final received = String.fromCharCodes(dg.data);
          if (received == 'ping') socket.send(const Utf8Codec().encode('ping ack'), dg.address, port);
          print('$received from ${dg.address.address}:${dg.port}');
        }
      });
    });
    print('udp listening on $port');
  }
}
