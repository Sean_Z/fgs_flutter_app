import 'dart:io';

import 'package:path_provider/path_provider.dart';

class PathProvider {
  /// Path to the temporary directory on the device that is not backed up and is
  /// suitable for storing caches of downloaded files.
  static Future<Directory> get temporaryDirectory async {
    return await getTemporaryDirectory();
  }

  /// Path to a directory where the application may place application support
  /// files.
  /// Use this for files you don’t want exposed to the user. Your app should not
  // /// use this directory for user data files.
  static Future<Directory> get applicationSupportDirectory async {
    return await getApplicationSupportDirectory();
  }

  /// Path to the directory where application can store files that are persistent,
  /// backed up, and not visible to the user, such as sqlite.db.
  ///
  /// On [Platform.isAndroid], this function throws an [UnsupportedError] as no equivalent
  /// path exists.
  static Future<Directory> get libraryDirectory async {
    return await getLibraryDirectory();
  }

  /// Path to a directory where the application may place data that is
  /// user-generated, or that cannot otherwise be recreated by your application.
  static Future<Directory> get applicationDocumentsDirectory async {
    return await getApplicationDocumentsDirectory();
  }

  /// On [Platform.isIOS], this function throws an [UnsupportedError] as it is not possible
  /// to access outside the app's sandbox.
  static Future<Directory?> get externalStorageDirectory async {
    return Platform.isAndroid ? await getExternalStorageDirectory() : await getApplicationDocumentsDirectory();
  }

  /// Paths to directories where application specific external cache data can be
  /// stored. These paths typically reside on external storage like separate
  /// partitions or SD cards. Phones may have multiple storage directories
  /// available.
  static Future<List<Directory>?> get externalCacheDirectories async {
    return await getExternalCacheDirectories();
  }

  static Future<Directory> get cacheFileDirectory async {
    return (await externalStorageDirectory)!;
  }
}
