import 'package:geolocator/geolocator.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';

class PermissionHandler {
  static Future<void> getLocationPermission()async {
    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      UIViewProvider.showScaffoldError(ScaffoldMessageConfig(message: 'location permission needed!'));
      permission = await Geolocator.requestPermission();
    }
  }
}