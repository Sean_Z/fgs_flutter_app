class RegExpUtil {
  static final _emailRegExp = RegExp('^[a-z0-9]+([._\\\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+\$');
  static final _icaoRegExp = RegExp(r'^[A-Za-z0-9]+$');
  static final _usernameRegExp = RegExp(r'^[a-zA-Z0-9_-]{4,16}$');

  static final pwdExp = RegExp(r'^(?![0-9]+$)(?![a-z]+$)[0-9a-z]{6,12}$');

  static bool validateEmail(String? email) {
    return _emailRegExp.hasMatch(email ?? '');
  }

  static bool isUsername(String? username) {
    return _usernameRegExp.hasMatch(username ?? '');
  }

  static bool isPassword(String? password) {
    return pwdExp.hasMatch(password ?? '');
  }

  static bool isICAO(String? icao) {
    return _icaoRegExp.hasMatch(icao ?? '');
  }
}
