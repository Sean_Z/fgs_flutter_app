import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/request/ThirdPartServiceCaller.dart';
import 'package:ifgs/request/modal/LatLngDecodeBean.dart';

class GeographyUtil {
  static Future<LatLngDecodeBean> decodeLocation(String location) async {
    final _url =
        'https://restapi.amap.com/v3/geocode/regeo?output=json&location=$location&key=${KeyManager.amapGeoKey}&radius=1000&extensions=base';
    final _response = await ThirdPartServiceCaller.get(_url);
    return LatLngDecodeBean.fromJson(_response);
  }
}
