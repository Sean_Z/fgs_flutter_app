import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:ifgs/widget/OnlineMap/OsmInfoWindow.dart';
import 'package:ifgs/widget/PublicWidget/SkeletonWidget.dart';
import 'package:material_floating_search_bar_2/material_floating_search_bar_2.dart';

class FlightListFragment extends StatefulWidget {
  @override
  _FlightListFragmentState createState() => _FlightListFragmentState();
}

class _FlightListFragmentState extends State<FlightListFragment> with AutomaticKeepAliveClientMixin<FlightListFragment> {
  List<FgsOnlineFlightData> _flightBeans = [];
  List<FgsOnlineFlightData> _flightBeansCopy = [];
  bool _isShowLoading = true;
  bool _isShowActionButton = false;
  int _serverType = 0;
  bool _isLoaded = true;
  String _appBarTitle = 'ES';
  final ScrollController _scrollController = ScrollController(initialScrollOffset: 0.0, keepScrollOffset: true);

  /// 获取所有航班
  Future<void> fetchFlightData({bool shouldUseSkeleton = true}) async {
    setState(() {
      _isLoaded = shouldUseSkeleton ? true : false;
    });
    final flightBeans = await InfiniteFlightLiveDataApi.getServerFlight(_serverType);
    _flightBeansCopy = flightBeans;
    setState(() {
      _flightBeans = flightBeans;
      _isShowLoading = false;
      _isLoaded = false;
    });
  }

  /// Get current server name
  String getCurrentServer(int serverType) {
    return {
          0: 'ES',
          1: 'TS',
          2: 'CS',
        }[serverType] ??
        'Unknown';
  }

  @override
  void initState() {
    super.initState();
    fetchFlightData();
    _serverType = 0;
    _scrollController.addListener(() {
      if (_scrollController.offset >= 1000) {
        setState(() {
          _isShowActionButton = true;
        });
      } else if (_scrollController.offset < 1000) {
        setState(() {
          _isShowActionButton = false;
        });
      }
    });
  }

  Future<void> refreshData(int value) async {
    setState(() {
      _appBarTitle = getCurrentServer(value);
      _serverType = value;
    });
    await fetchFlightData(shouldUseSkeleton: false);
  }

  void _onSearchFlight(String data) {
    try {
      var targetList = _flightBeansCopy
          .where((element) => element.displayName.toString().trim().toLowerCase().contains(data.trim().toLowerCase()))
          .toList();
      setState(() {
        _flightBeans = targetList;
      });
      if (data == '') {
        setState(() {
          _flightBeans = _flightBeansCopy;
        });
      }
    } catch (error) {
      UIViewProvider.showScaffoldMessenger(context, 'target not found!', backgroundColor: Colors.redAccent);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(

        /// avoid resize when keyboard up from bottom
        resizeToAvoidBottomInset: false,
        appBar: AppBar(title: Text('$_appBarTitle List'), centerTitle: false, systemOverlayStyle: SystemUiOverlayStyle.light, actions: [
          Container(
            width: 200,
            child: FloatingSearchBar(
              backgroundColor: Colors.white,
              automaticallyImplyBackButton: false,
              onQueryChanged: (value) {
                _onSearchFlight(value);
              },
              actions: [
                FloatingSearchBarAction(
                    child: PopupMenuButton(
                  color: UIAdapter.setContainerColor(context),
                  icon: const Icon(Icons.menu, color: Colors.blueGrey),
                  iconSize: 30,
                  onSelected: (value) => {refreshData(int.parse(value.toString()))},
                  itemBuilder: (BuildContext context) {
                    return <PopupMenuEntry>[
                      const PopupMenuItem(value: 0, child: Text('Expert Server', style: TextStyle(color: Colors.blueGrey))),
                      const PopupMenuItem(value: 1, child: Text('Training Server', style: TextStyle(color: Colors.blueGrey))),
                      const PopupMenuItem(value: 2, child: Text('Casual Server', style: TextStyle(color: Colors.blueGrey)))
                    ];
                  },
                )),
                FloatingSearchBarAction.searchToClear(
                  showIfClosed: false,
                  duration: const Duration(microseconds: 500),
                ),
              ],
              builder: (BuildContext context, Animation<double> transition) {
                return Container();
              },
            ),
          )
        ]),
        floatingActionButton: _isShowActionButton
            ? FloatingActionButton(
                onPressed: () => _scrollController.animateTo(
                  .0,
                  duration: const Duration(seconds: 2),
                  curve: Curves.ease,
                ),
                child: const Icon(Icons.arrow_upward),
              )
            : Container(),
        body: RefreshIndicator(
          onRefresh: () => refreshData(_serverType),
          child: Stack(
            children: <Widget>[
              Container(
                child: !_isLoaded
                    ? ListView.builder(
                        physics: const BouncingScrollPhysics(),
                        controller: _scrollController,
                        itemCount: _flightBeans.length,
                        addAutomaticKeepAlives: true,
                        itemBuilder: (context, i) {
                          return OsmInfoWindow(targetInfo: _flightBeans[i]);
                        })
                    : Container(margin: const EdgeInsets.all(10), child: const SkeletonWidget(count: 8)),
              ),
              _isShowLoading ? const LinearProgressIndicator() : Container(),
              _isLoaded ? Container(margin: const EdgeInsets.all(10), child: const SkeletonWidget(count: 8)) : Container()
            ],
          ),
        ));
  }

  @override
  bool get wantKeepAlive => true;
}
