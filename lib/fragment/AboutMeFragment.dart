import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/entity/ui/ScaffoldMessageConfig.dart';
import 'package:ifgs/util/FileUtil.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:ifgs/widget/AboutMeWidget/AboutMeFragmentVipCardWidget.dart';
import 'package:ifgs/widget/UserCenter/MenuListCard.dart';
import 'package:image_pickers/image_pickers.dart';

class AboutMeFragment extends StatefulWidget {
  @override
  _AboutMeFragmentState createState() => _AboutMeFragmentState();
}

class _AboutMeFragmentState extends State<AboutMeFragment> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  var _email = '--/--';
  var _position = '----';
  var _currentPosition = '--/--';
  var _username = '';

  Future<void> _getUserInfo() async {
    var username = await InfoMethod.getUserName;
    var userInfo = await FileGetter.userInfo;
    var targetUser = userInfo?.list.firstWhere((element) => element.username == username);
    var email = targetUser?.mail;
    var position = targetUser?.position;
    var currentPosition = targetUser?.country;
    setState(() {
      _username = username;
      _position = position ?? 'FGS Pilot';
      _currentPosition = currentPosition!;
      _email = email!;
    });
  }

  @override
  void initState() {
    _controller = AnimationController(vsync: this);
    super.initState();
    _getUserInfo();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          ConstrainedBox(
            constraints: const BoxConstraints.tightFor(height: 300),
            child: Stack(
              children: [
                _TopBlock(),
                _TopBlockInfo(),
                AboutMeFragmentVipCardWidget(email: _email),
              ],
            ),
          ),
          const MenuListCard(),
        ],
      ),
    );
  }

  Widget _TopBlock() {
    return Positioned(
      child: ShaderMask(
        shaderCallback: (Rect bounds) {
          return LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Theme.of(context).primaryColor, Colors.transparent],
          ).createShader(Rect.fromLTRB(0, 150, bounds.width, bounds.height));
        },
        blendMode: BlendMode.dstIn,
        child: Container(
          height: 200,
          color: Theme.of(context).primaryColor,
          child: ShaderMask(
            shaderCallback: (Rect bounds) {
              return const LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.white38, Colors.transparent],
              ).createShader(Rect.fromLTRB(0, 10, bounds.width, bounds.height));
            },
            blendMode: BlendMode.dstIn,
            child: ConstrainedBox(
              constraints: const BoxConstraints.tightFor(height: 500),
              child: Stack(
                children: [
                  Positioned(
                    top: -60,
                    right: -40,
                    child: Container(
                      width: 300,
                      height: 300,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white38,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _uploadAvatar() async {
    final result = await ImagePickers.pickerPaths(
      galleryMode: GalleryMode.image,
      selectCount: 1,
      cropConfig: CropConfig(enableCrop: true, width: 1, height: 1),
      showGif: false,
      uiConfig: UIConfig(uiThemeColor: Theme.of(context).primaryColor)
    );
    final croppedFilePath = '${result.first.path}';
    CustomViewProvider.showMaterialDialogLoading(context, content: 'uploading...');
    await StorageUtil.saveIntoTencentCos(croppedFilePath, '/profile/$_username.jpg');
    final _dio = Dio();
    await _dio.download('${Links.fgsStorageBaseUrl}profile/$_username.jpg', await FileUtil.appSupportFilePath + '/profile/$_username.jpg');
    CustomViewProvider.hideMaterialDialogLoading();
    await Get.forceAppUpdate();
    UIViewProvider.showScaffoldSuccess(ScaffoldMessageConfig(message: 'Avatar reset successfully, restart ifgs to take effect!', duration: 3));
  }

  void uploadAvatar() {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              onPressed: () {
                _uploadAvatar();
                Navigator.of(context).pop();
              },
              style: const ButtonStyle(alignment: Alignment.center),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.upload),
                  SizedBox(width: 8),
                  Text('upload'),
                ],
              ),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.cancel, color: Colors.red),
                  SizedBox(width: 8),
                  Text('Cancel', style: TextStyle(color: Colors.red)),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _TopBlockInfo() {
    return Positioned(
      top: 60,
      left: 30,
      child: Wrap(
        children: [
          GestureDetector(
            onTap: () => uploadAvatar(),
            child: UIViewProvider.getUserAvatar(radius: 40),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Wrap(
              direction: Axis.vertical,
              alignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Wrap(
                    direction: Axis.horizontal,
                    spacing: 8,
                    children: [
                      Text(
                        _username,
                        style: const TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600),
                        textAlign: TextAlign.start,
                      ),
                      const Stack(
                        children: [
                          Icon(CupertinoIcons.hexagon_fill, color: Colors.white),
                          Icon(Icons.star, color: Color(0xFFD91919)),
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 8, right: 8, top: 5, bottom: 5),
                        margin: const EdgeInsets.only(left: 10),
                        decoration: const BoxDecoration(
                          boxShadow: [BoxShadow(color: Colors.black12)],
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                        ),
                        child: Text(
                          _position,
                          style: const TextStyle(color: Colors.cyanAccent, fontSize: 10),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 8, right: 8, top: 5, bottom: 5),
                        margin: const EdgeInsets.only(left: 10),
                        decoration: const BoxDecoration(
                          boxShadow: [BoxShadow(color: Colors.black12)],
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                        ),
                        child: Text(
                          _currentPosition,
                          style: const TextStyle(color: Colors.cyanAccent, fontSize: 10),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
