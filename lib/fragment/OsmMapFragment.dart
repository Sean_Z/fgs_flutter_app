import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:geolocator/geolocator.dart' as geo;
import 'package:ifgs/api/InfiniteFlightLiveDataApi.dart';
import 'package:ifgs/constant/ConfigData.dart';
import 'package:ifgs/core/AlgorithmCore.dart';
import 'package:ifgs/core/Color.dart';
import 'package:ifgs/core/Common.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/request/modal/AirwayLineListBean.dart';
import 'package:ifgs/request/modal/FgsAllFlightsBean.dart';
import 'package:ifgs/request/modal/OnlineFlightPlanBean.dart';
import 'package:ifgs/util/PermissionHandler.dart';
import 'package:ifgs/util/StorageUtil.dart';
import 'package:ifgs/widget/OnlineMap/FlightDataChart.dart';
import 'package:ifgs/widget/OnlineMap/OsmInfoWindow.dart';
import 'package:ifgs/widget/OnlineMap/SearchBarWidget.dart';
import 'package:ifgs/widget/PublicWidget/FgsPopupMenu.dart';
import 'package:latlong2/latlong.dart' as osm_l;
import 'package:provider/provider.dart';

import '../stylesheet/Theme.dart';

class OsmMapFragment extends StatefulWidget {
  const OsmMapFragment({super.key});

  @override
  State<OsmMapFragment> createState() => _OsmMapFragmentState();
}

class _OsmMapFragmentState extends State<OsmMapFragment> {

  final popupLayerController = PopupController();
  final osmMapController = MapController();

  /// 所有飞机锚点
  var markerList = <Marker>[];

  /// 所有航班List
  var flightBeanList = <FgsOnlineFlightData>[];

  /// 默认服务器使用ES
  var _serverType = 0;

  /// 航线
  var polyline = <Polyline>[];

  /// 全服飞机坐标Map
  final _positionList = <String, Position>{};

  /// 由于初始图标横向+90度，渲染时需要修正-90度
  final _rotationList = {} as LinkedHashMap;

  /// 在线计划对象
  late OnlineFlightPlanBean onlinePlan;

  /// 登录用户航班对象
  FgsOnlineFlightData? _targetUserFlight;

  /// 登录用户航班Marker下标，用于右下角定位方法时
  /// 从[markerList]找到对应的marker传入[_onMarkClick]方法
  int _MyFlightIndex = 0;
  var mapType = OsmMapStyle.dark;

  /// 地图类型菜单
  final List<PopupMenuClass> buttonInfoList = [
    PopupMenuClass(const Icon(Icons.nightlight_round), 'dark'),
    PopupMenuClass(const Icon(Icons.traffic), 'street'),
    PopupMenuClass(const Icon(Icons.wb_sunny), 'light'),
    PopupMenuClass(const Icon(Icons.satellite), 'satellite'),
  ];

  /// Set Map theme
  void setMapType(int _mapType) {
    final mapTypeMap = {
      0: OsmMapStyle.dark,
      1: OsmMapStyle.streets,
      2: OsmMapStyle.light,
      3: OsmMapStyle.satellite,
    };
    setState(() {
      mapType = mapTypeMap[_mapType] ?? OsmMapStyle.light;
    });
  }

  List<osm_l.LatLng> cutSuperLongAirwayList(List<osm_l.LatLng> airwayPointList) {
    var list = airwayPointList;
    for (var i = 0; i < airwayPointList.length; i += 2) {
      list.removeAt(i);
    }
    if(list.length > 200) {
      cutSuperLongAirwayList(airwayPointList);
    }
    return list;
  }

  void showStateBottomSheet(BuildContext context, FgsOnlineFlightData flightData, List<AirwayLineListBean> airwayLineEntity) {
    Provider.of<FlightInfoStateProvider>(context, listen: false).setAirwayLineEntity(airwayLineEntity);
    Provider.of<FlightInfoStateProvider>(context, listen: false).setFlightInfoEntity(flightData);
    showModalBottomSheet(
        context: context,
        clipBehavior: Clip.antiAlias,
        barrierColor: Colors.transparent,
        builder: (BuildContext context) {
          return Container(
            constraints: const BoxConstraints(maxHeight: 300),
            child: const FlightDataChart(),
          );
        });
  }

  void _moveCameraToTarget(String callSign) {
    final filteredList = flightBeanList.where((element) => element.callSign == callSign).toList();
    final position = filteredList[0].position;
    final targetPosition = osm_l.LatLng(position.latitude, position.longitude);
    osmMapController.move(targetPosition, osmMapController.camera.zoom);
  }

  void drawPolyline(List<AirwayLineListBean> airwayLineResponse, List<WholeRangePosition> wholeAirwayLineResponse) {
    var airwayPointList = airwayLineResponse.map((e) => osm_l.LatLng(e.latitude, e.longitude)).toList();
    airwayPointList = cutSuperLongAirwayList(airwayPointList);
    final wholeAirwayLinePointList = wholeAirwayLineResponse.map((e) => osm_l.LatLng(e.latitude, e.longitude)).toList();
    final tempPolyline = <Polyline>[];
    final singleAirwayLinPolyLine = Polyline(
      points: airwayPointList,
      gradientColors: [Colors.yellowAccent, Colors.blueAccent],
      colorsStop: ColorCommonMethod.colorStop(airwayLineResponse),
      strokeWidth: 3,
    );
    final singleWholeAirwayLinPolyLine = Polyline(
      points: wholeAirwayLinePointList,
      color: Colors.greenAccent,
      strokeWidth: 1,
    );
    tempPolyline.add(singleAirwayLinPolyLine);
    tempPolyline.add(singleWholeAirwayLinPolyLine);
    setState(() {
      polyline = tempPolyline;
    });
  }

  Future<void> _onMarkClick(BuildContext context, Marker _marker) async {
    popupLayerController.hideAllPopups();
    final _index = int.parse(_marker.key.toString().substring(3, _marker.key.toString().length - 3));
    CustomViewProvider.showMaterialLoading('loading', context);
    final callSign = flightBeanList[_index].callSign;
    final targetLatitude = flightBeanList[_index].position.latitude;
    final targetLongitude = flightBeanList[_index].position.longitude;
    osmMapController.move(osm_l.LatLng(targetLatitude, targetLongitude), osmMapController.camera.zoom);
    final resList = await Future.wait([
      InfiniteFlightLiveDataApi.getAirwayLine(flightBeanList[_index].flightId),
      InfiniteFlightLiveDataApi.getOnlinePlan(flightBeanList[_index].flightId),
    ]);
    _moveCameraToTarget(callSign);
    final airwayLineResponse = resList[0] as List<AirwayLineListBean>;
    final onlineFlightBean = resList[1] as OnlineFlightPlanBean;
    onlinePlan = onlineFlightBean;
    final targetInfo = flightBeanList[_index];
    showStateBottomSheet(context, targetInfo, airwayLineResponse);
    final wholeAirwayLineResponse = onlineFlightBean.wholeRangePosition;
    drawPolyline(airwayLineResponse, wholeAirwayLineResponse);
    popupLayerController.togglePopup(_marker);
    CustomViewProvider.hideMaterialLoading();
  }

  void _renderMarkers(BuildContext context) {
    for (var i = 0; i < _positionList.length; i++) {
      final vs = flightBeanList[i].verticalSpeed;
      final altitude = flightBeanList[i].altitude;
      final speed = flightBeanList[i].speed;
      final state = StatusJudgment.getStatus(vs, altitude, speed);
      final location = _positionList['locations' + i.toString()];
      final angle = _rotationList['rotations' + i.toString()] as double;
      final airplaneIcon = Icon(Icons.airplanemode_active, color: state.color);
      final airplaneAngle = AlwaysStoppedAnimation(angle / 360);
      final rotationTransition = RotationTransition(turns: airplaneAngle, child: airplaneIcon);
      final marker = Marker(
          key: Key(i.toString()),
          point: osm_l.LatLng(location!.latitude, location.longitude),
          alignment: Alignment.center,
          child: GestureDetector(onTap: () => _onMarkClick(context, markerList[i]), child: rotationTransition));
      markerList.add(marker);
    }
    if (_targetUserFlight != null) {
      _onMarkClick(context, markerList[_MyFlightIndex]);
    }
  }

  Future<void> _getAllFlight(BuildContext context, int? pickedServerType, {bool shouldLocateSelf = true, bool shouldLoading = true}) async {
    _serverType = await StorageUtil.getIntItem('currentServerEnum') ?? 0;
    if(pickedServerType != null) {
      _serverType = pickedServerType;
    }
    final username = await InfoMethod.infiniteFlightUsername;
    if (shouldLoading) {
      CustomViewProvider.showMaterialDialogLoading(context, content: 'loading...');
    }
    final onlineFlightDataList = await InfiniteFlightLiveDataApi.getServerFlight(_serverType);
    UIViewProvider.showToast(IFServerMap.serverMap[_serverType] ?? 'Unknown server');
    flightBeanList = onlineFlightDataList;
    _positionList.clear();
    for (var i = 0; i < onlineFlightDataList.length; i++) {
      final item = onlineFlightDataList[i];
      if (item.displayName == username) {
        _targetUserFlight = item;
        _MyFlightIndex = i;
        final _zoom = osmMapController.camera.zoom;
        if (shouldLocateSelf) {
          osmMapController.move(osm_l.LatLng(item.position.latitude, item.position.longitude), _zoom);
        }
      }
      _positionList['locations' + i.toString()] = item.position;
      _rotationList['rotations' + i.toString()] = item.angle;
    }
    setState(() {
      markerList = [];
    });
    _renderMarkers(context);
    CustomViewProvider.hideMaterialDialogLoading();
  }

  void locateSearchedFlight(BuildContext context, String callSign) {
    final targetIndex = flightBeanList.lastIndexWhere((element) => element.callSign == callSign);
    _onMarkClick(context, markerList[targetIndex]);
  }

  void locateMyFlight(BuildContext context) async {
    if (_targetUserFlight == null) {
      await PermissionHandler.getLocationPermission();
      final _position = await geo.Geolocator.getCurrentPosition(
        desiredAccuracy: geo.LocationAccuracy.best,
        forceAndroidLocationManager: true,
      );
      final _zoom = osmMapController.camera.zoom;
      osmMapController.move(osm_l.LatLng(_position.latitude, _position.longitude), _zoom);
      UIViewProvider.showScaffoldMessenger(context, 'target not found!', backgroundColor: Colors.redAccent);
    } else {
      await _onMarkClick(context, markerList[_MyFlightIndex]);
    }
  }

  @override
  void dispose() {
    super.dispose();
    CustomViewProvider.hideMaterialLoading();
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      _getAllFlight(context, null);
    });
  }

  void popupMenuFunctionList(BuildContext context, int key) {
    final serverEnumMap = {4: 0, 5: 1, 6: 2};
    final pickedServerType = serverEnumMap[key] ?? 0;
    _targetUserFlight = null;
    popupLayerController.hideAllPopups();
    _getAllFlight(context, pickedServerType);
    setState(() {
      polyline = [];
      _serverType = pickedServerType;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _OsmMap(),
          _SearchBar(context),
          Container(
            margin: const EdgeInsets.all(16),
            alignment: Alignment.bottomRight,
            child: FloatingActionButton(
              backgroundColor: LightTheme.themeColor,
              onPressed: () => locateMyFlight(context),
              child: const Icon(Icons.gps_fixed, color: Colors.white),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 140, right: 10),
            alignment: Alignment.topRight,
            child: FgsPopupMenuButton(
              callback: (value) => setMapType(int.parse(value.toString())),
              icon: Icons.map,
              buttonInfoList: buttonInfoList,
            ),
          )
        ],
      ),
    );
  }

  SearchBarWidget _SearchBar(BuildContext context) {
    return SearchBarWidget(
        searchResult: flightBeanList,
        functionsCallback: (data) => popupMenuFunctionList(context, data as int),
        searchBarCallBack: (data) {
          locateSearchedFlight(context, data.toString());
        });
  }

  FlutterMap _OsmMap() {
    return FlutterMap(
      mapController: osmMapController,
      options: MapOptions(
        interactionOptions: const InteractionOptions(
          flags: InteractiveFlag.pinchZoom | InteractiveFlag.drag,
        ),
        initialCenter: const osm_l.LatLng(39.9042, 116.4074),
        onLongPress: (TapPosition position, osm_l.LatLng point) => {
          popupLayerController.hideAllPopups(),
        },
        onTap: (TapPosition position, osm_l.LatLng point) {
          popupLayerController.hideAllPopups();
          setState(() {
            polyline = [];
          });
        },
        initialZoom: 5,
      ),
      children: [
        TileLayer(
          urlTemplate: Links.osmMapUrl,
          additionalOptions: {
            'id': Links.getOsmTheme(mapType),
          },
        ),
        PolylineLayer(polylines: polyline),
        PopupMarkerLayer(
          options: PopupMarkerLayerOptions(
            popupController: popupLayerController,
            markers: markerList,
            popupDisplayOptions: PopupDisplayOptions(
                animation: const PopupAnimation.fade(duration: Duration(microseconds: 400), curve: Curves.bounceInOut),
                builder: (BuildContext context, Marker marker) {
                  final index = marker.key.toString().substring(3, marker.key.toString().length - 3);
                  final targetInfo = flightBeanList[int.parse(index)];
                  final infoWindow = OsmInfoWindow(targetInfo: targetInfo, onlinePlanBean: onlinePlan);
                  return Transform.scale(scale: 0.8, child: infoWindow);
                }),
            // markerRotateAlignment: PopupMarkerLayerOptions.rotationAlignmentFor(AnchorAlign.center),
          ),
        )
      ],
    );
  }
}

class PopupMenuClass {
  late Icon iconData;
  late String title;

  PopupMenuClass(this.iconData, this.title);
}
