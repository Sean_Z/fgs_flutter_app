import 'dart:async';
import 'dart:convert';
import 'dart:isolate';

void main()async {
  final message = await getMsgFromIsolate(json.encode({'haha': 'hah'}), isolateTest);
  print(message);
}

Future<T> getMsgFromIsolate<T>(String param, void Function(SendPort sendPort) entryPoint)async {
  final receivePort = ReceivePort();
  await Isolate.spawn(entryPoint, receivePort.sendPort);
  final sendPort = await receivePort.first as SendPort;
  final message = await sendReceive(sendPort, param);
  return message as T;
}

void isolateTest(SendPort sendPort) async {
  final receivePort = ReceivePort();
  sendPort.send(receivePort.sendPort);
  await for(var msg in receivePort) {
    final data = '子线程返回消息： hahahaha';
    print('主线程发来消息: ${msg[0]}');
    final reply = msg[1] as SendPort;
    reply.send(data);
    if(data == 'close') {
      receivePort.close();
    }
  }
}

Future sendReceive(SendPort port, Object msg) {
  final response = ReceivePort();
  port.send([msg, response.sendPort]);
  return response.first;
}
