import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:ifgs/core/CustomUIViewProvider.dart';
import 'package:ifgs/core/UIViewProvider.dart';
import 'package:ifgs/provider/FlightInfoStateProvider.dart';
import 'package:ifgs/provider/MainProvider.dart';
import 'package:ifgs/provider/OsmMapFragmentProvider.dart';
import 'package:ifgs/provider/atc/AtcMonitorProvider.dart';
import 'package:ifgs/request/Router/RouteSheet.dart';
import 'package:ifgs/request/Router/UpstreamRouter.dart';
import 'package:ifgs/stylesheet/Theme.dart';
import 'package:oktoast/oktoast.dart';
import 'package:provider/provider.dart';

import 'core/GetController.dart';
import 'core/StartActivityEventHandler.dart';
import 'generated/l10n.dart';

final startActivity = StartActivityEventHandler();

/// FGS Flutter App 启动类
Future<void> main()async {
  startActivity.systemUiOverlayStyle();
  WidgetsFlutterBinding.ensureInitialized();
  FlutterError.onError = (FlutterErrorDetails details) {
  };
  if(!kDebugMode) {
    runZonedGuarded(() => runApp(FlightGlobalStudioApp()), (error, stack) {
      handleErrorInApp(error);
    });
  }else {
    runApp(FlightGlobalStudioApp());
  }
}

void handleErrorInApp(Object error) {
  CustomViewProvider.hideMaterialLoading();
  CustomViewProvider.hideMaterialDialogLoading();
  UIViewProvider.showToast(error.toString());
}

final RouteObserver<PageRoute> routeObserver = RouteObserver();

class FlightGlobalStudioApp extends StatefulWidget {
  @override
  _FlightGlobalStudioAppState createState() => _FlightGlobalStudioAppState();
}

final GlobalKey<NavigatorState> navKey = GlobalKey();

class _FlightGlobalStudioAppState extends State<FlightGlobalStudioApp> {

  @override
  void initState() {
    startActivity.appStartEvent();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    RoUpstreamRouter.globalContext = context;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => FlightInfoStateProvider()),
        ChangeNotifierProvider(create: (_) => OsmMapFragmentProvider()),
        ChangeNotifierProvider(create: (_) => MainProvider()),
        ChangeNotifierProvider(create: (_) => AtcMonitorProvider()),
      ],
      child: GetBuilder<GetThemeController>(
        builder: (GetThemeController controller) {
          return OKToast(
            child: GetMaterialApp(
                key: const Key('mainKey'),
                debugShowCheckedModeBanner: true,
                navigatorObservers: [routeObserver],
                darkTheme: DarkTheme.themeConf,
                theme: LightTheme.themeConf,
                themeMode: ThemeMode.light,
                checkerboardOffscreenLayers: true,
                localizationsDelegates: const [
                  S.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate
                ],
                navigatorKey: navKey,
                initialRoute: '/index',
                getPages: MainRouterSheet.routes),
          );
        },
      ),
    );
  }
}
