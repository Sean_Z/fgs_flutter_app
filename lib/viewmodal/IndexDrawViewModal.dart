import 'package:flutter/material.dart';

class DrawListBean {
  DrawListBean({required this.icon, required this.title, required this.iconColor, required this.route});
  final IconData icon;
  final String title;
  final Color iconColor;
  final String route;
}
