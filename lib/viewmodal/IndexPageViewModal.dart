import 'package:flutter/material.dart';

class MenuListDataClass {
  final String title;
  final String value;
  final IconData icon;
  MenuListDataClass({required this.title, required this.value, required this.icon});
}

class MenuListBean {
  MenuListBean(this.icon, this.iconName, this.iconColor, this.routerMethod);
  final IconData icon;
  final String iconName;
  final Color iconColor;
  final VoidCallback routerMethod;
}
